from betgate_models.enums import Market


MARKETS = (
    (Market.UNKNOWN.value, "Unknown"),
    (Market.FT_WIN_1.value, "1"),
    (Market.FT_WIN_X.value, "X"),
    (Market.FT_WIN_2.value, "2"),
    (Market.FT_WIN_1X.value, "1X"),
    (Market.FT_WIN_12.value, "12"),
    (Market.FT_WIN_X2.value, "X2"),
    (Market.FT_WIN_DNB_1.value, "1 DNB"),
    (Market.FT_WIN_DNB_2.value, "2 DNB"),
    (Market.FH_WIN_1.value, "1 (1ª Parte)"),
    (Market.FH_WIN_X.value, "X (1ª Parte)"),
    (Market.FH_WIN_2.value, "2 (1ª Parte)"),
    (Market.FH_WIN_1X.value, "1X (1ª Parte)"),
    (Market.FH_WIN_12.value, "12 (1ª Parte)"),
    (Market.FH_WIN_X2.value, "X2 (1ª Parte)"),
    (Market.FH_WIN_DNB_1.value, "1 DNB (1ª Parte)"),
    (Market.FH_WIN_DNB_2.value, "2 DNB (1ª Parte)"),
    (Market.FT_BTTS_YES.value, "Ambos Marcan"),
    (Market.FT_BTTS_NO.value, "Ambos Marcan No"),
    (Market.FH_BTTS_YES.value, "Ambos Marcan (1ª Parte)"),
    (Market.FH_BTTS_NO.value, "Ambos Marcan No (1ª Parte)"),
    (Market.FT_DRAW_1_P10.value, "X(1:0)"),
    (Market.FT_DRAW_1_P20.value, "X(2:0)"),
    (Market.FT_DRAW_1_P30.value, "X(3:0)"),
    (Market.FT_DRAW_1_P40.value, "X(4:0)"),
    (Market.FT_DRAW_1_M10.value, "X(0:1)"),
    (Market.FT_DRAW_1_M20.value, "X(0:2)"),
    (Market.FT_DRAW_1_M30.value, "X(0:3)"),
    (Market.FT_DRAW_1_M40.value, "X(0:4)"),
    (Market.FT_WIN_1_P05.value, "H1(+0.5)"),
    (Market.FT_WIN_1_M05.value, "H1(-0.5)"),
    (Market.FT_WIN_1_P10.value, "1(1:0)"),
    (Market.FT_WIN_1_M10.value, "1(0:1)"),
    (Market.FT_WIN_1_P15.value, "H1(+1.5)"),
    (Market.FT_WIN_1_M15.value, "H1(-1.5)"),
    (Market.FT_WIN_1_P20.value, "1(2:0)"),
    (Market.FT_WIN_1_M20.value, "1(0:2)"),
    (Market.FT_WIN_1_P25.value, "H1(+2.5)"),
    (Market.FT_WIN_1_M25.value, "H1(-2.5)"),
    (Market.FT_WIN_1_P30.value, "1(3:0)"),
    (Market.FT_WIN_1_M30.value, "1(0:3)"),
    (Market.FT_WIN_1_P35.value, "H1(+3.5)"),
    (Market.FT_WIN_1_M35.value, "H1(-3.5)"),
    (Market.FT_WIN_1_P40.value, "1(4:0)"),
    (Market.FT_WIN_1_M40.value, "1(0:4)"),
    (Market.FT_WIN_2_P05.value, "H2(+0.5)"),
    (Market.FT_WIN_2_M05.value, "H2(-0.5)"),
    (Market.FT_WIN_2_P10.value, "2(0:1)"),
    (Market.FT_WIN_2_M10.value, "2(1:0)"),
    (Market.FT_WIN_2_P15.value, "H2(+1.5)"),
    (Market.FT_WIN_2_M15.value, "H2(-1.5)"),
    (Market.FT_WIN_2_P20.value, "2(0:2)"),
    (Market.FT_WIN_2_M20.value, "2(2:0)"),
    (Market.FT_WIN_2_P25.value, "H2(+2.5)"),
    (Market.FT_WIN_2_M25.value, "H2(-2.5)"),
    (Market.FT_WIN_2_P30.value, "2(0:3)"),
    (Market.FT_WIN_2_M30.value, "2(3:0)"),
    (Market.FT_WIN_2_P35.value, "H2(+3.5)"),
    (Market.FT_WIN_2_M35.value, "H2(-3.5)"),
    (Market.FT_WIN_2_P40.value, "2(0:4)"),
    (Market.FT_WIN_2_M40.value, "2(4:0)"),
    (Market.FT_WINA_1_P00.value, "H1(0)"),
    (Market.FT_WINA_1_P025.value, "H1(+0.25)"),
    (Market.FT_WINA_1_M025.value, "H1(-0.25)"),
    (Market.FT_WINA_1_P05.value, "H1(+0.5)"),
    (Market.FT_WINA_1_M05.value, "H1(-0.5)"),
    (Market.FT_WINA_1_P075.value, "H1(+0.75)"),
    (Market.FT_WINA_1_M075.value, "H1(-0.75)"),
    (Market.FT_WINA_1_P10.value, "H1(+1.0)"),
    (Market.FT_WINA_1_M10.value, "H1(-1.0)"),
    (Market.FT_WINA_1_P125.value, "H1(+1.25)"),
    (Market.FT_WINA_1_M125.value, "H1(-1.25)"),
    (Market.FT_WINA_1_P15.value, "H1(+1.5)"),
    (Market.FT_WINA_1_M15.value, "H1(-1.5)"),
    (Market.FT_WINA_1_P175.value, "H1(+1.75)"),
    (Market.FT_WINA_1_M175.value, "H1(-1.75)"),
    (Market.FT_WINA_1_P20.value, "H1(+2.0)"),
    (Market.FT_WINA_1_M20.value, "H1(-2.0)"),
    (Market.FT_WINA_1_P225.value, "H1(+2.25)"),
    (Market.FT_WINA_1_M225.value, "H1(-2.25)"),
    (Market.FT_WINA_1_P25.value, "H1(+2.5)"),
    (Market.FT_WINA_1_M25.value, "H1(-2.5)"),
    (Market.FT_WINA_1_P275.value, "H1(+2.75)"),
    (Market.FT_WINA_1_M275.value, "H1(-2.75)"),
    (Market.FT_WINA_1_P30.value, "H1(+3.0)"),
    (Market.FT_WINA_1_M30.value, "H1(-3.0)"),
    (Market.FT_WINA_1_P325.value, "H1(+3.25)"),
    (Market.FT_WINA_1_M325.value, "H1(-3.25)"),
    (Market.FT_WINA_1_P35.value, "H1(+3.5)"),
    (Market.FT_WINA_1_M35.value, "H1(-3.5)"),
    (Market.FT_WINA_2_P00.value, "H2(0)"),
    (Market.FT_WINA_2_P025.value, "H2(+0.25)"),
    (Market.FT_WINA_2_M025.value, "H2(-0.25)"),
    (Market.FT_WINA_2_P05.value, "H2(+0.5)"),
    (Market.FT_WINA_2_M05.value, "H2(-0.5)"),
    (Market.FT_WINA_2_P075.value, "H2(+0.75)"),
    (Market.FT_WINA_2_M075.value, "H2(-0.75)"),
    (Market.FT_WINA_2_P10.value, "H2(+1.0)"),
    (Market.FT_WINA_2_M10.value, "H2(-1.0)"),
    (Market.FT_WINA_2_P125.value, "H2(+1.25)"),
    (Market.FT_WINA_2_M125.value, "H2(-1.25)"),
    (Market.FT_WINA_2_P15.value, "H2(+1.5)"),
    (Market.FT_WINA_2_M15.value, "H2(-1.5)"),
    (Market.FT_WINA_2_P175.value, "H2(+1.75)"),
    (Market.FT_WINA_2_M175.value, "H2(-1.75)"),
    (Market.FT_WINA_2_P20.value, "H2(+2.0)"),
    (Market.FT_WINA_2_M20.value, "H2(-2.0)"),
    (Market.FT_WINA_2_P225.value, "H2(+2.25)"),
    (Market.FT_WINA_2_M225.value, "H2(-2.25)"),
    (Market.FT_WINA_2_P25.value, "H2(+2.5)"),
    (Market.FT_WINA_2_M25.value, "H2(-2.5)"),
    (Market.FT_WINA_2_P275.value, "H2(+2.75)"),
    (Market.FT_WINA_2_M275.value, "H2(-2.75)"),
    (Market.FT_WINA_2_P30.value, "H2(+3.0)"),
    (Market.FT_WINA_2_M30.value, "H2(-3.0)"),
    (Market.FT_WINA_2_P325.value, "H2(+3.25)"),
    (Market.FT_WINA_2_M325.value, "H2(-3.25)"),
    (Market.FT_WINA_2_P35.value, "H2(+3.5)"),
    (Market.FT_WINA_2_M35.value, "H2(-3.5)"),
    (Market.FH_DRAW_1_P10.value, "X(1:0) (1ª Parte)"),
    (Market.FH_DRAW_1_P20.value, "X(2:0) (1ª Parte)"),
    (Market.FH_DRAW_1_P30.value, "X(3:0) (1ª Parte)"),
    (Market.FH_DRAW_1_M10.value, "X(0:1) (1ª Parte)"),
    (Market.FH_DRAW_1_M20.value, "X(0:2) (1ª Parte)"),
    (Market.FH_DRAW_1_M30.value, "X(0:3) (1ª Parte)"),
    (Market.FH_WIN_1_P05.value, "H1(+0.5) (1ª Parte)"),
    (Market.FH_WIN_1_M05.value, "H1(-0.5) (1ª Parte)"),
    (Market.FH_WIN_1_P10.value, "1(1:0) (1ª Parte)"),
    (Market.FH_WIN_1_M10.value, "1(0:1) (1ª Parte)"),
    (Market.FH_WIN_1_P15.value, "H1(+1.5) (1ª Parte)"),
    (Market.FH_WIN_1_M15.value, "H1(-1.5) (1ª Parte)"),
    (Market.FH_WIN_1_P20.value, "1(2:0) (1ª Parte)"),
    (Market.FH_WIN_1_M20.value, "1(0:1) (1ª Parte)"),
    (Market.FH_WIN_1_P25.value, "H1(+2.5) (1ª Parte)"),
    (Market.FH_WIN_1_M25.value, "H1(-2.5) (1ª Parte)"),
    (Market.FH_WIN_1_P30.value, "1(3:0) (1ª Parte)"),
    (Market.FH_WIN_1_M30.value, "1(0:3) (1ª Parte)"),
    (Market.FH_WIN_1_P35.value, "H1(+3.5) (1ª Parte)"),
    (Market.FH_WIN_1_M35.value, "H1(-3.5) (1ª Parte)"),
    (Market.FH_WIN_2_P05.value, "H2(+0.5) (1ª Parte)"),
    (Market.FH_WIN_2_M05.value, "H2(-0.5) (1ª Parte)"),
    (Market.FH_WIN_2_P10.value, "2(0:1) (1ª Parte)"),
    (Market.FH_WIN_2_M10.value, "2(1:0) (1ª Parte)"),
    (Market.FH_WIN_2_P15.value, "H2(+1.5) (1ª Parte)"),
    (Market.FH_WIN_2_M15.value, "H2(-1.5) (1ª Parte)"),
    (Market.FH_WIN_2_P20.value, "2(0:2) (1ª Parte)"),
    (Market.FH_WIN_2_M20.value, "2(2:0) (1ª Parte)"),
    (Market.FH_WIN_2_P25.value, "H2(+2.5) (1ª Parte)"),
    (Market.FH_WIN_2_M25.value, "H2(-2.5) (1ª Parte)"),
    (Market.FH_WIN_2_P30.value, "2(0:3) (1ª Parte)"),
    (Market.FH_WIN_2_M30.value, "2(3:0) (1ª Parte)"),
    (Market.FH_WIN_2_P35.value, "H2(+3.5) (1ª Parte)"),
    (Market.FH_WIN_2_M35.value, "H2(-3.5) (1ª Parte)"),
    (Market.FH_WINA_1_P00.value, "H1(0) (1ª Parte)"),
    (Market.FH_WINA_1_P025.value, "H1(+0.25) (1ª Parte)"),
    (Market.FH_WINA_1_M025.value, "H1(-0.25) (1ª Parte)"),
    (Market.FH_WINA_1_P05.value, "H1(+0.5) (1ª Parte)"),
    (Market.FH_WINA_1_M05.value, "H1(-0.5) (1ª Parte)"),
    (Market.FH_WINA_1_P075.value, "H1(+0.75) (1ª Parte)"),
    (Market.FH_WINA_1_M075.value, "H1(-0.75) (1ª Parte)"),
    (Market.FH_WINA_1_P10.value, "H1(+1.0) (1ª Parte)"),
    (Market.FH_WINA_1_M10.value, "H1(-1.0) (1ª Parte)"),
    (Market.FH_WINA_1_P125.value, "H1(+1.25) (1ª Parte)"),
    (Market.FH_WINA_1_M125.value, "H1(-1.25) (1ª Parte)"),
    (Market.FH_WINA_1_P15.value, "H1(+1.5) (1ª Parte)"),
    (Market.FH_WINA_1_M15.value, "H1(-1.5) (1ª Parte)"),
    (Market.FH_WINA_1_P175.value, "H1(+1.75) (1ª Parte)"),
    (Market.FH_WINA_1_M175.value, "H1(-1.75) (1ª Parte)"),
    (Market.FH_WINA_1_P20.value, "H1(+2.0) (1ª Parte)"),
    (Market.FH_WINA_1_M20.value, "H1(-2.0) (1ª Parte)"),
    (Market.FH_WINA_1_P225.value, "H1(+2.25) (1ª Parte)"),
    (Market.FH_WINA_1_M225.value, "H1(-2.25) (1ª Parte)"),
    (Market.FH_WINA_1_P25.value, "H1(+2.5) (1ª Parte)"),
    (Market.FH_WINA_1_M25.value, "H1(-2.5) (1ª Parte)"),
    (Market.FH_WINA_1_P275.value, "H1(+2.75) (1ª Parte)"),
    (Market.FH_WINA_1_M275.value, "H1(-2.75) (1ª Parte)"),
    (Market.FH_WINA_1_P30.value, "H1(+3.0) (1ª Parte)"),
    (Market.FH_WINA_1_M30.value, "H1(-3.0) (1ª Parte)"),
    (Market.FH_WINA_1_P325.value, "H1(+3.25) (1ª Parte)"),
    (Market.FH_WINA_1_M325.value, "H1(-3.25) (1ª Parte)"),
    (Market.FH_WINA_1_P35.value, "H1(+3.5) (1ª Parte)"),
    (Market.FH_WINA_1_M35.value, "H1(-3.5) (1ª Parte)"),
    (Market.FH_WINA_2_P00.value, "H2(0) (1ª Parte)"),
    (Market.FH_WINA_2_P025.value, "H2(+0.25) (1ª Parte)"),
    (Market.FH_WINA_2_M025.value, "H2(-0.25) (1ª Parte)"),
    (Market.FH_WINA_2_P05.value, "H2(+0.5) (1ª Parte)"),
    (Market.FH_WINA_2_M05.value, "H2(-0.5) (1ª Parte)"),
    (Market.FH_WINA_2_P075.value, "H2(+0.75) (1ª Parte)"),
    (Market.FH_WINA_2_M075.value, "H2(-0.75) (1ª Parte)"),
    (Market.FH_WINA_2_P10.value, "H2(+1.0) (1ª Parte)"),
    (Market.FH_WINA_2_M10.value, "H2(-1.0) (1ª Parte)"),
    (Market.FH_WINA_2_P125.value, "H2(+1.25) (1ª Parte)"),
    (Market.FH_WINA_2_M125.value, "H2(-1.25) (1ª Parte)"),
    (Market.FH_WINA_2_P15.value, "H2(+1.5) (1ª Parte)"),
    (Market.FH_WINA_2_M15.value, "H2(-1.5) (1ª Parte)"),
    (Market.FH_WINA_2_P175.value, "H2(+1.75) (1ª Parte)"),
    (Market.FH_WINA_2_M175.value, "H2(-1.75) (1ª Parte)"),
    (Market.FH_WINA_2_P20.value, "H2(+2.0) (1ª Parte)"),
    (Market.FH_WINA_2_M20.value, "H2(-2.0) (1ª Parte)"),
    (Market.FH_WINA_2_P225.value, "H2(+2.25) (1ª Parte)"),
    (Market.FH_WINA_2_M225.value, "H2(-2.25) (1ª Parte)"),
    (Market.FH_WINA_2_P25.value, "H2(+2.5) (1ª Parte)"),
    (Market.FH_WINA_2_M25.value, "H2(-2.5) (1ª Parte)"),
    (Market.FH_WINA_2_P275.value, "H2(+2.75) (1ª Parte)"),
    (Market.FH_WINA_2_M275.value, "H2(-2.75) (1ª Parte)"),
    (Market.FH_WINA_2_P30.value, "H2(+3.0) (1ª Parte)"),
    (Market.FH_WINA_2_M30.value, "H2(-3.0) (1ª Parte)"),
    (Market.FH_WINA_2_P325.value, "H2(+3.25) (1ª Parte)"),
    (Market.FH_WINA_2_M325.value, "H2(-3.25) (1ª Parte)"),
    (Market.FH_WINA_2_P35.value, "H2(+3.5) (1ª Parte)"),
    (Market.FH_WINA_2_M35.value, "H2(-3.5) (1ª Parte)"),
    (Market.FT_TG_EXACT_0.value, "0 goles"),
    (Market.FT_TG_OVER_05.value, "+0.5 goles"),
    (Market.FT_TG_UNDER_05.value, "-0.5 goles"),
    (Market.FT_TG_OVER_15.value, "+1.5 goles"),
    (Market.FT_TG_UNDER_15.value, "-1.5 goles"),
    (Market.FT_TG_OVER_25.value, "+2.5 goles"),
    (Market.FT_TG_UNDER_25.value, "-2.5 goles"),
    (Market.FT_TG_OVER_35.value, "+3.5 goles"),
    (Market.FT_TG_UNDER_35.value, "-3.5 goles"),
    (Market.FT_TG_OVER_45.value, "+4.5 goles"),
    (Market.FT_TG_UNDER_45.value, "-4.5 goles"),
    (Market.FT_TG_OVER_55.value, "+5.5 goles"),
    (Market.FT_TG_UNDER_55.value, "-5.5 goles"),
    (Market.FH_TG_EXACT_0.value, "0 goles (1ª Parte)"),
    (Market.FH_TG_OVER_05.value, "+0.5 goles (1ª Parte)"),
    (Market.FH_TG_UNDER_05.value, "-0.5 goles (1ª Parte)"),
    (Market.FH_TG_OVER_15.value, "+1.5 goles (1ª Parte)"),
    (Market.FH_TG_UNDER_15.value, "-1.5 goles (1ª Parte)"),
    (Market.FH_TG_OVER_25.value, "+2.5 goles (1ª Parte)"),
    (Market.FH_TG_UNDER_25.value, "-2.5 goles (1ª Parte)"),
    (Market.FH_TG_OVER_35.value, "+3.5 goles (1ª Parte)"),
    (Market.FH_TG_UNDER_35.value, "-3.5 goles (1ª Parte)"),
    (Market.FH_TG_OVER_45.value, "+4.5 goles (1ª Parte)"),
    (Market.FH_TG_UNDER_45.value, "-4.5 goles (1ª Parte)"),
    (Market.FT_TGA_OVER_05.value, "+0.5 goles"),
    (Market.FT_TGA_UNDER_05.value, "-0.5 goles"),
    (Market.FT_TGA_OVER_075.value, "+0.75 goles"),
    (Market.FT_TGA_UNDER_075.value, "-0.75 goles"),
    (Market.FT_TGA_OVER_10.value, "+1.0 goles"),
    (Market.FT_TGA_UNDER_10.value, "-1.0 goles"),
    (Market.FT_TGA_OVER_125.value, "+1.25 goles"),
    (Market.FT_TGA_UNDER_125.value, "-1.25 goles"),
    (Market.FT_TGA_OVER_15.value, "+1.5 goles"),
    (Market.FT_TGA_UNDER_15.value, "-1.5 goles"),
    (Market.FT_TGA_OVER_175.value, "+1.75 goles"),
    (Market.FT_TGA_UNDER_175.value, "-1.75 goles"),
    (Market.FT_TGA_OVER_20.value, "+2.0 goles"),
    (Market.FT_TGA_UNDER_20.value, "-2.0 goles"),
    (Market.FT_TGA_OVER_225.value, "+2.25 goles"),
    (Market.FT_TGA_UNDER_225.value, "-2.25 goles"),
    (Market.FT_TGA_OVER_25.value, "+2.5 goles"),
    (Market.FT_TGA_UNDER_25.value, "-2.5 goles"),
    (Market.FT_TGA_OVER_275.value, "+2.75 goles"),
    (Market.FT_TGA_UNDER_275.value, "-2.75 goles"),
    (Market.FT_TGA_OVER_30.value, "+3.0 goles"),
    (Market.FT_TGA_UNDER_30.value, "-3.0 goles"),
    (Market.FT_TGA_OVER_325.value, "+3.25 goles"),
    (Market.FT_TGA_UNDER_325.value, "-3.25 goles"),
    (Market.FT_TGA_OVER_35.value, "+3.5 goles"),
    (Market.FT_TGA_UNDER_35.value, "-3.5 goles"),
    (Market.FT_TGA_OVER_375.value, "+3.75 goles"),
    (Market.FT_TGA_UNDER_375.value, "-3.75 goles"),
    (Market.FT_TGA_OVER_40.value, "+4.0 goles"),
    (Market.FT_TGA_UNDER_40.value, "-4.0 goles"),
    (Market.FT_TGA_OVER_425.value, "+4.25 goles"),
    (Market.FT_TGA_UNDER_425.value, "-4.25 goles"),
    (Market.FT_TGA_OVER_45.value, "+4.5 goles"),
    (Market.FT_TGA_UNDER_45.value, "-4.5 goles"),
    (Market.FT_TGA_OVER_475.value, "+4.75 goles"),
    (Market.FT_TGA_UNDER_475.value, "-4.75 goles"),
    (Market.FT_TGA_OVER_50.value, "+5.0 goles"),
    (Market.FT_TGA_UNDER_50.value, "-5.0 goles"),
    (Market.FT_TGA_OVER_525.value, "+5.25 goles"),
    (Market.FT_TGA_UNDER_525.value, "-5.25 goles"),
    (Market.FT_TGA_OVER_55.value, "+5.5 goles"),
    (Market.FT_TGA_UNDER_55.value, "-5.5 goles"),
    (Market.FH_TGA_OVER_05.value, "+0.5 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_05.value, "-0.5 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_075.value, "+0.75 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_075.value, "-0.75 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_10.value, "+1.0 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_10.value, "-1.0 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_125.value, "+1.25 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_125.value, "-1.25 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_15.value, "+1.5 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_15.value, "-1.5 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_175.value, "+1.75 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_175.value, "-1.75 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_20.value, "+2.0 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_20.value, "-2.0 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_225.value, "+2.25 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_225.value, "-2.25 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_25.value, "+2.5 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_25.value, "-2.5 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_275.value, "+2.75 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_275.value, "-2.75 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_30.value, "+3.0 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_30.value, "-3.0 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_325.value, "+3.25 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_325.value, "-3.25 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_35.value, "+3.5 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_35.value, "-3.5 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_375.value, "+3.75 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_375.value, "-3.75 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_40.value, "+4.0 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_40.value, "-4.0 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_425.value, "+4.25 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_425.value, "-4.25 goles (1ª Parte)"),
    (Market.FH_TGA_OVER_45.value, "+4.5 goles (1ª Parte)"),
    (Market.FH_TGA_UNDER_45.value, "-4.5 goles (1ª Parte)"),
    (Market.FT_RESULT_0_0.value, "0-0"),
    (Market.FH_RESULT_0_0.value, "0-0 (1ª Parte)"),
    (Market.FT_CORNERS_OVER_0_5.value, "+0.5 corners"),
    (Market.FT_CORNERS_EXACT_0.value, "0 corners"),
    (Market.FT_CORNERS_UNDER_0_5.value, "-0.5 corners"),
    (Market.FT_CORNERS_OVER_1_5.value, "+1.5 corners"),
    (Market.FT_CORNERS_EXACT_1.value, "1 corner"),
    (Market.FT_CORNERS_UNDER_1_5.value, "-1.5 corners"),
    (Market.FT_CORNERS_OVER_2_5.value, "+2.5 corners"),
    (Market.FT_CORNERS_EXACT_2.value, "2 corners"),
    (Market.FT_CORNERS_UNDER_2_5.value, "-2.5 corners"),
    (Market.FT_CORNERS_OVER_3_5.value, "+3.5 corners"),
    (Market.FT_CORNERS_EXACT_3.value, "3 corners"),
    (Market.FT_CORNERS_UNDER_3_5.value, "-3.5 corners"),
    (Market.FT_CORNERS_OVER_4_5.value, "+4.5 corners"),
    (Market.FT_CORNERS_EXACT_4.value, "4 corners"),
    (Market.FT_CORNERS_UNDER_4_5.value, "-4.5 corners"),
    (Market.FT_CORNERS_OVER_5_5.value, "+5.5 corners"),
    (Market.FT_CORNERS_EXACT_5.value, "5 corners"),
    (Market.FT_CORNERS_UNDER_5_5.value, "-5.5 corners"),
    (Market.FT_CORNERS_OVER_6_5.value, "+6.5 corners"),
    (Market.FT_CORNERS_EXACT_6.value, "6 corners"),
    (Market.FT_CORNERS_UNDER_6_5.value, "-6.5 corners"),
    (Market.FT_CORNERS_OVER_7_5.value, "+7.5 corners"),
    (Market.FT_CORNERS_EXACT_7.value, "7 corners"),
    (Market.FT_CORNERS_UNDER_7_5.value, "-7.5 corners"),
    (Market.FT_CORNERS_OVER_8_5.value, "+8.5 corners"),
    (Market.FT_CORNERS_EXACT_8.value, "8 corners"),
    (Market.FT_CORNERS_UNDER_8_5.value, "-8.5 corners"),
    (Market.FT_CORNERS_OVER_9_5.value, "+9.5 corners"),
    (Market.FT_CORNERS_EXACT_9.value, "9 corners"),
    (Market.FT_CORNERS_UNDER_9_5.value, "-9.5 corners"),
    (Market.FT_CORNERS_OVER_10_5.value, "+10.5 corners"),
    (Market.FT_CORNERS_EXACT_10.value, "10 corners"),
    (Market.FT_CORNERS_UNDER_10_5.value, "-10.5 corners"),
    (Market.FT_CORNERS_OVER_11_5.value, "+11.5 corners"),
    (Market.FT_CORNERS_EXACT_11.value, "11 corners"),
    (Market.FT_CORNERS_UNDER_11_5.value, "-11.5 corners"),
    (Market.FT_CORNERS_OVER_12_5.value, "+12.5 corners"),
    (Market.FT_CORNERS_EXACT_12.value, "12 corners"),
    (Market.FT_CORNERS_UNDER_12_5.value, "-12.5 corners"),
    (Market.FT_CORNERS_OVER_13_5.value, "+13.5 corners"),
    (Market.FT_CORNERS_EXACT_13.value, "13 corners"),
    (Market.FT_CORNERS_UNDER_13_5.value, "-13.5 corners"),
    (Market.FT_CORNERS_OVER_14_5.value, "+14.5 corners"),
    (Market.FT_CORNERS_EXACT_14.value, "14 corners"),
    (Market.FT_CORNERS_UNDER_14_5.value, "-14.5 corners"),
    (Market.FT_CORNERS_OVER_15_5.value, "+15.5 corners"),
    (Market.FT_CORNERS_EXACT_15.value, "15 corners"),
    (Market.FT_CORNERS_UNDER_15_5.value, "-15.5 corners"),
    (Market.FT_CORNERS_OVER_16_5.value, "+16.5 corners"),
    (Market.FT_CORNERS_EXACT_16.value, "16 corners"),
    (Market.FT_CORNERS_UNDER_16_5.value, "-16.5 corners"),
    (Market.FT_CORNERS_OVER_17_5.value, "+17.5 corners"),
    (Market.FT_CORNERS_EXACT_17.value, "17 corners"),
    (Market.FT_CORNERS_UNDER_17_5.value, "-17.5 corners"),
    (Market.FT_CORNERS_OVER_18_5.value, "+18.5 corners"),
    (Market.FT_CORNERS_EXACT_18.value, "18 corners"),
    (Market.FT_CORNERS_UNDER_18_5.value, "-18.5 corners"),
    (Market.FT_CORNERS_OVER_19_5.value, "+19.5 corners"),
    (Market.FT_CORNERS_EXACT_19.value, "19 corners"),
    (Market.FT_CORNERS_UNDER_19_5.value, "-19.5 corners"),
    (Market.FT_CORNERS_OVER_20_5.value, "+20.5 corners"),
    (Market.FT_CORNERS_EXACT_20.value, "20 corners"),
    (Market.FT_CORNERS_UNDER_20_5.value, "-20.5 corners"),
    (Market.FT_TG_EXACT_0_HOME.value, "0 goles - Equipo 1"),
    (Market.FT_TG_OVER_0_5_HOME.value, "+0.5 goles - Equipo 1"),
    (Market.FT_TG_OVER_1_5_HOME.value, "+1.5 goles - Equipo 1"),
    (Market.FT_TG_OVER_2_5_HOME.value, "+2.5 goles - Equipo 1"),
    (Market.FT_TG_OVER_3_5_HOME.value, "+3.5 goles - Equipo 1"),
    (Market.FT_CARDS_OVER_0_5.value, "+0.5 tarjetas"),
    (Market.FT_CARDS_UNDER_0_5.value, "-0.5 tarjetas"),
    (Market.FT_CARDS_OVER_1_5.value, "+1.5 tarjetas"),
    (Market.FT_CARDS_UNDER_1_5.value, "-1.5 tarjetas"),
    (Market.FT_CARDS_OVER_2_5.value, "+2.5 tarjetas"),
    (Market.FT_CARDS_UNDER_2_5.value, "-2.5 tarjetas"),
    (Market.FT_CARDS_OVER_3_5.value, "+3.5 tarjetas"),
    (Market.FT_CARDS_UNDER_3_5.value, "-3.5 tarjetas"),
    (Market.FT_CARDS_OVER_4_5.value, "+4.5 tarjetas"),
    (Market.FT_CARDS_UNDER_4_5.value, "-4.5 tarjetas"),
    (Market.FT_CARDS_OVER_5_5.value, "+5.5 tarjetas"),
    (Market.FT_CARDS_UNDER_5_5.value, "-5.5 tarjetas"),
    (Market.FT_CARDS_OVER_6_5.value, "+6.5 tarjetas"),
    (Market.FT_CARDS_UNDER_6_5.value, "-6.5 tarjetas"),
    (Market.FT_CARDS_OVER_7_5.value, "+7.5 tarjetas"),
    (Market.FT_CARDS_UNDER_7_5.value, "-7.5 tarjetas"),
    (Market.FT_CARDS_OVER_8_5.value, "+8.5 tarjetas"),
    (Market.FT_CARDS_UNDER_8_5.value, "-8.5 tarjetas"),
    (Market.FT_CARDS_OVER_9_5.value, "+9.5 tarjetas"),
    (Market.FT_CARDS_UNDER_9_5.value, "-9.5 tarjetas"),
    (Market.FT_CARDS_OVER_10_5.value, "+10.5 tarjetas"),
    (Market.FT_CARDS_UNDER_10_5.value, "-10.5 tarjetas"),
)

MARKETS_DICT = dict(MARKETS)
