from betgate_models.choices.teams import TEAMS
from betgate_models.choices.categories import CATEGORIES, CATEGORIES_DICT
from betgate_models.choices.bookies import BOOKIES, GLOBAL_OR_BOOKIES, GLOBAL_OR_BOOKIES_DICT
from betgate_models.choices.markets import MARKETS, MARKETS_DICT
