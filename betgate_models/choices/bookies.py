from betgate_models.enums import Bookie


BOOKIES = (
    (Bookie.MARATHONBET.value, "MarathonBet"),
    (Bookie.LUCKIA.value, "Luckia"),
    (Bookie.SPORT888.value, "888Sport"),
    (Bookie.BETFAIR.value, "Betfair"),
    (Bookie.RETABET.value, "Retabet"),
    (Bookie.WILLIAM_HILL.value, "William Hill"),
    (Bookie.SPORTIUM.value, "Sportium"),
)

GLOBAL_OR_BOOKIES = ((0, "Global"),) + BOOKIES

GLOBAL_OR_BOOKIES_DICT = dict(GLOBAL_OR_BOOKIES)
