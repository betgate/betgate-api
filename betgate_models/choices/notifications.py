from betgate_models.enums import NotificationId


NOTIFICATIONS = (
    (NotificationId.MAX_SUREBET.value, "Max Surebet"),
    (NotificationId.MAX_SUREBET_MEDIUM.value, "Max Surebet Medium"),
    (NotificationId.MAX_SUREBET_LOW.value, "Max Surebet Low"),
)
