# Generated by Django 3.0.2 on 2020-01-20 18:55

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('betgate_models', '0004_auto_20200119_1643'),
    ]

    operations = [
        migrations.AddField(
            model_name='surebet',
            name='creation_timestamp',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2020, 1, 20, 18, 55, 43, 36112, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
