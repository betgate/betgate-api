from django.db import models
from django.db.models import Count


class EventManager(models.Manager):
    def count_odds_by_bookie(self):
        counters = dict()
        for evt in self.all():
            for odd in evt.odds:
                counters[odd["bookie"], odd["market"]] = \
                    counters.get((odd["bookie"], odd["market"]), 0) + 1
        return counters

    def count_odds(self):
        return sum(self.count_odds_by_bookie().values())

    def count_bookies(self):
        return sum(len(evt.bookies) for evt in self.all())

    def max_odds(self, pk):
        event = self.get(pk=pk)

        max_odds = dict()
        for odd in event.odds:
            max_odd = max_odds.get(odd["market"], None)
            if max_odd is None or odd["value"] > max_odd["value"]:
                max_odd = odd
            max_odds[odd["market"]] = max_odd

        event.odds = max_odds.values()
        return event

    def count_by_category(self):
        fieldname = 'category'
        return self.values(fieldname).order_by(fieldname).annotate(total=Count(fieldname))
