from enum import IntEnum, unique


@unique
class Bookie(IntEnum):
    UNKNOWN = 0
    BETFAIR = 1
    SPORT888 = 2
    LUCKIA = 3
    MARATHONBET = 4
    TEST = 5
    RETABET = 6
    WILLIAM_HILL = 7
    SPORTIUM = 8


@unique
class NotificationId(IntEnum):
    UNKNOWN = 0
    MAX_SUREBET = 1
    MAX_SUREBET_MEDIUM = 2
    MAX_SUREBET_LOW = 3
