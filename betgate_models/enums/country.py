from enum import IntEnum, unique


@unique
class Country(IntEnum):
    AUSTRIA = 31
    BELGIUM = 1
    BRAZIL = 20
    CHILE = 2
    CLUBS_INTERNATIONAL = 29
    COLOMBIA = 3
    CROATIA = 4
    DENMARK = 30
    ECUADOR = 22
    ENGLAND = 5
    FINLAND = 27
    FRANCE = 6
    GERMANY = 7
    GREECE = 26
    ICELAND = 23
    ITALY = 8
    JAPAN = 21
    MOROCCO = 25
    NETHERLANDS = 9
    NORWAY = 10
    POLAND = 18
    PORTUGAL = 11
    RUSSIA = 12
    SCOTLAND = 13
    SERBIA = 28
    SPAIN = 14
    SWEDEN = 15
    SWITZERLAND = 16
    TURKEY = 17
    UKRAINE = 24
    UNKNOWN = 0
    USA = 19
