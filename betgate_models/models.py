from datetime import datetime
from decimal import Decimal
from django.db import models
from django.db.models import Q, F, Manager
from django.contrib.postgres.fields import ArrayField, JSONField
from betgate_models.choices import TEAMS, CATEGORIES, MARKETS_DICT, GLOBAL_OR_BOOKIES_DICT
from betgate_models.managers import EventManager
from betgate_models.choices.notifications import NOTIFICATIONS


class Odd(dict):
    def __init__(self, market, value, bookie, surebet_coeff=None):
        super(Odd).__init__()
        self.market = market
        self.market_name = MARKETS_DICT.get(market.value, None)
        self.value = value
        self.bookie = bookie
        self.bookie_name = GLOBAL_OR_BOOKIES_DICT.get(bookie.value, None)
        self.surebet_coeff = surebet_coeff

    @property
    def market(self):
        return self.get("market", None)

    @market.setter
    def market(self, value):
        self["market"] = value

    @property
    def market_name(self):
        return self.get("market_name", None)

    @market_name.setter
    def market_name(self, value):
        self["market_name"] = value

    @property
    def value(self):
        return self.get("value", None)

    @value.setter
    def value(self, value):
        self["value"] = float(Decimal(value).quantize(Decimal('1.000')))

    @property
    def surebet_coeff(self):
        return self.get("surebet_coeff", None)

    @surebet_coeff.setter
    def surebet_coeff(self, value):
        if value is not None:
            self["surebet_coeff"] = float(value)

    @property
    def bookie(self):
        return self.get("bookie", None)

    @bookie.setter
    def bookie(self, value):
        self["bookie"] = value

    @property
    def bookie_name(self):
        return self.get("bookie_name", None)

    @bookie_name.setter
    def bookie_name(self, value):
        self["bookie_name"] = value


class BookieId(dict):
    def __init__(self, value, bookie):
        super(BookieId).__init__()
        self.value = value
        self.bookie = bookie

    @property
    def value(self):
        return self.get("value", None)

    @value.setter
    def value(self, value):
        self["value"] = value

    @property
    def bookie(self):
        return self.get("bookie", None)

    @bookie.setter
    def bookie(self, value):
        self["bookie"] = value


class Event(models.Model):
    id = models.BigIntegerField(primary_key=True, editable=False)
    home_team = models.PositiveIntegerField(choices=TEAMS)
    away_team = models.PositiveIntegerField(choices=TEAMS)
    category = models.PositiveSmallIntegerField(choices=CATEGORIES)
    start = models.DateTimeField()
    bookies = JSONField(default=dict)
    odds = ArrayField(JSONField(default=dict), default=list, blank=True)
    update_timestamp = models.DateTimeField(auto_now=True)

    objects = EventManager()

    # extra properties
    html_source = None
    home_team_name = None
    away_team_name = None

    def __str__(self):
        return f"{self.get_home_team_display()} vs {self.get_away_team_display()} -- {self.start}"

    class Meta:
        ordering = ['start']

    def __init__(self, *args, **values):
        if "home_team" in values and "away_team" in values and "category" in values and "start" in values:
            values['id'] = self._get_id(**values)
        self.html_source = values.pop('html_source', None)
        self.home_team_name = values.pop('home_team_name', None)
        self.away_team_name = values.pop('away_team_name', None)

        super(Event, self).__init__(*args, **values)

    @staticmethod
    def _reduce_datetime(start, seed_strdate='2018-08-23', reduction_size=32768):
        """
        Reduce function for comparing similar datetimes.

        Total days from seed_strdate reduced to reduction_size.
        """
        seed_date = datetime.strptime(seed_strdate, '%Y-%m-%d').date()
        return (start.date() - seed_date).days % reduction_size

    def _get_id(self, home_team, away_team, category, start, **kwargs):
        return category | (home_team << 4) | (away_team << 16) | (self._reduce_datetime(start) << 28)


class SurebetManager(models.Manager):
    def distint_events(self):
        return list({
            surebet.event
            for surebet in self.select_related('event')
        })


class Surebet(models.Model):
    id = models.BigIntegerField(primary_key=True, editable=False)
    profit_factor = models.DecimalField(max_digits=5, decimal_places=4)
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    odds = ArrayField(JSONField(default=dict), default=list)
    bookies = ArrayField(models.IntegerField(), default=list)
    creation_timestamp = models.DateTimeField(auto_now_add=True)
    update_timestamp = models.DateTimeField(auto_now=True)

    objects = SurebetManager()

    def __init__(self, *args, **values):
        if "event" in values and "odds" in values:
            values['id'] = self._get_id(**values)

        super().__init__(*args, **values)

    @staticmethod
    def _get_id(event, odds, **kwargs):
        odds_stamp = 0
        sorted_odds = sorted(odds, key=lambda i: i['market'])
        for odd in sorted_odds:
            odds_stamp ^= (odd.market << 3) ^ odd.bookie
            odds_stamp <<= 12
        return event.id ^ (odds_stamp << 4)

    def __str__(self):
        return f"{self.profit_factor} -- {self.event}"

    class Meta:
        ordering = ['-profit_factor']


class Notification(models.Model):
    notification_id = models.PositiveSmallIntegerField(primary_key=True, choices=NOTIFICATIONS)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.get_notification_id_display()


class NotifiedSurebetManager(Manager):
    def all_surebets_greater_or_not_in(self):
        return Surebet.objects.filter(
            Q(event__notifiedsurebet__isnull=True) |
            Q(profit_factor__gt=F('event__notifiedsurebet__profit_factor'))
        )


class NotifiedSurebet(models.Model):
    event = models.OneToOneField(Event, on_delete=models.CASCADE, related_name='notifiedsurebet')
    profit_factor = models.DecimalField(max_digits=5, decimal_places=4)

    objects = NotifiedSurebetManager()


class CategoryTask(models.Model):
    update_timestamp = models.DateTimeField(auto_now=True)
    category = models.PositiveSmallIntegerField(choices=CATEGORIES)
