from django.apps import AppConfig


class BetgateModelsConfig(AppConfig):
    name = 'betgate_models'
