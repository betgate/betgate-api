from django.test import TestCase
from django.utils import timezone
from betgate_models.enums import Team, Category
from betgate_models.models import Surebet, Event
from betgate_models.models import Notification, NotifiedSurebet
from betgate_tasks.apps.celery_tasks.notifications.max_surebet_notification import MaxSurebetNotification


class NotificationTasksTestCase(TestCase):

    def setUp(self):
        event = Event.objects.create(
            home_team=Team.AALESUND,
            away_team=Team.AC_AJACCIO,
            category=Category.BELGIUM__FIRST_DIVISION_A,
            start=timezone.now()
        )
        surebet = Surebet.objects.create(profit_factor=1.05, event=event, odds=[])
        self.max_surebet_notification = MaxSurebetNotification()
        self.max_surebet_notification._surebet = surebet

    def test_update_timestamp(self):
        self.assertEqual(0, Notification.objects.count())
        self.assertEqual(0, NotifiedSurebet.objects.count())

        created = self.max_surebet_notification.update_timestamp()

        self.assertTrue(created)
        self.assertEqual(1, Notification.objects.count())
        self.assertEqual(1, NotifiedSurebet.objects.count())
