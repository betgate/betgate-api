from betgate.apps.code.tests.testcases import BetgateTestCase
from betgate_tasks.apps.celery_tasks.tasks import update_surebets, search_event_odds
from betgate_models.models import Surebet, Event
from betgate_models.enums import Category


class TasksTestCase(BetgateTestCase):

    def setUp(self):
        self.set_up_scenario()

    def test_update_surebets(self):
        self.set_up_db(50, 4, 5, 40, Category.SPAIN__PRIMERA_DIVISION)
        self.set_up_spain_mock(4)
        self.assertEqual(4, Surebet.objects.count())
        self.assertEqual(50, Event.objects.count())

        update_surebets()

        self.assertEqual(4, Surebet.objects.count())
        self.assertEqual(50, Event.objects.count())

    def test_update_surebets_no_results(self):
        self.set_up_db(50, 4, 5, 40, Category.SPAIN__PRIMERA_DIVISION)
        self.assertEqual(4, Surebet.objects.count())
        self.assertEqual(50, Event.objects.count())

        update_surebets()

        self.assertEqual(0, Surebet.objects.count())
        self.assertEqual(46, Event.objects.count())

    def test_search_event_odds(self):
        self.set_up_db(50, 0, 5, 40, Category.SPAIN__PRIMERA_DIVISION)
        self.set_up_spain_primera_mock(20, 4, 40)
        self.assertEqual(0, Surebet.objects.count())
        self.assertEqual(50, Event.objects.count())

        search_event_odds(category=Category.SPAIN__PRIMERA_DIVISION)

        self.assertEqual(4, Surebet.objects.count())
        self.assertEqual(20, Event.objects.count())

    def test_search_event_odds_with_surebets(self):
        self.set_up_db(50, 4, 5, 40, Category.SPAIN__PRIMERA_DIVISION)
        self.set_up_spain_primera_mock(20, 4, 40)
        self.assertEqual(4, Surebet.objects.count())
        self.assertEqual(50, Event.objects.count())

        search_event_odds(category=Category.SPAIN__PRIMERA_DIVISION)

        self.assertEqual(8, Surebet.objects.count())
        self.assertEqual(24, Event.objects.count())
