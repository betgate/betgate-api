from django.test import TestCase
from betgate_models.enums import Team, Market, Category, Bookie
from betgate_models.choices import TEAMS, MARKETS, CATEGORIES, BOOKIES


class ChoicesTestCase(TestCase):
    _skip_bookies = [Bookie.TEST, Bookie.UNKNOWN]
    _skip_categories = [Category.UNKNOWN]
    _skip_teams = [Team.UNKNOWN]

    def setUp(self):
        pass

    def assertIsNotDuplicatedIn(self, item, choices, msg=None):
        count = 0
        for choice in choices:
            if item in choice:
                count += 1
        self.assertLessEqual(count, 1, msg)

    def test_all_teams_are_in_choices_once(self):
        for t in Team:
            if t not in self._skip_teams:
                self.assertTrue(any(t.value in team for team in TEAMS), "{0} is not in TEAMS choices".format(t.name))
                self.assertIsNotDuplicatedIn(t, TEAMS, "{0} is duplicated in TEAM choices".format(t.name))

    def test_all_markets_are_in_choices_once(self):
        for m in Market:
            self.assertTrue(any(m.value in market for market in MARKETS), "{0} is not in MARKETS choices".format(m.name))
            self.assertIsNotDuplicatedIn(m, MARKETS, "{0} is duplicated in MARKETS choices".format(m.name))

    def test_all_categories_are_in_choices_once(self):
        for c in Category:
            if c not in self._skip_categories:
                self.assertTrue(
                    any(c.value in category for category in CATEGORIES), "{0} is not in CATEGORIES choices".format(c.name))
                self.assertIsNotDuplicatedIn(c, CATEGORIES, "{0} is duplicated in CATEGORIES choices".format(c.name))

    def test_all_bookies_are_in_choices_once(self):
        for b in Bookie:
            if b not in self._skip_bookies:
                self.assertTrue(any(b.value in bookie for bookie in BOOKIES), "{0} is not in BOOKIES choices".format(b.name))
                self.assertIsNotDuplicatedIn(b, BOOKIES, "{0} is duplicated in BOOKIES choices".format(b.name))
