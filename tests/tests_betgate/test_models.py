import pytz
from decimal import Decimal
from django.test import TestCase
from datetime import datetime
from betgate_models.models import NotifiedSurebet, Event, Surebet, Odd
from betgate_models.enums import Team, Category, Market, Bookie


class TestModels(TestCase):
    def test_create_and_delete_surebet_and_event(self):
        self.assertEqual(0, Event.objects.all().count())
        self.assertEqual(0, Surebet.objects.all().count())

        event = Event.objects.create(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc)
        )
        self.assertEqual(1, Event.objects.all().count())

        surebet = Surebet.objects.create(
            event=event,
            odds=[
                Odd(market=Market.FT_CORNERS_EXACT_11, bookie=Bookie.MARATHONBET, value=3.4),
                Odd(market=Market.FT_CORNERS_EXACT_4, bookie=Bookie.SPORTIUM, value=5.3)
            ],
            profit_factor=Decimal('1.025')
        )
        self.assertEqual(21468538870013, surebet.id)
        self.assertEqual(1, Surebet.objects.all().count())

        Surebet.objects.all().delete()
        self.assertEqual(0, Surebet.objects.all().count())
        Event.objects.all().delete()
        self.assertEqual(0, Event.objects.all().count())

    def test_surebet_distinct_event(self):
        event = Event.objects.create(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc)
        )
        Surebet.objects.create(
            event=event,
            profit_factor=1.1,
            odds=[
                Odd(market=Market.FT_CORNERS_EXACT_11, bookie=Bookie.MARATHONBET, value=3.4)
            ],
            bookies=[]
        )
        Surebet.objects.create(
            event=event,
            profit_factor=1.2,
            odds=[
                Odd(market=Market.FT_CORNERS_EXACT_11, bookie=Bookie.SPORTIUM, value=3.4)
            ],
            bookies=[]
        )
        events = Surebet.objects.distint_events()
        self.assertEqual(1, len(events))

    def test_max_surebet_notifications(self):
        event = Event.objects.create(
            home_team=Team.OREBRO_SK.value, away_team=Team.BK_HACKEN.value,
            category=Category.ENGLAND__PREMIER_LEAGUE.value, start=datetime.now(tz=pytz.utc)
        )
        Surebet.objects.create(
            profit_factor=Decimal('1.025'), event=event, odds=[]
        )
        NotifiedSurebet.objects.create(profit_factor=Decimal('1.015'), event=event)

        count = NotifiedSurebet.objects.all_surebets_greater_or_not_in().count()
        self.assertEqual(1, count)

    def test_max_surebet_notifications_lower_profit(self):
        event = Event.objects.create(
            home_team=Team.OREBRO_SK.value, away_team=Team.BK_HACKEN.value,
            category=Category.ENGLAND__PREMIER_LEAGUE.value, start=datetime.now(tz=pytz.utc)
        )
        Surebet.objects.create(
            profit_factor=Decimal('1.025'), event=event, odds=[]
        )
        NotifiedSurebet.objects.create(profit_factor=Decimal('1.035'), event=event)

        count = NotifiedSurebet.objects.all_surebets_greater_or_not_in().count()
        self.assertEqual(0, count)

    def test_max_surebet_notifications_equal_profit(self):
        event = Event.objects.create(
            home_team=Team.OREBRO_SK.value, away_team=Team.BK_HACKEN.value,
            category=Category.ENGLAND__PREMIER_LEAGUE.value, start=datetime.now(tz=pytz.utc)
        )
        Surebet.objects.create(
            profit_factor=Decimal('1.025'), event=event, odds=[]
        )
        NotifiedSurebet.objects.create(profit_factor=Decimal('1.025'), event=event)

        count = NotifiedSurebet.objects.all_surebets_greater_or_not_in().count()
        self.assertEqual(0, count)

    def test_max_surebet_notifications_no_previous_surebet(self):
        event = Event.objects.create(
            home_team=Team.OREBRO_SK.value, away_team=Team.BK_HACKEN.value,
            category=Category.ENGLAND__PREMIER_LEAGUE.value, start=datetime.now(tz=pytz.utc)
        )
        Surebet.objects.create(
            profit_factor=Decimal('1.025'), event=event, odds=[]
        )

        count = NotifiedSurebet.objects.all_surebets_greater_or_not_in().count()
        self.assertEqual(1, count)
