import unittest
from betgate_models.enums import Team, Market, Category, Bookie, PrimitiveMarket


class EnumTestCase(unittest.TestCase):

    def assertNotContainDuplicatedItems(self, enum_obj):
        values = []
        for t in enum_obj:
            values.append(t.value)
        self.assertEqual(len(values), len(set(values)))

    def test_there_is_no_duplicated_team_values(self):
        self.assertNotContainDuplicatedItems(Team)

    def test_there_is_no_duplicated_market_values(self):
        self.assertNotContainDuplicatedItems(Market)

    def test_casting_primitive_market_values(self):
        self.assertEqual(Market.FT_WIN_1, PrimitiveMarket.from_market(Market.FT_WIN_1))
        self.assertEqual(Market.FT_WIN_1X, PrimitiveMarket.from_market(Market.FT_WIN_1_P05))
        self.assertEqual(PrimitiveMarket.from_market(Market.FT_WINA_1_P00),
                         PrimitiveMarket.from_market(Market.FT_WIN_DNB_1))

    def test_there_is_no_duplicated_category_values(self):
        self.assertNotContainDuplicatedItems(Category)

    def test_there_is_no_duplicated_bookie_values(self):
        self.assertNotContainDuplicatedItems(Bookie)
