import unittest
from decimal import Decimal
from datetime import datetime
import pytz
from betgate_models.enums import Market, Bookie, Team, Category
from betgate_models.models import Odd, Event
from betgate_core.utils.surebets import find_all_surebets


class SurebetsFinderTestCase(unittest.TestCase):

    def test_complex_surebets_finder(self):
        odds = [
            Odd(value=3.8, market=Market.FT_WIN_1, bookie=Bookie.SPORT888),
            Odd(value=3.6, market=Market.FT_WIN_X, bookie=Bookie.SPORT888),
            Odd(value=1.65, market=Market.FT_WINA_2_P00, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event=event)
        self.assertEqual(1, len(surebets))
        self.assertEqual(Decimal('1.0218'), surebets[0].profit_factor)
        self.assertEqual(0.2631578947368421, surebets[0].odds[0].surebet_coeff)
        self.assertEqual(0.6060606060606061, surebets[0].odds[1].surebet_coeff)
        self.assertEqual(0.10942760942760942, surebets[0].odds[2].surebet_coeff)

    def test_complex_2_surebets_finder(self):
        odds = [
            Odd(value=3.1, market=Market.FT_WIN_2, bookie=Bookie.SPORT888),
            Odd(value=3.3, market=Market.FT_WIN_X, bookie=Bookie.SPORT888),
            Odd(value=2.05, market=Market.FT_WINA_1_P00, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event=event)
        self.assertEqual(1, len(surebets))
        self.assertEqual(Decimal('1.0356'), surebets[0].profit_factor)
        self.assertEqual(0.3225806451612903, surebets[0].odds[0].surebet_coeff)
        self.assertEqual(0.48780487804878053, surebets[0].odds[1].surebet_coeff)
        self.assertEqual(0.15521064301552107, surebets[0].odds[2].surebet_coeff)

    def test_double_asian_complex_surebets_finder(self):
        odds = [
            Odd(value=2.16, market=Market.FT_WINA_2_M10, bookie=Bookie.SPORT888),
            Odd(value=1.91, market=Market.FT_WINA_1_P10, bookie=Bookie.SPORT888),
            Odd(value=1.55, market=Market.FT_WINA_1_P15, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event)
        self.assertEqual(2, len(surebets))
        self.assertEqual(Decimal('1.0101'), surebets[1].profit_factor)
        self.assertEqual(0.018249373088082683, surebets[1].odds[0].surebet_coeff)
        self.assertEqual(0.5087505087505089, surebets[1].odds[1].surebet_coeff)
        self.assertEqual(0.4629629629629629, surebets[1].odds[2].surebet_coeff)

    def test_double_asian_complex_surebets_finder_european(self):
        odds = [
            Odd(value=1.6, market=Market.FT_WINA_2_P00, bookie=Bookie.SPORT888),
            Odd(value=2.85, market=Market.FT_WINA_1_P00, bookie=Bookie.SPORT888),
            Odd(value=1.72, market=Market.FT_WIN_1_P10, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event)
        self.assertEqual(2, len(surebets))
        self.assertEqual(Decimal('1.0158'), surebets[1].profit_factor)
        self.assertEqual(0.021605908233815238, surebets[1].odds[0].surebet_coeff)
        self.assertEqual(0.33783783783783783, surebets[1].odds[1].surebet_coeff)
        self.assertEqual(0.625, surebets[1].odds[2].surebet_coeff)

    def test_double_asian_complex_2_surebets_finder(self):
        odds = [
            Odd(value=1.65, market=Market.FT_WIN_2, bookie=Bookie.SPORT888),
            Odd(value=2.16, market=Market.FT_WINA_2_M10, bookie=Bookie.SPORT888),
            Odd(value=1.91, market=Market.FT_WINA_1_P10, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event=event)
        self.assertEqual(2, len(surebets))
        self.assertEqual(Decimal('1.0100'), surebets[1].profit_factor)
        self.assertEqual(0.015208959061640081, surebets[1].odds[0].surebet_coeff)
        self.assertEqual(0.4513450081242101, surebets[1].odds[1].surebet_coeff)
        self.assertEqual(0.5235602094240838, surebets[1].odds[2].surebet_coeff)

    def test_double_half_asian_complex_surebets_finder(self):
        odds = [
            Odd(value=2.16, market=Market.FT_WINA_2_M10, bookie=Bookie.SPORT888),
            Odd(value=1.91, market=Market.FT_WINA_1_P10, bookie=Bookie.SPORT888),
            Odd(value=1.68, market=Market.FT_WINA_1_P125, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event=event)
        self.assertEqual(2, len(surebets))
        self.assertEqual(Decimal('1.0101'), surebets[1].profit_factor)
        self.assertEqual(0.029270799113874105, surebets[1].odds[0].surebet_coeff)
        self.assertEqual(0.49781416622444574, surebets[1].odds[1].surebet_coeff)
        self.assertEqual(0.4629629629629629, surebets[1].odds[2].surebet_coeff)

    def test_double_half_asian_complex_2_surebets_finder(self):
        odds = [
            Odd(value=2.16, market=Market.FT_WINA_2_M10, bookie=Bookie.SPORT888),
            Odd(value=1.8, market=Market.FT_WINA_2_M075, bookie=Bookie.SPORT888),
            Odd(value=1.91, market=Market.FT_WINA_1_P10, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event=event)
        self.assertEqual(2, len(surebets))
        self.assertEqual(Decimal('1.0096'), surebets[1].profit_factor)
        self.assertEqual(0.023782636964035216, surebets[1].odds[0].surebet_coeff)
        self.assertEqual(0.4431440988262668, surebets[1].odds[1].surebet_coeff)
        self.assertEqual(0.5235602094240838, surebets[1].odds[2].surebet_coeff)

    def test_double_half_asian_complex_3_surebets_finder(self):
        odds = [
            Odd(value=1.97, market=Market.FT_WINA_2_M20, bookie=Bookie.SPORT888),
            Odd(value=2.1, market=Market.FT_WINA_1_P20, bookie=Bookie.SPORT888),
            Odd(value=1.82, market=Market.FT_WINA_1_P225, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event=event)
        self.assertEqual(2, len(surebets))
        self.assertEqual(Decimal('1.0124'), surebets[1].profit_factor)
        self.assertEqual(0.029807320143965175, surebets[1].odds[0].surebet_coeff)
        self.assertEqual(0.45035746539903987, surebets[1].odds[1].surebet_coeff)
        self.assertEqual(0.5076142131979695, surebets[1].odds[2].surebet_coeff)

    def test_false_double_half_asian_complex_surebets_finder(self):
        odds = [
            Odd(value=8.75, market=Market.FT_WIN_DNB_1, bookie=Bookie.SPORT888),
            Odd(value=1.05, market=Market.FT_WINA_2_P025, bookie=Bookie.SPORT888),
            Odd(value=1.02, market=Market.FT_WINA_2_P00, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event=event)
        self.assertEqual(0, len(surebets))

    def test_double_asian_european_complex_surebets_finder(self):
        odds = [
            Odd(value=3.8, market=Market.FT_DRAW_1_P10, bookie=Bookie.SPORT888),
            Odd(value=2.16, market=Market.FT_WINA_2_M10, bookie=Bookie.SPORT888),
            Odd(value=1.91, market=Market.FT_WINA_1_P10, bookie=Bookie.SPORT888)
        ]
        event = Event(
            home_team=Team.ALAVES,
            away_team=Team.MAASTRICHT,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime(day=1, month=1, year=2019, hour=3, minute=0, tzinfo=pytz.utc),
            odds=odds
        )
        surebets = find_all_surebets(event=event)
        self.assertEqual(2, len(surebets))
        self.assertEqual(Decimal('1.0100'), surebets[1].profit_factor)
        self.assertEqual(0.003546533582356138, surebets[1].odds[0].surebet_coeff)
        self.assertEqual(0.4629629629629629, surebets[1].odds[1].surebet_coeff)
        self.assertEqual(0.5235602094240838, surebets[1].odds[2].surebet_coeff)
