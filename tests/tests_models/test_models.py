import unittest
from betgate_core.models.football.teams import TEAM_NAMES


class TeamNamesTestCase(unittest.TestCase):

    def test_there_is_no_duplicated_team_values(self):
        values = []
        for team_name in TEAM_NAMES:
            self.assertEqual(team_name.lower(), team_name)
            self.assertNotIn(team_name, values, "{0} is duplicated".format(team_name))
            values.append(team_name)
        self.assertEqual(len(values), len(set(values)), "Some team names are duplicated")
