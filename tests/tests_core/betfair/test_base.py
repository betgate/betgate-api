from django.test import tag
from betgate_core.test.testcases import BookieTestCase
from betgate_models.enums import Bookie


@tag('external-service', 'betfair')
class TestBetfair(BookieTestCase):
    bookie_to_test = Bookie.BETFAIR
