from django.test import tag
from betgate_core.test.testcases import BookieTestCase
from betgate_models.enums import Bookie


@tag('external-service', 'kambi')
class TestKambi(BookieTestCase):
    bookie_to_test = Bookie.SPORT888
