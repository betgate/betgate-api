from django.test import tag
from betgate_core.test.testcases import BookieTestCase
from betgate_models.enums import Bookie


@tag('external-service', 'luckia')
class TestLuckia(BookieTestCase):
    bookie_to_test = Bookie.LUCKIA
