from betgate.apps.code.tests.testcases import PerformanceTestCase
from betgate_models.models import Surebet, Event
from betgate_tasks.apps.celery_tasks.tasks import search_event_odds
from betgate_models.enums import Category


class TasksPerformanceTestCase(PerformanceTestCase):
    def setUp(self):
        self.set_up_scenario()

    def test_search_surebets_and_save_markets(self):
        self.set_up_db(300, 5, 5, 40, Category.PORTUGAL__PRIMEIRA_LIGA)
        self.set_up_db(300, 0, 5, 40, Category.SPAIN__PRIMERA_DIVISION)
        self.set_up_spain_primera_mock(30, 4, 40)
        self.assertEqual(5, Surebet.objects.count())
        self.assertEqual(600, Event.objects.count())

        self.init_performance_test()
        search_event_odds(category=Category.SPAIN__PRIMERA_DIVISION.value, search_surebets=True)
        self.end_performance_test("Search surebets and save markets")

        self.assertEqual(9, Surebet.objects.count())
        self.assertEqual(330, Event.objects.count())

    def test_search_surebets_and_save_markets_with_previous_surebets(self):
        self.set_up_db(300, 0, 5, 40, Category.PORTUGAL__PRIMEIRA_LIGA)
        self.set_up_db(300, 5, 5, 40, Category.SPAIN__PRIMERA_DIVISION)
        self.set_up_spain_primera_mock(30, 4, 40)
        self.assertEqual(5, Surebet.objects.count())
        self.assertEqual(600, Event.objects.count())

        self.init_performance_test()
        search_event_odds(category=Category.SPAIN__PRIMERA_DIVISION.value, search_surebets=True)
        self.end_performance_test("Search surebets and save markets with previous surebets")

        self.assertEqual(9, Surebet.objects.count())
        self.assertEqual(335, Event.objects.count())

    def _test_update_surebets(self):
        self.assertEqual(10, Surebet.objects.count())

        self.init_performance_test()
        #update_surebets()
        self.end_performance_test("Update surebets")

        self.assertEqual(4, Surebet.objects.count())
