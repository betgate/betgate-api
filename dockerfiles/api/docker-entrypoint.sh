#!/bin/sh
# wait-for-postgres.sh
set -e

export BETGATE_VERSION=$(cat version.txt)
export POSTGRES_HOST=${POSTGRES_HOST:-localhost}
export POSTGRES_USER=${POSTGRES_USER:-postgres}
export POSTGRES_DB=${POSTGRES_DB:-postgres}
export POSTGRES_PASSWORD
export DJANGO_SETTINGS_MODULE

require_postgres()
{
    until PGPASSWORD=$POSTGRES_PASSWORD psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB  -c '\q'; do
      >&2 echo "Postgres is unavailable - sleeping"
      sleep 1
    done
}

# commands
while test $# -gt 0
do
    case "$1" in
        --collectstatic)
            python manage.py collectstatic --no-input
            ;;
        --runserver)
            require_postgres
            python manage.py runserver 0.0.0.0:8000
            ;;
        --gunicorn)
            require_postgres
            exec gunicorn betgate_api.wsgi:application --bind 0.0.0.0:8000 --workers 3
            ;;
        --migrate)
            require_postgres
            python manage.py migrate --no-input
            ;;
        --seed)
            require_postgres
            python manage.py seed
            ;;
        --test)
            require_postgres
            python manage.py test tests.tests_betgate --settings betgate_api.settings.test
            python manage.py test tests.tests_models --settings betgate_api.settings.test
            ;;
        --fake)
            require_postgres
            python manage.py fake
            ;;
        --*) echo "bad option $1"
            ;;
        *) echo "argument $1"
            ;;
    esac
    shift
done
