#!/bin/sh
# wait-for-postgres.sh
set -e

export BETGATE_VERSION=$(cat version.txt)
export POSTGRES_HOST=${POSTGRES_HOST:-localhost}
export POSTGRES_USER=${POSTGRES_USER:-postgres}
export POSTGRES_DB=${POSTGRES_DB:-postgres}
export POSTGRES_PASSWORD
export DJANGO_SETTINGS_MODULE

require_postgres()
{
    until PGPASSWORD=$POSTGRES_PASSWORD psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB  -c '\q'; do
      >&2 echo "Postgres is unavailable - sleeping"
      sleep 1
    done
}

# commands
while test $# -gt 0
do
    case "$1" in
        --migrate)
            require_postgres
            python manage.py migrate --no-input
            ;;
        --seed)
            require_postgres
            python manage.py seed
            ;;
        --test)
            require_postgres
            python manage.py test tests.tests_betgate --settings betgate_tasks.settings.test
            python manage.py test tests.tests_models --settings betgate_tasks.settings.test
            ;;
        --fake)
            require_postgres
            python manage.py fake
            ;;
        --worker)
            require_postgres
            celery -A betgate_tasks worker -E -l info --concurrency=3
            ;;
        --beat)
            # Avoid old beats to be sent
            rm -f celerybeat-schedule
            supervisord -c supervisor.d/betgate_beat.ini -n
            ;;
        --*) echo "bad option $1"
            ;;
        *) echo "argument $1"
            ;;
    esac
    shift
done
