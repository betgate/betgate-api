from sdklib.http import HttpSdk


class ParseMode:
    HTML = "HTML"
    Markdown = "Markdown"


class TelegramApi(HttpSdk):

    DEFAULT_HOST = "https://api.telegram.org"
    # DEFAULT_PROXY = "http://localhost:8080"

    GET_ME_API_URL_PATH = "/getMe"
    SEND_MESSAGE_API_URL_PATH = "/sendMessage"
    GET_UPDATES_API_URL_PATH = "/getUpdates"

    def __init__(self, token):
        super().__init__()

        self.prefix_url_path = f"/bot{token}"

    def get_me(self):
        return self.get(self.GET_ME_API_URL_PATH)

    def send_message(self, chat_id, text, parse_mode=None, disable_web_page_preview=False, disable_notification=None,
                     reply_to_message_id=None, reply_markup=None):
        params = {
            "chat_id": chat_id,
            "text": text,
            "disable_web_page_preview": disable_web_page_preview
        }
        if parse_mode is not None:
            params["parse_mode"] = parse_mode
        return self.post(self.SEND_MESSAGE_API_URL_PATH, body_params=params)

    def get_updates(self):
        return self.get(self.GET_UPDATES_API_URL_PATH)
