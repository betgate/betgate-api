# Betgate #

[![pipeline status](https://gitlab.com/betgate/betgate-api/badges/master/pipeline.svg)](https://gitlab.com/betgate/betgate-api/commits/master)
[![coverage report](https://gitlab.com/betgate/betgate-api/badges/master/coverage.svg)](https://gitlab.com/betgate/betgate-api/commits/master)

Maths for winning, Surebets!

## Usage ##

* Admin user: username=betgate, password=marcosestagordo


## Development ##

### Requirements ###

* Install requirements from *requirements.txt* file:

        pip install -r requirements.txt

* Install docker for deploying.

### Make Migrations ###

If you make some changes on models, you must update migrations files:

        python manage.py makemigrations --settings betgate_app.settings.nodb

### Generate new code ###

* Add new Championship.

        python manage.py generate championship spain.primera_division

* Add new Team.

        python manage.py generate team fc_barcelona [ --country spain --name "F.C. Barcelona" "FCBarcelona" ]

### Run in devel mode

* From docker:

        docker-compose up -d

* Exec commands:

        docker exec -it betgate_api <command>

### Create or Update Data Base ###

        python manage.py migrate
        
### Update Data Base Content ###

* Create initial data in DB.

         docker-compose run web --seed

* Add fake data to DB.

        docker-compose run web --fake

### Run in local mode ###

* PostgresQL.

        docker run --name postgres -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres postgres:10

* RabbitMQ.

        docker run --name rabbit -d -p 4369:4369 -p 5671:5671 -p 5672:5672 -p 25672:25672 -e RABBITMQ_DEFAULT_USER=guest -e RABBITMQ_DEFAULT_USER=guest rabbitmq:3

* Celery worker.

        export DJANGO_SETTINGS_MODULE=betgate_app.settings.local
        celery -A betgate_app worker --loglevel=DEBUG

* Celery beat.

        export DJANGO_SETTINGS_MODULE=betgate_app.settings.local
        celery -A betgate_app beat -l info
