import itertools
import abc
import copy
from decimal import Decimal
from betgate_models.enums import Market, PrimitiveMarket
from betgate_models.models import Surebet


class SurebetFinderBase:
    __metaclass__ = abc.ABCMeta

    market_odds = dict()
    _required_markets = []

    def __init__(self, *args):
        self._required_markets = [PrimitiveMarket.from_market(arg) for arg in args]

    @abc.abstractmethod
    def get_profit_factor(self, odds):
        raise NotImplementedError

    def find_surebets(self, market_odds_per_primitive, event=None):
        surebets = []
        if all(mkt in market_odds_per_primitive for mkt in self._required_markets):
            all_combinations = [market_odds_per_primitive[mkt] for mkt in self._required_markets]
            for combined_markets in itertools.product(*all_combinations):
                factor = self.get_profit_factor(combined_markets)
                if factor > 1:
                    bookies = {odd.bookie for odd in combined_markets}
                    surebets.append(Surebet(
                        profit_factor=factor,
                        odds=copy.deepcopy(combined_markets),
                        event=event,
                        bookies=list(bookies)
                    ))
        return surebets


class SimpleSurebetFinder(SurebetFinderBase):
    def get_profit_factor(self, odds):
        profit_factor = 0
        for odd in odds:
            odd.surebet_coeff = 1/odd.value
            profit_factor += odd.surebet_coeff
        return Decimal(1/profit_factor).quantize(Decimal('1.0000'))


class HAComplexSurebetFinder(SurebetFinderBase):
    simple_market = None
    assian_market = None
    main_market = None

    def __init__(self, simple_market, assian_market, main_market):
        super().__init__(simple_market, assian_market, main_market)
        self.simple_market = PrimitiveMarket.from_market(simple_market)
        self.assian_market = PrimitiveMarket.from_market(assian_market)
        self.main_market = PrimitiveMarket.from_market(main_market)

    def get_profit_factor(self, odds):
        assian_odd = [odd for odd in odds
                      if PrimitiveMarket.from_market(odd.market) == self.assian_market][0]
        main_odd = [odd for odd in odds
                    if PrimitiveMarket.from_market(odd.market) == self.main_market][0]
        simple_odd = [odd for odd in odds
                      if PrimitiveMarket.from_market(odd.market) == self.simple_market][0]

        main_odd.surebet_coeff = (1 - (1 / assian_odd.value)) / main_odd.value
        simple_odd.surebet_coeff = 1/simple_odd.value
        assian_odd.surebet_coeff = 1/assian_odd.value

        if all(0 < coeff < 1
               for coeff in [main_odd.surebet_coeff, simple_odd.surebet_coeff, assian_odd.surebet_coeff]):
            return Decimal(1/(main_odd.surebet_coeff + simple_odd.surebet_coeff + assian_odd.surebet_coeff))\
                .quantize(Decimal('1.0000'))
        return Decimal('0.0000')


class DoubleHAComplexSurebetFinder(SurebetFinderBase):
    main_market = None
    main_asian_market = None
    asian_market = None

    def __init__(self, main_market, main_asian_market, asian_market):
        super().__init__(main_market, main_asian_market, asian_market)
        self.main_market = PrimitiveMarket.from_market(main_market)
        self.main_asian_market = PrimitiveMarket.from_market(main_asian_market)
        self.asian_market = PrimitiveMarket.from_market(asian_market)

    def get_profit_factor(self, odds):
        main_asian_odd = [odd for odd in odds
                          if PrimitiveMarket.from_market(odd.market) == self.main_asian_market][0]
        asian_odd = [odd for odd in odds
                     if PrimitiveMarket.from_market(odd.market) == self.asian_market][0]
        main_odd = [odd for odd in odds
                    if PrimitiveMarket.from_market(odd.market) == self.main_market][0]

        try:
            main_odd.surebet_coeff = (main_asian_odd.value - main_asian_odd.value/asian_odd.value - 1) /\
                         ((main_asian_odd.value - 1) * main_odd.value)
            main_asian_odd.surebet_coeff = (1 - main_odd.surebet_coeff*main_odd.value) / main_asian_odd.value
            asian_odd.surebet_coeff = 1/asian_odd.value

            if all(0 < coeff < 1
                   for coeff in [main_odd.surebet_coeff, main_asian_odd.surebet_coeff, asian_odd.surebet_coeff]):
                return Decimal(1/(main_odd.surebet_coeff + main_asian_odd.surebet_coeff + asian_odd.surebet_coeff))\
                    .quantize(Decimal('1.0000'))
        except:
            pass
        return Decimal('0.0000')


class DoubleHAXComplexSurebetFinder(SurebetFinderBase):
    european_market = None
    asian_market1 = None
    asian_market2 = None

    def __init__(self, european_market, asian_market1, asian_market2):
        super().__init__(european_market, asian_market1, asian_market2)
        self.european_market = PrimitiveMarket.from_market(european_market)
        self.asian_market1 = PrimitiveMarket.from_market(asian_market1)
        self.asian_market2 = PrimitiveMarket.from_market(asian_market2)

    def get_profit_factor(self, odds):
        european_odd = [odd for odd in odds
                        if PrimitiveMarket.from_market(odd.market) == self.european_market][0]
        asian_odd = [odd for odd in odds
                     if PrimitiveMarket.from_market(odd.market) == self.asian_market1][0]
        asian_odd2 = [odd for odd in odds
                      if PrimitiveMarket.from_market(odd.market) == self.asian_market2][0]

        try:
            european_odd.surebet_coeff = (1 - 1/asian_odd.value - 1/asian_odd2.value) / european_odd.value
            asian_odd.surebet_coeff = 1/asian_odd.value
            asian_odd2.surebet_coeff = 1 / asian_odd2.value

            if all(0 < coeff < 1
                   for coeff in [european_odd.surebet_coeff, asian_odd.surebet_coeff, asian_odd2.surebet_coeff]):
                return Decimal(1/(european_odd.surebet_coeff + asian_odd.surebet_coeff + asian_odd2.surebet_coeff))\
                    .quantize(Decimal('1.0000'))
        except:
            pass
        return Decimal('0.0000')


class DoubleHalfHAComplexSurebetFinder(SurebetFinderBase):
    main_market = None
    main_asian_market = None
    asian_market = None

    def __init__(self, main_market, main_asian_market, asian_market):
        super().__init__(main_market, main_asian_market, asian_market)
        self.main_market = PrimitiveMarket.from_market(main_market)
        self.main_asian_market = PrimitiveMarket.from_market(main_asian_market)
        self.asian_market = PrimitiveMarket.from_market(asian_market)

    def get_profit_factor(self, odds):
        main_asian_odd = [odd for odd in odds
                          if PrimitiveMarket.from_market(odd.market) == self.main_asian_market][0]
        asian_odd = [odd for odd in odds
                     if PrimitiveMarket.from_market(odd.market) == self.asian_market][0]
        main_odd = [odd for odd in odds
                    if PrimitiveMarket.from_market(odd.market) == self.main_market][0]

        avalue = asian_odd.value
        bvalue = main_asian_odd.value
        cvalue = main_odd.value

        try:
            main_odd.surebet_coeff = (2*avalue - 2*avalue*bvalue + 2*bvalue) /\
                                     (avalue*(-bvalue + 2*cvalue - cvalue*bvalue))
            main_asian_odd.surebet_coeff = 1 +\
                                           (avalue*bvalue + avalue*cvalue*bvalue - avalue -avalue*cvalue - 2*cvalue) /\
                                           (avalue*(2*cvalue - cvalue*bvalue - bvalue))
            asian_odd.surebet_coeff = 1/asian_odd.value

            if all(0 < coeff < 1
                   for coeff in [main_odd.surebet_coeff, main_asian_odd.surebet_coeff, asian_odd.surebet_coeff]):
                return Decimal(1/(main_odd.surebet_coeff + main_asian_odd.surebet_coeff + asian_odd.surebet_coeff))\
                    .quantize(Decimal('1.0000'))
        except:
            pass
        return Decimal('0.0000')


ALL_SUREBETS_FINDERS = [
    SimpleSurebetFinder(Market.FT_WIN_X, Market.FT_WIN_1, Market.FT_WIN_2),
    SimpleSurebetFinder(Market.FT_WIN_1, Market.FT_WIN_X2),
    SimpleSurebetFinder(Market.FT_WIN_1X, Market.FT_WIN_2),
    SimpleSurebetFinder(Market.FT_WIN_12, Market.FT_WIN_X),
    SimpleSurebetFinder(Market.FT_WIN_DNB_1, Market.FT_WIN_DNB_2),
    SimpleSurebetFinder(Market.FT_WIN_1_P10, Market.FT_DRAW_1_P10, Market.FT_WIN_2_M10),
    SimpleSurebetFinder(Market.FT_WIN_1_M10, Market.FT_DRAW_1_M10, Market.FT_WIN_2_P10),
    SimpleSurebetFinder(Market.FT_WIN_1_P15, Market.FT_WIN_2_M15),
    SimpleSurebetFinder(Market.FT_WIN_1_M15, Market.FT_WIN_2_P15),
    SimpleSurebetFinder(Market.FT_WIN_1_P20, Market.FT_DRAW_1_P20, Market.FT_WIN_2_M20),
    SimpleSurebetFinder(Market.FT_WIN_1_M20, Market.FT_DRAW_1_M20, Market.FT_WIN_2_P20),
    SimpleSurebetFinder(Market.FT_WIN_1_P25, Market.FT_WIN_2_M25),
    SimpleSurebetFinder(Market.FT_WIN_1_M25, Market.FT_WIN_2_P25),
    SimpleSurebetFinder(Market.FT_WIN_1_P30, Market.FT_DRAW_1_P30, Market.FT_WIN_2_M30),
    SimpleSurebetFinder(Market.FT_WIN_1_M30, Market.FT_DRAW_1_M30, Market.FT_WIN_2_P30),
    SimpleSurebetFinder(Market.FT_WIN_1_P35, Market.FT_WIN_2_M35),
    SimpleSurebetFinder(Market.FT_WIN_1_M35, Market.FT_WIN_2_P35),
    # Asian winners
    SimpleSurebetFinder(Market.FT_WINA_1_P025, Market.FT_WINA_2_M025),
    SimpleSurebetFinder(Market.FT_WINA_1_M025, Market.FT_WINA_2_P025),
    SimpleSurebetFinder(Market.FT_WINA_1_P075, Market.FT_WINA_2_M075),
    SimpleSurebetFinder(Market.FT_WINA_1_M075, Market.FT_WINA_2_P075),
    SimpleSurebetFinder(Market.FT_WINA_1_P10, Market.FT_WINA_2_M10),
    SimpleSurebetFinder(Market.FT_WINA_1_M10, Market.FT_WINA_2_P10),
    SimpleSurebetFinder(Market.FT_WINA_1_P125, Market.FT_WINA_2_M125),
    SimpleSurebetFinder(Market.FT_WINA_1_M125, Market.FT_WINA_2_P125),
    SimpleSurebetFinder(Market.FT_WINA_1_P15, Market.FT_WINA_2_M15),
    SimpleSurebetFinder(Market.FT_WINA_1_M15, Market.FT_WINA_2_P15),
    SimpleSurebetFinder(Market.FT_WINA_1_P175, Market.FT_WINA_2_M175),
    SimpleSurebetFinder(Market.FT_WINA_1_M175, Market.FT_WINA_2_P175),
    SimpleSurebetFinder(Market.FT_WINA_1_P20, Market.FT_WINA_2_M20),
    SimpleSurebetFinder(Market.FT_WINA_1_M20, Market.FT_WINA_2_P20),
    SimpleSurebetFinder(Market.FT_WINA_1_P225, Market.FT_WINA_2_M225),
    SimpleSurebetFinder(Market.FT_WINA_1_M225, Market.FT_WINA_2_P225),
    SimpleSurebetFinder(Market.FT_WINA_1_P25, Market.FT_WINA_2_M25),
    SimpleSurebetFinder(Market.FT_WINA_1_M25, Market.FT_WINA_2_P25),
    SimpleSurebetFinder(Market.FT_WINA_1_P275, Market.FT_WINA_2_M275),
    SimpleSurebetFinder(Market.FT_WINA_1_M275, Market.FT_WINA_2_P275),
    SimpleSurebetFinder(Market.FT_WINA_1_P30, Market.FT_WINA_2_M30),
    SimpleSurebetFinder(Market.FT_WINA_1_M30, Market.FT_WINA_2_P30),
    SimpleSurebetFinder(Market.FT_WINA_1_P325, Market.FT_WINA_2_M325),
    SimpleSurebetFinder(Market.FT_WINA_1_M325, Market.FT_WINA_2_P325),
    SimpleSurebetFinder(Market.FT_WINA_1_P35, Market.FT_WINA_2_M35),
    SimpleSurebetFinder(Market.FT_WINA_1_M35, Market.FT_WINA_2_P35),
    # fullTime goals
    SimpleSurebetFinder(Market.FT_TG_OVER_05, Market.FT_TG_UNDER_05),
    SimpleSurebetFinder(Market.FT_TGA_OVER_075, Market.FT_TGA_UNDER_075),
    SimpleSurebetFinder(Market.FT_TGA_OVER_10, Market.FT_TGA_UNDER_10),
    SimpleSurebetFinder(Market.FT_TGA_OVER_125, Market.FT_TGA_UNDER_125),
    SimpleSurebetFinder(Market.FT_TG_OVER_15, Market.FT_TG_UNDER_15),
    SimpleSurebetFinder(Market.FT_TGA_OVER_175, Market.FT_TGA_UNDER_175),
    SimpleSurebetFinder(Market.FT_TGA_OVER_20, Market.FT_TGA_UNDER_20),
    SimpleSurebetFinder(Market.FT_TGA_OVER_225, Market.FT_TGA_UNDER_225),
    SimpleSurebetFinder(Market.FT_TG_OVER_25, Market.FT_TG_UNDER_25),
    SimpleSurebetFinder(Market.FT_TGA_OVER_275, Market.FT_TGA_UNDER_275),
    SimpleSurebetFinder(Market.FT_TGA_OVER_30, Market.FT_TGA_UNDER_30),
    SimpleSurebetFinder(Market.FT_TGA_OVER_325, Market.FT_TGA_UNDER_325),
    SimpleSurebetFinder(Market.FT_TG_OVER_35, Market.FT_TG_UNDER_35),
    SimpleSurebetFinder(Market.FT_TGA_OVER_375, Market.FT_TGA_UNDER_375),
    SimpleSurebetFinder(Market.FT_TGA_OVER_40, Market.FT_TGA_UNDER_40),
    SimpleSurebetFinder(Market.FT_TGA_OVER_425, Market.FT_TGA_UNDER_425),
    SimpleSurebetFinder(Market.FT_TG_OVER_45, Market.FT_TG_UNDER_45),
    SimpleSurebetFinder(Market.FT_TGA_OVER_475, Market.FT_TGA_UNDER_475),
    SimpleSurebetFinder(Market.FT_TGA_OVER_50, Market.FT_TGA_UNDER_50),
    SimpleSurebetFinder(Market.FT_TGA_OVER_525, Market.FT_TGA_UNDER_525),
    SimpleSurebetFinder(Market.FT_TG_OVER_55, Market.FT_TG_UNDER_55),
    SimpleSurebetFinder(Market.FT_BTTS_YES, Market.FT_BTTS_NO),

    SimpleSurebetFinder(Market.FH_WIN_X, Market.FH_WIN_1, Market.FH_WIN_2),
    SimpleSurebetFinder(Market.FH_WIN_1, Market.FH_WIN_X2),
    SimpleSurebetFinder(Market.FH_WIN_1X, Market.FH_WIN_2),
    SimpleSurebetFinder(Market.FH_WIN_12, Market.FH_WIN_X),
    SimpleSurebetFinder(Market.FH_WIN_DNB_1, Market.FH_WIN_DNB_2),
    SimpleSurebetFinder(Market.FH_WIN_1_P10, Market.FH_DRAW_1_P10, Market.FH_WIN_2_M10),
    SimpleSurebetFinder(Market.FH_WIN_1_M10, Market.FH_DRAW_1_M10, Market.FH_WIN_2_P10),
    SimpleSurebetFinder(Market.FH_WIN_1_P15, Market.FH_WIN_2_M15),
    SimpleSurebetFinder(Market.FH_WIN_1_M15, Market.FH_WIN_2_P15),
    SimpleSurebetFinder(Market.FH_WIN_1_P20, Market.FH_DRAW_1_P20, Market.FH_WIN_2_M20),
    SimpleSurebetFinder(Market.FH_WIN_1_M20, Market.FH_DRAW_1_M20, Market.FH_WIN_2_P20),
    SimpleSurebetFinder(Market.FH_WIN_1_P25, Market.FH_WIN_2_M25),
    SimpleSurebetFinder(Market.FH_WIN_1_M25, Market.FH_WIN_2_P25),
    SimpleSurebetFinder(Market.FH_WIN_1_P30, Market.FH_DRAW_1_P30, Market.FH_WIN_2_M30),
    SimpleSurebetFinder(Market.FH_WIN_1_M30, Market.FH_DRAW_1_M30, Market.FH_WIN_2_P30),
    SimpleSurebetFinder(Market.FH_WIN_1_P35, Market.FH_WIN_2_M35),
    SimpleSurebetFinder(Market.FH_WIN_1_M35, Market.FH_WIN_2_P35),
    # Asian winners
    SimpleSurebetFinder(Market.FH_WINA_1_P025, Market.FH_WINA_2_M025),
    SimpleSurebetFinder(Market.FH_WINA_1_M025, Market.FH_WINA_2_P025),
    SimpleSurebetFinder(Market.FH_WINA_1_P075, Market.FH_WINA_2_M075),
    SimpleSurebetFinder(Market.FH_WINA_1_M075, Market.FH_WINA_2_P075),
    SimpleSurebetFinder(Market.FH_WINA_1_P10, Market.FH_WINA_2_M10),
    SimpleSurebetFinder(Market.FH_WINA_1_M10, Market.FH_WINA_2_P10),
    SimpleSurebetFinder(Market.FH_WINA_1_P125, Market.FH_WINA_2_M125),
    SimpleSurebetFinder(Market.FH_WINA_1_M125, Market.FH_WINA_2_P125),
    SimpleSurebetFinder(Market.FH_WINA_1_P15, Market.FH_WINA_2_M15),
    SimpleSurebetFinder(Market.FH_WINA_1_M15, Market.FH_WINA_2_P15),
    SimpleSurebetFinder(Market.FH_WINA_1_P175, Market.FH_WINA_2_M175),
    SimpleSurebetFinder(Market.FH_WINA_1_M175, Market.FH_WINA_2_P175),
    SimpleSurebetFinder(Market.FH_WINA_1_P20, Market.FH_WINA_2_M20),
    SimpleSurebetFinder(Market.FH_WINA_1_M20, Market.FH_WINA_2_P20),
    SimpleSurebetFinder(Market.FH_WINA_1_P225, Market.FH_WINA_2_M225),
    SimpleSurebetFinder(Market.FH_WINA_1_M225, Market.FH_WINA_2_P225),
    SimpleSurebetFinder(Market.FH_WINA_1_P25, Market.FH_WINA_2_M25),
    SimpleSurebetFinder(Market.FH_WINA_1_M25, Market.FH_WINA_2_P25),
    SimpleSurebetFinder(Market.FH_WINA_1_P275, Market.FH_WINA_2_M275),
    SimpleSurebetFinder(Market.FH_WINA_1_M275, Market.FH_WINA_2_P275),
    SimpleSurebetFinder(Market.FH_WINA_1_P30, Market.FH_WINA_2_M30),
    SimpleSurebetFinder(Market.FH_WINA_1_M30, Market.FH_WINA_2_P30),
    SimpleSurebetFinder(Market.FH_WINA_1_P325, Market.FH_WINA_2_M325),
    SimpleSurebetFinder(Market.FH_WINA_1_M325, Market.FH_WINA_2_P325),
    SimpleSurebetFinder(Market.FH_WINA_1_P35, Market.FH_WINA_2_M35),
    SimpleSurebetFinder(Market.FH_WINA_1_M35, Market.FH_WINA_2_P35),
    # fullTime goals
    SimpleSurebetFinder(Market.FH_TG_OVER_05, Market.FH_TG_UNDER_05),
    SimpleSurebetFinder(Market.FH_TGA_OVER_075, Market.FH_TGA_UNDER_075),
    SimpleSurebetFinder(Market.FH_TGA_OVER_10, Market.FH_TGA_UNDER_10),
    SimpleSurebetFinder(Market.FH_TGA_OVER_125, Market.FH_TGA_UNDER_125),
    SimpleSurebetFinder(Market.FH_TG_OVER_15, Market.FH_TG_UNDER_15),
    SimpleSurebetFinder(Market.FH_TGA_OVER_175, Market.FH_TGA_UNDER_175),
    SimpleSurebetFinder(Market.FH_TGA_OVER_20, Market.FH_TGA_UNDER_20),
    SimpleSurebetFinder(Market.FH_TGA_OVER_225, Market.FH_TGA_UNDER_225),
    SimpleSurebetFinder(Market.FH_TG_OVER_25, Market.FH_TG_UNDER_25),
    SimpleSurebetFinder(Market.FH_TGA_OVER_275, Market.FH_TGA_UNDER_275),
    SimpleSurebetFinder(Market.FH_TGA_OVER_30, Market.FH_TGA_UNDER_30),
    SimpleSurebetFinder(Market.FH_TGA_OVER_325, Market.FH_TGA_UNDER_325),
    SimpleSurebetFinder(Market.FH_TG_OVER_35, Market.FH_TG_UNDER_35),
    SimpleSurebetFinder(Market.FH_TGA_OVER_375, Market.FH_TGA_UNDER_375),
    SimpleSurebetFinder(Market.FH_TGA_OVER_40, Market.FH_TGA_UNDER_40),
    SimpleSurebetFinder(Market.FH_TGA_OVER_425, Market.FH_TGA_UNDER_425),
    SimpleSurebetFinder(Market.FH_TG_OVER_45, Market.FH_TG_UNDER_45),
    SimpleSurebetFinder(Market.FH_BTTS_YES, Market.FH_BTTS_NO),

    SimpleSurebetFinder(Market.FT_CORNERS_OVER_0_5, Market.FT_CORNERS_UNDER_0_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_1_5, Market.FT_CORNERS_UNDER_1_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_2_5, Market.FT_CORNERS_UNDER_2_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_3_5, Market.FT_CORNERS_UNDER_3_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_4_5, Market.FT_CORNERS_UNDER_4_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_5_5, Market.FT_CORNERS_UNDER_5_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_6_5, Market.FT_CORNERS_UNDER_6_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_7_5, Market.FT_CORNERS_UNDER_7_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_8_5, Market.FT_CORNERS_UNDER_8_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_9_5, Market.FT_CORNERS_UNDER_9_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_10_5, Market.FT_CORNERS_UNDER_10_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_11_5, Market.FT_CORNERS_UNDER_11_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_12_5, Market.FT_CORNERS_UNDER_12_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_13_5, Market.FT_CORNERS_UNDER_13_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_14_5, Market.FT_CORNERS_UNDER_14_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_15_5, Market.FT_CORNERS_UNDER_15_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_16_5, Market.FT_CORNERS_UNDER_16_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_17_5, Market.FT_CORNERS_UNDER_17_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_18_5, Market.FT_CORNERS_UNDER_18_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_19_5, Market.FT_CORNERS_UNDER_19_5),
    SimpleSurebetFinder(Market.FT_CORNERS_OVER_20_5, Market.FT_CORNERS_UNDER_20_5),

    HAComplexSurebetFinder(
        main_market=Market.FT_WIN_X, simple_market=Market.FT_WIN_1, assian_market=Market.FT_WINA_2_P00),
    HAComplexSurebetFinder(
        main_market=Market.FT_WIN_X, simple_market=Market.FT_WIN_2, assian_market=Market.FT_WINA_1_P00),
    HAComplexSurebetFinder(
        main_market=Market.FH_WIN_X, simple_market=Market.FH_WIN_1, assian_market=Market.FH_WINA_2_P00),
    HAComplexSurebetFinder(
        main_market=Market.FH_WIN_X, simple_market=Market.FH_WIN_2, assian_market=Market.FH_WINA_1_P00),

    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_P05, main_asian_market=Market.FT_WINA_1_P00, asian_market=Market.FT_WINA_2_P00),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_P15, main_asian_market=Market.FT_WINA_1_P10, asian_market=Market.FT_WINA_2_M10),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_P25, main_asian_market=Market.FT_WINA_1_P20, asian_market=Market.FT_WINA_2_M20),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_P35, main_asian_market=Market.FT_WINA_1_P30, asian_market=Market.FT_WINA_2_M30),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_P05, main_asian_market=Market.FT_WINA_2_P00, asian_market=Market.FT_WINA_1_P00),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_P15, main_asian_market=Market.FT_WINA_2_P10, asian_market=Market.FT_WINA_1_M10),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_P25, main_asian_market=Market.FT_WINA_2_P20, asian_market=Market.FT_WINA_1_M20),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_P35, main_asian_market=Market.FT_WINA_2_P30, asian_market=Market.FT_WINA_1_M30),

    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_M05, main_asian_market=Market.FT_WINA_1_M10, asian_market=Market.FT_WINA_2_P10),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_M15, main_asian_market=Market.FT_WINA_1_M20, asian_market=Market.FT_WINA_2_P20),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_M25, main_asian_market=Market.FT_WINA_1_M30, asian_market=Market.FT_WINA_2_P30),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_M05, main_asian_market=Market.FT_WINA_2_M10, asian_market=Market.FT_WINA_1_P10),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_M15, main_asian_market=Market.FT_WINA_2_M20, asian_market=Market.FT_WINA_1_P20),
    DoubleHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_M25, main_asian_market=Market.FT_WINA_2_M30, asian_market=Market.FT_WINA_1_P30),

    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_P025, main_asian_market=Market.FT_WINA_1_P00, asian_market=Market.FT_WINA_2_P00),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_P125, main_asian_market=Market.FT_WINA_1_P10, asian_market=Market.FT_WINA_2_M10),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_P225, main_asian_market=Market.FT_WINA_1_P20, asian_market=Market.FT_WINA_2_M20),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_P325, main_asian_market=Market.FT_WINA_1_P30, asian_market=Market.FT_WINA_2_M30),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_P025, main_asian_market=Market.FT_WINA_2_P00, asian_market=Market.FT_WINA_1_P00),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_P125, main_asian_market=Market.FT_WINA_2_P10, asian_market=Market.FT_WINA_1_M10),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_P225, main_asian_market=Market.FT_WINA_2_P20, asian_market=Market.FT_WINA_1_M20),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_P325, main_asian_market=Market.FT_WINA_2_P30, asian_market=Market.FT_WINA_1_M30),

    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_M075, main_asian_market=Market.FT_WINA_1_M10, asian_market=Market.FT_WINA_2_P10),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_M175, main_asian_market=Market.FT_WINA_1_M20, asian_market=Market.FT_WINA_2_P20),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_1_M275, main_asian_market=Market.FT_WINA_1_M30, asian_market=Market.FT_WINA_2_P30),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_M075, main_asian_market=Market.FT_WINA_2_M10, asian_market=Market.FT_WINA_1_P10),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_M175, main_asian_market=Market.FT_WINA_2_M20, asian_market=Market.FT_WINA_1_P20),
    DoubleHalfHAComplexSurebetFinder(
        main_market=Market.FT_WINA_2_M275, main_asian_market=Market.FT_WINA_2_M30, asian_market=Market.FT_WINA_1_P30),

    DoubleHAXComplexSurebetFinder(
        european_market=Market.FT_WIN_X, asian_market1=Market.FT_WINA_2_P00, asian_market2=Market.FT_WINA_1_P00),
    DoubleHAXComplexSurebetFinder(
        european_market=Market.FT_DRAW_1_P10, asian_market1=Market.FT_WINA_2_M10, asian_market2=Market.FT_WINA_1_P10),
    DoubleHAXComplexSurebetFinder(
        european_market=Market.FT_DRAW_1_P20, asian_market1=Market.FT_WINA_2_M20, asian_market2=Market.FT_WINA_1_P20),
    DoubleHAXComplexSurebetFinder(
        european_market=Market.FT_DRAW_1_P30, asian_market1=Market.FT_WINA_2_M30, asian_market2=Market.FT_WINA_1_P30),
    DoubleHAXComplexSurebetFinder(
        european_market=Market.FT_DRAW_1_M10, asian_market1=Market.FT_WINA_2_P10, asian_market2=Market.FT_WINA_1_M10),
    DoubleHAXComplexSurebetFinder(
        european_market=Market.FT_DRAW_1_M20, asian_market1=Market.FT_WINA_2_P20, asian_market2=Market.FT_WINA_1_M20),
    DoubleHAXComplexSurebetFinder(
        european_market=Market.FT_DRAW_1_M30, asian_market1=Market.FT_WINA_2_P30, asian_market2=Market.FT_WINA_1_M30),
]
