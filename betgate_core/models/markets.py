from decimal import Decimal


def is_valid_odd(value):
    try:
        Decimal(value).quantize(Decimal('1.000'))
        return True
    except:
        return False
