from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import GreeceSuperLeagueMixin


class SuperLeague(GreeceSuperLeagueMixin, FootballWebBase):
    url_path_params = {"country": "Greece", "category": "Super+League"}
    category_id = 46355
    category_name = "Greece.Super League"
