from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import ScotlandPremiershipMixin


class Premiership(ScotlandPremiershipMixin, FootballWebBase):
    url_path_params = {"country": "Scotland", "category": "Premiership"}
    category_id = "22435"
    category_name = "Scotland.Premiership"
