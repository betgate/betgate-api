from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import SpainPrimeraDivisionMixin


class PrimeraDivision(SpainPrimeraDivisionMixin, FootballWebBase):
    url_path_params = {"country": "Spain", "category": "Primera+Division"}
    category_id = "8736"
    category_name = "Spain.Primera Division"
