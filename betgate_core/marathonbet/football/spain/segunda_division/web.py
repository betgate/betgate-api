from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import SpainSegundaDivisionMixin


class SegundaDivision(SpainSegundaDivisionMixin, FootballWebBase):
    url_path_params = {"country": "Spain", "category": "Segunda+Division"}
    category_id = "48300"
    category_name = "Spain.Segunda Division"
