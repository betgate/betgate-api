from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import GermanyBundesliga2Mixin


class Bundesliga2(GermanyBundesliga2Mixin, FootballWebBase):
    url_path_params = {"country": "Germany", "category": "Bundesliga+2"}
    category_id = "42528"
    category_name = "Germany.Bundesliga 2"
