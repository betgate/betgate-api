from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import GermanyBundesligaMixin


class Bundesliga(GermanyBundesligaMixin, FootballWebBase):
    url_path_params = {"country": "Germany", "category": "Bundesliga"}
    category_id = "22436"
    category_name = "Germany.Bundesliga"
