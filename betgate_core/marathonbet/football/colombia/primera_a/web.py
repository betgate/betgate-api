from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import ColombiaPrimeraAMixin


class PrimeraA(ColombiaPrimeraAMixin, FootballWebBase):
    url_path_params = {"country": "Colombia", "category": "Primera+A"}
    category_id = "312055"
    category_name = "Colombia.Primera A"
