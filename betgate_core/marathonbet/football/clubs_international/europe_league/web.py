from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import ClubsInternationalEuropeLeagueMixin


class EuropeLeague(ClubsInternationalEuropeLeagueMixin, FootballWebBase):
    url_path_params = {"country": "Clubs.+International", "category": "UEFA+Europa+League"}
    category_id = "127778"
    category_name = ""
