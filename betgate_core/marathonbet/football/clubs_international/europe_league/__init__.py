from betgate_core.marathonbet.football.clubs_international.europe_league.web import EuropeLeague


def get_events(**kwargs):
    with EuropeLeague() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with EuropeLeague() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with EuropeLeague() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
