from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import ClubsInternationalChampionsLeagueMixin


class ChampionsLeague(ClubsInternationalChampionsLeagueMixin, FootballWebBase):
    url_path_params = {"country": "Clubs.+International", "category": "UEFA+Champions+League"}
    category_id = "120845"
    category_name = "UEFA Champions League.Group Stage"
