from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import SwedenSuperettanMixin


class Superettan(SwedenSuperettanMixin, FootballWebBase):
    url_path_params = {"country": "Sweden", "category": "Superettan"}
    category_id = "184441"
    category_name = "Sweden.Superettan"
