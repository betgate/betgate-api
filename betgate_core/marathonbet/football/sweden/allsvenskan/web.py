from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import SwedenAllsvenskanMixin


class Allsvenskan(SwedenAllsvenskanMixin, FootballWebBase):
    url_path_params = {"country": "Sweden", "category": "Allsvenskan"}
    category_id = "22728"
    category_name = "Sweden.Allsvenskan"
