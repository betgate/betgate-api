from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import SwitzerlandSuperLeagueMixin


class SuperLeague(SwitzerlandSuperLeagueMixin, FootballWebBase):
    url_path_params = {"country": "Switzerland", "category": "Super+League"}
    category_id = "46331"
    category_name = "Switzerland.Super League"
