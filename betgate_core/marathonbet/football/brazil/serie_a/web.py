from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import BrazilSerieAMixin


class SerieA(BrazilSerieAMixin, FootballWebBase):
    url_path_params = {"country": "Brazil", "category": "Serie+A"}
    category_id = 211229
    category_name = "Brazil.Serie A"
