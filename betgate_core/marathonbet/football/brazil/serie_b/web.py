from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import BrazilSerieBMixin


class SerieB(BrazilSerieBMixin, FootballWebBase):
    url_path_params = {"country": "Brazil", "category": "Serie+B"}
    category_id = 386546
    category_name = "Brazil.Serie B"
