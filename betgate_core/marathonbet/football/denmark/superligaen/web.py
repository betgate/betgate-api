from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import DenmarkSuperligaenMixin


class Superligaen(DenmarkSuperligaenMixin, FootballWebBase):
    url_path_params = {"country": "Denmark", "category": "Superliga"}
    category_id = -1
    category_name = "Denmark.Superliga"
