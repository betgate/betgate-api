from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import EcuadorSerieAMixin


class SerieA(EcuadorSerieAMixin, FootballWebBase):
    url_path_params = {"country": "Ecuador", "category": "Serie+A"}
    category_id = 341061
    category_name = "Ecuador.Serie A"
