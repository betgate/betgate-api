from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandLeagueOneMixin


class LeagueOne(EnglandLeagueOneMixin, FootballWebBase):
    url_path_params = {"country": "England", "category": "League+1"}
    category_id = "22808"
    category_name = "England.League 1"
