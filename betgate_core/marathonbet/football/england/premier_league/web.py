from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandPremierLeagueMixin


class PremierLeague(EnglandPremierLeagueMixin, FootballWebBase):
    url_path_params = {"country": "England", "category": "Premier+League"}
    category_id = "21520"
    category_name = "England.Premier League"
