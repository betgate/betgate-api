from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandNationalLeagueMixin


class NationalLeague(EnglandNationalLeagueMixin, FootballWebBase):
    url_path_params = {"country": "England", "category": "National+League"}
    category_id = "3310256"
    category_name = "England.National League"
