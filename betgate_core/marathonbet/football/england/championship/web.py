from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandChampionshipMixin


class Championship(EnglandChampionshipMixin, FootballWebBase):
    url_path_params = {"country": "England", "category": "Championship"}
    category_id = "22807"
    category_name = "England.Championship"
