from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandLeagueTwoMixin


class LeagueTwo(EnglandLeagueTwoMixin, FootballWebBase):
    url_path_params = {"country": "England", "category": "League+2"}
    category_id = "22809"
    category_name = "England.League 2"
