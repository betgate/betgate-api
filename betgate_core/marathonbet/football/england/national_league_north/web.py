from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandNationalLeagueNorthMixin


class NationalLeagueNorth(EnglandNationalLeagueNorthMixin, FootballWebBase):
    url_path_params = {"country": "England", "category": "National+League+North"}
    category_id = "3310257"
    category_name = "England.National League North"
