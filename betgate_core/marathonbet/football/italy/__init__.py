from betgate_core.marathonbet.football.italy import serie_a, serie_b


def get_events(**kwargs):
    return serie_a.get_events() + serie_b.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return serie_a.lazy_get_events() + serie_b.lazy_get_events(**kwargs)
