from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import ItalySerieAMixin


class SerieA(ItalySerieAMixin, FootballWebBase):
    url_path_params = {"country": "Italy", "category": "Serie+A"}
    category_id = "22434"
    category_name = "Italy.Serie A"
