from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import ItalySerieBMixin


class SerieB(ItalySerieBMixin, FootballWebBase):
    url_path_params = {"country": "Italy", "category": "Serie+B"}
    category_id = "46723"
    category_name = "Italy.Serie B"
