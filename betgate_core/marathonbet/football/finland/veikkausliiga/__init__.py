from betgate_core.marathonbet.football.finland.veikkausliiga.web import Veikkausliiga


def get_events(**kwargs):
    with Veikkausliiga() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Veikkausliiga() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Veikkausliiga() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
