from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import FinlandVeikkausliigaMixin


class Veikkausliiga(FinlandVeikkausliigaMixin, FootballWebBase):
    url_path_params = {"country": "Finland", "category": "Veikkausliiga"}
    category_id = 381809
    category_name = "Finland.Veikkausliiga"
