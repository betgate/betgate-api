from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import PolandEkstraklasaMixin


class Ekstraklasa(PolandEkstraklasaMixin, FootballWebBase):
    url_path_params = {"country": "Poland", "category": "Ekstraklasa"}
    category_id = "47116"
    category_name = "Poland.Ekstraklasa"
