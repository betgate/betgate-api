from betgate_core.marathonbet.football.poland import ekstraklasa


def get_events(**kwargs):
    return ekstraklasa.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return ekstraklasa.lazy_get_events(**kwargs)
