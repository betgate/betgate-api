from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import UsaMlsMixin


class Mls(UsaMlsMixin, FootballWebBase):
    url_path_params = {"country": "USA", "category": "MLS"}
    category_id = "138152"
    category_name = "USA.MLS"
