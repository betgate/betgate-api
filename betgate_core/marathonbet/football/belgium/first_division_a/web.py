from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import BelgiumFirstDivisionAMixin


class FirstDivisionA(BelgiumFirstDivisionAMixin, FootballWebBase):
    url_path_params = {"country": "Belgium", "category": "1st+Division+A"}
    category_id = "46181"
    category_name = "Belgium.1st Division A"
