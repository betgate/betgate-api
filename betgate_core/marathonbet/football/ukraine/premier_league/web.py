from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import UkrainePremierLeagueMixin


class PremierLeague(UkrainePremierLeagueMixin, FootballWebBase):
    url_path_params = {"country": "Ukraine", "category": "Premier+League"}
    category_id = 46183
    category_name = "Ukraine.Premier League"
