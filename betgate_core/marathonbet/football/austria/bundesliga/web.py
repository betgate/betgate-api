from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import AustriaBundesligaMixin


class Bundesliga(AustriaBundesligaMixin, FootballWebBase):
    url_path_params = {"country": "Austria", "category": "Bundesliga"}
    category_id = -1
    category_name = "Austria.Bundesliga"
