from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import TurkeySuperLeagueMixin


class SuperLeague(TurkeySuperLeagueMixin, FootballWebBase):
    url_path_params = {"country": "Turkey", "category": "Super+Lig"}
    category_id = "46180"
    category_name = "Turkey.Super Lig"
