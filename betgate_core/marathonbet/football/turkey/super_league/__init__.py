from betgate_core.marathonbet.football.turkey.super_league.web import SuperLeague


def get_events(**kwargs):
    with SuperLeague() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with SuperLeague() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with SuperLeague() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
