from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import NetherlandsEersteDivisieMixin


class EersteDivisie(NetherlandsEersteDivisieMixin, FootballWebBase):
    url_path_params = {"country": "Netherlands", "category": "Eerste+Divisie"}
    category_id = "345004"
    category_name = "Netherlands.Eerste Divisie"
