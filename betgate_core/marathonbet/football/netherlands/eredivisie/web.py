from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import NetherlandsEredivisieMixin


class Eredivisie(NetherlandsEredivisieMixin, FootballWebBase):
    url_path_params = {"country": "Netherlands", "category": "Eredivisie"}
    category_id = "38090"
    category_name = "Netherlands.Eredivisie"
