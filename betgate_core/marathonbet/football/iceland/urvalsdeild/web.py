from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import IcelandUrvalsdeildMixin


class Urvalsdeild(IcelandUrvalsdeildMixin, FootballWebBase):
    url_path_params = {"country": "Iceland", "category": "Premier+League"}
    category_id = 239653
    category_name = "Iceland.Premier League"
