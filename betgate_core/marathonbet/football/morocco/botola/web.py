from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import MoroccoBotolaMixin


class Botola(MoroccoBotolaMixin, FootballWebBase):
    url_path_params = {"country": "Morocco", "category": "Botola/1st+Division"}
    category_id = 505494
    category_name = "Morocco. Botola. 1st Division"
