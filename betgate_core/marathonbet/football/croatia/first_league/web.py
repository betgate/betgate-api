from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import CroatiaFirstLeagueMixin


class FirstLeague(CroatiaFirstLeagueMixin, FootballWebBase):
    url_path_params = {"country": "Croatia", "category": "1st+League"}
    category_id = "353647"
    category_name = "Croatia.1st League"
