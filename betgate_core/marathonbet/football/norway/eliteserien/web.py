from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import NorwayEliteserienMixin


class Eliteserien(NorwayEliteserienMixin, FootballWebBase):
    url_path_params = {"country": "Norway", "category": "Eliteserien"}
    category_id = "22827"
    category_name = "Norway.Eliteserien"
