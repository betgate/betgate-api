from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import ChilePrimeraDivisionMixin


class PrimeraDivision(ChilePrimeraDivisionMixin, FootballWebBase):
    url_path_params = {"country": "Chile", "category": "Primera+Division"}
    category_id = "341825"
    category_name = "Chile.Primera Division"
