from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import JapanJLeague1Mixin


class JLeague1(JapanJLeague1Mixin, FootballWebBase):
    url_path_params = {"country": "Japan", "category": "J.League/Division+1"}
    category_id = 441958
    category_name = "Japan.J.League.Division 1"
