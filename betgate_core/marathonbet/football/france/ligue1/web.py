from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import FranceLigue1Mixin


class Ligue1(FranceLigue1Mixin, FootballWebBase):
    url_path_params = {"country": "France", "category": "Ligue+1"}
    category_id = "21533"
    category_name = "France.Ligue 1"
