from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import FranceLigue2Mixin


class Ligue2(FranceLigue2Mixin, FootballWebBase):
    url_path_params = {"country": "France", "category": "Ligue+2"}
    category_id = "46785"
    category_name = "France.Ligue 2"
