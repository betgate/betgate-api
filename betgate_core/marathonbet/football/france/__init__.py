from betgate_core.marathonbet.football.france import ligue1, ligue2


def get_events(**kwargs):
    return ligue1.get_events(**kwargs) + ligue2.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return ligue1.lazy_get_events(**kwargs) + ligue2.lazy_get_events(**kwargs)
