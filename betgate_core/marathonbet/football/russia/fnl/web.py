from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import RussiaFnlMixin


class Fnl(RussiaFnlMixin, FootballWebBase):
    url_path_params = {"country": "Russia", "category": "FNL"}
    category_id = 45766
    category_name = "Russia.FNL"
