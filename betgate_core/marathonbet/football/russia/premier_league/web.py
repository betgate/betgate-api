from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import RussiaPremierLeagueMixin


class PremierLeague(RussiaPremierLeagueMixin, FootballWebBase):
    url_path_params = {"country": "Russia", "category": "Premier+League"}
    category_id = "22433"
    category_name = "Russia.Premier League"
