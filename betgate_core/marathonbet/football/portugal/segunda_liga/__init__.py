from betgate_core.marathonbet.football.portugal.segunda_liga.web import SegundaLiga


def get_events(**kwargs):
    with SegundaLiga() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with SegundaLiga() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with SegundaLiga() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
