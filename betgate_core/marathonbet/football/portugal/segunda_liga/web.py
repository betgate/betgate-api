from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import PortugalSegundaLigaMixin


class SegundaLiga(PortugalSegundaLigaMixin, FootballWebBase):
    url_path_params = {"country": "Portugal", "category": "Segunda+Liga"}
    category_id = "344256"
    category_name = "Portugal.Segunda Liga"
