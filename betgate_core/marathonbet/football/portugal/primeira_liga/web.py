from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import PortugalPrimeiraLigaMixin


class PrimeiraLiga(PortugalPrimeiraLigaMixin, FootballWebBase):
    url_path_params = {"country": "Portugal", "category": "Primeira+Liga"}
    category_id = "43058"
    category_name = "Portugal.Primeira Liga"
