import logging
from sdklib.shortcuts import cache
from betgate_models.models import Odd, Event, BookieId
from betgate_models.enums import Market, Category, Bookie
from betgate_core.marathonbet.web import MarathonBetWebBase


class FootballWebBase(MarathonBetWebBase):
    FOOTBALL_CATEGORY_URL = "/en/betting/Football/{country}/{category}/"
    FOOTBALL_CATEGORY_DETAIL_URL = "/en/betting/{event_id}"

    url_path_params = {"country": None, "category": None}
    category_id = None

    markets = {
        "Match_Result.1": Market.FT_WIN_1,
        "Match_Result.draw": Market.FT_WIN_X,
        "Match_Result.3": Market.FT_WIN_2,
        "Result.HD": Market.FT_WIN_1X,
        "Result.AD": Market.FT_WIN_X2,
        "Result.HA": Market.FT_WIN_12,
        "Result_-_1st_Half.RN_H": Market.FH_WIN_1,
        "Result_-_1st_Half.RN_D": Market.FH_WIN_X,
        "Result_-_1st_Half.RN_A": Market.FH_WIN_2,
        "Result_-_1st_Half0.HD": Market.FH_WIN_1X,
        "Result_-_1st_Half0.AD": Market.FH_WIN_X2,
        "Result_-_1st_Half0.HA": Market.FH_WIN_12,
        "Both_Teams_To_Score.yes": Market.FT_BTTS_YES,
        "Both_Teams_To_Score.no": Market.FT_BTTS_NO,
        "Correct_Score_(Dynamic_Type).0_0": Market.FT_RESULT_0_0,
        "Correct_Score_1st_Half_(Dynamic_Type).0_0": Market.FH_RESULT_0_0,
    }

    markets2 = {
        ("MG1_-569792649", "(0.5)", 2): Market.FT_TG_OVER_05,
        ("MG1_-569792649", "(0.5)", 1): Market.FT_TG_UNDER_05,
        ("MG1_-569792649", "(1.0)", 2): Market.FT_TGA_OVER_10,
        ("MG1_-569792649", "(1.0)", 1): Market.FT_TGA_UNDER_10,
        ("MG1_-569792649", "(1.5)", 2): Market.FT_TG_OVER_15,
        ("MG1_-569792649", "(1.5)", 1): Market.FT_TG_UNDER_15,
        ("MG1_-569792649", "(2.0)", 2): Market.FT_TGA_OVER_20,
        ("MG1_-569792649", "(2.0)", 1): Market.FT_TGA_UNDER_20,
        ("MG1_-569792649", "(2.5)", 2): Market.FT_TG_OVER_25,
        ("MG1_-569792649", "(2.5)", 1): Market.FT_TG_UNDER_25,
        ("MG1_-569792649", "(3.0)", 2): Market.FT_TGA_OVER_30,
        ("MG1_-569792649", "(3.0)", 1): Market.FT_TGA_UNDER_30,
        ("MG1_-569792649", "(3.5)", 2): Market.FT_TG_OVER_35,
        ("MG1_-569792649", "(3.5)", 1): Market.FT_TG_UNDER_35,
        ("MG1_-569792649", "(4.0)", 2): Market.FT_TGA_OVER_40,
        ("MG1_-569792649", "(4.0)", 1): Market.FT_TGA_UNDER_40,
        ("MG1_-569792649", "(4.5)", 2): Market.FT_TG_OVER_45,
        ("MG1_-569792649", "(4.5)", 1): Market.FT_TG_UNDER_45,
        ("MG1_-569792649", "(5.0)", 2): Market.FT_TGA_OVER_50,
        ("MG1_-569792649", "(5.0)", 1): Market.FT_TGA_UNDER_50,
        ("MG1_-569792649", "(5.5)", 2): Market.FT_TG_OVER_55,
        ("MG1_-569792649", "(5.5)", 1): Market.FT_TG_UNDER_55,
        ("MG242_1001580216", "(0.5,1.0)", 2): Market.FT_TGA_OVER_075,
        ("MG242_1001580216", "(0.5,1.0)", 1): Market.FT_TGA_UNDER_075,
        ("MG242_1001580216", "(1.0,1.5)", 2): Market.FT_TGA_OVER_125,
        ("MG242_1001580216", "(1.0,1.5)", 1): Market.FT_TGA_UNDER_125,
        ("MG242_1001580216", "(1.5,2.0)", 2): Market.FT_TGA_OVER_175,
        ("MG242_1001580216", "(1.5,2.0)", 1): Market.FT_TGA_UNDER_175,
        ("MG242_1001580216", "(2.0,2.5)", 2): Market.FT_TGA_OVER_225,
        ("MG242_1001580216", "(2.0,2.5)", 1): Market.FT_TGA_UNDER_225,
        ("MG242_1001580216", "(2.5,3.0)", 2): Market.FT_TGA_OVER_275,
        ("MG242_1001580216", "(2.5,3.0)", 1): Market.FT_TGA_UNDER_275,
        ("MG242_1001580216", "(3.0,3.5)", 2): Market.FT_TGA_OVER_325,
        ("MG242_1001580216", "(3.0,3.5)", 1): Market.FT_TGA_UNDER_325,
        ("MG242_1001580216", "(3.5,4.0)", 2): Market.FT_TGA_OVER_375,
        ("MG242_1001580216", "(3.5,4.0)", 1): Market.FT_TGA_UNDER_375,
        ("MG242_1001580216", "(4.0,4.5)", 2): Market.FT_TGA_OVER_425,
        ("MG242_1001580216", "(4.0,4.5)", 1): Market.FT_TGA_UNDER_425,
        ("MG242_1001580216", "(4.5,5.0)", 2): Market.FT_TGA_OVER_475,
        ("MG242_1001580216", "(4.5,5.0)", 1): Market.FT_TGA_UNDER_475,
        ("MG242_1001580216", "(5.0,5.5)", 2): Market.FT_TGA_OVER_525,
        ("MG242_1001580216", "(5.0,5.5)", 1): Market.FT_TGA_UNDER_525,
        ("MG400_-1489313743", "(0.5)", 2): Market.FH_TG_OVER_05,
        ("MG400_-1489313743", "(0.5)", 1): Market.FH_TG_UNDER_05,
        ("MG400_-1489313743", "(1.0)", 2): Market.FH_TGA_OVER_10,
        ("MG400_-1489313743", "(1.0)", 1): Market.FH_TGA_UNDER_10,
        ("MG400_-1489313743", "(1.5)", 2): Market.FH_TG_OVER_15,
        ("MG400_-1489313743", "(1.5)", 1): Market.FH_TG_UNDER_15,
        ("MG400_-1489313743", "(2.0)", 2): Market.FH_TGA_OVER_20,
        ("MG400_-1489313743", "(2.0)", 1): Market.FH_TGA_UNDER_20,
        ("MG400_-1489313743", "(2.5)", 2): Market.FH_TG_OVER_25,
        ("MG400_-1489313743", "(2.5)", 1): Market.FH_TG_UNDER_25,
        ("MG400_-1489313743", "(3.0)", 2): Market.FH_TGA_OVER_30,
        ("MG400_-1489313743", "(3.0)", 1): Market.FH_TGA_UNDER_30,
        ("MG400_-1489313743", "(3.5)", 2): Market.FH_TG_OVER_35,
        ("MG400_-1489313743", "(3.5)", 1): Market.FH_TG_UNDER_35,
        ("MG400_-1489313743", "(4.0)", 2): Market.FH_TGA_OVER_40,
        ("MG400_-1489313743", "(4.0)", 1): Market.FH_TGA_UNDER_40,
        ("MG400_-1489313743", "(4.5)", 2): Market.FH_TG_OVER_45,
        ("MG400_-1489313743", "(4.5)", 1): Market.FH_TG_UNDER_45,
        ("MG234_4628131", "(0)", 1): Market.FT_WINA_1_P00,
        ("MG234_4628131", "(0)", 2): Market.FT_WINA_2_P00,
        ("MG234_4628131", "(+0.5)", 1): Market.FT_WINA_1_P05,
        ("MG234_4628131", "(-0.5)", 2): Market.FT_WINA_2_M05,
        ("MG234_4628131", "(+1.0)", 1): Market.FT_WINA_1_P10,
        ("MG234_4628131", "(-1.0)", 2): Market.FT_WINA_2_M10,
        ("MG234_4628131", "(+1.5)", 1): Market.FT_WINA_1_P15,
        ("MG234_4628131", "(-1.5)", 2): Market.FT_WINA_2_M15,
        ("MG234_4628131", "(+2.0)", 1): Market.FT_WINA_1_P20,
        ("MG234_4628131", "(-2.0)", 2): Market.FT_WINA_2_M20,
        ("MG234_4628131", "(+2.5)", 1): Market.FT_WINA_1_P25,
        ("MG234_4628131", "(-2.5)", 2): Market.FT_WINA_2_M25,
        ("MG234_4628131", "(+3.0)", 1): Market.FT_WINA_1_P30,
        ("MG234_4628131", "(-3.0)", 2): Market.FT_WINA_2_M30,
        ("MG234_4628131", "(+3.5)", 1): Market.FT_WINA_1_P35,
        ("MG234_4628131", "(-3.5)", 2): Market.FT_WINA_2_M35,
        ("MG234_4628131", "(-0.5)", 1): Market.FT_WINA_1_M05,
        ("MG234_4628131", "(+0.5)", 2): Market.FT_WINA_2_P05,
        ("MG234_4628131", "(-1.0)", 1): Market.FT_WINA_1_M10,
        ("MG234_4628131", "(+1.0)", 2): Market.FT_WINA_2_P10,
        ("MG234_4628131", "(-1.5)", 1): Market.FT_WINA_1_M15,
        ("MG234_4628131", "(+1.5)", 2): Market.FT_WINA_2_P15,
        ("MG234_4628131", "(-2.0)", 1): Market.FT_WINA_1_M20,
        ("MG234_4628131", "(+2.0)", 2): Market.FT_WINA_2_P20,
        ("MG234_4628131", "(-2.5)", 1): Market.FT_WINA_1_M25,
        ("MG234_4628131", "(+2.5)", 2): Market.FT_WINA_2_P25,
        ("MG234_4628131", "(-3.0)", 1): Market.FT_WINA_1_M30,
        ("MG234_4628131", "(+3.0)", 2): Market.FT_WINA_2_P30,
        ("MG234_4628131", "(-3.5)", 1): Market.FT_WINA_1_M35,
        ("MG234_4628131", "(+3.5)", 2): Market.FT_WINA_2_P35,
        ("MG238_1833581717", "(+0.5)", 1): Market.FH_WINA_1_P05,
        ("MG238_1833581717", "(-0.5)", 2): Market.FH_WINA_2_M05,
        ("MG238_1833581717", "(+1.0)", 1): Market.FH_WINA_1_P10,
        ("MG238_1833581717", "(-1.0)", 2): Market.FH_WINA_2_M10,
        ("MG238_1833581717", "(+1.5)", 1): Market.FH_WINA_1_P15,
        ("MG238_1833581717", "(-1.5)", 2): Market.FH_WINA_2_M15,
        ("MG238_1833581717", "(+2.0)", 1): Market.FH_WINA_1_P20,
        ("MG238_1833581717", "(-2.0)", 2): Market.FH_WINA_2_M20,
        ("MG238_1833581717", "(+2.5)", 1): Market.FH_WINA_1_P25,
        ("MG238_1833581717", "(-2.5)", 2): Market.FH_WINA_2_M25,
        ("MG238_1833581717", "(+3.0)", 1): Market.FH_WINA_1_P30,
        ("MG238_1833581717", "(-3.0)", 2): Market.FH_WINA_2_M30,
        ("MG238_1833581717", "(+3.5)", 1): Market.FH_WINA_1_P35,
        ("MG238_1833581717", "(-3.5)", 2): Market.FH_WINA_2_M35,
        ("MG238_1833581717", "(-0.5)", 1): Market.FH_WINA_1_M05,
        ("MG238_1833581717", "(+0.5)", 2): Market.FH_WINA_2_P05,
        ("MG238_1833581717", "(-1.0)", 1): Market.FH_WINA_1_M10,
        ("MG238_1833581717", "(+1.0)", 2): Market.FH_WINA_2_P10,
        ("MG238_1833581717", "(-1.5)", 1): Market.FH_WINA_1_M15,
        ("MG238_1833581717", "(+1.5)", 2): Market.FH_WINA_2_P15,
        ("MG238_1833581717", "(-2.0)", 1): Market.FH_WINA_1_M20,
        ("MG238_1833581717", "(+2.0)", 2): Market.FH_WINA_2_P20,
        ("MG238_1833581717", "(-2.5)", 1): Market.FH_WINA_1_M25,
        ("MG238_1833581717", "(+2.5)", 2): Market.FH_WINA_2_P25,
        ("MG238_1833581717", "(-3.0)", 1): Market.FH_WINA_1_M30,
        ("MG238_1833581717", "(+3.0)", 2): Market.FH_WINA_2_P30,
        ("MG238_1833581717", "(-3.5)", 1): Market.FH_WINA_1_M35,
        ("MG238_1833581717", "(+3.5)", 2): Market.FH_WINA_2_P35,
        ("MG256_-794781779", "(0.5)", 1): Market.FT_CORNERS_UNDER_0_5,
        ("MG256_-794781779", "(0.5)", 2): Market.FT_CORNERS_OVER_0_5,
        ("MG256_-794781779", "(1.5)", 1): Market.FT_CORNERS_UNDER_1_5,
        ("MG256_-794781779", "(1.5)", 2): Market.FT_CORNERS_OVER_1_5,
        ("MG256_-794781779", "(2.5)", 1): Market.FT_CORNERS_UNDER_2_5,
        ("MG256_-794781779", "(2.5)", 2): Market.FT_CORNERS_OVER_2_5,
        ("MG256_-794781779", "(3.5)", 1): Market.FT_CORNERS_UNDER_3_5,
        ("MG256_-794781779", "(3.5)", 2): Market.FT_CORNERS_OVER_3_5,
        ("MG256_-794781779", "(4.5)", 1): Market.FT_CORNERS_UNDER_4_5,
        ("MG256_-794781779", "(4.5)", 2): Market.FT_CORNERS_OVER_4_5,
        ("MG256_-794781779", "(5.5)", 1): Market.FT_CORNERS_UNDER_5_5,
        ("MG256_-794781779", "(5.5)", 2): Market.FT_CORNERS_OVER_5_5,
        ("MG256_-794781779", "(6.5)", 1): Market.FT_CORNERS_UNDER_6_5,
        ("MG256_-794781779", "(6.5)", 2): Market.FT_CORNERS_OVER_6_5,
        ("MG256_-794781779", "(7.5)", 1): Market.FT_CORNERS_UNDER_7_5,
        ("MG256_-794781779", "(7.5)", 2): Market.FT_CORNERS_OVER_7_5,
        ("MG256_-794781779", "(8.5)", 1): Market.FT_CORNERS_UNDER_8_5,
        ("MG256_-794781779", "(8.5)", 2): Market.FT_CORNERS_OVER_8_5,
        ("MG256_-794781779", "(9.5)", 1): Market.FT_CORNERS_UNDER_9_5,
        ("MG256_-794781779", "(9.5)", 2): Market.FT_CORNERS_OVER_9_5,
        ("MG256_-794781779", "(10.5)", 1): Market.FT_CORNERS_UNDER_10_5,
        ("MG256_-794781779", "(10.5)", 2): Market.FT_CORNERS_OVER_10_5,
        ("MG256_-794781779", "(11.5)", 1): Market.FT_CORNERS_UNDER_11_5,
        ("MG256_-794781779", "(11.5)", 2): Market.FT_CORNERS_OVER_11_5,
        ("MG256_-794781779", "(12.5)", 1): Market.FT_CORNERS_UNDER_12_5,
        ("MG256_-794781779", "(12.5)", 2): Market.FT_CORNERS_OVER_12_5,
        ("MG256_-794781779", "(13.5)", 1): Market.FT_CORNERS_UNDER_13_5,
        ("MG256_-794781779", "(13.5)", 2): Market.FT_CORNERS_OVER_13_5,
        ("MG256_-794781779", "(14.5)", 1): Market.FT_CORNERS_UNDER_14_5,
        ("MG256_-794781779", "(14.5)", 2): Market.FT_CORNERS_OVER_14_5,
        ("MG256_-794781779", "(15.5)", 1): Market.FT_CORNERS_UNDER_15_5,
        ("MG256_-794781779", "(15.5)", 2): Market.FT_CORNERS_OVER_15_5,
        ("MG256_-794781779", "(16.5)", 1): Market.FT_CORNERS_UNDER_16_5,
        ("MG256_-794781779", "(16.5)", 2): Market.FT_CORNERS_OVER_16_5,
        ("MG256_-794781779", "(17.5)", 1): Market.FT_CORNERS_UNDER_17_5,
        ("MG256_-794781779", "(17.5)", 2): Market.FT_CORNERS_OVER_17_5,
        ("MG256_-794781779", "(18.5)", 1): Market.FT_CORNERS_UNDER_18_5,
        ("MG256_-794781779", "(18.5)", 2): Market.FT_CORNERS_OVER_18_5,
        ("MG256_-794781779", "(19.5)", 1): Market.FT_CORNERS_UNDER_19_5,
        ("MG256_-794781779", "(19.5)", 2): Market.FT_CORNERS_OVER_19_5,
        ("MG256_-794781779", "(20.5)", 1): Market.FT_CORNERS_UNDER_20_5,
        ("MG256_-794781779", "(20.5)", 2): Market.FT_CORNERS_OVER_20_5,
        ("MG256_-794781778", "(0.0)", 2): Market.FT_CORNERS_EXACT_0,
        ("MG256_-794781778", "(1.0)", 2): Market.FT_CORNERS_EXACT_1,
        ("MG256_-794781778", "(2.0)", 2): Market.FT_CORNERS_EXACT_2,
        ("MG256_-794781778", "(3.0)", 2): Market.FT_CORNERS_EXACT_3,
        ("MG256_-794781778", "(4.0)", 2): Market.FT_CORNERS_EXACT_4,
        ("MG256_-794781778", "(5.0)", 2): Market.FT_CORNERS_EXACT_5,
        ("MG256_-794781778", "(6.0)", 2): Market.FT_CORNERS_EXACT_6,
        ("MG256_-794781778", "(7.0)", 2): Market.FT_CORNERS_EXACT_7,
        ("MG256_-794781778", "(8.0)", 2): Market.FT_CORNERS_EXACT_8,
        ("MG256_-794781778", "(9.0)", 2): Market.FT_CORNERS_EXACT_9,
        ("MG256_-794781778", "(10.0)", 2): Market.FT_CORNERS_EXACT_10,
        ("MG256_-794781778", "(11.0)", 2): Market.FT_CORNERS_EXACT_11,
        ("MG256_-794781778", "(12.0)", 2): Market.FT_CORNERS_EXACT_12,
        ("MG256_-794781778", "(13.0)", 2): Market.FT_CORNERS_EXACT_13,
        ("MG256_-794781778", "(14.0)", 2): Market.FT_CORNERS_EXACT_14,
        ("MG256_-794781778", "(15.0)", 2): Market.FT_CORNERS_EXACT_15,
        ("MG256_-794781778", "(16.0)", 2): Market.FT_CORNERS_EXACT_16,
        ("MG256_-794781778", "(17.0)", 2): Market.FT_CORNERS_EXACT_17,
        ("MG256_-794781778", "(18.0)", 2): Market.FT_CORNERS_EXACT_18,
        ("MG256_-794781778", "(19.0)", 2): Market.FT_CORNERS_EXACT_19,
        ("MG256_-794781778", "(20.0)", 2): Market.FT_CORNERS_EXACT_20
    }

    def __init__(self):
        super(FootballWebBase, self).__init__()
        if self.url_path_params is None or "country" not in self.url_path_params or \
                "category" not in self.url_path_params:
            raise BaseException("You must define a 'url_path_params = {0}' attribute in your {1}.{2} class.".format(
                "{'country': '', 'category': ''}", type(self).__module__, type(self).__name__
            ))
        if self.category_id is None:
            raise BaseException("You must define a 'category_id' attribute in your {0}.{1} class.".format(
                type(self).__module__, type(self).__name__
            ))

    @cache(maxsize=None)
    def request_event_detail(self, event_id):
        return self.get(self.FOOTBALL_CATEGORY_DETAIL_URL.format(event_id=event_id))

    @cache(maxsize=None)
    def request_football_category(self):
        return self.get(self.FOOTBALL_CATEGORY_URL)

    def _lazy_get_event_detail(self, html_obj, event_id, validate=True):
        try:
            category = self.get_category()
            home_tname, away_tname = self.get_team_names(html_obj, event_id=event_id)
            dtime = self.get_datetime(html=html_obj, event_id=event_id)
            event = Event(
                bookies={str(Bookie.MARATHONBET.value): BookieId(bookie=Bookie.MARATHONBET, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                html_source=html_obj,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    def lazy_get_category_events(self, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            res = self.request_football_category()
            event_ids = list(set([elem.get("data-event-path") for elem in res.html.find_elements_by_xpath(
                "//*[@data-category-treeid='%s']//*[@data-event-treeid]" % self.category_id)]))
            category_events = []
            for eid in event_ids:
                event_detail = self._lazy_get_event_detail(res.html, eid, validate=validate)
                if event_detail is not None:
                    category_events.append(event_detail)
            return category_events
        except:
            logging.exception("Something was wrong catching category events")
            return []

    @classmethod
    def _get_market_from_event_detail(cls, html, market):
        elem = html.find_element_by_xpath("//*[contains(@data-selection-key, '%s')]" % market)
        if elem is not None:
            price = elem.get("data-selection-price")
            return Odd(value=price, market=cls.markets[market], bookie=Bookie.MARATHONBET)

    @classmethod
    def _get_markets2_from_event_detail(cls, html):
        markets_to_return = []
        for mutable_id, text, pos in cls.markets2:
            elem = html.find_element_by_xpath(
                "//*[@data-mutable-id='%s']//td[%d]//div[@class='coeff-handicap' and contains(.,'%s')]" %
                (mutable_id, pos, text))
            if elem is not None:
                parent = elem.getparent()
                odd = parent.find_element_by_xpath(".//*[@data-selection-price]").text
                market = cls.markets2[(mutable_id, text, pos)]
                markets_to_return.append(Odd(value=odd, market=market, bookie=Bookie.MARATHONBET))
        return markets_to_return

    def get_markets(self, event_id=None, html=None):
        if html is None and event_id is None:
            raise BaseException("Both 'html' and 'event_id' can't be equal to None")
        elif html is None and event_id is not None:
            html = self.request_event_detail(event_id).html

        markets = []
        for mkt in self.markets:
            market = self._get_market_from_event_detail(html, mkt)
            if market:
                markets.append(market)

        more_markets = self._get_markets2_from_event_detail(html)
        markets.extend(more_markets)
        return markets

    def get_event(self, event_id, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            res = self.request_event_detail(event_id)
            home_tname, away_tname = self.get_team_names(res.html, event_id=event_id)
            category = self.get_category()
            dtime = self.get_datetime(html=res.html, event_id=event_id)
            markets = self.get_markets(html=res.html)
            event = Event(
                bookies={str(Bookie.MARATHONBET.value): BookieId(bookie=Bookie.MARATHONBET, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                odds=markets,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    @classmethod
    def get_team_names(cls, data, event_id=None):
        if event_id is None:
            raise BaseException("'event_id' can't be None")

        eid = event_id.split("+")[-1]

        elem = data.find_element_by_xpath(f"//*[@data-event-treeid='{eid}']")
        tname1, tname2 = elem.get("data-event-name").split(" vs ")
        return tname1, tname2

    def get_category_identifier(self, event_id):
        res = self.request_event_detail(event_id)
        elems = res.html.find_elements_by_xpath(
            "//div[@class='sport-category-content']/div[1]//*[@class='category-label']/span"
        )
        categories = [cat.text for cat in elems]
        return "".join(categories)


class FootballWeb(FootballWebBase):
    category = Category.UNKNOWN
    url_path_params = {"country": None, "category": None}
    category_id = -1
