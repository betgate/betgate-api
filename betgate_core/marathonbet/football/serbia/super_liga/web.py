from betgate_core.marathonbet.football.web import FootballWebBase
from betgate_core.mixins.football import SerbiaSuperLigaMixin


class SuperLiga(SerbiaSuperLigaMixin, FootballWebBase):
    url_path_params = {"country": "Serbia", "category": "Super+Liga"}
    category_id = 305511
    category_name = "Serbia.Super Liga"
