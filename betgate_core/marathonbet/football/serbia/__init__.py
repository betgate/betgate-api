from betgate_core.marathonbet.football.serbia import super_liga


def get_events(**kwargs):
    return super_liga.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return super_liga.lazy_get_events(**kwargs)
