import time
from datetime import datetime
import pytz
from betgate_models.enums import Bookie
from betgate_core.web import BookieWebBase


class MarathonBetWebBase(BookieWebBase):

    DEFAULT_HOST = "https://www.marathonbet.es"

    incognito_mode = True

    category_name = None
    bookie = Bookie.MARATHONBET

    @property
    def category_identifier(self):
        return self.category_name

    def __init__(self):
        super().__init__()
        self.incognito_mode = True

    def get_datetime(self, event_id=None, html=None):
        if event_id is None or html is None:
            raise BaseException("Both args are required")

        eid = event_id.split("+")[-1]

        text = html.find_element_by_xpath("//div[@data-event-treeid='{0}']//td[@class='date']".format(eid)).text
        strip_text = text.replace("\\n", "")
        split_text = strip_text.split()
        if len(split_text) == 1:
            day = time.strftime("%d")
            month = time.strftime("%b")
            year = time.strftime("%Y")
            hour = split_text[0]
        elif len(split_text) == 3:
            day = split_text[0]
            month = split_text[1]
            year = time.strftime("%Y")
            hour = split_text[2]
        elif len(split_text) == 4:
            day = split_text[0]
            month = split_text[1]
            year = split_text[2]
            hour = split_text[3]
        else:
            return
        dtime = datetime.strptime("%s %s %s %s" % (day, month, year, hour), '%d %b %Y %H:%M')
        local_tz = pytz.timezone('Europe/London')
        return local_tz.localize(dtime, is_dst=None)
