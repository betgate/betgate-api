from sdklib.http import HttpSdk
from sdklib.http.headers import (
    PRAGMA_HEADER_NAME, CONNECTION_HEADER_NAME, ACCEPT_ENCODING_HEADER_NAME, USER_AGENT_HEADER_NAME
)
from betgate_core.utils.teams import get_team_from_name


class BookieBase(HttpSdk):

    # DEFAULT_PROXY = "http://localhost:8080"

    use_tor = False
    category = None
    bookie = None
    teams = []

    def __init__(self):
        super(BookieBase, self).__init__()
        if self.category is None:
            raise BaseException("You must define a 'category' attribute in your {0}.{1} class.".format(
                type(self).__module__, type(self).__name__
            ))

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        pass

    def default_headers(self):
        headers = super(BookieBase, self).default_headers()
        headers[PRAGMA_HEADER_NAME] = "no-cache"
        headers[CONNECTION_HEADER_NAME] = "close"
        headers[ACCEPT_ENCODING_HEADER_NAME] = "gzip, deflate, br"
        headers[USER_AGENT_HEADER_NAME] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML," \
                                          " like Gecko) Chrome/56.0.2924.87 Safari/537.36"
        return headers

    def _http_request(self, method, url_path, headers=None, query_params=None, body_params=None, files=None, **kwargs):
        proxy = kwargs.get('proxy', self.proxy) if self.proxy else None
        if proxy:
            kwargs['proxy'] = proxy
        return super()._http_request(
            method,
            url_path,
            headers=headers,
            query_params=query_params,
            body_params=body_params,
            files=files,
            **kwargs
        )

    def request_event_detail(self, event_id):
        """
        Retrieve event detail information from HTTP Server.

        :param event_id: id of desired event
        :type event_id: str or int
        :rtype: HttpResponse
        """
        raise NotImplementedError("You should implement this method.")

    def get_category_event_ids(self):
        """
        Deprecated!!!
        Get all event ids.

        :return: all event ids found in the given category
        :rtype: list of str
        """
        raise NotImplementedError("You should implement this method.")

    def lazy_get_category_events(self, **kwargs):
        """
        Get all event ids with detail.

        :return: all event ids found in the given category encapsulated in a Event model.
        """
        raise NotImplementedError("You should implement this method.")

    def get_category_events(self, **kwargs):
        """
        Get all events and markets.

        :return: all event found in the given category
        """
        category_events = []
        for event in self.lazy_get_category_events():
            event_detail = self.get_event(event.bookies[str(self.bookie.value)].value, **kwargs)
            if event_detail is not None:
                category_events.append(event_detail)
        return category_events

    @classmethod
    def get_team(cls, name):
        """
        Return team value from its name.

        :return: team value
        :rtype: enum (should be)
        """
        return get_team_from_name(name)

    def get_event(self, event_id, **kwargs):
        """
        Return Event info from its ID.

        :return: event information
        :rtype: Event
        """
        raise NotImplementedError("You should implement this method.")

    @classmethod
    def get_category(cls):
        return cls.category

    def get_category_identifier(self, event_id):
        raise NotImplementedError("You should implement this method.")
