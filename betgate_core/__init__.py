import importlib
from betgate_models.enums import Bookie, Category

ALL_BOOKIES = [
    str(Bookie.BETFAIR.value),
    str(Bookie.SPORT888.value),
    str(Bookie.LUCKIA.value),
    str(Bookie.MARATHONBET.value),
    str(Bookie.SPORTIUM.value)
]

BOOKIE_NAMES = {
    str(Bookie.BETFAIR.value): "betfair",
    str(Bookie.SPORT888.value): "kambi",
    str(Bookie.LUCKIA.value): "luckia",
    str(Bookie.MARATHONBET.value): "marathonbet",
    str(Bookie.RETABET.value): "retabet",
    str(Bookie.TEST.value): "test",
    str(Bookie.WILLIAM_HILL.value): "williamhill",
    str(Bookie.SPORTIUM.value): "sportium"
}


def _get_all_football_events(func_name, bookie=None, country=None, category=None, **kwargs):
    bookies = [str(bookie.value)] if bookie is not None else [bke for bke in ALL_BOOKIES]

    events_by_id = dict()
    for bkie in bookies:
        bookie_name = BOOKIE_NAMES.get(bkie, None)
        if bookie_name is None:
            raise BaseException("Bookie not found")

        bookie_module_name = "betgate_core." + bookie_name + ".football"
        if category is not None:
            bookie_module_name += "." + category.name.lower().replace("__", ".", 1)
        elif country is not None:
            bookie_module_name += "." + country.name.lower()

        mod = importlib.import_module(bookie_module_name)
        events = getattr(mod, func_name)(**kwargs)

        for evt in events:
            if evt.id not in events_by_id:
                events_by_id[evt.id] = evt
            else:
                events_by_id[evt.id].bookies.update(evt.bookies)
                events_by_id[evt.id].odds.extend(evt.odds)

    return list(events_by_id.values())


def get_all_football_events(bookie=None, country=None, category=None, validate=True):
    return _get_all_football_events("get_events", bookie=bookie, country=country, category=category, validate=validate)


def lazy_get_all_football_events(bookie=None, country=None, category=None, validate=True):
    return _get_all_football_events(
        "lazy_get_events", bookie=bookie, country=country, category=category, validate=validate
    )


def get_odds(event):
    all_odds = []
    for bookie, bookie_id in event.bookies.items():
        bookie_name = BOOKIE_NAMES.get(bookie, None)
        if bookie_name is None:
            raise BaseException("Bookie not found")

        bookie_module_name = "betgate_core." + bookie_name + ".football" + "." + \
                             Category(event.category).name.lower().replace("__", ".", 1)
        mod = importlib.import_module(bookie_module_name)
        all_odds.extend(getattr(mod, "get_odds")(bookie_id["value"]))

    return all_odds
