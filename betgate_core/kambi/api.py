from sdklib.shortcuts import cache
from betgate_models.enums import Bookie
from betgate_core.api import BookieApiBase


class KambiApiBase(BookieApiBase):

    DEFAULT_HOST = "https://eu-offering.kambicdn.org"

    BETOFFER_EVENT_API_URL = "/offering/api/v2/888es/betoffer/event/"

    url_path_format = "json"

    category_name = None
    bookie = Bookie.SPORT888

    @property
    def category_identifier(self):
        return self.category_name

    def default_headers(self):
        headers = super(KambiApiBase, self).default_headers()
        headers["Origin"] = "https://www.888sport.es"
        headers["Referer"] = "https://www.888sport.es/apuestas-online/"
        return headers

    @cache(maxsize=None)
    def request_event_detail(self, event_id):
        params = {
            "lang": "es_ES",
            "market": "ES",
            "client_id": "2",
            "channel_id": "1",
            "ncid": "1488631142765",
        }
        return self.get(self.BETOFFER_EVENT_API_URL + str(event_id), query_params=params)

    def get_category_identifier(self, event_id):
        res = self.request_event_detail(event_id)
        if "events" in res.json:
            group = res.json["events"][0]["group"]
            return group.strip()
