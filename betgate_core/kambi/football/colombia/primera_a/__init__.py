from betgate_core.kambi.football.colombia.primera_a.api import PrimeraA


def get_events(**kwargs):
    with PrimeraA() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with PrimeraA() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with PrimeraA() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
