from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import ColombiaPrimeraAMixin


class PrimeraA(ColombiaPrimeraAMixin, FootballApiBase):
    url_path_params = {"country": "colombia", "category_id": "liga_betplay_dimayor"}
    category_name = "Primera A"
