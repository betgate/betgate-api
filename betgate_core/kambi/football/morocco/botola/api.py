from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import MoroccoBotolaMixin


class Botola(MoroccoBotolaMixin, FootballApiBase):
    url_path_params = {"country": "morocco", "category_id": "botola"}
    category_name = "Botola"
