from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import NorwayEliteserienMixin


class Eliteserien(NorwayEliteserienMixin, FootballApiBase):
    url_path_params = {"country": "norway", "category_id": "eliteserien"}
    category_name = "Eliteserien"
