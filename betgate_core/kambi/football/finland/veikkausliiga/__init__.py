from betgate_core.kambi.football.finland.veikkausliiga.api import Veikkausliiga


def get_events(**kwargs):
    with Veikkausliiga() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Veikkausliiga() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Veikkausliiga() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
