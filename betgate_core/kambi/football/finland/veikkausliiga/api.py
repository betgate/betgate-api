from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import FinlandVeikkausliigaMixin


class Veikkausliiga(FinlandVeikkausliigaMixin, FootballApiBase):
    url_path_params = {"country": "finland", "category_id": "veikkausliiga"}
    category_name = "Veikkausliiga"
