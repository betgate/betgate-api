from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import RussiaPremierLeagueMixin


class PremierLeague(RussiaPremierLeagueMixin, FootballApiBase):
    url_path_params = {"country": "russia", "category_id": "premier_league"}
    category_name = "Premier Liga"
