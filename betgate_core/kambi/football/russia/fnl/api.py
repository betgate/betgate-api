from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import RussiaFnlMixin


class Fnl(RussiaFnlMixin, FootballApiBase):
    url_path_params = {"country": "russia", "category_id": "fnl"}
    category_name = "FNL"
