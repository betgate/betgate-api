from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import JapanJLeague1Mixin


class JLeague1(JapanJLeague1Mixin, FootballApiBase):
    url_path_params = {"country": "japan", "category_id": "j1-league"}
    category_name = "J1-League"
