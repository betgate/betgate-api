from betgate_core.kambi.football.japan import j_league1


def get_events(**kwargs):
    return j_league1.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return j_league1.lazy_get_events(**kwargs)
