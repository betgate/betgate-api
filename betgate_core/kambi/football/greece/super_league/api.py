from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import GreeceSuperLeagueMixin


class SuperLeague(GreeceSuperLeagueMixin, FootballApiBase):
    url_path_params = {"country": "greece", "category_id": "super_league"}
    category_name = "Super Liga"
