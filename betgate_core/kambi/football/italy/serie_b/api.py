from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import ItalySerieBMixin

class SerieB(ItalySerieBMixin, FootballApiBase):
    url_path_params = {"country": "italy", "category_id": "serie_b"}
    category_name = "Serie B"
