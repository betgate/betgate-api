from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import ItalySerieAMixin


class SerieA(ItalySerieAMixin, FootballApiBase):
    url_path_params = {"country": "italy", "category_id": "serie_a"}
    category_name = "Serie A"
