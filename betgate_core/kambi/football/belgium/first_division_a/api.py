from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import BelgiumFirstDivisionAMixin


class FirstDivisionA(BelgiumFirstDivisionAMixin, FootballApiBase):
    url_path_params = {"country": "belgium", "category_id": "jupiler_pro_league"}
    category_name = "Jupiler Pro League"
