from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import UsaMlsMixin


class Mls(UsaMlsMixin, FootballApiBase):
    url_path_params = {"country": "usa", "category_id": "mls"}
    category_name = "MLS"
