from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import SerbiaSuperLigaMixin


class SuperLiga(SerbiaSuperLigaMixin, FootballApiBase):
    url_path_params = {"country": "serbia", "category_id": "super_liga"}
    category_name = "Super Liga"
