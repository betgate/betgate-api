from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import AustriaBundesligaMixin


class Bundesliga(AustriaBundesligaMixin, FootballApiBase):
    url_path_params = {"country": "", "category_id": ""}
    category_name = ""
