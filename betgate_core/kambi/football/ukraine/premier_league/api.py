from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import UkrainePremierLeagueMixin


class PremierLeague(UkrainePremierLeagueMixin, FootballApiBase):
    url_path_params = {"country": "ukraine", "category_id": "premier_league"}
    category_name = "Premier Liga"
