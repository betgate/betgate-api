from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import BrazilSerieBMixin


class SerieB(BrazilSerieBMixin, FootballApiBase):
    url_path_params = {"country": "brazil", "category_id": "serie_b"}
    category_name = "Série B"
