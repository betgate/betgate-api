from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import BrazilSerieAMixin


class SerieA(BrazilSerieAMixin, FootballApiBase):
    url_path_params = {"country": "brazil", "category_id": "serie_a"}
    category_name = "Série A"
