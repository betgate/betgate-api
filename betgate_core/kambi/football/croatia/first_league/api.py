from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import CroatiaFirstLeagueMixin


class FirstLeague(CroatiaFirstLeagueMixin, FootballApiBase):
    url_path_params = {"country": "croatia", "category_id": "1__hnl_league"}
    category_name = "1. HNL Liga"
