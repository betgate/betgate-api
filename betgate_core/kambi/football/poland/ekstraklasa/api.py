from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import PolandEkstraklasaMixin


class Ekstraklasa(PolandEkstraklasaMixin, FootballApiBase):
    url_path_params = {"country": "poland", "category_id": "ekstraklasa"}
    category_name = "Ekstraklasa"
