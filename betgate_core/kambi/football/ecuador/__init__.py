from betgate_core.kambi.football.ecuador import serie_a


def get_events(**kwargs):
    return serie_a.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return serie_a.lazy_get_events(**kwargs)
