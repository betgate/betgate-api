from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import EcuadorSerieAMixin


class SerieA(EcuadorSerieAMixin, FootballApiBase):
    url_path_params = {"country": "ecuador", "category_id": "primera_a"}
    category_name = "Primera A"
