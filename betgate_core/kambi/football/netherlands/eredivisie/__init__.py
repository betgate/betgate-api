from betgate_core.kambi.football.netherlands.eredivisie.api import Eredivisie


def get_events(**kwargs):
    with Eredivisie() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Eredivisie() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Eredivisie() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
