from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import NetherlandsEredivisieMixin


class Eredivisie(NetherlandsEredivisieMixin, FootballApiBase):
    url_path_params = {"country": "netherlands", "category_id": "eredivisie"}
    category_name = "Eredivisie"
