from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import NetherlandsEersteDivisieMixin


class EersteDivisie(NetherlandsEersteDivisieMixin, FootballApiBase):
    url_path_params = {"country": "netherlands", "category_id": "eerste_divisie"}
    category_name = "Jupiler League"
