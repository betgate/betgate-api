import datetime
import pytz
from sdklib.shortcuts import cache
from betgate_models.models import Odd, Event, BookieId
from betgate_models.enums import Market, Category, Bookie
from betgate_core.kambi.api import KambiApiBase


class FootballApiBase(KambiApiBase):
    OFFERING_FOOTBALL_CATEGORY_URL = "/offering/api/v3/888es/listView/football/{category_id}"
    OFFERING_FOOTBALL_ALL_URL = "/offering/api/v3/888es/listView/football/{country}/{category_id}"

    url_path_params = {"country": None, "category_id": None}

    markets = {
        # (criterion_id, "outcome_type"): BetClass
        (1001159858, "OT_ONE"): Market.FT_WIN_1,
        (1001159858, "OT_CROSS"): Market.FT_WIN_X,
        (1001159858, "OT_TWO"): Market.FT_WIN_2,
        (1001159922, "OT_ONE_OR_CROSS"): Market.FT_WIN_1X,
        (1001159922, "OT_ONE_OR_TWO"): Market.FT_WIN_12,
        (1001159922, "OT_CROSS_OR_TWO"): Market.FT_WIN_X2,
        (1001159666, "OT_ONE"): Market.FT_WIN_DNB_1,
        (1001159666, "OT_TWO"): Market.FT_WIN_DNB_2,
        (1000316018, "OT_ONE"): Market.FH_WIN_1,
        (1000316018, "OT_CROSS"): Market.FH_WIN_X,
        (1000316018, "OT_TWO"): Market.FH_WIN_2,
        (1001159668, "OT_ONE_OR_CROSS"): Market.FH_WIN_1X,
        (1001159668, "OT_ONE_OR_TWO"): Market.FH_WIN_12,
        (1001159668, "OT_CROSS_OR_TWO"): Market.FH_WIN_X2,
        (1001159884, "OT_ONE"): Market.FH_WIN_DNB_1,
        (1001159884, "OT_TWO"): Market.FH_WIN_DNB_2,
        (1001642858, "OT_YES"): Market.FT_BTTS_YES,
        (1001642858, "OT_NO"): Market.FT_BTTS_NO,
        (1001159926, "OT_OVER", 500): Market.FT_TG_OVER_05,
        (1001159926, "OT_UNDER", 500): Market.FT_TG_UNDER_05,
        (1001159926, "OT_OVER", 1500): Market.FT_TG_OVER_15,
        (1001159926, "OT_UNDER", 1500): Market.FT_TG_UNDER_15,
        (1001159926, "OT_OVER", 2500): Market.FT_TG_OVER_25,
        (1001159926, "OT_UNDER", 2500): Market.FT_TG_UNDER_25,
        (1001159926, "OT_OVER", 3500): Market.FT_TG_OVER_35,
        (1001159926, "OT_UNDER", 3500): Market.FT_TG_UNDER_35,
        (1001159926, "OT_OVER", 4500): Market.FT_TG_OVER_45,
        (1001159926, "OT_UNDER", 4500): Market.FT_TG_UNDER_45,
        (1001159926, "OT_OVER", 5500): Market.FT_TG_OVER_55,
        (1001159926, "OT_UNDER", 5500): Market.FT_TG_UNDER_55,
        (1001159532, "OT_OVER", 500): Market.FH_TG_OVER_05,
        (1001159532, "OT_UNDER", 500): Market.FH_TG_UNDER_05,
        (1001159532, "OT_OVER", 1500): Market.FH_TG_OVER_15,
        (1001159532, "OT_UNDER", 1500): Market.FH_TG_UNDER_15,
        (1001159532, "OT_OVER", 2500): Market.FH_TG_OVER_25,
        (1001159532, "OT_UNDER", 2500): Market.FH_TG_UNDER_25,
        (1001159532, "OT_OVER", 3500): Market.FH_TG_OVER_35,
        (1001159532, "OT_UNDER", 3500): Market.FH_TG_UNDER_35,
        (1001159532, "OT_OVER", 4500): Market.FH_TG_OVER_45,
        (1001159532, "OT_UNDER", 4500): Market.FH_TG_UNDER_45,
        (1002244276, "OT_OVER", 500): Market.FT_TGA_OVER_05,
        (1002244276, "OT_UNDER", 500): Market.FT_TGA_UNDER_05,
        (1002244276, "OT_OVER", 750): Market.FT_TGA_OVER_075,
        (1002244276, "OT_UNDER", 750): Market.FT_TGA_UNDER_075,
        (1002244276, "OT_OVER", 1000): Market.FT_TGA_OVER_10,
        (1002244276, "OT_UNDER", 1000): Market.FT_TGA_UNDER_10,
        (1002244276, "OT_OVER", 1250): Market.FT_TGA_OVER_125,
        (1002244276, "OT_UNDER", 1250): Market.FT_TGA_UNDER_125,
        (1002244276, "OT_OVER", 1500): Market.FT_TGA_OVER_15,
        (1002244276, "OT_UNDER", 1500): Market.FT_TGA_UNDER_15,
        (1002244276, "OT_OVER", 1750): Market.FT_TGA_OVER_175,
        (1002244276, "OT_UNDER", 1750): Market.FT_TGA_UNDER_175,
        (1002244276, "OT_OVER", 2000): Market.FT_TGA_OVER_20,
        (1002244276, "OT_UNDER", 2000): Market.FT_TGA_UNDER_20,
        (1002244276, "OT_OVER", 2250): Market.FT_TGA_OVER_225,
        (1002244276, "OT_UNDER", 2250): Market.FT_TGA_UNDER_225,
        (1002244276, "OT_OVER", 2500): Market.FT_TGA_OVER_25,
        (1002244276, "OT_UNDER", 2500): Market.FT_TGA_UNDER_25,
        (1002244276, "OT_OVER", 2750): Market.FT_TGA_OVER_275,
        (1002244276, "OT_UNDER", 2750): Market.FT_TGA_UNDER_275,
        (1002244276, "OT_OVER", 3000): Market.FT_TGA_OVER_30,
        (1002244276, "OT_UNDER", 3000): Market.FT_TGA_UNDER_30,
        (1002244276, "OT_OVER", 3250): Market.FT_TGA_OVER_325,
        (1002244276, "OT_UNDER", 3250): Market.FT_TGA_UNDER_325,
        (1002244276, "OT_OVER", 3500): Market.FT_TGA_OVER_35,
        (1002244276, "OT_UNDER", 3500): Market.FT_TGA_UNDER_35,
        (1002244276, "OT_OVER", 3750): Market.FT_TGA_OVER_375,
        (1002244276, "OT_UNDER", 3750): Market.FT_TGA_UNDER_375,
        (1002244276, "OT_OVER", 4000): Market.FT_TGA_OVER_40,
        (1002244276, "OT_UNDER", 4000): Market.FT_TGA_UNDER_40,
        (1002244276, "OT_OVER", 4250): Market.FT_TGA_OVER_425,
        (1002244276, "OT_UNDER", 4250): Market.FT_TGA_UNDER_425,
        (1002244276, "OT_OVER", 4500): Market.FT_TGA_OVER_45,
        (1002244276, "OT_UNDER", 4500): Market.FT_TGA_UNDER_45,
        (1002244276, "OT_OVER", 4750): Market.FT_TGA_OVER_475,
        (1002244276, "OT_UNDER", 4750): Market.FT_TGA_UNDER_475,
        (1002244276, "OT_OVER", 5000): Market.FT_TGA_OVER_50,
        (1002244276, "OT_UNDER", 5000): Market.FT_TGA_UNDER_50,
        (1002244276, "OT_OVER", 5250): Market.FT_TGA_OVER_525,
        (1002244276, "OT_UNDER", 5250): Market.FT_TGA_UNDER_525,
        (1002244276, "OT_OVER", 5500): Market.FT_TGA_OVER_55,
        (1002244276, "OT_UNDER", 5500): Market.FT_TGA_UNDER_55,
        (1002558602, "OT_OVER", 500): Market.FH_TGA_OVER_05,
        (1002558602, "OT_UNDER", 500): Market.FH_TGA_UNDER_05,
        (1002558602, "OT_OVER", 750): Market.FH_TGA_OVER_075,
        (1002558602, "OT_UNDER", 750): Market.FH_TGA_UNDER_075,
        (1002558602, "OT_OVER", 1000): Market.FH_TGA_OVER_10,
        (1002558602, "OT_UNDER", 1000): Market.FH_TGA_UNDER_10,
        (1002558602, "OT_OVER", 1250): Market.FH_TGA_OVER_125,
        (1002558602, "OT_UNDER", 1250): Market.FH_TGA_UNDER_125,
        (1002558602, "OT_OVER", 1500): Market.FH_TGA_OVER_15,
        (1002558602, "OT_UNDER", 1500): Market.FH_TGA_UNDER_15,
        (1002558602, "OT_OVER", 1750): Market.FH_TGA_OVER_175,
        (1002558602, "OT_UNDER", 1750): Market.FH_TGA_UNDER_175,
        (1002558602, "OT_OVER", 2000): Market.FH_TGA_OVER_20,
        (1002558602, "OT_UNDER", 2000): Market.FH_TGA_UNDER_20,
        (1002558602, "OT_OVER", 2250): Market.FH_TGA_OVER_225,
        (1002558602, "OT_UNDER", 2250): Market.FH_TGA_UNDER_225,
        (1002558602, "OT_OVER", 2500): Market.FH_TGA_OVER_25,
        (1002558602, "OT_UNDER", 2500): Market.FH_TGA_UNDER_25,
        (1002558602, "OT_OVER", 2750): Market.FH_TGA_OVER_275,
        (1002558602, "OT_UNDER", 2750): Market.FH_TGA_UNDER_275,
        (1002558602, "OT_OVER", 3000): Market.FH_TGA_OVER_30,
        (1002558602, "OT_UNDER", 3000): Market.FH_TGA_UNDER_30,
        (1002558602, "OT_OVER", 3250): Market.FH_TGA_OVER_325,
        (1002558602, "OT_UNDER", 3250): Market.FH_TGA_UNDER_325,
        (1002558602, "OT_OVER", 3500): Market.FH_TGA_OVER_35,
        (1002558602, "OT_UNDER", 3500): Market.FH_TGA_UNDER_35,
        (1002558602, "OT_OVER", 3750): Market.FH_TGA_OVER_375,
        (1002558602, "OT_UNDER", 3750): Market.FH_TGA_UNDER_375,
        (1002558602, "OT_OVER", 4000): Market.FH_TGA_OVER_40,
        (1002558602, "OT_UNDER", 4000): Market.FH_TGA_UNDER_40,
        (1002558602, "OT_OVER", 4250): Market.FH_TGA_OVER_425,
        (1002558602, "OT_UNDER", 4250): Market.FH_TGA_UNDER_425,
        (1002558602, "OT_OVER", 4500): Market.FH_TGA_OVER_45,
        (1002558602, "OT_UNDER", 4500): Market.FH_TGA_UNDER_45,
        (1002275572, "OT_UNTYPED", 0, 0): Market.FT_WINA_1_P00,
        (1002275572, "OT_UNTYPED", 0, 1): Market.FT_WINA_2_P00,
        (1002275572, "OT_UNTYPED", 250, 0): Market.FT_WINA_1_P025,
        (1002275572, "OT_UNTYPED", -250, 1): Market.FT_WINA_2_M025,
        (1002275572, "OT_UNTYPED", 500, 0): Market.FT_WINA_1_P05,
        (1002275572, "OT_UNTYPED", -500, 1): Market.FT_WINA_2_M05,
        (1002275572, "OT_UNTYPED", 750, 0): Market.FT_WINA_1_P075,
        (1002275572, "OT_UNTYPED", -750, 1): Market.FT_WINA_2_M075,
        (1002275572, "OT_UNTYPED", 1000, 0): Market.FT_WINA_1_P10,
        (1002275572, "OT_UNTYPED", -1000, 1): Market.FT_WINA_2_M10,
        (1002275572, "OT_UNTYPED", 1250, 0): Market.FT_WINA_1_P125,
        (1002275572, "OT_UNTYPED", -1250, 1): Market.FT_WINA_2_M125,
        (1002275572, "OT_UNTYPED", 1500, 0): Market.FT_WINA_1_P15,
        (1002275572, "OT_UNTYPED", -1500, 1): Market.FT_WINA_2_M15,
        (1002275572, "OT_UNTYPED", 1750, 0): Market.FT_WINA_1_P175,
        (1002275572, "OT_UNTYPED", -1750, 1): Market.FT_WINA_2_M175,
        (1002275572, "OT_UNTYPED", 2000, 0): Market.FT_WINA_1_P20,
        (1002275572, "OT_UNTYPED", -2000, 1): Market.FT_WINA_2_M20,
        (1002275572, "OT_UNTYPED", 2250, 0): Market.FT_WINA_1_P225,
        (1002275572, "OT_UNTYPED", -2250, 1): Market.FT_WINA_2_M225,
        (1002275572, "OT_UNTYPED", 2500, 0): Market.FT_WINA_1_P25,
        (1002275572, "OT_UNTYPED", -2500, 1): Market.FT_WINA_2_M25,
        (1002275572, "OT_UNTYPED", 2750, 0): Market.FT_WINA_1_P275,
        (1002275572, "OT_UNTYPED", -2750, 1): Market.FT_WINA_2_M275,
        (1002275572, "OT_UNTYPED", 3000, 0): Market.FT_WINA_1_P30,
        (1002275572, "OT_UNTYPED", -3000, 1): Market.FT_WINA_2_M30,
        (1002275572, "OT_UNTYPED", 3250, 0): Market.FT_WINA_1_P325,
        (1002275572, "OT_UNTYPED", -3250, 1): Market.FT_WINA_2_M325,
        (1002275572, "OT_UNTYPED", 3500, 0): Market.FT_WINA_1_P35,
        (1002275572, "OT_UNTYPED", -3500, 1): Market.FT_WINA_2_M35,
        (1002275572, "OT_UNTYPED", -250, 0): Market.FT_WINA_1_M025,
        (1002275572, "OT_UNTYPED", 250, 1): Market.FT_WINA_2_P025,
        (1002275572, "OT_UNTYPED", -500, 0): Market.FT_WINA_1_M05,
        (1002275572, "OT_UNTYPED", 500, 1): Market.FT_WINA_2_P05,
        (1002275572, "OT_UNTYPED", -750, 0): Market.FT_WINA_1_M075,
        (1002275572, "OT_UNTYPED", 750, 1): Market.FT_WINA_2_P075,
        (1002275572, "OT_UNTYPED", -1000, 0): Market.FT_WINA_1_M10,
        (1002275572, "OT_UNTYPED", 1000, 1): Market.FT_WINA_2_P10,
        (1002275572, "OT_UNTYPED", -1500, 0): Market.FT_WINA_1_M15,
        (1002275572, "OT_UNTYPED", 1500, 1): Market.FT_WINA_2_P15,
        (1002275572, "OT_UNTYPED", -1750, 0): Market.FT_WINA_1_M175,
        (1002275572, "OT_UNTYPED", 1750, 1): Market.FT_WINA_2_P175,
        (1002275572, "OT_UNTYPED", -2000, 0): Market.FT_WINA_1_M20,
        (1002275572, "OT_UNTYPED", 2000, 1): Market.FT_WINA_2_P20,
        (1002275572, "OT_UNTYPED", -2250, 0): Market.FT_WINA_1_M225,
        (1002275572, "OT_UNTYPED", 2250, 1): Market.FT_WINA_2_P225,
        (1002275572, "OT_UNTYPED", -2500, 0): Market.FT_WINA_1_M25,
        (1002275572, "OT_UNTYPED", 2500, 1): Market.FT_WINA_2_P25,
        (1002275572, "OT_UNTYPED", -2750, 0): Market.FT_WINA_1_M275,
        (1002275572, "OT_UNTYPED", 2750, 1): Market.FT_WINA_2_P275,
        (1002275572, "OT_UNTYPED", -3000, 0): Market.FT_WINA_1_M30,
        (1002275572, "OT_UNTYPED", 3000, 1): Market.FT_WINA_2_P30,
        (1002275572, "OT_UNTYPED", -3250, 0): Market.FT_WINA_1_M325,
        (1002275572, "OT_UNTYPED", 3250, 1): Market.FT_WINA_2_P325,
        (1002275572, "OT_UNTYPED", -3500, 0): Market.FT_WINA_1_M35,
        (1002275572, "OT_UNTYPED", 3500, 1): Market.FT_WINA_2_P35,
        (1002275573, "OT_UNTYPED", 0, 0): Market.FH_WINA_1_P00,
        (1002275573, "OT_UNTYPED", 0, 1): Market.FH_WINA_2_P00,
        (1002275573, "OT_UNTYPED", 250, 0): Market.FH_WINA_1_P025,
        (1002275573, "OT_UNTYPED", -250, 1): Market.FH_WINA_2_M025,
        (1002275573, "OT_UNTYPED", 500, 0): Market.FH_WINA_1_P05,
        (1002275573, "OT_UNTYPED", -500, 1): Market.FH_WINA_2_M05,
        (1002275573, "OT_UNTYPED", 750, 0): Market.FH_WINA_1_P075,
        (1002275573, "OT_UNTYPED", -750, 1): Market.FH_WINA_2_M075,
        (1002275573, "OT_UNTYPED", 1000, 0): Market.FH_WINA_1_P10,
        (1002275573, "OT_UNTYPED", -1000, 1): Market.FH_WINA_2_M10,
        (1002275573, "OT_UNTYPED", 1250, 0): Market.FH_WINA_1_P125,
        (1002275573, "OT_UNTYPED", -1250, 1): Market.FH_WINA_2_M125,
        (1002275573, "OT_UNTYPED", 1500, 0): Market.FH_WINA_1_P15,
        (1002275573, "OT_UNTYPED", -1500, 1): Market.FH_WINA_2_M15,
        (1002275573, "OT_UNTYPED", 1750, 0): Market.FH_WINA_1_P175,
        (1002275573, "OT_UNTYPED", -1750, 1): Market.FH_WINA_2_M175,
        (1002275573, "OT_UNTYPED", 2000, 0): Market.FH_WINA_1_P20,
        (1002275573, "OT_UNTYPED", -2000, 1): Market.FH_WINA_2_M20,
        (1002275573, "OT_UNTYPED", 2250, 0): Market.FH_WINA_1_P225,
        (1002275573, "OT_UNTYPED", -2250, 1): Market.FH_WINA_2_M225,
        (1002275573, "OT_UNTYPED", 2500, 0): Market.FH_WINA_1_P25,
        (1002275573, "OT_UNTYPED", -2500, 1): Market.FH_WINA_2_M25,
        (1002275573, "OT_UNTYPED", 2750, 0): Market.FH_WINA_1_P275,
        (1002275573, "OT_UNTYPED", -2750, 1): Market.FH_WINA_2_M275,
        (1002275573, "OT_UNTYPED", 3000, 0): Market.FH_WINA_1_P30,
        (1002275573, "OT_UNTYPED", -3000, 1): Market.FH_WINA_2_M30,
        (1002275573, "OT_UNTYPED", 3250, 0): Market.FH_WINA_1_P325,
        (1002275573, "OT_UNTYPED", -3250, 1): Market.FH_WINA_2_M325,
        (1002275573, "OT_UNTYPED", 3500, 0): Market.FH_WINA_1_P35,
        (1002275573, "OT_UNTYPED", -3500, 1): Market.FH_WINA_2_M35,
        (1002275573, "OT_UNTYPED", -250, 0): Market.FH_WINA_1_M025,
        (1002275573, "OT_UNTYPED", 250, 1): Market.FH_WINA_2_P025,
        (1002275573, "OT_UNTYPED", -500, 0): Market.FH_WINA_1_M05,
        (1002275573, "OT_UNTYPED", 500, 1): Market.FH_WINA_2_P05,
        (1002275573, "OT_UNTYPED", -750, 0): Market.FH_WINA_1_M075,
        (1002275573, "OT_UNTYPED", 750, 1): Market.FH_WINA_2_P075,
        (1002275573, "OT_UNTYPED", -1000, 0): Market.FH_WINA_1_M10,
        (1002275573, "OT_UNTYPED", 1000, 1): Market.FH_WINA_2_P10,
        (1002275573, "OT_UNTYPED", -1250, 0): Market.FH_WINA_1_M125,
        (1002275573, "OT_UNTYPED", 1250, 1): Market.FH_WINA_2_P125,
        (1002275573, "OT_UNTYPED", -1500, 0): Market.FH_WINA_1_M15,
        (1002275573, "OT_UNTYPED", 1500, 1): Market.FH_WINA_2_P15,
        (1002275573, "OT_UNTYPED", -1750, 0): Market.FH_WINA_1_M175,
        (1002275573, "OT_UNTYPED", 1750, 1): Market.FH_WINA_2_P175,
        (1002275573, "OT_UNTYPED", -2000, 0): Market.FH_WINA_1_M20,
        (1002275573, "OT_UNTYPED", 2000, 1): Market.FH_WINA_2_P20,
        (1002275573, "OT_UNTYPED", -2250, 0): Market.FH_WINA_1_M225,
        (1002275573, "OT_UNTYPED", 2250, 1): Market.FH_WINA_2_P225,
        (1002275573, "OT_UNTYPED", -2500, 0): Market.FH_WINA_1_M25,
        (1002275573, "OT_UNTYPED", 2500, 1): Market.FH_WINA_2_P25,
        (1002275573, "OT_UNTYPED", -2750, 0): Market.FH_WINA_1_M275,
        (1002275573, "OT_UNTYPED", 2750, 1): Market.FH_WINA_2_P275,
        (1002275573, "OT_UNTYPED", -3000, 0): Market.FH_WINA_1_M30,
        (1002275573, "OT_UNTYPED", 3000, 1): Market.FH_WINA_2_P30,
        (1002275573, "OT_UNTYPED", -3250, 0): Market.FH_WINA_1_M325,
        (1002275573, "OT_UNTYPED", 3250, 1): Market.FH_WINA_2_P325,
        (1002275573, "OT_UNTYPED", -3500, 0): Market.FH_WINA_1_M35,
        (1002275573, "OT_UNTYPED", 3500, 1): Market.FH_WINA_2_P35,
        (1001159780, "OT_UNTYPED", "0-0"): Market.FT_RESULT_0_0,
        (1000505272, "OT_UNTYPED", "0-0"): Market.FH_RESULT_0_0,
        (1001159897, "OT_OVER", 500): Market.FT_CORNERS_OVER_0_5,
        (1001159897, "OT_UNDER", 500): Market.FT_CORNERS_UNDER_0_5,
        (1001159897, "OT_OVER", 1500): Market.FT_CORNERS_OVER_1_5,
        (1001159897, "OT_UNDER", 1500): Market.FT_CORNERS_UNDER_1_5,
        (1001159897, "OT_OVER", 2500): Market.FT_CORNERS_OVER_2_5,
        (1001159897, "OT_UNDER", 2500): Market.FT_CORNERS_UNDER_2_5,
        (1001159897, "OT_OVER", 3500): Market.FT_CORNERS_OVER_3_5,
        (1001159897, "OT_UNDER", 3500): Market.FT_CORNERS_UNDER_3_5,
        (1001159897, "OT_OVER", 4500): Market.FT_CORNERS_OVER_4_5,
        (1001159897, "OT_UNDER", 4500): Market.FT_CORNERS_UNDER_4_5,
        (1001159897, "OT_OVER", 5500): Market.FT_CORNERS_OVER_5_5,
        (1001159897, "OT_UNDER", 5500): Market.FT_CORNERS_UNDER_5_5,
        (1001159897, "OT_OVER", 6500): Market.FT_CORNERS_OVER_6_5,
        (1001159897, "OT_UNDER", 6500): Market.FT_CORNERS_UNDER_6_5,
        (1001159897, "OT_OVER", 7500): Market.FT_CORNERS_OVER_7_5,
        (1001159897, "OT_UNDER", 7500): Market.FT_CORNERS_UNDER_7_5,
        (1001159897, "OT_OVER", 8500): Market.FT_CORNERS_OVER_8_5,
        (1001159897, "OT_UNDER", 8500): Market.FT_CORNERS_UNDER_8_5,
        (1001159897, "OT_OVER", 9500): Market.FT_CORNERS_OVER_9_5,
        (1001159897, "OT_UNDER", 9500): Market.FT_CORNERS_UNDER_9_5,
        (1001159897, "OT_OVER", 10500): Market.FT_CORNERS_OVER_10_5,
        (1001159897, "OT_UNDER", 10500): Market.FT_CORNERS_UNDER_10_5,
        (1001159897, "OT_OVER", 11500): Market.FT_CORNERS_OVER_11_5,
        (1001159897, "OT_UNDER", 11500): Market.FT_CORNERS_UNDER_11_5,
        (1001159897, "OT_OVER", 12500): Market.FT_CORNERS_OVER_12_5,
        (1001159897, "OT_UNDER", 12500): Market.FT_CORNERS_UNDER_12_5,
        (1001159897, "OT_OVER", 13500): Market.FT_CORNERS_OVER_13_5,
        (1001159897, "OT_UNDER", 13500): Market.FT_CORNERS_UNDER_13_5,
        (1001159897, "OT_OVER", 14500): Market.FT_CORNERS_OVER_14_5,
        (1001159897, "OT_UNDER", 14500): Market.FT_CORNERS_UNDER_14_5,
        (1001159897, "OT_OVER", 15500): Market.FT_CORNERS_OVER_15_5,
        (1001159897, "OT_UNDER", 15500): Market.FT_CORNERS_UNDER_15_5,
        (1001159897, "OT_OVER", 16500): Market.FT_CORNERS_OVER_16_5,
        (1001159897, "OT_UNDER", 16500): Market.FT_CORNERS_UNDER_16_5,
        (1001159897, "OT_OVER", 17500): Market.FT_CORNERS_OVER_17_5,
        (1001159897, "OT_UNDER", 17500): Market.FT_CORNERS_UNDER_17_5,
        (1001159897, "OT_OVER", 18500): Market.FT_CORNERS_OVER_18_5,
        (1001159897, "OT_UNDER", 18500): Market.FT_CORNERS_UNDER_18_5,
        (1001159897, "OT_OVER", 19500): Market.FT_CORNERS_OVER_19_5,
        (1001159897, "OT_UNDER", 19500): Market.FT_CORNERS_UNDER_19_5,
        (1001159897, "OT_OVER", 20500): Market.FT_CORNERS_OVER_20_5,
        (1001159897, "OT_UNDER", 20500): Market.FT_CORNERS_UNDER_20_5,
    }

    def __init__(self):
        super(FootballApiBase, self).__init__()
        if self.url_path_params is None or "country" not in self.url_path_params or \
                "category_id" not in self.url_path_params:
            raise BaseException("You must define a 'url_path_params = {0}' attribute in your {1}.{2} class.".format(
                "{'country': '', 'category_id': ''}", type(self).__module__, type(self).__name__
            ))

    @cache(maxsize=None)
    def request_offering_category(self):
        params = {
            "lang": "es_ES",
            "market": "ES",
            "client_id": "2",
            "channel_id": "3",
            "ncid": "1488470914575",
            "categoryGroup": "COMBINED",
            "displayDefault": "true"
        }
        if self.url_path_params.get("country", False):
            url = self.OFFERING_FOOTBALL_ALL_URL
        else:
            url = self.OFFERING_FOOTBALL_CATEGORY_URL
        return self.get(url, query_params=params)

    def lazy_get_category_events(self, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            to_return = []
            res = self.request_offering_category()
            for evt in res.json["events"]:
                try:
                    if evt["event"]["displayType"] == "ET_MATCH":
                        event_id = evt["event"]["id"]
                        home_tname = evt["event"]["homeName"] if "homeName" in evt["event"] else None
                        away_tname = evt["event"]["awayName"] if "awayName" in evt["event"] else None
                        utc_dt = datetime.datetime.utcfromtimestamp(evt["event"]["start"]/1000)
                        date_time = utc_dt.replace(tzinfo=pytz.utc)

                        event = Event(
                            bookies={str(Bookie.SPORT888.value): BookieId(bookie=Bookie.SPORT888, value=event_id)},
                            home_team=self.get_team(home_tname),
                            away_team=self.get_team(away_tname),
                            category=self.get_category(),
                            start=date_time,
                            home_team_name=home_tname,
                            away_team_name=away_tname
                        )
                        if validate:
                            event.full_clean(validate_unique=False)
                        to_return.append(event)
                except:
                    pass
            return to_return
        except:
            return []

    def get_team_names(self, data):
        try:
            home_team_name = data["events"][0]["homeName"].strip()
            away_team_name = data["events"][0]["awayName"].strip()
            return home_team_name, away_team_name
        except:
            return None, None

    def _get_markets(self, market):
        markets_to_return = []
        for i, outcome in enumerate(market.get("outcomes", [])):
            if "line" in outcome and outcome["type"] == 'OT_UNTYPED':
                lookup_key = (market["criterion"]["id"], outcome["type"], outcome["line"], i)
            elif "line" in outcome:
                lookup_key = (market["criterion"]["id"], outcome["type"], outcome["line"])
            elif outcome["type"] == 'OT_UNTYPED':
                lookup_key = (market["criterion"]["id"], outcome["type"], outcome["label"])
            else:
                lookup_key = (market["criterion"]["id"], outcome["type"])
            mkt = self.markets.get(lookup_key)
            if mkt:
                odd = outcome["odds"]/1000
                markets_to_return.append(Odd(value=odd, market=mkt, bookie=Bookie.SPORT888))
        return markets_to_return

    def get_markets(self, json_model):
        markets_to_return = []
        for mkt in json_model.get("betoffers", []):
            markets = self._get_markets(mkt)
            markets_to_return.extend(markets)
        return markets_to_return

    def get_datetime(self, json_model):
        try:
            text = json_model["events"][0]["start"]
            utc_dt = datetime.datetime.strptime(text, '%Y-%m-%dT%H:%MZ')
            local_tz = pytz.timezone('Europe/Madrid')
            local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(local_tz)
            return local_dt
        except:
            pass

    def get_event(self, event_id, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            res = self.request_event_detail(event_id)
            home_tname, away_tname = self.get_team_names(res.json)
            category = self.get_category()
            dtime = self.get_datetime(json_model=res.json)
            markets = self.get_markets(json_model=res.json)
            event = Event(
                bookies={str(Bookie.SPORT888.value): BookieId(bookie=Bookie.SPORT888, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                odds=markets,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass


class FootballApi(FootballApiBase):
    category = Category.UNKNOWN
