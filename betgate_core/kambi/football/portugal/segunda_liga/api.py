from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import PortugalSegundaLigaMixin


class SegundaLiga(PortugalSegundaLigaMixin, FootballApiBase):
    url_path_params = {"country": "portugal", "category_id": "ligapro"}
    category_name = "LigaPro"
