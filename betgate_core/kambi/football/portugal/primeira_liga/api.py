from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import PortugalPrimeiraLigaMixin


class PrimeiraLiga(PortugalPrimeiraLigaMixin, FootballApiBase):
    url_path_params = {"country": "portugal", "category_id": "primeira_liga"}
    category_name = "Primeira Liga"
