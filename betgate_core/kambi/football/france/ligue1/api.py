from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import FranceLigue1Mixin


class Ligue1(FranceLigue1Mixin, FootballApiBase):
    url_path_params = {"country": "france", "category_id": "ligue_1"}
    category_name = "Ligue 1"
