from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import FranceLigue2Mixin


class Ligue2(FranceLigue2Mixin, FootballApiBase):
    url_path_params = {"country": "france", "category_id": "ligue_2"}
    category_name = "Liga 2"
