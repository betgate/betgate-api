from betgate_core.kambi.football.france.ligue2.api import Ligue2


def get_events(**kwargs):
    with Ligue2() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Ligue2() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Ligue2() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
