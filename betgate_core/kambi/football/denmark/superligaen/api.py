from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import DenmarkSuperligaenMixin


class Superligaen(DenmarkSuperligaenMixin, FootballApiBase):
    url_path_params = {"country": "denmark", "category_id": "superligaen"}
    category_name = "Superligaen"
