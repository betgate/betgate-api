from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import TurkeySuperLeagueMixin


class SuperLeague(TurkeySuperLeagueMixin, FootballApiBase):
    url_path_params = {"country": "turkey", "category_id": "super_lig"}
    category_name = "Süper Liga"
