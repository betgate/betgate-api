from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import SpainPrimeraDivisionMixin


class PrimeraDivision(SpainPrimeraDivisionMixin, FootballApiBase):
    url_path_params = {"country": "spain", "category_id": "la_liga"}
    category_name = "La Liga"
