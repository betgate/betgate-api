from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import SpainSegundaDivisionMixin


class SegundaDivision(SpainSegundaDivisionMixin, FootballApiBase):
    url_path_params = {"country": "spain", "category_id": "la_liga_2"}
    category_name = "La Liga 2"
