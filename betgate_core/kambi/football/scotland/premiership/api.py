from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import ScotlandPremiershipMixin


class Premiership(ScotlandPremiershipMixin, FootballApiBase):
    url_path_params = {"country": "scotland", "category_id": "scottish_premiership"}
    category_name = "Premiership de Escocia"
