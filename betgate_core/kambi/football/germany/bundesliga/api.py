from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import GermanyBundesligaMixin


class Bundesliga(GermanyBundesligaMixin, FootballApiBase):
    url_path_params = {"country": "germany", "category_id": "bundesliga"}
    category_name = "Bundesliga"
