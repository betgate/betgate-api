from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import GermanyBundesliga2Mixin


class Bundesliga2(GermanyBundesliga2Mixin, FootballApiBase):
    url_path_params = {"country": "germany", "category_id": "2__bundesliga"}
    category_name = "2. Bundesliga"
