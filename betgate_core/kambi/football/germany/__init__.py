from betgate_core.kambi.football.germany import bundesliga, bundesliga2


def get_events(**kwargs):
    return bundesliga.get_events() + bundesliga2.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return bundesliga.lazy_get_events() + bundesliga2.lazy_get_events(**kwargs)
