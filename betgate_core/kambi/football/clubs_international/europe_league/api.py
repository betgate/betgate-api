from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import ClubsInternationalEuropeLeagueMixin


class EuropeLeague(ClubsInternationalEuropeLeagueMixin, FootballApiBase):
    url_path_params = {"country": None, "category_id": "europa_league"}
    category_name = "Europa League"
