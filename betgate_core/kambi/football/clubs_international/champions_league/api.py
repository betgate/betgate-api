from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import ClubsInternationalChampionsLeagueMixin


class ChampionsLeague(ClubsInternationalChampionsLeagueMixin, FootballApiBase):
    url_path_params = {"country": None, "category_id": "champions_league"}
    category_name = "Liga de Campeones"
