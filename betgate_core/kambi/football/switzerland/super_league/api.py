from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import SwitzerlandSuperLeagueMixin


class SuperLeague(SwitzerlandSuperLeagueMixin, FootballApiBase):
    url_path_params = {"country": "switzerland", "category_id": "super_league"}
    category_name = "Super League"
