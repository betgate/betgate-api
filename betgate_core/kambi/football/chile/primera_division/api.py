from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import ChilePrimeraDivisionMixin


class PrimeraDivision(ChilePrimeraDivisionMixin, FootballApiBase):
    url_path_params = {"country": "chile", "category_id": "primera"}
    category_name = "Primera"
