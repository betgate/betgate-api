from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandChampionshipMixin


class Championship(EnglandChampionshipMixin, FootballApiBase):
    url_path_params = {"country": "england", "category_id": "the_championship"}
    category_name = "Championship"
