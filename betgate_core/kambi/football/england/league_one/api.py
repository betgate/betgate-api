from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandLeagueOneMixin


class LeagueOne(EnglandLeagueOneMixin, FootballApiBase):
    url_path_params = {"country": "england", "category_id": "league_one"}
    category_name = "Liga Uno"
