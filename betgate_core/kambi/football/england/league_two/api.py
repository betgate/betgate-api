from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandLeagueTwoMixin


class LeagueTwo(EnglandLeagueTwoMixin, FootballApiBase):
    url_path_params = {"country": "england", "category_id": "league_two"}
    category_name = "Liga Dos"
