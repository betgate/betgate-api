from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandPremierLeagueMixin


class PremierLeague(EnglandPremierLeagueMixin, FootballApiBase):
    url_path_params = {"country": "england", "category_id": "premier_league"}
    category_name = "Premier League"
