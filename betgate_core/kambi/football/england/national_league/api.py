from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandNationalLeagueMixin


class NationalLeague(EnglandNationalLeagueMixin, FootballApiBase):
    url_path_params = {"country": "england", "category_id": "national_league"}
    category_name = "Liga Nacional"
