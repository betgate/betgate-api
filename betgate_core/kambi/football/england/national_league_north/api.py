from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandNationalLeagueNorthMixin


class NationalLeagueNorth(EnglandNationalLeagueNorthMixin, FootballApiBase):
    url_path_params = {"country": "england", "category_id": "national_league_north"}
    category_name = "Liga Nacional Norte"
