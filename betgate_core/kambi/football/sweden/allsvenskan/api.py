from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import SwedenAllsvenskanMixin


class Allsvenskan(SwedenAllsvenskanMixin, FootballApiBase):
    url_path_params = {"country": "sweden", "category_id": "allsvenskan"}
    category_name = "Allsvenskan"
