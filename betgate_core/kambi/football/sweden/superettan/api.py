from betgate_core.kambi.football.api import FootballApiBase
from betgate_core.mixins.football import SwedenSuperettanMixin


class Superettan(SwedenSuperettanMixin, FootballApiBase):
    url_path_params = {"country": "sweden", "category_id": "superettan"}
    category_name = "Superattan"
