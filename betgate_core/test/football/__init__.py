

class Mock:
    def __init__(self):
        self._odds = dict()
        self._events = list()

    def set_odds(self, event_id, odds):
        self._odds[event_id] = odds

    def get_odds(self, event_id):
        return self._odds.get(event_id, [])

    def set_events(self, events):
        self._events = events

    def append_event(self, event):
        self._events.append(event)

    def lazy_get_events(self, **kwargs):  # pylint: disable=unused-argument
        return self._events

    def get_events(self, **kwargs):  # pylint: disable=unused-argument
        return self._events


mock = Mock()


def get_odds(event_id):
    return mock.get_odds(event_id)


def lazy_get_events(**kwargs):
    return mock.lazy_get_events(**kwargs)


def get_events(**kwargs):
    return mock.get_events(**kwargs)
