import unittest
import datetime
import pytz
from betgate_models.enums import Category
from betgate_core import get_all_football_events, lazy_get_all_football_events
from betgate_core.utils.teams import get_team_from_name


class BookieTestCaseMeta(type):
    _bookie_to_test_att_name = "bookie_to_test"

    def __new__(mcs, *args, **kwargs):
        if mcs._bookie_to_test_att_name in args[2]:
            bookie_to_test = args[2][mcs._bookie_to_test_att_name]

            all_lazy_events = lazy_get_all_football_events(bookie=bookie_to_test, validate=False)
            assert len(all_lazy_events) > 0, "We aren't testing any lazy event ..."
            all_events = get_all_football_events(bookie=bookie_to_test, validate=False)
            assert len(all_events) > 0, "We aren't testing any event ..."

            # add team names tests
            team_names_to_test = [(evt.home_team_name, evt.category) for evt in all_lazy_events+all_events]
            assert len(team_names_to_test) > 0, "We aren't testing any team name ..."

            for team_name, category in team_names_to_test:
                fname = "test_team_name_'{}'".format(team_name)
                args[2][fname] = mcs._add_team_name_test(team_name, bookie_to_test, category)

            events_dict = dict()
            for evt in all_events+all_lazy_events:
                event_values = events_dict.get(evt.id, [])
                event_values.append(evt)
                events_dict[evt.id] = event_values

            for event_id, evts in events_dict.items():
                fname = "test_datetime_'{0}'".format(event_id)
                args[2][fname] = mcs._add_datetime_test(evts, bookie_to_test)

            # event contains any odd
            for evt in all_events:
                fname = "test_event_'{}'_contains_any_odd".format(evt.id)
                args[2][fname] = mcs._add_contains_any_odd_test(evt, bookie_to_test)

            # lazy event should not contain any market
            for evt in all_lazy_events:
                fname = "test_event_'{}'_not_contain_any_odd".format(evt.id)
                args[2][fname] = mcs._add_not_contain_any_odd_test(evt, bookie_to_test)

        return type.__new__(mcs, *args, **kwargs)

    @classmethod
    def _add_team_name_test(mcs, team_name, bookie, category):
        def func(self):
            team = get_team_from_name(team_name)

            self.assertNotEqual(-1, team, "'{0}' name of {1} found by '{2}' is not defined in Team module.".format(
                team_name,
                Category(category).name,
                bookie
            ))
        return func

    @classmethod
    def _add_datetime_test(mcs, events, bookie):
        def func(self):
            local_tz = pytz.timezone('Europe/Madrid')
            yesterday = local_tz.localize(datetime.datetime.now() - datetime.timedelta(1), is_dst=None)
            start = events[0].start
            for event in events:
                self.assertIsNotNone(
                    event.start,
                    "Datetime of event '{0}' was not found by '{1}'.".format(
                        event.id,
                        bookie
                    )
                )
                self.assertGreater(
                    event.start,
                    yesterday,
                    "'{0}' datetime of event '{1}' found by '{2}' is not a future event.".format(
                        event.start,
                        event.id,
                        bookie
                    )
                )
                self.assertTrue(
                    event.start.tzinfo is not None and event.start.tzinfo.utcoffset(event.start) is not None,
                    "'{0}' datetime of event '{1}' found by '{2}' is naive.".format(
                        event.start,
                        event.id,
                        bookie
                    )
                )
                self.assertEqual(
                    event.start,
                    start,
                    "'{0}' datetime of event '{1}' found by '{2}' are different to base datetime {3}.".format(
                        event.start,
                        event.id,
                        bookie,
                        start
                    )
                )
        return func


    @classmethod
    def _add_contains_any_odd_test(mcs, event, bookie):
        def func(self):
            self.assertGreater(
                len(event.odds),
                0,
                "Event '{0}' found by '{1}' doesn't contain any odd.".format(
                    event.id,
                    bookie
                )
            )
            for odd in event.odds:
                self.assertEqual(
                    1,
                    len([item for item in event.odds if item.market == odd.market and item.bookie == odd.bookie]),
                    "Event '{0}' found by '{1}' does contain some duplicated odds: {2}".format(
                        event.id,
                        bookie,
                        odd.market
                    )
                )
        return func

    @classmethod
    def _add_not_contain_any_odd_test(mcs, event, bookie):
        def func(self):
            self.assertEqual(
                len(event.odds),
                0,
                "Event '{0}' found by '{1}' contains some odds.".format(
                    event.id,
                    bookie
                )
            )
        return func


class BookieTestCase(unittest.TestCase, metaclass=BookieTestCaseMeta):
    pass
