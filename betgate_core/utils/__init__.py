

def locale_month_to_month(locale_month):
    month = None
    if locale_month.lower() in ["enero", "ene"]:
        month = 1
    elif locale_month.lower() in ["febrero", "feb"]:
        month = 2
    elif locale_month.lower() in ["marzo", "mar"]:
        month = 3
    elif locale_month.lower() in ["abril", "abr"]:
        month = 4
    elif locale_month.lower() in ["mayo", "may"]:
        month = 5
    elif locale_month.lower() in ["junio", "jun"]:
        month = 6
    elif locale_month.lower() in ["julio", "jul"]:
        month = 7
    elif locale_month.lower() in ["agosto", "ago"]:
        month = 8
    elif locale_month.lower() in ["septiembre", "sep"]:
        month = 9
    elif locale_month.lower() in ["octubre", "oct"]:
        month = 10
    elif locale_month.lower() in ["noviembre", "nov"]:
        month = 11
    elif locale_month.lower() in ["diciembre", "dic"]:
        month = 12
    return month
