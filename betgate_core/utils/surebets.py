from betgate_core.models.surebets import ALL_SUREBETS_FINDERS
from betgate_models.enums import PrimitiveMarket


def find_all_surebets(event):
    odds_per_primitive = dict()
    for odd in event.odds:
        primitive_market = PrimitiveMarket.from_market(odd.market)
        temp_odds = odds_per_primitive.get(primitive_market, [])
        temp_odds.append(odd)
        odds_per_primitive[primitive_market] = temp_odds

    all_surebets = []
    for finder in ALL_SUREBETS_FINDERS:
        all_surebets.extend(finder.find_surebets(odds_per_primitive, event=event))
    return all_surebets
