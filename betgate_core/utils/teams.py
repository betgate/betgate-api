import logging
from betgate_core.models.football.teams import TEAM_NAMES


logger = logging.getLogger(__name__)


def get_team_from_name(name):
    if name is None:
        return

    cleaned_name = name.strip().lower()
    team = TEAM_NAMES.get(cleaned_name, -1)

    if team == -1:
        logger.error("Team name not found: %s", name)

    return team
