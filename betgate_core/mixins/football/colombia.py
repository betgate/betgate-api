from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'ColombiaPrimeraAMixin'
]


class ColombiaFootballMixin(FootballMixinBase):
    country = Country.COLOMBIA


class ColombiaPrimeraAMixin(ColombiaFootballMixin):
    category = Category.COLOMBIA__PRIMERA_A
