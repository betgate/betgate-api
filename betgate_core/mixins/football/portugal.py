from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'PortugalSegundaLigaMixin',
    'PortugalPrimeiraLigaMixin'
]


class PortugalFootballMixin(FootballMixinBase):
    country = Country.PORTUGAL


class PortugalSegundaLigaMixin(PortugalFootballMixin):
    category = Category.PORTUGAL__SEGUNDA_LIGA


class PortugalPrimeiraLigaMixin(PortugalFootballMixin):
    category = Category.PORTUGAL__PRIMEIRA_LIGA
