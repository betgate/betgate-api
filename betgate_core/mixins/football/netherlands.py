from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


class NetherlandsFootballMixin(FootballMixinBase):
    country = Country.NETHERLANDS


class NetherlandsEredivisieMixin(NetherlandsFootballMixin):
    category = Category.NETHERLANDS__EREDIVISIE


class NetherlandsEersteDivisieMixin(NetherlandsFootballMixin):
    category = Category.NETHERLANDS__EERSTE_DIVISIE
