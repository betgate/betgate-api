from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'FinlandVeikkausliigaMixin'
]


class FinlandFootballMixin(FootballMixinBase):
    country = Country.FINLAND


class FinlandVeikkausliigaMixin(FinlandFootballMixin):
    category = Category.FINLAND__VEIKKAUSLIIGA
