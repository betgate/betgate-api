from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'EcuadorSerieAMixin'
]


class EcuadorFootballMixin(FootballMixinBase):
    country = Country.ECUADOR


class EcuadorSerieAMixin(EcuadorFootballMixin):
    category = Category.ECUADOR__SERIE_A
