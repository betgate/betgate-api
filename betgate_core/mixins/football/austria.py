from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'AustriaBundesligaMixin'
]


class AustriaFootballMixin(FootballMixinBase):
    country = Country.AUSTRIA


class AustriaBundesligaMixin(AustriaFootballMixin):
    category = Category.AUSTRIA__BUNDESLIGA
