from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'ChilePrimeraDivisionMixin'
]


class ChileFootballMixin(FootballMixinBase):
    country = Country.CHILE


class ChilePrimeraDivisionMixin(ChileFootballMixin):
    category = Category.CHILE__PRIMERA_DIVISION
