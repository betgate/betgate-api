from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'IcelandUrvalsdeildMixin'
]


class IcelandFootballMixin(FootballMixinBase):
    country = Country.ICELAND


class IcelandUrvalsdeildMixin(IcelandFootballMixin):
    category = Category.ICELAND__URVALSDEILD
