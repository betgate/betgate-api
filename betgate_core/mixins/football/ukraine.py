from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'UkrainePremierLeagueMixin'
]


class UkraineFootballMixin(FootballMixinBase):
    country = Country.UKRAINE


class UkrainePremierLeagueMixin(UkraineFootballMixin):
    category = Category.UKRAINE__PREMIER_LEAGUE
