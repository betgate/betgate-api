from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'MoroccoBotolaMixin'
]


class MoroccoFootballMixin(FootballMixinBase):
    country = Country.MOROCCO


class MoroccoBotolaMixin(MoroccoFootballMixin):
    category = Category.MOROCCO__BOTOLA
