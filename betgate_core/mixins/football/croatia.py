from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'CroatiaFirstLeagueMixin'
]


class CroatiaFootballMixin(FootballMixinBase):
    country = Country.CROATIA


class CroatiaFirstLeagueMixin(CroatiaFootballMixin):
    category = Category.CROATIA__FIRST_LEAGUE
