from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'RussiaFnlMixin',
    'RussiaPremierLeagueMixin'
]


class RussiaFootballMixin(FootballMixinBase):
    country = Country.RUSSIA


class RussiaFnlMixin(RussiaFootballMixin):
    category = Category.RUSSIA__FNL


class RussiaPremierLeagueMixin(RussiaFootballMixin):
    category = Category.RUSSIA__PREMIER_LEAGUE
