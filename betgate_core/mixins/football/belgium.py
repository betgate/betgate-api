from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'BelgiumFirstDivisionAMixin'
]


class BelgiumFootballMixin(FootballMixinBase):
    country = Country.BELGIUM


class BelgiumFirstDivisionAMixin(BelgiumFootballMixin):
    category = Category.BELGIUM__FIRST_DIVISION_A
