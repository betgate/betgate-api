from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'DenmarkSuperligaenMixin'
]


class DenmarkFootballMixin(FootballMixinBase):
    country = Country.DENMARK


class DenmarkSuperligaenMixin(DenmarkFootballMixin):
    category = Category.DENMARK__SUPERLIGAEN
