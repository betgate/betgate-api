from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'SpainPrimeraDivisionMixin',
    'SpainSegundaDivisionMixin'
]


class SpainFootballMixin(FootballMixinBase):
    country = Country.SPAIN


class SpainPrimeraDivisionMixin(SpainFootballMixin):
    category = Category.SPAIN__PRIMERA_DIVISION


class SpainSegundaDivisionMixin(SpainFootballMixin):
    category = Category.SPAIN__SEGUNDA_DIVISION
