from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'UsaMlsMixin'
]


class UsaFootballMixin(FootballMixinBase):
    country = Country.USA


class UsaMlsMixin(UsaFootballMixin):
    category = Category.USA__MLS
