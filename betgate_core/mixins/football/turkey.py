from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'TurkeySuperLeagueMixin'
]


class TurkeyFootballMixin(FootballMixinBase):
    country = Country.TURKEY


class TurkeySuperLeagueMixin(TurkeyFootballMixin):
    category = Category.TURKEY__SUPER_LEAGUE
