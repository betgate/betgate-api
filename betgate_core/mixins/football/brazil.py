from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'BrazilSerieBMixin',
    'BrazilSerieAMixin'
]


class BrazilFootballMixin(FootballMixinBase):
    country = Country.BRAZIL


class BrazilSerieBMixin(BrazilFootballMixin):
    category = Category.BRAZIL__SERIE_B


class BrazilSerieAMixin(BrazilFootballMixin):
    category = Category.BRAZIL__SERIE_A
