from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'PolandEkstraklasaMixin'
]


class PolandFootballMixin(FootballMixinBase):
    country = Country.POLAND


class PolandEkstraklasaMixin(PolandFootballMixin):
    category = Category.POLAND__EKSTRAKLASA
