from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'JapanJLeague1Mixin'
]


class JapanFootballMixin(FootballMixinBase):
    country = Country.JAPAN


class JapanJLeague1Mixin(JapanFootballMixin):
    category = Category.JAPAN__J_LEAGUE1
