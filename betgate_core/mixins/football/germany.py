from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'GermanyBundesliga2Mixin',
    'GermanyBundesligaMixin'
]


class GermanyFootballMixin(FootballMixinBase):
    country = Country.GERMANY


class GermanyBundesliga2Mixin(GermanyFootballMixin):
    category = Category.GERMANY__BUNDESLIGA2


class GermanyBundesligaMixin(GermanyFootballMixin):
    category = Category.GERMANY__BUNDESLIGA
