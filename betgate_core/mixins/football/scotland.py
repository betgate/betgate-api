from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = ['ScotlandPremiershipMixin']


class ScotlandFootballMixin(FootballMixinBase):
    country = Country.SCOTLAND


class ScotlandPremiershipMixin(ScotlandFootballMixin):
    category = Category.SCOTLAND__PREMIERSHIP
