from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'SwedenAllsvenskanMixin',
    'SwedenSuperettanMixin'
]


class SwedenFootballMixin(FootballMixinBase):
    country = Country.SWEDEN


class SwedenAllsvenskanMixin(SwedenFootballMixin):
    category = Category.SWEDEN__ALLSVENSKAN


class SwedenSuperettanMixin(SwedenFootballMixin):
    category = Category.SWEDEN__SUPERETTAN
