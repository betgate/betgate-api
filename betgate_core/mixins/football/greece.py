from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'GreeceSuperLeagueMixin'
]


class GreeceFootballMixin(FootballMixinBase):
    country = Country.GREECE


class GreeceSuperLeagueMixin(GreeceFootballMixin):
    category = Category.GREECE__SUPER_LEAGUE
