from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'NorwayEliteserienMixin'
]


class NorwayFootballMixin(FootballMixinBase):
    country = Country.NORWAY


class NorwayEliteserienMixin(NorwayFootballMixin):
    category = Category.NORWAY__ELITESERIEN
