from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'ClubsInternationalChampionsLeagueMixin',
    'ClubsInternationalEuropeLeagueMixin'
]


class ClubsInternationalFootballMixin(FootballMixinBase):
    country = Country.CLUBS_INTERNATIONAL


class ClubsInternationalChampionsLeagueMixin(ClubsInternationalFootballMixin):
    category = Category.CLUBS_INTERNATIONAL__CHAMPIONS_LEAGUE


class ClubsInternationalEuropeLeagueMixin(ClubsInternationalFootballMixin):
    category = Category.CLUBS_INTERNATIONAL__EUROPE_LEAGUE
