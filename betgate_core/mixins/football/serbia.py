from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'SerbiaSuperLigaMixin'
]


class SerbiaFootballMixin(FootballMixinBase):
    country = Country.SERBIA


class SerbiaSuperLigaMixin(SerbiaFootballMixin):
    category = Category.SERBIA__SUPER_LIGA
