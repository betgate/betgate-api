from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'FranceLigue2Mixin',
    'FranceLigue1Mixin'
]


class FranceFootballMixin(FootballMixinBase):
    country = Country.FRANCE


class FranceLigue2Mixin(FranceFootballMixin):
    category = Category.FRANCE__LIGUE2


class FranceLigue1Mixin(FranceFootballMixin):
    category = Category.FRANCE__LIGUE1
