from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'SwitzerlandSuperLeagueMixin'
]


class SwitzerlandFootballMixin(FootballMixinBase):
    country = Country.SWITZERLAND


class SwitzerlandSuperLeagueMixin(SwitzerlandFootballMixin):
    category = Category.SWITZERLAND__SUPER_LEAGUE
