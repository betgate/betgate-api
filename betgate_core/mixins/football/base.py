from betgate_core.models.football.countries import COUNTRY_INFO_DICT


class FootballMixinBase:
    country = None

    @property
    def teams(self):
        return COUNTRY_INFO_DICT[self.country]["teams"]
