from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'ItalySerieBMixin',
    'ItalySerieAMixin'
]


class ItalyFootballMixin(FootballMixinBase):
    country = Country.ITALY


class ItalySerieBMixin(ItalyFootballMixin):
    category = Category.ITALY__SERIE_B


class ItalySerieAMixin(ItalyFootballMixin):
    category = Category.ITALY__SERIE_A
