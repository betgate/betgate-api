from betgate_core.mixins.football.base import FootballMixinBase
from betgate_models.enums import Category, Country


__all__ = [
    'EnglandPremierLeagueMixin',
    'EnglandChampionshipMixin',
    'EnglandLeagueTwoMixin',
    'EnglandNationalLeagueMixin',
    'EnglandLeagueOneMixin',
    'EnglandNationalLeagueNorthMixin'
]


class EnglandFootballMixin(FootballMixinBase):
    country = Country.ENGLAND


class EnglandPremierLeagueMixin(EnglandFootballMixin):
    category = Category.ENGLAND__PREMIER_LEAGUE


class EnglandChampionshipMixin(EnglandFootballMixin):
    category = Category.ENGLAND__CHAMPIONSHIP


class EnglandLeagueTwoMixin(EnglandFootballMixin):
    category = Category.ENGLAND__LEAGUE_TWO


class EnglandNationalLeagueMixin(EnglandFootballMixin):
    category = Category.ENGLAND__NATIONAL_LEAGUE


class EnglandLeagueOneMixin(EnglandFootballMixin):
    category = Category.ENGLAND__LEAGUE_ONE


class EnglandNationalLeagueNorthMixin(EnglandFootballMixin):
    category = Category.ENGLAND__NATIONAL_LEAGUE_NORTH
