from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import IcelandUrvalsdeildMixin


class Urvalsdeild(IcelandUrvalsdeildMixin, FootballWebBase):
    category_id = 887158
    competition_event_id = 26699733
    category_name = "Urvalsdeild"
