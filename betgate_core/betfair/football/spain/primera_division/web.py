from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import SpainPrimeraDivisionMixin


class PrimeraDivision(SpainPrimeraDivisionMixin, FootballWebBase):
    category_id = "117"
    competition_event_id = "259241"
    category_name = "La Liga Española"
