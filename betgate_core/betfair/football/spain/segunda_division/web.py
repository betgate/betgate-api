from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import SpainSegundaDivisionMixin


class SegundaDivision(SpainSegundaDivisionMixin, FootballWebBase):
    category_id = "12204313"
    competition_event_id = "425190"
    category_name = "Segunda División"
