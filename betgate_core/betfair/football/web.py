from sdklib.shortcuts import cache
from sdklib.http.headers import REFERRER_HEADER_NAME
from betgate_models.models import Odd, Event, BookieId
from betgate_models.enums import Market, Category, Bookie
from betgate_core.models.markets import is_valid_odd
from betgate_core.betfair.web import BetfairWebBase


class FootballWebBase(BetfairWebBase):

    FOOTBALL_URL = "/sport/football"
    FOOTBALL_EVENTS_URL = "/sport/football/event"

    category_id = None
    competition_event_id = None

    main_markets = {
        "Cuotas de partido": [Market.FT_WIN_1, Market.FT_WIN_X, Market.FT_WIN_2],
        "Apuesta sin Empate": [Market.FT_WIN_DNB_1, Market.FT_WIN_DNB_2],
        "Empate no Apuesta al Descanso": [Market.FH_WIN_DNB_1, Market.FH_WIN_DNB_2],
        "Córners Más/Menos 0.5": [Market.FT_CORNERS_OVER_0_5, Market.FT_CORNERS_UNDER_0_5],
        "Córners Más/Menos 1.5": [Market.FT_CORNERS_OVER_1_5, Market.FT_CORNERS_UNDER_1_5],
        "Córners Más/Menos 2.5": [Market.FT_CORNERS_OVER_2_5, Market.FT_CORNERS_UNDER_2_5],
        "Córners Más/Menos 3.5": [Market.FT_CORNERS_OVER_3_5, Market.FT_CORNERS_UNDER_3_5],
        "Córners Más/Menos 4.5": [Market.FT_CORNERS_OVER_4_5, Market.FT_CORNERS_UNDER_4_5],
        "Córners Más/Menos 5.5": [Market.FT_CORNERS_OVER_5_5, Market.FT_CORNERS_UNDER_5_5],
        "Córners Más/Menos 6.5": [Market.FT_CORNERS_OVER_6_5, Market.FT_CORNERS_UNDER_6_5],
        "Córners Más/Menos 7.5": [Market.FT_CORNERS_OVER_7_5, Market.FT_CORNERS_UNDER_7_5],
        "Córners Más/Menos 8.5": [Market.FT_CORNERS_OVER_8_5, Market.FT_CORNERS_UNDER_8_5],
        "Córners Más/Menos 9.5": [Market.FT_CORNERS_OVER_9_5, Market.FT_CORNERS_UNDER_9_5],
        "Córners Más/Menos 10.5": [Market.FT_CORNERS_OVER_10_5, Market.FT_CORNERS_UNDER_10_5],
        "Córners Más/Menos 11.5": [Market.FT_CORNERS_OVER_11_5, Market.FT_CORNERS_UNDER_11_5],
        "Córners Más/Menos 12.5": [Market.FT_CORNERS_OVER_12_5, Market.FT_CORNERS_UNDER_12_5],
        "Córners Más/Menos 13.5": [Market.FT_CORNERS_OVER_13_5, Market.FT_CORNERS_UNDER_13_5],
        "Córners Más/Menos 14.5": [Market.FT_CORNERS_OVER_14_5, Market.FT_CORNERS_UNDER_14_5],
        "Córners Más/Menos 15.5": [Market.FT_CORNERS_OVER_15_5, Market.FT_CORNERS_UNDER_15_5],
        "Córners Más/Menos 16.5": [Market.FT_CORNERS_OVER_16_5, Market.FT_CORNERS_UNDER_16_5],
        "Córners Más/Menos 17.5": [Market.FT_CORNERS_OVER_17_5, Market.FT_CORNERS_UNDER_17_5],
        "Córners Más/Menos 18.5": [Market.FT_CORNERS_OVER_18_5, Market.FT_CORNERS_UNDER_18_5],
        "Córners Más/Menos 19.5": [Market.FT_CORNERS_OVER_19_5, Market.FT_CORNERS_UNDER_19_5],
        "Córners Más/Menos 20.5": [Market.FT_CORNERS_OVER_20_5, Market.FT_CORNERS_UNDER_20_5],
    }

    secondary_markets = {
        "¿Marcarán ambos equipos?": [Market.FT_BTTS_YES, Market.FT_BTTS_NO],
        "Doble oportunidad": [Market.FT_WIN_1X, Market.FT_WIN_X2, Market.FT_WIN_12],
        "Doble oportunidad de la 1.ª parte": [Market.FH_WIN_1X, Market.FH_WIN_X2, Market.FH_WIN_12]
    }

    tertiary_markets = {
        ("Mercados de Resultado en el Descanso", "Primer tiempo"): [Market.FH_WIN_1, Market.FH_WIN_X, Market.FH_WIN_2],
        ("Más/Menos", "0.5"): [Market.FT_TG_OVER_05, Market.FT_TG_UNDER_05],
        ("Más/Menos", "1.5"): [Market.FT_TG_OVER_15, Market.FT_TG_UNDER_15],
        ("Más/Menos", "2.5"): [Market.FT_TG_OVER_25, Market.FT_TG_UNDER_25],
        ("Más/Menos", "3.5"): [Market.FT_TG_OVER_35, Market.FT_TG_UNDER_35],
        ("Más/Menos", "4.5"): [Market.FT_TG_OVER_45, Market.FT_TG_UNDER_45],
        ("Más/Menos Primera Parte", "0.5"): [Market.FH_TG_OVER_05, Market.FH_TG_UNDER_05],
        ("Más/Menos Primera Parte", "1.5"): [Market.FH_TG_OVER_15, Market.FH_TG_UNDER_15],
        ("Más/Menos Primera Parte", "2.5"): [Market.FH_TG_OVER_25, Market.FH_TG_UNDER_25],
        ("Más/Menos Primera Parte", "3.5"): [Market.FH_TG_OVER_35, Market.FH_TG_UNDER_35],
        ("Más/Menos Primera Parte", "4.5"): [Market.FH_TG_OVER_45, Market.FH_TG_UNDER_45],
    }

    quaternary_markets = {}

    results_markets = {
        ("Marcador Final", "0 - 0"): Market.FT_RESULT_0_0,
        ("Marcador al descanso", "0 - 0"): Market.FH_RESULT_0_0,
    }

    @property
    def category_identifier(self):
        return self.category_name

    def __init__(self):
        super(FootballWebBase, self).__init__()
        if self.category_id is None:
            raise BaseException("You must define a 'category_id' attribute in your {0}.{1} class.".format(
                type(self).__module__, type(self).__name__
            ))
        if self.competition_event_id is None:
            raise BaseException("You must define a 'competition_event_id' attribute in your {0}.{1} class.".format(
                type(self).__module__, type(self).__name__
            ))

    def default_headers(self):
        headers = super(FootballWebBase, self).default_headers()
        headers[REFERRER_HEADER_NAME] = "{0}{1}".format(self.DEFAULT_HOST, self.FOOTBALL_URL)
        return headers

    @cache(maxsize=None)
    def request_football_category(self):
        query_params = {
            "id": self.category_id,
            "competitionEventId": self.competition_event_id,
            "action": "loadCompetition",
            "selectedTabType": "COMPETITION",
            "modules": "multipickavbId"
        }
        return self.get(self.FOOTBALL_URL, query_params=query_params)

    def _lazy_get_event_detail(self, elem, validate=True):
        try:
            category = self.get_category()
            home_tname, away_tname = self.get_team_names_from_list_view(elem)
            dtime = self.get_datetime_from_list_view(elem)
            event_id = elem.get("data-eventid")
            event = Event(
                bookies={str(Bookie.BETFAIR.value): BookieId(bookie=Bookie.BETFAIR, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                html_source=elem,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    def lazy_get_category_events(self, **kwargs):
        validate = kwargs.pop("validate", True)
        events = []
        try:
            res = self.request_football_category()
            for elem in res.html.find_elements_by_xpath("//*[@data-eventid and contains(@class, 'event-information')]"):
                event_detail = self._lazy_get_event_detail(elem, validate=validate)
                if event_detail is not None:
                    events.append(event_detail)
        except:
            pass

        return events

    def _get_main_markets_from_event_detail(self, html):
        markets_to_return = []
        for text, markets in self.main_markets.items():
            try:
                elem = html.find_element_by_xpath(
                    "//div[@class='mod-minimarketview mod-minimarketview-minimarketview' and "
                    ".//span[normalize-space() = '%s' and @class='title']]" % text)
                if elem is not None:
                    elems = elem.find_elements_by_xpath(".//a/span")
                    for elem, mkt in zip(elems, markets):
                        if is_valid_odd(elem.text):
                            markets_to_return.append(Odd(value=elem.text, market=mkt, bookie=Bookie.BETFAIR))
            except:
                pass
        return markets_to_return

    def _get_secondary_markets_from_event_detail(self, html):
        markets_to_return = []
        try:
            for text, markets in self.secondary_markets.items():
                elem = html.find_element_by_xpath("//li[@title='%s']" % text)
                if elem is not None:
                    elems = elem.find_elements_by_xpath(".//a/span")
                    for elem, mkt in zip(elems, markets):
                        if is_valid_odd(elem.text):
                            markets_to_return.append(Odd(value=elem.text, market=mkt, bookie=Bookie.BETFAIR))
        except:
            pass
        return markets_to_return

    def _get_market_id(self, elem):
        classes = elem.get("class").split()
        for cls in classes:
            if cls.startswith("ui-"):
                return cls.replace("ui-", "").replace("_", ".")

    def _get_tertiary_markets_from_event_detail(self, html):
        markets_to_return = []
        for (title, span_text), markets in self.tertiary_markets.items():
            try:
                elem = html.find_element_by_xpath(f"//span[normalize-space() = '{title}' and @class='title']")
                antecesor = elem.getparent(height=5)
                elem = antecesor.find_element_by_xpath(
                    f".//span[text() = '\n{span_text}\n' and contains(@class, 'market-name')]")
                market_id = self._get_market_id(elem)
                elems = antecesor.find_elements_by_xpath(f".//a[@data-marketid='{market_id}']/span")
                for elem, mkt in zip(elems, markets):
                    if is_valid_odd(elem.text):
                        markets_to_return.append(Odd(value=elem.text, market=mkt, bookie=Bookie.BETFAIR))
            except:
                pass
        return markets_to_return

    def _get_quaternary_markets_from_event_detail(self, html):
        markets_to_return = []
        try:
            for (title, span_text), market in self.quaternary_markets.items():
                elem = html.find_element_by_xpath(f"//span[normalize-space() = '{title}' and @class='title']")
                if elem is not None:
                    parent_7 = elem.getparent(height=7)
                    elem = parent_7.find_element_by_xpath(f".//span[@title='{span_text}']")
                    if elem is not None:
                        parent = elem.getparent()
                        elem = parent.find_element_by_xpath(".//a/span")
                        if elem is not None and is_valid_odd(elem.text):
                            markets_to_return.append(Odd(value=elem.text, market=market, bookie=Bookie.BETFAIR))
        except:
            pass
        return markets_to_return

    def _get_results_markets_from_event_detail(self, html):
        markets_to_return = []
        try:
            for (title, span_text), market in self.results_markets.items():
                elem = html.find_element_by_xpath("//span[normalize-space() = '%s' and @class='title']" % title)

                if elem is not None:
                    parent = elem.getparent(height=6)
                    elem = parent.find_element_by_xpath(
                        ".//span[normalize-space() = '%s' and @class='runner-name']" % span_text)
                    if elem is not None:
                        parent = elem.getparent()
                        elem = parent.find_element_by_xpath(".//a/span")
                        if elem is not None and is_valid_odd(elem.text):
                            markets_to_return.append(Odd(value=elem.text, market=market, bookie=Bookie.BETFAIR))
        except:
            pass
        return markets_to_return

    def get_markets(self, event_id=None, html=None):
        if event_id is None:
            raise BaseException("'event_id' can't be equal to None")
        elif html is None:
            html = self.request_event_detail(event_id).html

        markets_to_return = []
        data_mod = self._get_data_mod(html)
        if data_mod is not None:
            res = self.request_all_market_prices(event_id, data_mod)
            markets_to_return.extend(self._get_main_markets_from_event_detail(res.html))
            markets_to_return.extend(self._get_secondary_markets_from_event_detail(res.html))
            markets_to_return.extend(self._get_tertiary_markets_from_event_detail(res.html))
            markets_to_return.extend(self._get_quaternary_markets_from_event_detail(res.html))
            markets_to_return.extend(self._get_results_markets_from_event_detail(res.html))
        return markets_to_return

    @cache(maxsize=None)
    def request_all_market_prices(self, event_id, data_mod):
        query_params = {
            "eventId": event_id,
            "selectedGroup": "all_markets",
            "modules": "marketgroups@{0}".format(data_mod),
            "action": "changeMarketGroup",
            "selectedTabType": "market"
        }
        return self.get(self.FOOTBALL_EVENTS_URL, query_params=query_params)

    def get_team_names_from_list_view(self, html):
        try:
            elems = html.find_elements_by_xpath(".//span[@class='team-name']")
            return elems[0].text.strip(), elems[1].text.strip()
        except:
            return None, None

    def get_team_names_from_detail_view(self, html):
        try:
            home_tname = html.find_element_by_xpath("//tr[@class='home-header']/td[@class='home-runner']").text.strip()
            away_tname = html.find_element_by_xpath("//tr[@class='away-header']/td[@class='away-runner']").text.strip()
            return home_tname, away_tname
        except:
            return None, None

    def get_datetime_from_list_view(self, html):
        try:
            date_time_text = html.find_element_by_xpath(".//span[@class='date ui-countdown']").text.strip()
            return self.get_datetime(date_time_text)
        except:
            return None

    def get_datetime_from_detail_view(self, html):
        try:
            date_time_text = html.find_element_by_xpath(
                "//div[@class='generic-match-status']//span[@class='ui-countdown ']").text.strip()
            return self.get_datetime(date_time_text)
        except:
            return None

    @cache(maxsize=None)
    def request_event_detail(self, event_id):
        return self.get(self.FOOTBALL_EVENTS_URL, query_params={"eventId": event_id})

    def get_event(self, event_id, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            html = self.request_event_detail(event_id).html
            home_tname, away_tname = self.get_team_names_from_detail_view(html)
            category = self.get_category()
            dtime = self.get_datetime_from_detail_view(html)
            markets = self.get_markets(event_id=event_id, html=html)
            event = Event(
                bookies={str(Bookie.BETFAIR.value): BookieId(bookie=Bookie.BETFAIR, value=event_id)},
                home_team=self.get_team(home_tname),
                home_team_name=home_tname,
                away_team=self.get_team(away_tname),
                away_team_name=away_tname,
                category=category,
                start=dtime,
                odds=markets
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    def get_category_identifier(self, event_id):
        r_html = self.request_event_detail(event_id).html
        elem = r_html.find_element_by_xpath("//*[@class='ui-nav competition-info']/span")
        if elem is not None:
            return elem.text.strip()


class FootballWeb(FootballWebBase):
    category = Category.UNKNOWN
    category_id = -1
    category_name = ""
    competition_event_id = -1
