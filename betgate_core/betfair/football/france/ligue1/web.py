from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import FranceLigue1Mixin


class Ligue1(FranceLigue1Mixin, FootballWebBase):
    category_id = "55"
    competition_event_id = "268416"
    category_name = "Ligue 1 Francesa"
