from betgate_core.betfair.football.france.ligue1.web import Ligue1


def get_events(**kwargs):
    with Ligue1() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Ligue1() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Ligue1() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
