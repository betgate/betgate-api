from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import FranceLigue2Mixin


class Ligue2(FranceLigue2Mixin, FootballWebBase):
    category_id = "57"
    competition_event_id = "425029"
    category_name = "French Ligue 2"
