from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import BelgiumFirstDivisionAMixin


class FirstDivisionA(BelgiumFirstDivisionAMixin, FootballWebBase):
    category_id = "89979"
    competition_event_id = "645494"
    category_name = "Belgian First Division A"
