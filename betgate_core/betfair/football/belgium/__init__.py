from betgate_core.betfair.football.belgium import first_division_a


def get_events(**kwargs):
    return first_division_a.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return first_division_a.lazy_get_events(**kwargs)
