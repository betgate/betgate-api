from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import SwitzerlandSuperLeagueMixin


class SuperLeague(SwitzerlandSuperLeagueMixin, FootballWebBase):
    category_id = "133"
    competition_event_id = "277000"
    category_name = "Swiss Super League"
