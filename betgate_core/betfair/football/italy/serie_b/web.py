from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import ItalySerieBMixin


class SerieB(ItalySerieBMixin, FootballWebBase):
    category_id = "12199689"
    competition_event_id = ""
    category_name = "Serie B"
