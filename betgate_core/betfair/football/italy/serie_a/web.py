from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import ItalySerieAMixin


class SerieA(ItalySerieAMixin, FootballWebBase):
    category_id = "81"
    competition_event_id = "287610"
    category_name = "Serie A Italiana"
