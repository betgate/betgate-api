from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import GermanyBundesligaMixin


class Bundesliga(GermanyBundesligaMixin, FootballWebBase):
    category_id = 59
    competition_event_id = 605621
    category_name = "Bundesliga 1 Alemana"
