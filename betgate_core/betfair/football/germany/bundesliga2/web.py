from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import GermanyBundesliga2Mixin


class Bundesliga2(GermanyBundesliga2Mixin, FootballWebBase):
    category_id = "61"
    competition_event_id = "287610"
    category_name = "Bundesliga 2 Alemana"
