from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import MoroccoBotolaMixin


class Botola(MoroccoBotolaMixin, FootballWebBase):
    category_id = 2609677
    competition_event_id = 28470041
    category_name = "Moroccan Botola Pro 1"
