from betgate_core.betfair.football.morocco import botola


def get_events(**kwargs):
    return botola.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return botola.lazy_get_events(**kwargs)
