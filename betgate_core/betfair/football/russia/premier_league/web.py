from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import RussiaPremierLeagueMixin


class PremierLeague(RussiaPremierLeagueMixin, FootballWebBase):
    category_id = "101"
    competition_event_id = "682399"
    category_name = "Russian Premier League"
