from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import RussiaFnlMixin


class Fnl(RussiaFnlMixin, FootballWebBase):
    category_id = -1
    competition_event_id = -1
    category_name = "Russian Football National League"
