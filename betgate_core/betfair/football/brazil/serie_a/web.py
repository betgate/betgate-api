from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import BrazilSerieAMixin


class SerieA(BrazilSerieAMixin, FootballWebBase):
    category_id = 13
    competition_event_id = 268489
    category_name = "Serie A Brasileirao"
