from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import BrazilSerieBMixin


class SerieB(BrazilSerieBMixin, FootballWebBase):
    category_id = 321319
    competition_event_id = 26484647
    category_name = "Brazilian Serie B"
