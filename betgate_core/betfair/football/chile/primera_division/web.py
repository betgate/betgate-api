from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import ChilePrimeraDivisionMixin


class PrimeraDivision(ChilePrimeraDivisionMixin, FootballWebBase):
    category_id = "744098"
    competition_event_id = "26647705"
    category_name = "Chilean Primera Division"
