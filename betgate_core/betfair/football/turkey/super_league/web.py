from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import TurkeySuperLeagueMixin


class SuperLeague(TurkeySuperLeagueMixin, FootballWebBase):
    category_id = 194215
    competition_event_id = 290769
    category_name = "Super League Turca"
