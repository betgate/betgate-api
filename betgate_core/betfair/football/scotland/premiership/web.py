from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import ScotlandPremiershipMixin


class Premiership(ScotlandPremiershipMixin, FootballWebBase):
    category_id = "105"
    competition_event_id = "44847"
    category_name = "Premiership Escocesa"
