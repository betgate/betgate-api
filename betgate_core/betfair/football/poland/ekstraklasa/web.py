from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import PolandEkstraklasaMixin


class Ekstraklasa(PolandEkstraklasaMixin, FootballWebBase):
    category_id = 97
    competition_event_id = 3053420
    category_name = "Ekstraklasa Polaca"
