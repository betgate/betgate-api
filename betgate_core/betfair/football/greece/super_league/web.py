from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import GreeceSuperLeagueMixin


class SuperLeague(GreeceSuperLeagueMixin, FootballWebBase):
    category_id = 67
    competition_event_id = 296079
    category_name = "Superliga Griega"
