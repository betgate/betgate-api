from betgate_core.betfair.football.ukraine import premier_league


def get_events(**kwargs):
    return premier_league.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return premier_league.lazy_get_events(**kwargs)
