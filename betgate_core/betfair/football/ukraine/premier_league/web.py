from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import UkrainePremierLeagueMixin


class PremierLeague(UkrainePremierLeagueMixin, FootballWebBase):
    category_id = "139"
    competition_event_id = 28664927
    category_name = "Ukraine Premier League"
