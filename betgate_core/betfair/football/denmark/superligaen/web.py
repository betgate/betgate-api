from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import DenmarkSuperligaenMixin


class Superligaen(DenmarkSuperligaenMixin, FootballWebBase):
    category_id = 23
    competition_event_id = -1
    category_name = "Superliga Danesa"
