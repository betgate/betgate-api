from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import NetherlandsEersteDivisieMixin


class EersteDivisie(NetherlandsEersteDivisieMixin, FootballWebBase):
    category_id = "11"
    competition_event_id = ""
    category_name = "Eerste Divisie Holandesa"
