from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import NetherlandsEredivisieMixin


class Eredivisie(NetherlandsEredivisieMixin, FootballWebBase):
    category_id = "9404054"
    competition_event_id = "3209064"
    category_name = "Eredivisie Holandesa"
