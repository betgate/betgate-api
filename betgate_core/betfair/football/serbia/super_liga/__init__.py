from betgate_core.betfair.football.serbia.super_liga.web import SuperLiga


def get_events(**kwargs):
    with SuperLiga() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with SuperLiga() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with SuperLiga() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
