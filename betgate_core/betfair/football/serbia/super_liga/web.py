from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import SerbiaSuperLigaMixin


class SuperLiga(SerbiaSuperLigaMixin, FootballWebBase):
    category_id = 103
    competition_event_id = -1
    category_name = "Serbian Super League"
