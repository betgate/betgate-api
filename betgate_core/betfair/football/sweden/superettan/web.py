from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import SwedenSuperettanMixin


class Superettan(SwedenSuperettanMixin, FootballWebBase):
    category_id = 12202373
    competition_event_id = 29122325
    category_name = "Swedish Superettan"
