from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import SwedenAllsvenskanMixin


class Allsvenskan(SwedenAllsvenskanMixin, FootballWebBase):
    category_id = "129"
    competition_event_id = "294466"
    category_name = "Allsvenskan"
