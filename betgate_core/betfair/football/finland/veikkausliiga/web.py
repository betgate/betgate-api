from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import FinlandVeikkausliigaMixin


class Veikkausliiga(FinlandVeikkausliigaMixin, FootballWebBase):
    category_id = 45
    competition_event_id = 485021
    category_name = "Finnish Veikkausliiga finlandesa"
