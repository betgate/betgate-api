from betgate_core.betfair.football.finland import veikkausliiga


def get_events(**kwargs):
    return veikkausliiga.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return veikkausliiga.lazy_get_events(**kwargs)
