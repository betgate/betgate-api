from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import ClubsInternationalEuropeLeagueMixin


class EuropeLeague(ClubsInternationalEuropeLeagueMixin, FootballWebBase):
    category_id = 2005
    competition_event_id = -1
    category_name = "UEFA Europa League"
