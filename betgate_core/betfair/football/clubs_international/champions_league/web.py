from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import ClubsInternationalChampionsLeagueMixin


class ChampionsLeague(ClubsInternationalChampionsLeagueMixin, FootballWebBase):
    category_id = 228
    competition_event_id = -1
    category_name = "UEFA Champions League"
