from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import CroatiaFirstLeagueMixin


class FirstLeague(CroatiaFirstLeagueMixin, FootballWebBase):
    category_id = "17"
    competition_event_id = "779400"
    category_name = "Croatian 1 HNL"
