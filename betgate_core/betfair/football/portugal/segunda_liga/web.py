from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import PortugalSegundaLigaMixin


class SegundaLiga(PortugalSegundaLigaMixin, FootballWebBase):
    category_id = 9513
    competition_event_id = 695561
    category_name = "Portuguese Segunda Liga"
