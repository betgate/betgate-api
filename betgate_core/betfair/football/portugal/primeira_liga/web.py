from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import PortugalPrimeiraLigaMixin


class PrimeiraLiga(PortugalPrimeiraLigaMixin, FootballWebBase):
    category_id = "99"
    competition_event_id = "269462"
    category_name = "Primeira Liga Portuguesa"
