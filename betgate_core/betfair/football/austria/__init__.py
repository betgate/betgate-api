from betgate_core.betfair.football.austria import bundesliga


def get_events(**kwargs):
    return bundesliga.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return bundesliga.lazy_get_events(**kwargs)
