from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import AustriaBundesligaMixin


class Bundesliga(AustriaBundesligaMixin, FootballWebBase):
    category_id = 10479956
    competition_event_id = -1
    category_name = "Bundesliga Austriaca"
