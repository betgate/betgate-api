from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import EcuadorSerieAMixin


class SerieA(EcuadorSerieAMixin, FootballWebBase):
    category_id = 803690
    competition_event_id = 26669832
    category_name = "Ecuadorian Serie A"
