from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import NorwayEliteserienMixin


class Eliteserien(NorwayEliteserienMixin, FootballWebBase):
    category_id = 11068551
    competition_event_id = 28166295
    category_name = "Eliteserien Noruega"
