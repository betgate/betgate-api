from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import JapanJLeague1Mixin


class JLeague1(JapanJLeague1Mixin, FootballWebBase):
    category_id = 89
    competition_event_id = 264751
    category_name = "Japanese J League"
