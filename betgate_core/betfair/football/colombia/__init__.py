from betgate_core.betfair.football.colombia import primera_a


def get_events(**kwargs):
    return primera_a.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return primera_a.lazy_get_events(**kwargs)
