from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import ColombiaPrimeraAMixin


class PrimeraA(ColombiaPrimeraAMixin, FootballWebBase):
    category_id = "844197"
    competition_event_id = "26670500"
    category_name = "Primera A Colombiana"
