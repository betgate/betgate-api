from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandNationalLeagueMixin


class NationalLeague(EnglandNationalLeagueMixin, FootballWebBase):
    category_id = "11086347"
    competition_event_id = "2912033"
    category_name = "National League Inglesa"
