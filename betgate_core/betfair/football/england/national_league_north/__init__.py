from betgate_core.betfair.football.england.national_league_north.web import NationalLeagueNorth


def get_events(**kwargs):
    with NationalLeagueNorth() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with NationalLeagueNorth() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with NationalLeagueNorth() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
