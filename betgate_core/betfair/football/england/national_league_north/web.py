from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandNationalLeagueNorthMixin


class NationalLeagueNorth(EnglandNationalLeagueNorthMixin, FootballWebBase):
    category_id = 41
    competition_event_id = 3138756
    category_name = "National League Inglesa Norte"
