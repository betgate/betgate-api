from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandLeagueOneMixin


class LeagueOne(EnglandLeagueOneMixin, FootballWebBase):
    category_id = 35
    competition_event_id = "779400"
    category_name = "League 1 Inglesa"
