from betgate_core.betfair.football.england.championship.web import Championship


def get_events(**kwargs):
    with Championship() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Championship() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Championship() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
