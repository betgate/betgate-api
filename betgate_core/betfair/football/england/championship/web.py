from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandChampionshipMixin


class Championship(EnglandChampionshipMixin, FootballWebBase):
    category_id = 7129730
    competition_event_id = 1908053
    category_name = "Championship Inglesa"
