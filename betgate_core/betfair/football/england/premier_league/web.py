from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandPremierLeagueMixin


class PremierLeague(EnglandPremierLeagueMixin, FootballWebBase):
    category_id = "10932509"
    competition_event_id = "2022802"
    category_name = "Premier League Inglesa"
