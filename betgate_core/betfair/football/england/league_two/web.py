from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandLeagueTwoMixin


class LeagueTwo(EnglandLeagueTwoMixin, FootballWebBase):
    category_id = 37
    competition_event_id = 1908056
    category_name = "League 2 Inglesa"
