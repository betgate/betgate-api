from betgate_core.betfair.football.web import FootballWebBase
from betgate_core.mixins.football import UsaMlsMixin


class Mls(UsaMlsMixin, FootballWebBase):
    category_id = 141
    competition_event_id = 28602424
    category_name = "US Major League Soccer"
