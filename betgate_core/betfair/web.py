from datetime import datetime, date, timedelta
from urllib.parse import parse_qs
import pytz
from sdklib.util.urls import urlsplit
from betgate_models.enums import Bookie
from betgate_core.web import BookieWebBase
from betgate_core.utils import locale_month_to_month


class BetfairWebBase(BookieWebBase):

    DEFAULT_HOST = "https://www.betfair.es"

    url_path_params = {"lang": "en"}

    MARKETS_API_URL = "/www/sports/fixedodds/readonly/v1/getMarketPrices"

    category_name = None
    bookie = Bookie.BETFAIR

    def __init__(self):
        super(BetfairWebBase, self).__init__()
        if self.category_name is None:
            raise BaseException("You must define a 'category_name' attribute in your {0}.{1} class.".format(
                type(self).__module__, type(self).__name__
            ))

    def default_headers(self):
        headers = super(BetfairWebBase, self).default_headers()
        return headers

    def get_datetime(self, date_time_text):
        try:
            strip_text = date_time_text.replace("\\n", "").replace(",", "").strip()
            split_date_time = strip_text.split()
            if len(split_date_time) == 3 and ":" in split_date_time[2]:
                day = int(split_date_time[0])
                month = locale_month_to_month(split_date_time[1])
                split_time = split_date_time[2].split(":")
                hour = int(split_time[0])
                minute = int(split_time[1])
                year = datetime.now().year
            elif len(split_date_time) == 2 and split_date_time[0] == "Mañana" and ":" in split_date_time[1]:
                tomorrow = date.today() + timedelta(days=1)
                day = tomorrow.day
                month = tomorrow.month
                year = tomorrow.year
                split_time = split_date_time[1].split(":")
                hour = int(split_time[0])
                minute = int(split_time[1])
            elif len(split_date_time) == 1 and ":" in split_date_time[0]:
                day = datetime.now().day
                month = datetime.now().month
                year = datetime.now().year
                split_time = split_date_time[0].split(":")
                hour = int(split_time[0])
                minute = int(split_time[1])
            elif split_date_time[:2] == ['Comienza', 'en']:
                return
            elif len(split_date_time) == 4 and split_date_time[:3] == ['Comienza', 'a', 'las'] and \
                    ":" in split_date_time[3]:
                day = datetime.now().day
                month = datetime.now().month
                year = datetime.now().year
                split_time = split_date_time[3].split(":")
                hour = int(split_time[0])
                minute = int(split_time[1])
            elif len(split_date_time) == 4 and ":" in split_date_time[3]:
                day = int(split_date_time[1])
                month = locale_month_to_month(split_date_time[2])
                year = datetime.now().year
                split_time = split_date_time[3].split(":")
                hour = int(split_time[0])
                minute = int(split_time[1])
            else:
                return
            dtime = datetime(year=year, month=month, day=day, hour=hour, minute=minute)
            local_tz = pytz.timezone('Europe/Madrid')
            return local_tz.localize(dtime)
        except:
            return

    @staticmethod
    def _get_xsrftoken(html):
        elem = html.find_element_by_xpath("//a[contains(@href, 'xsrftoken')]")
        _, _, _, _, query = urlsplit(elem.get("href"))
        return parse_qs(query)["xsrftoken"][0]

    @staticmethod
    def _get_data_mod(html):
        elem = html.find_element_by_xpath("//*[@data-mod-id and @data-mod-type='marketgroups']")
        return elem.get("data-mod-id") if elem else None
