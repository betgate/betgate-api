import json
from sdklib.http.response import HttpResponse


class LuckiaHttpResponse(HttpResponse):

    @property
    def json(self):
        data = self.urllib3_response.data
        encoded_data = data.decode("utf-8")
        replaced_data = encoded_data.replace(",,", ", null,")
        replaced_data = replaced_data.replace(",,", ", null,")
        return json.loads(replaced_data)
