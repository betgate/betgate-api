import math
from decimal import Decimal
from datetime import datetime
import pytz
from sdklib.shortcuts import cache
from sdklib.http.renderers import FormRenderer
from betgate_models.enums import Bookie
from betgate_core.api import BookieApiBase
from betgate_core.luckia.response import LuckiaHttpResponse


def floor_base(num, base=1.0):
    return Decimal(base * math.floor(float(num)/base)).quantize(Decimal('1.000'))


def round_luckia(value):
    if value > 7:
        rounded = floor_base(value, base=0.25)
    elif value > 2.5:
        rounded = floor_base(value, base=0.05)
    elif value > 1.01:
        rounded = floor_base(value, base=0.01)
    else:
        rounded = floor_base(value, base=0.001)
    return rounded


def calculate_odd(value):
    """
    Calculate luckia odd value.

    :param value:
    :return:
    """
    if value < 0:
        odd = 1 + (-100/value)
    else:
        odd = 1 + value/100
    return round_luckia(odd)


class LuckiaApiBase(BookieApiBase):

    DEFAULT_HOST = "https://sports.luckia.es"
    MOBILE_HOST = "https://m-sports.luckia.es"

    DEFAULT_RENDERER = FormRenderer()
    response_class = LuckiaHttpResponse

    GET_LEAGUES_CONTENT_URL = "/pagemethods.aspx/GetLeaguesContent"
    GET_MASTER_EVENT_FOR_MOBILE_URL = "/pagemethods.aspx/GetMasterEventForMobile"
    UPDATE_EVENTS_URL = "/pagemethods.aspx/UpdateEvents"

    category_id = None
    bookie = Bookie.LUCKIA

    @property
    def category_identifier(self):
        return self.category_id

    def __init__(self):
        super(LuckiaApiBase, self).__init__()
        if self.category_id is None:
            raise BaseException("You must define a 'category_id' attribute in your {0}.{1} class.".format(
                type(self).__module__, type(self).__name__
            ))

    def default_headers(self):
        headers = super(LuckiaApiBase, self).default_headers()
        headers["Requesttarget"] = "AJAXService"
        headers["Origin"] = "https://sports.luckia.es"
        return headers

    @cache(maxsize=None)
    def request_events(self):
        params = {"branchID": "1", "leaguesCollection": self.category_id, "showLive": "true"}
        return self.post(self.GET_LEAGUES_CONTENT_URL, body_params=params)

    def get_category_event_ids(self):
        res = self.request_events()
        try:
            to_return = []
            for evt in res.json[0][0][1]:
                if evt[11] != 0 and evt[12] != 0 and evt[22] != 0:  # skip some invalid events
                    to_return.append(evt[0])
            return to_return
        except:
            return []

    @cache(maxsize=None)
    def request_event_detail(self, event_id):
        params = {"mastereventid": event_id, "_": "1482751783556"}
        return self.post(self.GET_MASTER_EVENT_FOR_MOBILE_URL, body_params=params, host=self.MOBILE_HOST)

    @cache(maxsize=None)
    def request_update_events(self, *market_ids):
        validated_market_ids = set(market_ids)  # remove duplicated
        validated_market_ids.discard(None)  # remove None
        request_string = "@".join([str(mid) for mid in validated_market_ids])
        params = {"requestString": request_string, "_": "1482851483679"}
        return self.post(self.UPDATE_EVENTS_URL, body_params=params, host=self.MOBILE_HOST)

    def get_category_identifier(self, event_id):
        res = self.request_event_detail(event_id)
        return res.json[0][3]

    def get_datetime(self, text):
        dtime = datetime.strptime(text[:19], '%Y-%m-%dT%H:%M:%S')
        return pytz.UTC.localize(dtime, is_dst=True)  # pylint: disable=no-value-for-parameter
