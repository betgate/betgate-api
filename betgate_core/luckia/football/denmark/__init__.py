from betgate_core.luckia.football.denmark import superligaen


def get_events(**kwargs):
    return superligaen.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return superligaen.lazy_get_events(**kwargs)
