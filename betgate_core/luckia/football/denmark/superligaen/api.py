from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import DenmarkSuperligaenMixin


class Superligaen(DenmarkSuperligaenMixin, FootballApiBase):
    category_id = 40816
