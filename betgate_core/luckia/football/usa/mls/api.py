from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import UsaMlsMixin


class Mls(UsaMlsMixin, FootballApiBase):
    category_id = 89345
