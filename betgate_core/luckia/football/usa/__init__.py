from betgate_core.luckia.football.usa import mls


def get_events(**kwargs):
    return mls.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return mls.lazy_get_events(**kwargs)
