from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import CroatiaFirstLeagueMixin


class FirstLeague(CroatiaFirstLeagueMixin, FootballApiBase):
    category_id = 44419
