from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import SerbiaSuperLigaMixin


class SuperLiga(SerbiaSuperLigaMixin, FootballApiBase):
    category_id = 45092
