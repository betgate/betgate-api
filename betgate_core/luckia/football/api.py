import logging
from betgate_models.models import Odd, Event, BookieId
from betgate_models.enums import Market, Category, Bookie
from betgate_core.luckia.api import LuckiaApiBase, calculate_odd


logger = logging.getLogger(__name__)


def get_main_market_2_options(json_model, markets):
    if json_model is None:
        return []

    to_return = []
    try:
        odd1 = calculate_odd(json_model[2][0][2][2])
        odd2 = calculate_odd(json_model[2][0][2][4])
        if odd1 > 1:
            to_return.append(Odd(value=odd1, market=markets[0], bookie=Bookie.LUCKIA))
        if odd2 > 1:
            to_return.append(Odd(value=odd2, market=markets[1], bookie=Bookie.LUCKIA))
    except:
        logger.exception("Error retrieving markets => %s", str(json_model))
    return to_return


def get_main_market_3_options(json_model, markets):
    to_return = []
    try:
        odd1 = calculate_odd(json_model[2][0][1][1])
        odd2 = calculate_odd(json_model[2][0][1][3])
        odd3 = calculate_odd(json_model[2][0][1][5])
        if odd1 > 1:
            to_return.append(Odd(value=odd1, market=markets[0], bookie=Bookie.LUCKIA))
        if odd2 > 1:
            to_return.append(Odd(value=odd2, market=markets[1], bookie=Bookie.LUCKIA))
        if odd3 > 1:
            to_return.append(Odd(value=odd3, market=markets[2], bookie=Bookie.LUCKIA))
    except:
        pass
    return to_return


def get_market_multiples_options(json_model, markets):
    markets_to_return = []
    try:
        options = json_model[2]
        keys = [opt[9] for opt in options]
        for key, mkt in markets:
            if key in keys:
                idx = keys.index(key)
                odd = calculate_odd(options[idx][1])
                if odd > 1:
                    markets_to_return.append(Odd(value=odd, market=mkt, bookie=Bookie.LUCKIA))
    except:
        pass
    return markets_to_return


def get_market_multiples_options_from_label(json_model, markets):
    markets_to_return = []
    try:
        options = json_model[2]
        labels = [opt[2] for opt in options]
        for label, mkt in markets:
            if label in labels:
                idx = labels.index(label)
                odd = calculate_odd(options[idx][1])
                if odd > 1:
                    markets_to_return.append(Odd(value=odd, market=mkt, bookie=Bookie.LUCKIA))
    except:
        pass
    return markets_to_return


def get_market_multiples_2options_index3(json_model, markets):
    markets_to_return = []
    try:
        options = json_model[2]
        labels = [opt[3][1] if len(opt) > 2 and isinstance(opt[3], list) else None for opt in options]
        for label, mkt1, mkt2 in markets:
            if label in labels:
                i = labels.index(label)
                odd = calculate_odd(options[i][3][2])
                if odd > 1:
                    markets_to_return.append(Odd(value=odd, market=mkt1, bookie=Bookie.LUCKIA))
                odd = calculate_odd(options[i][3][4])
                if odd > 1:
                    markets_to_return.append(Odd(value=odd, market=mkt2, bookie=Bookie.LUCKIA))
    except:
        pass
    return markets_to_return


def get_market_multiples_2options_index2(json_model, markets):
    markets_to_return = []
    try:
        options = json_model[2]
        labels = [opt[2][1] if len(opt) > 2 and isinstance(opt[2], list) else None for opt in options]
        for label, mkt1, mkt2 in markets:
            if label in labels:
                i = labels.index(label)
                odd = calculate_odd(options[i][2][2])
                if odd > 1:
                    markets_to_return.append(Odd(value=odd, market=mkt1, bookie=Bookie.LUCKIA))
                odd = calculate_odd(options[i][2][4])
                if odd > 1:
                    markets_to_return.append(Odd(value=odd, market=mkt2, bookie=Bookie.LUCKIA))
    except:
        pass
    return markets_to_return


class FootballApiBase(LuckiaApiBase):

    markets = [
        {
            "market_id": 0,
            "name": "full_time_1x2",
            "markets": [Market.FT_WIN_1, Market.FT_WIN_X, Market.FT_WIN_2],
            "method": get_main_market_3_options
        },
        {
            "market_id": 1,
            "name": "half_time_1x2",
            "markets": [Market.FH_WIN_1, Market.FH_WIN_X, Market.FH_WIN_2],
            "method": get_main_market_3_options
        },
        {
            "market_id": 61,
            "name": "double_chance",
            "markets": [("1 o X", Market.FT_WIN_1X), ("X o 2", Market.FT_WIN_X2), ("1 o 2", Market.FT_WIN_12)],
            "method": get_market_multiples_options_from_label
        },
        {
            "market_id": 145,
            "name": "double_chance_1st_half",
            "markets": [("1 o X", Market.FH_WIN_1X), ("X o 2", Market.FH_WIN_X2), ("1 o 2", Market.FH_WIN_12)],
            "method": get_market_multiples_options_from_label
        },
        {
            "market_id": 157,
            "name": "full_time_draw_no_net",
            "markets": [Market.FT_WIN_DNB_1, Market.FT_WIN_DNB_2],
            "method": get_main_market_2_options
        },
        {
            "market_id": 158,
            "name": "both_teams_to_score",
            "markets": [("Sí", Market.FT_BTTS_YES), ("No", Market.FT_BTTS_NO)],
            "method": get_market_multiples_options_from_label
        },
        {
            "market_id": 60,
            "name": "exact_result",
            "markets": [("0:0", Market.FT_RESULT_0_0),],
            "method": get_market_multiples_options_from_label
        },
        {
            "market_id": 144,
            "name": "exact_result_1st_half",
            "markets": [("0:0", Market.FH_RESULT_0_0), ],
            "method": get_market_multiples_options_from_label
        },
        {
            "market_id": 200,
            "name": "total_goals",
            "markets": [
                (0.5, Market.FT_TG_OVER_05, Market.FT_TG_UNDER_05), (1.5, Market.FT_TG_OVER_15, Market.FT_TG_UNDER_15),
                (2.5, Market.FT_TG_OVER_25, Market.FT_TG_UNDER_25), (3.5, Market.FT_TG_OVER_35, Market.FT_TG_UNDER_35),
                (4.5, Market.FT_TG_OVER_45, Market.FT_TG_UNDER_45), (5.5, Market.FT_TG_OVER_55, Market.FT_TG_UNDER_55)
            ],
            "method": get_market_multiples_2options_index3
        },
        {
            "market_id": 201,
            "name": "total_goals_1st_half",
            "markets": [
                (0.5, Market.FH_TG_OVER_05, Market.FH_TG_UNDER_05), (1.5, Market.FH_TG_OVER_15, Market.FH_TG_UNDER_15),
                (2.5, Market.FH_TG_OVER_25, Market.FH_TG_UNDER_25), (3.5, Market.FH_TG_OVER_35, Market.FH_TG_UNDER_35),
                (4.5, Market.FH_TG_OVER_45, Market.FH_TG_UNDER_45)
            ],
            "method": get_market_multiples_2options_index3
        },
        {
            "market_id": 0,
            "name": "total_goals_Asian",
            "markets": [
                (0.5, Market.FT_TGA_OVER_05, Market.FT_TGA_UNDER_05),
                (0.75, Market.FT_TGA_OVER_075, Market.FT_TGA_UNDER_075),
                (1.0, Market.FT_TGA_OVER_10, Market.FT_TGA_UNDER_10),
                (1.25, Market.FT_TGA_OVER_125, Market.FT_TGA_UNDER_125),
                (1.5, Market.FT_TGA_OVER_15, Market.FT_TGA_UNDER_15),
                (1.75, Market.FT_TGA_OVER_175, Market.FT_TGA_UNDER_175),
                (2.0, Market.FT_TGA_OVER_20, Market.FT_TGA_UNDER_20),
                (2.25, Market.FT_TGA_OVER_225, Market.FT_TGA_UNDER_225),
                (2.5, Market.FT_TGA_OVER_25, Market.FT_TGA_UNDER_25),
                (2.75, Market.FT_TGA_OVER_275, Market.FT_TGA_UNDER_275),
                (3.0, Market.FT_TGA_OVER_30, Market.FT_TGA_UNDER_30),
                (3.25, Market.FT_TGA_OVER_325, Market.FT_TGA_UNDER_325),
                (3.5, Market.FT_TGA_OVER_35, Market.FT_TGA_UNDER_35),
                (3.75, Market.FT_TGA_OVER_375, Market.FT_TGA_UNDER_375),
                (4.0, Market.FT_TGA_OVER_40, Market.FT_TGA_UNDER_40),
                (4.25, Market.FT_TGA_OVER_425, Market.FT_TGA_UNDER_425),
                (4.5, Market.FT_TGA_OVER_45, Market.FT_TGA_UNDER_45),
                (4.75, Market.FT_TGA_OVER_475, Market.FT_TGA_UNDER_475),
                (5.0, Market.FT_TGA_OVER_50, Market.FT_TGA_UNDER_50),
                (5.25, Market.FT_TGA_OVER_525, Market.FT_TGA_UNDER_525),
                (5.5, Market.FT_TGA_OVER_55, Market.FT_TGA_UNDER_55)
            ],
            "method": get_market_multiples_2options_index3
        },
        {
            "market_id": 1,
            "name": "total_goals_Asian_1st_halp",
            "markets": [
                (0.5, Market.FH_TGA_OVER_05, Market.FH_TGA_UNDER_05),
                (0.75, Market.FH_TGA_OVER_075, Market.FH_TGA_UNDER_075),
                (1.0, Market.FH_TGA_OVER_10, Market.FH_TGA_UNDER_10),
                (1.25, Market.FH_TGA_OVER_125, Market.FH_TGA_UNDER_125),
                (1.5, Market.FH_TGA_OVER_15, Market.FH_TGA_UNDER_15),
                (1.75, Market.FH_TGA_OVER_175, Market.FH_TGA_UNDER_175),
                (2.0, Market.FH_TGA_OVER_20, Market.FH_TGA_UNDER_20),
                (2.25, Market.FH_TGA_OVER_225, Market.FH_TGA_UNDER_225),
                (2.5, Market.FH_TGA_OVER_25, Market.FH_TGA_UNDER_25),
                (2.75, Market.FH_TGA_OVER_275, Market.FH_TGA_UNDER_275),
                (3.0, Market.FH_TGA_OVER_30, Market.FH_TGA_UNDER_30),
                (3.25, Market.FH_TGA_OVER_325, Market.FH_TGA_UNDER_325),
                (3.5, Market.FH_TGA_OVER_35, Market.FH_TGA_UNDER_35),
                (3.75, Market.FH_TGA_OVER_375, Market.FH_TGA_UNDER_375),
                (4.0, Market.FH_TGA_OVER_40, Market.FH_TGA_UNDER_40),
                (4.25, Market.FH_TGA_OVER_425, Market.FH_TGA_UNDER_425),
                (4.5, Market.FH_TGA_OVER_45, Market.FH_TGA_UNDER_45)
            ],
            "method": get_market_multiples_2options_index3
        },
        {
            "market_id": 270,
            "name": "full_time_handicap",
            "markets": [
                (-41, Market.FT_WIN_1_M40), (-40, Market.FT_DRAW_1_M40), (42, Market.FT_WIN_2_P40),
                (-31, Market.FT_WIN_1_M30), (-30, Market.FT_DRAW_1_M30), (32, Market.FT_WIN_2_P30),
                (-21, Market.FT_WIN_1_M20), (-20, Market.FT_DRAW_1_M20), (22, Market.FT_WIN_2_P20),
                (-11, Market.FT_WIN_1_M10), (-10, Market.FT_DRAW_1_M10), (12, Market.FT_WIN_2_P10),
                (11, Market.FT_WIN_1_P10), (10, Market.FT_DRAW_1_P10), (-12, Market.FT_WIN_2_M10),
                (21, Market.FT_WIN_1_P20), (20, Market.FT_DRAW_1_P20), (-22, Market.FT_WIN_2_M20),
                (31, Market.FT_WIN_1_P30), (30, Market.FT_DRAW_1_P30), (-32, Market.FT_WIN_2_M30),
                (41, Market.FT_WIN_1_P40), (40, Market.FT_DRAW_1_P40), (-42, Market.FT_WIN_2_M40),
            ],
            "method": get_market_multiples_options
        },
        {
            "market_id": 271,
            "name": "half_time_handicap",
            "markets": [
                (-31, Market.FH_WIN_1_M30), (-30, Market.FH_DRAW_1_M30), (32, Market.FH_WIN_2_P30),
                (-21, Market.FH_WIN_1_M20), (-20, Market.FH_DRAW_1_M20), (22, Market.FH_WIN_2_P20),
                (-11, Market.FH_WIN_1_M10), (-10, Market.FH_DRAW_1_M10), (12, Market.FH_WIN_2_P10),
                (11, Market.FH_WIN_1_P10), (10, Market.FH_DRAW_1_P10), (-12, Market.FH_WIN_2_M10),
                (21, Market.FH_WIN_1_P20), (20, Market.FH_DRAW_1_P20), (-22, Market.FH_WIN_2_M20),
                (31, Market.FH_WIN_1_P30), (30, Market.FH_DRAW_1_P30), (-32, Market.FH_WIN_2_M30),
            ],
            "method": get_market_multiples_options
        },
        {
            "market_id": 0,
            "name": "full_time_assian_handicap",
            "markets": [
                (0, Market.FT_WINA_1_P00, Market.FT_WINA_2_P00),
                (0.25, Market.FT_WINA_1_P025, Market.FT_WINA_2_M025),
                (-0.25, Market.FT_WINA_1_M025, Market.FT_WINA_2_P025),
                (0.5, Market.FT_WINA_1_P05, Market.FT_WINA_2_M05),
                (-0.5, Market.FT_WINA_1_M05, Market.FT_WINA_2_P05),
                (0.75, Market.FT_WINA_1_P075, Market.FT_WINA_2_M075),
                (-0.75, Market.FT_WINA_1_M075, Market.FT_WINA_2_P075),
                (1.0, Market.FT_WINA_1_P10, Market.FT_WINA_2_M10),
                (-1.0, Market.FT_WINA_1_M10, Market.FT_WINA_2_P10),
                (1.25, Market.FT_WINA_1_P125, Market.FT_WINA_2_M125),
                (-1.25, Market.FT_WINA_1_M125, Market.FT_WINA_2_P125),
                (1.5, Market.FT_WINA_1_P15, Market.FT_WINA_2_M15),
                (-1.5, Market.FT_WINA_1_M15, Market.FT_WINA_2_P15),
                (1.75, Market.FT_WINA_1_P175, Market.FT_WINA_2_M175),
                (-1.75, Market.FT_WINA_1_M175, Market.FT_WINA_2_P175),
                (2.0, Market.FT_WINA_1_P20, Market.FT_WINA_2_M20),
                (-2.0, Market.FT_WINA_1_M20, Market.FT_WINA_2_P20),
                (2.25, Market.FT_WINA_1_P225, Market.FT_WINA_2_M225),
                (-2.25, Market.FT_WINA_1_M225, Market.FT_WINA_2_P225),
                (2.5, Market.FT_WINA_1_P25, Market.FT_WINA_2_M25),
                (-2.5, Market.FT_WINA_1_M25, Market.FT_WINA_2_P25),
                (2.75, Market.FT_WINA_1_P275, Market.FT_WINA_2_M275),
                (-2.75, Market.FT_WINA_1_M275, Market.FT_WINA_2_P275),
                (3.0, Market.FT_WINA_1_P30, Market.FT_WINA_2_M30),
                (-3.0, Market.FT_WINA_1_M30, Market.FT_WINA_2_P30),
                (3.25, Market.FT_WINA_1_P325, Market.FT_WINA_2_M325),
                (-3.25, Market.FT_WINA_1_M325, Market.FT_WINA_2_P325),
                (3.5, Market.FT_WINA_1_P35, Market.FT_WINA_2_M35),
                (-3.5, Market.FT_WINA_1_M35, Market.FT_WINA_2_P35)
            ],
            "method": get_market_multiples_2options_index2
        },
        {
            "market_id": 621,
            "name": "corners_3_options",
            "markets": [
                ("Más de 0", Market.FT_CORNERS_OVER_0_5),
                ("Más de 1", Market.FT_CORNERS_OVER_1_5), ("Menos de 1", Market.FT_CORNERS_UNDER_0_5),
                ("Más de 2", Market.FT_CORNERS_OVER_2_5), ("Menos de 2", Market.FT_CORNERS_UNDER_1_5),
                ("Más de 3", Market.FT_CORNERS_OVER_3_5), ("Menos de 3", Market.FT_CORNERS_UNDER_2_5),
                ("Más de 4", Market.FT_CORNERS_OVER_4_5), ("Menos de 4", Market.FT_CORNERS_UNDER_3_5),
                ("Más de 5", Market.FT_CORNERS_OVER_5_5), ("Menos de 5", Market.FT_CORNERS_UNDER_4_5),
                ("Más de 6", Market.FT_CORNERS_OVER_6_5), ("Menos de 6", Market.FT_CORNERS_UNDER_5_5),
                ("Más de 7", Market.FT_CORNERS_OVER_7_5), ("Menos de 7", Market.FT_CORNERS_UNDER_6_5),
                ("Más de 8", Market.FT_CORNERS_OVER_8_5), ("Menos de 8", Market.FT_CORNERS_UNDER_7_5),
                ("Más de 9", Market.FT_CORNERS_OVER_9_5), ("Menos de 9", Market.FT_CORNERS_UNDER_8_5),
                ("Más de 10", Market.FT_CORNERS_OVER_10_5), ("Menos de 10", Market.FT_CORNERS_UNDER_9_5),
                ("Más de 11", Market.FT_CORNERS_OVER_11_5), ("Menos de 11", Market.FT_CORNERS_UNDER_10_5),
                ("Más de 12", Market.FT_CORNERS_OVER_12_5), ("Menos de 12", Market.FT_CORNERS_UNDER_11_5),
                ("Más de 13", Market.FT_CORNERS_OVER_13_5), ("Menos de 13", Market.FT_CORNERS_UNDER_12_5),
                ("Más de 14", Market.FT_CORNERS_OVER_14_5), ("Menos de 14", Market.FT_CORNERS_UNDER_13_5),
                ("Más de 15", Market.FT_CORNERS_OVER_15_5), ("Menos de 15", Market.FT_CORNERS_UNDER_14_5),
                ("Más de 16", Market.FT_CORNERS_OVER_16_5), ("Menos de 16", Market.FT_CORNERS_UNDER_15_5),
                ("Más de 17", Market.FT_CORNERS_OVER_17_5), ("Menos de 17", Market.FT_CORNERS_UNDER_16_5),
                ("Más de 18", Market.FT_CORNERS_OVER_18_5), ("Menos de 18", Market.FT_CORNERS_UNDER_17_5),
                ("Más de 19", Market.FT_CORNERS_OVER_19_5), ("Menos de 19", Market.FT_CORNERS_UNDER_18_5),
                ("Más de 20", Market.FT_CORNERS_OVER_20_5), ("Menos de 20", Market.FT_CORNERS_UNDER_19_5),
                ("Exactamente 0", Market.FT_CORNERS_EXACT_0), ("Exactamente 1", Market.FT_CORNERS_EXACT_1),
                ("Exactamente 2", Market.FT_CORNERS_EXACT_2), ("Exactamente 3", Market.FT_CORNERS_EXACT_3),
                ("Exactamente 4", Market.FT_CORNERS_EXACT_4), ("Exactamente 5", Market.FT_CORNERS_EXACT_5),
                ("Exactamente 6", Market.FT_CORNERS_EXACT_6), ("Exactamente 7", Market.FT_CORNERS_EXACT_7),
                ("Exactamente 8", Market.FT_CORNERS_EXACT_8), ("Exactamente 9", Market.FT_CORNERS_EXACT_9),
                ("Exactamente 10", Market.FT_CORNERS_EXACT_10), ("Exactamente 11", Market.FT_CORNERS_EXACT_11),
                ("Exactamente 12", Market.FT_CORNERS_EXACT_12), ("Exactamente 13", Market.FT_CORNERS_EXACT_13),
                ("Exactamente 14", Market.FT_CORNERS_EXACT_14), ("Exactamente 15", Market.FT_CORNERS_EXACT_15),
                ("Exactamente 16", Market.FT_CORNERS_EXACT_16), ("Exactamente 17", Market.FT_CORNERS_EXACT_17),
                ("Exactamente 18", Market.FT_CORNERS_EXACT_18), ("Exactamente 19", Market.FT_CORNERS_EXACT_19),
                ("Exactamente 20", Market.FT_CORNERS_EXACT_20)
            ],
            "method": get_market_multiples_options_from_label
        },
        {
            "market_id": 10,
            "name": "cards_2_options",
            "markets": [
                (0.5, Market.FT_CARDS_OVER_0_5, Market.FT_CARDS_UNDER_0_5),
                (1.5, Market.FT_CARDS_OVER_1_5, Market.FT_CARDS_UNDER_1_5),
                (2.5, Market.FT_CARDS_OVER_2_5, Market.FT_CARDS_UNDER_2_5),
                (3.5, Market.FT_CARDS_OVER_3_5, Market.FT_CARDS_UNDER_3_5),
                (4.5, Market.FT_CARDS_OVER_4_5, Market.FT_CARDS_UNDER_4_5),
                (5.5, Market.FT_CARDS_OVER_5_5, Market.FT_CARDS_UNDER_5_5),
                (6.5, Market.FT_CARDS_OVER_6_5, Market.FT_CARDS_UNDER_6_5),
                (7.5, Market.FT_CARDS_OVER_7_5, Market.FT_CARDS_UNDER_7_5),
                (8.5, Market.FT_CARDS_OVER_8_5, Market.FT_CARDS_UNDER_8_5),
                (9.5, Market.FT_CARDS_OVER_9_5, Market.FT_CARDS_UNDER_9_5),
                (10.5, Market.FT_CARDS_OVER_10_5, Market.FT_CARDS_UNDER_10_5),
            ],
            "method": get_market_multiples_2options_index3
        }
    ]

    @classmethod
    def _get_event_market_id_from_id(cls, markets, market_id):
        for mkt in markets:
            if mkt[1] == market_id:
                return mkt[0]

    @classmethod
    def _get_market(cls, markets, event_market_id):
        for mkt in markets:
            if mkt[0] == event_market_id:
                return mkt

    def get_markets(self, event_id=None, json_model=None):
        if json_model is None and event_id is None:
            raise BaseException("Both 'json_model' and 'event_id' can't be equal to None")
        elif json_model is None and event_id is not None:
            json_model = self.request_event_detail(event_id).json
        markets_to_return = []
        markets = []
        keys = []
        market_id_list = json_model[0][5] + json_model[0][6]
        for market in self.markets:
            market_event_id = self._get_event_market_id_from_id(market_id_list, market["market_id"])
            new_market = {
                "market_id": market_event_id,
                "markets": market["markets"],
                "method": market["method"]
            }
            keys.append(market_event_id)
            markets.append(new_market)

        res = self.request_update_events(*keys)
        for mkt in markets:
            market_json_model = self._get_market(res.json, mkt["market_id"])
            markets_to_return += mkt["method"](market_json_model, mkt["markets"])
        return markets_to_return

    def get_event(self, event_id, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            res = self.request_event_detail(event_id)
            home_tname, away_tname = self.get_team_names(res.json)
            category = self.get_category()
            dtime = self.get_datetime(res.json[0][4])
            markets = self.get_markets(json_model=res.json)
            event = Event(
                bookies={str(Bookie.LUCKIA.value): BookieId(bookie=Bookie.LUCKIA, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                odds=markets,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    def get_team_names(self, data):
        try:
            return data[0][1].strip(), data[0][2].strip()
        except:
            return None, None

    def lazy_get_category_events(self, **kwargs):
        validate = kwargs.pop("validate", True)
        res = self.request_events()

        try:
            events = res.json[0][0][1] if isinstance(res.json[0][0][1], list) else []
        except:
            return []

        to_return = []
        for evt in events:
            try:
                event_id = evt[0]
                home_tname = evt[1].strip()
                away_tname = evt[2].strip()
                dtime = self.get_datetime(evt[4])
                event = Event(
                    bookies={str(Bookie.LUCKIA.value): BookieId(bookie=Bookie.LUCKIA, value=event_id)},
                    home_team=self.get_team(home_tname),
                    away_team=self.get_team(away_tname),
                    category=self.get_category(),
                    start=dtime,
                    home_team_name=home_tname,
                    away_team_name=away_tname
                )
                if validate:
                    event.full_clean(validate_unique=False)
                to_return.append(event)
            except:
                pass
        return to_return


class FootballApi(FootballApiBase):
    category = Category.UNKNOWN
    category_id = -1
