from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import IcelandUrvalsdeildMixin


class Urvalsdeild(IcelandUrvalsdeildMixin, FootballApiBase):
    category_id = 38676
