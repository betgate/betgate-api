from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import ItalySerieBMixin


class SerieB(ItalySerieBMixin, FootballApiBase):
    category_id = 42884
