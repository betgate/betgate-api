from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import ItalySerieAMixin


class SerieA(ItalySerieAMixin, FootballApiBase):
    category_id = 40030
