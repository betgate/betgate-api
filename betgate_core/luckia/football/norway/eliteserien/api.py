from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import NorwayEliteserienMixin


class Eliteserien(NorwayEliteserienMixin, FootballApiBase):
    category_id = 84553
