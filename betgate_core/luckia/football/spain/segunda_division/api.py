from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import SpainSegundaDivisionMixin


class SegundaDivision(SpainSegundaDivisionMixin, FootballApiBase):
    category_id = 44411
