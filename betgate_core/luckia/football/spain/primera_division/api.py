from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import SpainPrimeraDivisionMixin


class PrimeraDivision(SpainPrimeraDivisionMixin, FootballApiBase):
    category_id = 40031
