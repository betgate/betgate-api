from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import EcuadorSerieAMixin


class SerieA(EcuadorSerieAMixin, FootballApiBase):
    category_id = 42501
