from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import GreeceSuperLeagueMixin


class SuperLeague(GreeceSuperLeagueMixin, FootballApiBase):
    category_id = 52938
