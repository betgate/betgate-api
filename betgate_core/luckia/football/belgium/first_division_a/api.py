from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import BelgiumFirstDivisionAMixin


class FirstDivisionA(BelgiumFirstDivisionAMixin, FootballApiBase):
    category_id = 40815
