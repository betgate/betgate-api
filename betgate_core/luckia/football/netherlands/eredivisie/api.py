from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import NetherlandsEredivisieMixin


class Eredivisie(NetherlandsEredivisieMixin, FootballApiBase):
    category_id = 41372
