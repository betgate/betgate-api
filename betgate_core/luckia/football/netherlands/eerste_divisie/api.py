from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import NetherlandsEersteDivisieMixin


class EersteDivisie(NetherlandsEersteDivisieMixin, FootballApiBase):
    category_id = 41371
