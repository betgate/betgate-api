from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import SwedenSuperettanMixin


class Superettan(SwedenSuperettanMixin, FootballApiBase):
    category_id = 84732
