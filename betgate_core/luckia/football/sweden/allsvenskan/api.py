from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import SwedenAllsvenskanMixin


class Allsvenskan(SwedenAllsvenskanMixin, FootballApiBase):
    category_id = 84731
