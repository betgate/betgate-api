from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import FinlandVeikkausliigaMixin


class Veikkausliiga(FinlandVeikkausliigaMixin, FootballApiBase):
    category_id = 89344
