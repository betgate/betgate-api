from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import GermanyBundesligaMixin


class Bundesliga(GermanyBundesligaMixin, FootballApiBase):
    category_id = 40481
