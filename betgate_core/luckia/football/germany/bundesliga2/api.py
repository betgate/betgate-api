from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import GermanyBundesliga2Mixin


class Bundesliga2(GermanyBundesliga2Mixin, FootballApiBase):
    category_id = 40481
