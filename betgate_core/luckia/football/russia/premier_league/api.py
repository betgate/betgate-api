from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import RussiaPremierLeagueMixin


class PremierLeague(RussiaPremierLeagueMixin, FootballApiBase):
    category_id = 40819
