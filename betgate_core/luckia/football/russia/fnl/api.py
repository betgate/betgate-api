from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import RussiaFnlMixin


class Fnl(RussiaFnlMixin, FootballApiBase):
    category_id = 9746
