from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import UkrainePremierLeagueMixin


class PremierLeague(UkrainePremierLeagueMixin, FootballApiBase):
    category_id = 45006
