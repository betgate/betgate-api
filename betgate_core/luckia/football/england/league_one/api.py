from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandLeagueOneMixin


class LeagueOne(EnglandLeagueOneMixin, FootballApiBase):
    category_id = 40822
