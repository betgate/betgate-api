from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandNationalLeagueMixin


class NationalLeague(EnglandNationalLeagueMixin, FootballApiBase):
    category_id = 42666
