from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandNationalLeagueNorthMixin


class NationalLeagueNorth(EnglandNationalLeagueNorthMixin, FootballApiBase):
    category_id = 55655
