from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandPremierLeagueMixin


class PremierLeague(EnglandPremierLeagueMixin, FootballApiBase):
    category_id = 40253
