from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandChampionshipMixin


class Championship(EnglandChampionshipMixin, FootballApiBase):
    category_id = 40817
