from betgate_core.luckia.football.england.league_two.api import LeagueTwo


def get_events(**kwargs):
    with LeagueTwo() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with LeagueTwo() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with LeagueTwo() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
