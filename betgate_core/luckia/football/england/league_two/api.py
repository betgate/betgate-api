from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandLeagueTwoMixin


class LeagueTwo(EnglandLeagueTwoMixin, FootballApiBase):
    category_id = 40823
