from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import BrazilSerieAMixin


class SerieA(BrazilSerieAMixin, FootballApiBase):
    category_id = 38529
