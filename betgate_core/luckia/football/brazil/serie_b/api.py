from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import BrazilSerieBMixin


class SerieB(BrazilSerieBMixin, FootballApiBase):
    category_id = 39039
