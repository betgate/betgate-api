from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import ClubsInternationalEuropeLeagueMixin


class EuropeLeague(ClubsInternationalEuropeLeagueMixin, FootballApiBase):
    category_id = 41410
