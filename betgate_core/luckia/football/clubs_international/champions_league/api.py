from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import ClubsInternationalChampionsLeagueMixin


class ChampionsLeague(ClubsInternationalChampionsLeagueMixin, FootballApiBase):
    category_id = 40685
