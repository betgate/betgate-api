from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import MoroccoBotolaMixin


class Botola(MoroccoBotolaMixin, FootballApiBase):
    category_id = 59513
