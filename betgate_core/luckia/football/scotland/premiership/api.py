from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import ScotlandPremiershipMixin


class Premiership(ScotlandPremiershipMixin, FootballApiBase):
    category_id = 40818
