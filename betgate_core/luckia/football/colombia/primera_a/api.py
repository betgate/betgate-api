from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import ColombiaPrimeraAMixin


class PrimeraA(ColombiaPrimeraAMixin, FootballApiBase):
    category_id = 33073
