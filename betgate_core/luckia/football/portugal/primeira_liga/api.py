from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import PortugalPrimeiraLigaMixin


class PrimeiraLiga(PortugalPrimeiraLigaMixin, FootballApiBase):
    category_id = 44069
