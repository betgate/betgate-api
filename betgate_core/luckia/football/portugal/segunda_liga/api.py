from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import PortugalSegundaLigaMixin


class SegundaLiga(PortugalSegundaLigaMixin, FootballApiBase):
    category_id = 55903
