from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import JapanJLeague1Mixin


class JLeague1(JapanJLeague1Mixin, FootballApiBase):
    category_id = 20619
