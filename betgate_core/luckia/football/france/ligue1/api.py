from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import FranceLigue1Mixin


class Ligue1(FranceLigue1Mixin, FootballApiBase):
    category_id = 40032
