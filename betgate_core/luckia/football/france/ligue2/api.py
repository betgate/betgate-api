from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import FranceLigue2Mixin


class Ligue2(FranceLigue2Mixin, FootballApiBase):
    category_id = 52971
