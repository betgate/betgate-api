from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import SwitzerlandSuperLeagueMixin


class SuperLeague(SwitzerlandSuperLeagueMixin, FootballApiBase):
    category_id = 41373
