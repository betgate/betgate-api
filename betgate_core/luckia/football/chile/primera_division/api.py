from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import ChilePrimeraDivisionMixin


class PrimeraDivision(ChilePrimeraDivisionMixin, FootballApiBase):
    category_id = 53411
