from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import AustriaBundesligaMixin


class Bundesliga(AustriaBundesligaMixin, FootballApiBase):
    category_id = 42282
