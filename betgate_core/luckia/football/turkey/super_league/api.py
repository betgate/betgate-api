from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import TurkeySuperLeagueMixin


class SuperLeague(TurkeySuperLeagueMixin, FootballApiBase):
    category_id = 41090
