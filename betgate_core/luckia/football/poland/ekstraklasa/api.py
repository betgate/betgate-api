from betgate_core.luckia.football.api import FootballApiBase
from betgate_core.mixins.football import PolandEkstraklasaMixin


class Ekstraklasa(PolandEkstraklasaMixin, FootballApiBase):
    category_id = 40145
