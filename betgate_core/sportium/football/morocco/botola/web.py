from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import MoroccoBotolaMixin


class Botola(MoroccoBotolaMixin, FootballWebBase):
    url_path_params = {"category_id": "42467", "category_label": "Moroccan-Botola"}
    category_name = "Morocco. Botola. 1st Division"
