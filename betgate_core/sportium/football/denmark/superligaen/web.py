from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import DenmarkSuperligaenMixin


class Superligaen(DenmarkSuperligaenMixin, FootballWebBase):
    url_path_params = {"category_id": "39511", "category_label": "Superliga-Danesa"}
    category_name = "Superliga Danesa"
