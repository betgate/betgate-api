from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import PolandEkstraklasaMixin


class Ekstraklasa(PolandEkstraklasaMixin, FootballWebBase):
    url_path_params = {"category_id": "45013", "category_label": "Primera-División-Polaca"}
    category_name = "Poland.Ekstraklasa"
