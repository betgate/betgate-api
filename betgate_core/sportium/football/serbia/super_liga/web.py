from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import SerbiaSuperLigaMixin


class SuperLiga(SerbiaSuperLigaMixin, FootballWebBase):
    url_path_params = {"category_id": "", "category_label": ""}
    category_name = ""
