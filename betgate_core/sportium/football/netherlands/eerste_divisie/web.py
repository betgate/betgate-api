from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import NetherlandsEersteDivisieMixin


class EersteDivisie(NetherlandsEersteDivisieMixin, FootballWebBase):
    url_path_params = {"category_id": "43084", "category_label": "Segunda-División-Holandesa"}
    category_name = "Netherlands.Eerste Divisie"
