from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import NetherlandsEredivisieMixin


class Eredivisie(NetherlandsEredivisieMixin, FootballWebBase):
    url_path_params = {"category_id": "43094", "category_label": "Eredivisie"}
    category_name = "Netherlands.Eredivisie"
