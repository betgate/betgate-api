from betgate_core.sportium.football.russia import premier_league, fnl


def get_events(**kwargs):
    return premier_league.get_events(**kwargs) + fnl.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return premier_league.lazy_get_events(**kwargs) + fnl.lazy_get_events(**kwargs)
