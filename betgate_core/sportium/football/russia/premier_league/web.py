from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import RussiaPremierLeagueMixin


class PremierLeague(RussiaPremierLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "44897", "category_label": "Premier-League-Rusia"}
    category_name = "Russia.Premier League"
