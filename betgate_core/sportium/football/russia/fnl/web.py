from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import RussiaFnlMixin


class Fnl(RussiaFnlMixin, FootballWebBase):
    url_path_params = {"category_id": "44907", "category_label": "Russian-National-Football-League"}
    category_name = "Russia.National Football League"
