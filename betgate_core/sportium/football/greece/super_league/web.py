from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import GreeceSuperLeagueMixin


class SuperLeague(GreeceSuperLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "45890", "category_label": "Super-Liga-Griega"}
    category_name = "Super Liga Griega"
