from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import ChilePrimeraDivisionMixin


class PrimeraDivision(ChilePrimeraDivisionMixin, FootballWebBase):
    url_path_params = {"category_id": "42162", "category_label": "Primera-Divisi%C3%B3n-Chilena"}
    category_name = "Chile.Primera Division"
