from betgate_core.sportium.football.chile import primera_division


def get_events(**kwargs):
    return primera_division.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return primera_division.lazy_get_events(**kwargs)
