from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import SwedenSuperettanMixin


class Superettan(SwedenSuperettanMixin, FootballWebBase):
    url_path_params = {"category_id": "45128", "category_label": "Superettan"}
    category_name = ""
