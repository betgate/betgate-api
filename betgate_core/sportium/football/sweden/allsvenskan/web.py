from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import SwedenAllsvenskanMixin


class Allsvenskan(SwedenAllsvenskanMixin, FootballWebBase):
    url_path_params = {"category_id": "45080", "category_label": "Allsvenskan"}
    category_name = "Sweden.Allsvenskan"
