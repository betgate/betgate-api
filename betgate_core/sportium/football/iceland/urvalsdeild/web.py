from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import IcelandUrvalsdeildMixin


class Urvalsdeild(IcelandUrvalsdeildMixin, FootballWebBase):
    url_path_params = {"category_id": "44345", "category_label": "Urvalsdeild"}
    category_name = "Iceland.Premier League"
