from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import BelgiumFirstDivisionAMixin


class FirstDivisionA(BelgiumFirstDivisionAMixin, FootballWebBase):
    url_path_params = {"category_id": "42039", "category_label": "Primera-Divisi%C3%B3n-A"}
    category_name = "Belgium.1st Division A"
