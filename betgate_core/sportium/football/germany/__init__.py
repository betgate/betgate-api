from betgate_core.sportium.football.germany import bundesliga, bundesliga2


def get_events(**kwargs):
    return bundesliga.get_events(**kwargs) + bundesliga2.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return bundesliga.lazy_get_events(**kwargs) + bundesliga2.lazy_get_events(**kwargs)
