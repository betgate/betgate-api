from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import GermanyBundesligaMixin


class Bundesliga(GermanyBundesligaMixin, FootballWebBase):
    url_path_params = {"category_id": "45915", "category_label": "Bundesliga"}
    category_name = "Germany.Bundesliga"
