from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import GermanyBundesliga2Mixin


class Bundesliga2(GermanyBundesliga2Mixin, FootballWebBase):
    url_path_params = {"category_id": "45985", "category_label": "2.-Bundesliga"}
    category_name = "Germany.Bundesliga 2"
