from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import ItalySerieAMixin


class SerieA(ItalySerieAMixin, FootballWebBase):
    url_path_params = {"category_id": "44571", "category_label": "Serie-A"}
    category_name = "Italy.Serie A"
