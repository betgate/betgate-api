from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import ItalySerieBMixin


class SerieB(ItalySerieBMixin, FootballWebBase):
    url_path_params = {"category_id": "44561", "category_label": "Serie-B"}
    category_name = "Italy.Serie B"
