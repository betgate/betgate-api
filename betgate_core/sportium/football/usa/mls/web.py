from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import UsaMlsMixin


class Mls(UsaMlsMixin, FootballWebBase):
    url_path_params = {"category_id": "44663", "category_label": "Major-League-Soccer-%28MLS%29"}
    category_name = "USA.MLS"
