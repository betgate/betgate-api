from betgate_core.sportium.football.usa.mls.web import Mls


def get_events(**kwargs):
    with Mls() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Mls() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Mls() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
