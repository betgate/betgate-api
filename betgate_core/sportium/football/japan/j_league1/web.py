from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import JapanJLeague1Mixin


class JLeague1(JapanJLeague1Mixin, FootballWebBase):
    url_path_params = {"category_id": "44501", "category_label": "J1-League"}
    category_name = "Japan.J.League.Division 1"
