from betgate_core.sportium.football.japan.j_league1.web import JLeague1


def get_events(**kwargs):
    with JLeague1() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with JLeague1() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with JLeague1() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
