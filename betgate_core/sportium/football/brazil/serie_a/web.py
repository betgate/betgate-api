from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import BrazilSerieAMixin


class SerieA(BrazilSerieAMixin, FootballWebBase):
    url_path_params = {"category_id": "42382", "category_label": "Brasil-Serie-A"}
    category_name = "Brazil.Serie A"
