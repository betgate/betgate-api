from betgate_core.sportium.football.brazil import serie_a
from betgate_core.sportium.football.brazil import serie_b


def get_events(**kwargs):
    return serie_a.get_events(**kwargs) + \
           serie_b.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return serie_a.lazy_get_events(**kwargs) + \
           serie_b.lazy_get_events(**kwargs)
