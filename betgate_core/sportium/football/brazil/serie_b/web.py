from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import BrazilSerieBMixin


class SerieB(BrazilSerieBMixin, FootballWebBase):
    url_path_params = {"category_id": "42429", "category_label": "Brasil-Serie-B"}
    category_name = "Brazil.Serie B"
