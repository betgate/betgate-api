from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import NorwayEliteserienMixin


class Eliteserien(NorwayEliteserienMixin, FootballWebBase):
    url_path_params = {"category_id": "42861", "category_label": "Tippeligaen"}
    category_name = "Norway.Eliteserien"
