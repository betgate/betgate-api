from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import ColombiaPrimeraAMixin


class PrimeraA(ColombiaPrimeraAMixin, FootballWebBase):
    url_path_params = {"category_id": "39074", "category_label": "Primera-A-Colombiana"}
    category_name = "Colombia.Primera A"
