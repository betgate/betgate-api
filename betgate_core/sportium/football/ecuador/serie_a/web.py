from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import EcuadorSerieAMixin


class SerieA(EcuadorSerieAMixin, FootballWebBase):
    url_path_params = {"category_id": "39416", "category_label": "Ecuadorean-Primera-Division"}
    category_name = "Ecuador.Serie A"
