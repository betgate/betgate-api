from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import SwitzerlandSuperLeagueMixin


class SuperLeague(SwitzerlandSuperLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "44743", "category_label": "Super-League"}
    category_name = "Switzerland.Super League"
