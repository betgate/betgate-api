from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import ScotlandPremiershipMixin


class Premiership(ScotlandPremiershipMixin, FootballWebBase):
    url_path_params = {"category_id": "44827", "category_label": "Ladbrokes-Premiership"}
    category_name = "Scotland.Premiership"
