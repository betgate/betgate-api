from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import TurkeySuperLeagueMixin


class SuperLeague(TurkeySuperLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "44674", "category_label": "Superliga-Turca"}
    category_name = "Turkey.Super Lig"
