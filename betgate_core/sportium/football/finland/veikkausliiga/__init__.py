from betgate_core.sportium.football.finland.veikkausliiga.web import Veikkaussliiga


def get_events(**kwargs):
    with Veikkaussliiga() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Veikkaussliiga() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Veikkaussliiga() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
