from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import FinlandVeikkausliigaMixin


class Veikkaussliiga(FinlandVeikkausliigaMixin, FootballWebBase):
    url_path_params = {"category_id": "46096", "category_label": "Veikkausliiga"}
    category_name = "Finland.Veikkausliiga"
