from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import CroatiaFirstLeagueMixin


class FirstLeague(CroatiaFirstLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "39286", "category_label": "Primera-Liga-Croata"}
    category_name = "Croatia.1st League"
