from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import UkrainePremierLeagueMixin


class PremierLeague(UkrainePremierLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "44633", "category_label": "Premier-League-Ucrania"}
    category_name = "Ukraine.Premier League"
