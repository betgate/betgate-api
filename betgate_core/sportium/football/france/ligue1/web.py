from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import FranceLigue1Mixin


class Ligue1(FranceLigue1Mixin, FootballWebBase):
    url_path_params = {"category_id": "46074", "category_label": "Ligue-1"}
    category_name = "France.Ligue 1"
