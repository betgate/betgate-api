from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import FranceLigue2Mixin


class Ligue2(FranceLigue2Mixin, FootballWebBase):
    url_path_params = {"category_id": "46075", "category_label": "Ligue-2"}
    category_name = "France.Ligue 2"
