from datetime import datetime
import logging
import pytz
from sdklib.shortcuts import cache
from betgate_models.models import Odd, Event, BookieId
from betgate_models.enums import Market, Category, Bookie
from betgate_core.sportium.web import SportiumWebBase

logger = logging.getLogger(__name__)


def _get_odd_by_position(html, markets):
    elems = html.find_elements_by_xpath(".//button[not(@disabled)]//span[@class='price dec']")
    if len(elems) == 0:  # no error message
        return []
    elif len(elems) != len(markets):
        logger.error(f"Markets length doesn't match html elems found. {markets}")
        return []

    return [
        Odd(value=elem.text, market=market, bookie=Bookie.SPORTIUM)
        for elem, market in zip(elems, markets)
    ]


def _get_odd_by_text(html, text_markets):
    odds = []
    for text, market in text_markets:
        button = html.find_element_by_xpath(f".//button[@title='{text}' and not(@disabled)]")
        if button is not None:
            elem = button.getparent(height=1).find_element_by_xpath(".//span[@class='price dec']")
            if elem is not None:
                odds.append(Odd(value=elem.text, market=market, bookie=Bookie.SPORTIUM))
    return odds


class FootballWebBase(SportiumWebBase):
    FOOTBALL_CATEGORY_URL = "/es/t/{category_id}/{category_label}"

    url_path_params = {"category_id": None, "category_label": None}

    markets_specification = {
        "Ganador del Partido (1X2)": {
            "method": _get_odd_by_position,
            "params": {
                "markets": [
                    Market.FT_WIN_1, Market.FT_WIN_X, Market.FT_WIN_2
                ]
            }
        },
        "Doble Oportunidad": {
            "method": _get_odd_by_position,
            "params": {
                "markets": [
                    Market.FT_WIN_1X, Market.FT_WIN_12, Market.FT_WIN_X2
                ]
            }
        },
        "Empate No cuenta": {
            "method": _get_odd_by_position,
            "params": {
                "markets": [
                    Market.FT_WIN_DNB_1, Market.FT_WIN_DNB_2
                ]
            }
        },
        "Marcador exacto": {
            "method": _get_odd_by_text,
            "params": {
                "text_markets": [(" 0-0 ", Market.FT_RESULT_0_0)]
            }
        },
        "Ambos equipos marcan": {
            "method": _get_odd_by_position,
            "params": {
                "markets": [
                    Market.FT_BTTS_YES, Market.FT_BTTS_NO
                ]
            }
        },
        "Total Goles Más de/Menos de": {
            "method": _get_odd_by_text,
            "params": {
                "text_markets": [
                    ("Más 0.5", Market.FT_TG_OVER_05), ("Menos 0.5", Market.FT_TG_UNDER_05),
                    ("Más 1.5", Market.FT_TG_OVER_15), ("Menos 1.5", Market.FT_TG_UNDER_15),
                    ("Más 2.5", Market.FT_TG_OVER_25), ("Menos 2.5", Market.FT_TG_UNDER_25),
                    ("Más 3.5", Market.FT_TG_OVER_35), ("Menos 3.5", Market.FT_TG_UNDER_35),
                    ("Más 4.5", Market.FT_TG_OVER_45), ("Menos 4.5", Market.FT_TG_UNDER_45),
                    ("Más 5.5", Market.FT_TG_OVER_55), ("Menos 5.5", Market.FT_TG_UNDER_55),
                ]
            }
        },
        "Ganador 1ª Mitad": {
            "method": _get_odd_by_position,
            "params": {
                "markets": [
                    Market.FH_WIN_1, Market.FH_WIN_X, Market.FH_WIN_2
                ]
            }
        },
        "1ª Mitad Más de/Menos de": {
            "method": _get_odd_by_text,
            "params": {
                "text_markets": [
                    ("Más 0.5", Market.FH_TG_OVER_05), ("Menos 0.5", Market.FH_TG_UNDER_05),
                    ("Más 1.5", Market.FH_TG_OVER_15), ("Menos 1.5", Market.FH_TG_UNDER_15),
                    ("Más 2.5", Market.FH_TG_OVER_25), ("Menos 2.5", Market.FH_TG_UNDER_25),
                    ("Más 3.5", Market.FH_TG_OVER_35), ("Menos 3.5", Market.FH_TG_UNDER_35),
                    ("Más 4.5", Market.FH_TG_OVER_45), ("Menos 4.5", Market.FH_TG_UNDER_45),
                ]
            }
        },
        "Goles Totales (Exacto)": {
            "method": _get_odd_by_text,
            "params": {
                "text_markets": [
                    ("0 ", Market.FT_TG_EXACT_0)
                ]
            }
        },
        "Ambos equipos marcan en la 1ª Mitad": {
            "method": _get_odd_by_position,
            "params": {
                "markets": [
                    Market.FH_BTTS_YES, Market.FH_BTTS_NO
                ]
            }
        }
    }

    def __init__(self):
        super(FootballWebBase, self).__init__()
        if self.url_path_params is None or "category_id" not in self.url_path_params or \
                "category_label" not in self.url_path_params:
            raise BaseException("You must define a 'url_path_params = {0}' attribute in your {1}.{2} class.".format(
                "{'category_id': '', 'category_label': ''}", type(self).__module__, type(self).__name__
            ))

    @cache(maxsize=None)
    def request_football_category(self):
        self.request_set_utc_tz()
        return self.get(self.FOOTBALL_CATEGORY_URL)

    def _lazy_get_event_detail(self, html_obj, validate=True):
        try:
            event_id = html_obj.find_element_by_xpath(".//div[@data-ev_id]").get("data-ev_id")
            event_href = html_obj.find_element_by_xpath(".//a[@title='Número de mercados']").get("href")
            category = self.get_category()
            home_tname, away_tname = [elem.text
                                      for elem in html_obj.find_elements_by_xpath(".//span[@class='seln-name']")]
            dtime = self.get_datetime(html_obj)
            event = Event(
                bookies={
                    str(Bookie.SPORTIUM.value): BookieId(
                        bookie=Bookie.SPORTIUM,
                        value={
                            "id": event_id,
                            "href": event_href
                        })
                },
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                html_source=html_obj,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    def lazy_get_category_events(self, **kwargs):
        validate = kwargs.pop("validate", True)
        all_events = []
        try:
            res = self.request_football_category()
            for html_elem in res.html.find_elements_by_xpath("//tr[@data-mkt_id]"):
                event = self._lazy_get_event_detail(html_elem, validate=validate)
                if event is not None:
                    all_events.append(event)
        except:
            pass
        return all_events

    def get_markets(self, html_list):
        odds = []
        for title, code in self.markets_specification.items():
            for html in html_list:
                elem = html.find_element_by_xpath(f"//span[@class='mkt-name' and normalize-space()='{title}']")
                if elem is not None:
                    content = elem.getparent(height=2).find_element_by_xpath(".//div[@class='expander-content']/*")
                    if content is not None:
                        odds.extend(code["method"](content, **code["params"]))
                        break
        return odds

    def _get_start_date_from_detail(self, html_obj):
        text = html_obj.find_element_by_xpath("//time[@itemprop='startDate']").get("datetime")

        dtime = datetime.strptime(text, "%Y-%m-%d %H:%M:%S")
        local_tz = pytz.utc
        return local_tz.localize(dtime)

    def get_event(self, event_id, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            res, htmls = self.request_event_detail(event_id)
            home_tname = res.html.find_element_by_xpath("//meta[@itemprop='performer'][1]").get("content")
            away_tname = res.html.find_element_by_xpath("//meta[@itemprop='performer'][2]").get("content")
            category = self.get_category()
            dtime = self._get_start_date_from_detail(res.html)
            markets = self.get_markets(html_list=htmls)
            event = Event(
                bookies={str(Bookie.SPORTIUM.value): BookieId(bookie=Bookie.SPORTIUM, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                odds=markets,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except Exception as err:  # pylint: disable=broad-except
            logger.error(err)

    @classmethod
    def get_team_names(cls, data, event_id=None):
        if event_id is None:
            raise BaseException("'event_id' can't be None")

        elem = data.find_element_by_xpath("//*[@data-event-treeid='%s']" % str(event_id))
        tname1, tname2 = elem.get("data-event-name").split(" vs ")
        return tname1, tname2

    def get_category_identifier(self, event_id):
        res = self.request_event_detail(event_id)
        elems = res.html.find_elements_by_xpath(
            "//div[@class='sport-category-content']/div[1]//*[@class='category-label']/span"
        )
        categories = [cat.text for cat in elems]
        return "".join(categories)


class FootballWeb(FootballWebBase):
    category = Category.UNKNOWN
    url_path_params = {"category_id": None, "category_label": None}
