from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import SpainSegundaDivisionMixin


class SegundaDivision(SpainSegundaDivisionMixin, FootballWebBase):
    url_path_params = {"category_id": "45215", "category_label": "Segunda-Divisi%C3%B3n"}
    category_name = "Spain.Segunda Division"
