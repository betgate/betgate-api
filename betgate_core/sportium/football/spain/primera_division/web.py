from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import SpainPrimeraDivisionMixin


class PrimeraDivision(SpainPrimeraDivisionMixin, FootballWebBase):
    url_path_params = {"category_id": "45211", "category_label": "La-Liga"}
    category_name = "Spain.Primera Division"
