from betgate_core.sportium.football.spain import primera_division, segunda_division


def get_events(**kwargs):
    return primera_division.get_events(**kwargs) + segunda_division.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return primera_division.lazy_get_events(**kwargs) + segunda_division.lazy_get_events(**kwargs)
