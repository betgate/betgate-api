from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandChampionshipMixin


class Championship(EnglandChampionshipMixin, FootballWebBase):
    url_path_params = {"category_id": "40602", "category_label": "Championship"}
    category_name = "England.Championship"
