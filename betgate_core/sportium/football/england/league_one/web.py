from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandLeagueOneMixin


class LeagueOne(EnglandLeagueOneMixin, FootballWebBase):
    url_path_params = {"category_id": "40542", "category_label": "League-One"}
    category_name = "England.League 1"
