from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandNationalLeagueMixin


class NationalLeague(EnglandNationalLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "40627", "category_label": "Liga-Nacional"}
    category_name = "England.National League"
