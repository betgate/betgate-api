from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandNationalLeagueNorthMixin


class NationalLeagueNorth(EnglandNationalLeagueNorthMixin, FootballWebBase):
    url_path_params = {"category_id": "40333", "category_label": "National-League-North"}
    category_name = "England.National League North"
