from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandLeagueTwoMixin


class LeagueTwo(EnglandLeagueTwoMixin, FootballWebBase):
    url_path_params = {"category_id": "40535", "category_label": "League-Two"}
    category_name = "England.League 2"
