from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandPremierLeagueMixin


class PremierLeague(EnglandPremierLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "40527", "category_label": "Premier-League"}
    category_name = "England.Premier League"
