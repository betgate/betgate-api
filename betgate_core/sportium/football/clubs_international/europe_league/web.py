from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import ClubsInternationalEuropeLeagueMixin


class EuropeLeague(ClubsInternationalEuropeLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "45223", "category_label": "Europa-League"}
    category_name = "Europa League"
