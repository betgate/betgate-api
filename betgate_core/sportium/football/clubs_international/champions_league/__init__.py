from betgate_core.sportium.football.clubs_international.champions_league.web import ChampionsLeague


def get_events(**kwargs):
    with ChampionsLeague() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with ChampionsLeague() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with ChampionsLeague() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
