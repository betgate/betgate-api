from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import ClubsInternationalChampionsLeagueMixin


class ChampionsLeague(ClubsInternationalChampionsLeagueMixin, FootballWebBase):
    url_path_params = {"category_id": "45225", "category_label": "Champions-League"}
    category_name = "Champions League"
