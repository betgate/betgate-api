from betgate_core.sportium.football.clubs_international import champions_league
from betgate_core.sportium.football.clubs_international import europe_league


def get_events(**kwargs):
    return champions_league.get_events(**kwargs) + \
           europe_league.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return champions_league.lazy_get_events(**kwargs) + \
           europe_league.lazy_get_events(**kwargs)
