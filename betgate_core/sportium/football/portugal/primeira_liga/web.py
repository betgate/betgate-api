from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import PortugalPrimeiraLigaMixin


class PrimeiraLiga(PortugalPrimeiraLigaMixin, FootballWebBase):
    url_path_params = {"category_id": "44979", "category_label": "Primeira-Liga"}
    category_name = "Portugal.Primeira Liga"
