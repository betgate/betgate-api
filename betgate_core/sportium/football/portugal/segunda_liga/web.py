from betgate_core.sportium.football.web import FootballWebBase
from betgate_core.mixins.football import PortugalSegundaLigaMixin


class SegundaLiga(PortugalSegundaLigaMixin, FootballWebBase):
    url_path_params = {"category_id": "44977", "category_label": "Liga-de-Honra"}
    category_name = "Portugal.Segunda Liga"
