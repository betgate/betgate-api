from datetime import datetime
import pytz
from sdklib.http.renderers import FormRenderer
from betgate_models.enums import Bookie
from betgate_core.web import BookieWebBase
from betgate_core.utils import locale_month_to_month


class SportiumWebBase(BookieWebBase):

    DEFAULT_HOST = "https://sports.sportium.es"

    TZ_URL = "/web_nr"

    category_name = None
    bookie = Bookie.SPORTIUM

    @property
    def category_identifier(self):
        return self.category_name

    def get_datetime(self, html_obj):
        hour, minute = html_obj.find_element_by_xpath(".//span[@class='time']").text.split(":")
        day, month_text = html_obj.find_element_by_xpath(".//span[@class='date']").text.split()
        month = locale_month_to_month(month_text)
        year = datetime.now().year

        dtime = datetime(year=year, month=month, day=int(day), hour=int(hour), minute=int(minute))
        local_tz = pytz.utc
        return local_tz.localize(dtime)

    def request_set_utc_tz(self):
        body_params = {
            "tz_offset": 0,
            "key": "region.do_set_region_opts"
        }
        self.post(self.TZ_URL, body_params=body_params, renderer=FormRenderer())

    def request_event_detail(self, event_id):
        tabs = [
            {"mkt_grp_code": "MITAD"},
            {"mkt_grp_code": "OTHER"}
            #{"mkt_grp_code": "HCP_S"}
        ]
        main_res = self.get(event_id["href"])
        html_tabs = [main_res.html]
        for query_params in tabs:
            html_tabs.append(
                self.get(event_id["href"], query_params=query_params).html
            )
        return main_res, html_tabs
