from datetime import datetime
import pytz
from sdklib.shortcuts import cache
from betgate_models.enums import Bookie
from betgate_core.web import BookieWebBase
from betgate_core.utils import locale_month_to_month


class RetabetWebBase(BookieWebBase):

    DEFAULT_HOST = "https://apuestas.retabet.es"

    EVENT_DETAIL_URL_PATH = "/ApuestasDeportivasDetalle.aspx"
    MENU_URL_PATH = "/ObtenerMenu.aspx"
    GET_CENTER_URL_PATH = "/ApuestasDeportivas.aspx/GetCenter"

    category_name = None
    bookie = Bookie.RETABET

    @property
    def category_identifier(self):
        return self.category_name

    def default_headers(self):
        headers = super(RetabetWebBase, self).default_headers()
        headers["Referer"] = "https://apuestas.retabet.es/ApuestasDeportivasHome.aspx"
        return headers

    def get_datetime(self, date_text, time_text):
        try:
            date_split_text = date_text.split()
            time_split_text = time_text.split(":")
            if len(date_split_text) > 5 and len(time_split_text) == 2:
                day = int(date_split_text[1])
                month = locale_month_to_month(date_split_text[3])
                year = int(date_split_text[5])
                hour = int(time_split_text[0])
                minute = int(time_split_text[1])
            else:
                return
            dtime = datetime(year=year, month=month, day=day, hour=hour, minute=minute)
            local_tz = pytz.timezone('Europe/Madrid')
            return local_tz.localize(dtime)
        except:
            pass

    def get_datetime_from_list_view(self, html_elem, date_text):
        try:
            time_text = html_elem.find_element_by_xpath(".//td[contains(@class ,'td_hora')]/span").text
            return self.get_datetime(date_text, time_text)
        except:
            pass

    def get_datetime_from_detail_view(self, html):
        try:
            date_text = html.find_element_by_xpath("//div[@class='apu_fecha']").text
            time_text = html.find_element_by_xpath("//div[@class='apu_fecha']/span").text
            return self.get_datetime(date_text, time_text.split()[0])
        except:
            pass

    @cache(maxsize=None)
    def request_event_detail(self, event_id):
        return self.get(self.EVENT_DETAIL_URL_PATH, query_params={"id": event_id})
