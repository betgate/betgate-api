from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import NetherlandsEredivisieMixin


class Eredivisie(NetherlandsEredivisieMixin, FootballWebBase):
    category_id = 122
    category_name = "HOLANDA EREDIVISIE"
