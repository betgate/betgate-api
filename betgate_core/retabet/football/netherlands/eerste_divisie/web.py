from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import NetherlandsEersteDivisieMixin


class EersteDivisie(NetherlandsEersteDivisieMixin, FootballWebBase):
    category_id = "345004"
    category_name = "Netherlands.Eerste Divisie"
