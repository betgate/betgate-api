from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import UsaMlsMixin


class Mls(UsaMlsMixin, FootballWebBase):
    category_id = 967
    category_name = "MAJOR LEAGUE SOCCER"
