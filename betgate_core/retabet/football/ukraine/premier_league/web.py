from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import UkrainePremierLeagueMixin


class PremierLeague(UkrainePremierLeagueMixin, FootballWebBase):
    category_id = 1399
    category_name = "UCRANIA PREMIER LEAGUE"
