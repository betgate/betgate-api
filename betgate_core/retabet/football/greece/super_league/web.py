from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import GreeceSuperLeagueMixin


class SuperLeague(GreeceSuperLeagueMixin, FootballWebBase):
    category_id = 1251
    category_name = "GRECIA SUPER LIGA"
