from betgate_core.retabet.football.sweden import allsvenskan
from betgate_core.retabet.football.sweden import superettan


def get_events(**kwargs):
    return allsvenskan.get_events(**kwargs) + \
           superettan.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return allsvenskan.lazy_get_events(**kwargs) + \
           superettan.lazy_get_events(**kwargs)
