from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import SwedenAllsvenskanMixin


class Allsvenskan(SwedenAllsvenskanMixin, FootballWebBase):
    category_id = 1180
    category_name = "SUECIA ALLSVENSKAN"
