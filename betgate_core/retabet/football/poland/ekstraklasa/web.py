from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import PolandEkstraklasaMixin


class Ekstraklasa(PolandEkstraklasaMixin, FootballWebBase):
    category_id = 1443
    category_name = "POLONIA EKSTRAKLASA"
