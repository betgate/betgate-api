from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import FinlandVeikkausliigaMixin


class Veikkausliiga(FinlandVeikkausliigaMixin, FootballWebBase):
    category_id = -1
    category_name = ""
