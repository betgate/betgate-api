from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import SpainSegundaDivisionMixin


class SegundaDivision(SpainSegundaDivisionMixin, FootballWebBase):
    category_id = 2
    category_name = "LALIGA 1,2,3"
