from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import SpainPrimeraDivisionMixin


class PrimeraDivision(SpainPrimeraDivisionMixin, FootballWebBase):
    category_id = 1
    category_name = "LALIGA SANTANDER"
