from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import SwitzerlandSuperLeagueMixin


class SuperLeague(SwitzerlandSuperLeagueMixin, FootballWebBase):
    category_id = 1430
    category_name = "SUIZA SUPER LIGA"
