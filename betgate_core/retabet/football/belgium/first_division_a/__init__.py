from betgate_core.retabet.football.belgium.first_division_a.web import FirstDivisionA


def get_events(**kwargs):
    with FirstDivisionA() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with FirstDivisionA() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with FirstDivisionA() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
