from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import BelgiumFirstDivisionAMixin


class FirstDivisionA(BelgiumFirstDivisionAMixin, FootballWebBase):
    category_id = 697
    category_name = "BÉLGICA PRO LEAGUE"
