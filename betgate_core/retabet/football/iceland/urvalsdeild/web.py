from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import IcelandUrvalsdeildMixin


class Urvalsdeild(IcelandUrvalsdeildMixin, FootballWebBase):
    category_id = 1384
    category_name = "ISLANDIA PEPSIDEILD"
