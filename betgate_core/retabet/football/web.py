from urllib.parse import urlparse, parse_qsl
from sdklib.shortcuts import cache
from betgate_models.models import Odd, Event, BookieId
from betgate_models.enums import Market, Category, Bookie
from betgate_core.retabet.web import RetabetWebBase


def _get_markets_by_cell_position(html_elem, markets):
    elems = html_elem.find_elements_by_xpath(".//tr/td/a[@data-cf]")
    return [
        Odd(value=elem.get("data-cf").replace(",", "."), market=mkt, bookie=Bookie.RETABET)
        for elem, mkt in zip(elems, markets)
    ]


def _get_market_by_text(html_elem, text_markets):
    to_return = []

    elems = html_elem.find_elements_by_xpath(".//tr/td/a[@data-cf]")
    for elem in elems:
        text_elem = elem.find_element_by_xpath("./span[@class='texto_opcion']")
        mkt = text_markets.get(text_elem.text, None) if text_elem else None
        if mkt:
            to_return.append(Odd(value=elem.get("data-cf").replace(",", "."), market=mkt, bookie=Bookie.RETABET))
    return to_return


class FootballWebBase(RetabetWebBase):
    BETTING_FOOTBALL_URL = "/es/betting/Football/"
    FOOTBALL_CATEGORY_URL = "/en/betting/Football/{country}/{category}/"

    category_id = None

    get_markets_specification = {
        "1-X-2": (_get_markets_by_cell_position, [Market.FT_WIN_1, Market.FT_WIN_X, Market.FT_WIN_2]),
        "1-X-2 1º TIEMPO": (_get_markets_by_cell_position, [Market.FH_WIN_1, Market.FH_WIN_X, Market.FH_WIN_2]),
        "DOBLE OPORTUNIDAD": (_get_markets_by_cell_position, [Market.FT_WIN_1X, Market.FT_WIN_X2, Market.FT_WIN_12]),
        "1º TIEMPO DOBLE OPORTUNIDAD": (_get_markets_by_cell_position, [
            Market.FH_WIN_1X, Market.FH_WIN_X2, Market.FH_WIN_12
        ]),
        "MARCARÁN AMBOS EQUIPOS": (_get_markets_by_cell_position, [Market.FT_BTTS_YES, Market.FT_BTTS_NO]),
        "APUESTA SIN EMPATE": (_get_markets_by_cell_position, [Market.FT_WIN_DNB_1, Market.FT_WIN_DNB_2]),
        "1º TIEMPO APUESTA SIN EMPATE": (_get_markets_by_cell_position, [Market.FH_WIN_DNB_1, Market.FH_WIN_DNB_2]),
        "MÁS/MENOS GOLES 0,5": (_get_markets_by_cell_position, [Market.FT_TG_OVER_05, Market.FT_TG_UNDER_05]),
        "MÁS/MENOS GOLES 1,5": (_get_markets_by_cell_position, [Market.FT_TG_OVER_15, Market.FT_TG_UNDER_15]),
        "MÁS/MENOS GOLES 2,5": (_get_markets_by_cell_position, [Market.FT_TG_OVER_25, Market.FT_TG_UNDER_25]),
        "MÁS/MENOS GOLES 3,5": (_get_markets_by_cell_position, [Market.FT_TG_OVER_35, Market.FT_TG_UNDER_35]),
        "MÁS/MENOS GOLES 4,5": (_get_markets_by_cell_position, [Market.FT_TG_OVER_45, Market.FT_TG_UNDER_45]),
        "MÁS/MENOS GOLES 5,5": (_get_markets_by_cell_position, [Market.FT_TG_OVER_55, Market.FT_TG_UNDER_55]),
        "MÁS/MENOS GOLES AL DESCANSO 0,5": (_get_markets_by_cell_position, [
            Market.FH_TG_OVER_05, Market.FH_TG_UNDER_05
        ]),
        "MÁS/MENOS GOLES AL DESCANSO 1,5": (_get_markets_by_cell_position, [
            Market.FH_TG_OVER_15, Market.FH_TG_UNDER_15
        ]),
        "MÁS/MENOS GOLES AL DESCANSO 2,5": (_get_markets_by_cell_position, [
            Market.FH_TG_OVER_25, Market.FH_TG_UNDER_25
        ]),
        "MÁS/MENOS GOLES AL DESCANSO 3,5": (_get_markets_by_cell_position, [
            Market.FH_TG_OVER_35, Market.FH_TG_UNDER_35
        ]),
        "MÁS/MENOS GOLES AL DESCANSO 4,5": (_get_markets_by_cell_position, [
            Market.FH_TG_OVER_45, Market.FH_TG_UNDER_45
        ]),
        "HÁNDICAP (1:0)": (_get_markets_by_cell_position, [
            Market.FT_WIN_1_P10, Market.FT_DRAW_1_P10, Market.FT_WIN_2_M10
        ]),
        "HÁNDICAP (2:0)": (_get_markets_by_cell_position, [
            Market.FT_WIN_1_P20, Market.FT_DRAW_1_P20, Market.FT_WIN_2_M20
        ]),
        "HÁNDICAP (3:0)": (_get_markets_by_cell_position, [
            Market.FT_WIN_1_P30, Market.FT_DRAW_1_P30, Market.FT_WIN_2_M30
        ]),
        "HÁNDICAP (4:0)": (_get_markets_by_cell_position, [
            Market.FT_WIN_1_P40, Market.FT_DRAW_1_P40, Market.FT_WIN_2_M40
        ]),
        "HÁNDICAP (0:1)": (_get_markets_by_cell_position, [
            Market.FT_WIN_1_M10, Market.FT_DRAW_1_M10, Market.FT_WIN_2_P10
        ]),
        "HÁNDICAP (0:2)": (_get_markets_by_cell_position, [
            Market.FT_WIN_1_M20, Market.FT_DRAW_1_M20, Market.FT_WIN_2_P20
        ]),
        "HÁNDICAP (0:3)": (_get_markets_by_cell_position, [
            Market.FT_WIN_1_M30, Market.FT_DRAW_1_M30, Market.FT_WIN_2_P30
        ]),
        "HÁNDICAP (0:4)": (_get_markets_by_cell_position, [
            Market.FT_WIN_1_M40, Market.FT_DRAW_1_M40, Market.FT_WIN_2_P40
        ]),
        "1º TIEMPO: HÁNDICAP (1:0)": (_get_markets_by_cell_position, [
            Market.FH_WIN_1_P10, Market.FH_DRAW_1_P10, Market.FH_WIN_2_M10
        ]),
        "1º TIEMPO: HÁNDICAP (2:0)": (_get_markets_by_cell_position, [
            Market.FH_WIN_1_P20, Market.FH_DRAW_1_P20, Market.FH_WIN_2_M20
        ]),
        "1º TIEMPO: HÁNDICAP (3:0)": (_get_markets_by_cell_position, [
            Market.FH_WIN_1_P30, Market.FH_DRAW_1_P30, Market.FH_WIN_2_M30
        ]),
        "1º TIEMPO: HÁNDICAP (0:1)": (_get_markets_by_cell_position, [
            Market.FH_WIN_1_M10, Market.FH_DRAW_1_M10, Market.FH_WIN_2_P10
        ]),
        "1º TIEMPO: HÁNDICAP (0:2)": (_get_markets_by_cell_position, [
            Market.FH_WIN_1_M20, Market.FH_DRAW_1_M20, Market.FH_WIN_2_P20
        ]),
        "RESULTADO EXACTO": (_get_market_by_text, {
            "0-0": Market.FT_RESULT_0_0
        }),
        "RESULTADO EXACTO AL DESCANSO": (_get_market_by_text, {
            "0-0": Market.FH_RESULT_0_0
        }),
    }

    def __init__(self):
        super(FootballWebBase, self).__init__()
        if self.category_id is None:
            raise BaseException("You must define a 'category_id' attribute in your {0}.{1} class.".format(
                type(self).__module__, type(self).__name__
            ))

    @cache(maxsize=None)
    def request_event_category(self):
        query_params = {
            "idMod": "",
            "selecMod": 0,
            "idSubMod": self.category_id,
            "selecSubMod": 1,
            "dat": 0.9734184953736369,
            "_": 1520872064114
        }
        self.get(self.MENU_URL_PATH, query_params=query_params)
        return self.get(self.GET_CENTER_URL_PATH, query_params={"withInfoOfSubmodalidad": ""})

    def _lazy_get_event_detail(self, html_elem_obj, date_text=None, event_id=None, date_time=None, validate=True):
        try:
            category = self.get_category()
            event_id = event_id or self.get_event_id(html_elem_obj)
            home_tname, away_tname = self.get_team_names(html_elem_obj)
            dtime = date_time or self.get_datetime_from_list_view(html_elem_obj, date_text)
            event = Event(
                bookies={str(Bookie.RETABET.value): BookieId(bookie=Bookie.RETABET, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                html_source=html_elem_obj,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    def lazy_get_category_events(self, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            all_events = []

            res = self.request_event_category()
            for elem in res.html.find_elements_by_xpath(
                    "//h3[@class='apu_titulo' and text()='1-X-2']/ancestor::div[@class='apu_detalle']"):
                date_text = elem.find_element_by_xpath(".//div[@class='apu_fecha']").text
                tr_elems = elem.find_elements_by_xpath(".//div[@class='apu_opciones']//table//tr")
                for tr_elem in tr_elems:
                    event_detail = self._lazy_get_event_detail(tr_elem, date_text, validate=validate)
                    if event_detail is not None:
                        all_events.append(event_detail)

            for elem in res.html.find_elements_by_xpath(
                    "//h3[@class='apu_titulo' and contains(text(),'| 1-X-2')]/ancestor::div[@class='apu_detalle']"):
                try:
                    div_fecha_elem = elem.find_element_by_xpath(".//div[@class='apu_fecha']")
                    date_text = div_fecha_elem.text
                    time_text = div_fecha_elem.find_element_by_xpath("./span").text
                    date_time = self.get_datetime(date_text, time_text)
                    event_id = self.get_event_id(div_fecha_elem)
                    tr_elem = elem.find_element_by_xpath(".//div[@class='apu_opciones']//table//tr")

                    event_detail = self._lazy_get_event_detail(
                        tr_elem, event_id=event_id, date_time=date_time, validate=validate
                    )
                    if event_detail is not None:
                        all_events.append(event_detail)
                except:
                    pass

            return all_events
        except:
            return []

    def get_markets(self, html):
        markets_to_return = []
        elems = html.find_elements_by_xpath("//div[@class='apu_evento']/h3[@class='apu_titulo']")
        for elem in elems:
            if elem.text in self.get_markets_specification:
                func_call = self.get_markets_specification[elem.text][0]
                func_args = self.get_markets_specification[elem.text][1:]
                markets_to_return.extend(func_call(elem.getparent(height=2), *func_args))
        return markets_to_return

    def get_event(self, event_id, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            res = self.request_event_detail(event_id)
            home_tname, away_tname = self.get_team_names_from_detail_view(res.html)
            category = self.get_category()
            dtime = self.get_datetime_from_detail_view(res.html)
            markets = self.get_markets(res.html)
            event = Event(
                bookies={str(Bookie.RETABET.value): BookieId(bookie=Bookie.RETABET, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=dtime,
                odds=markets,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    @classmethod
    def get_team_names(cls, data):
        try:
            span_elems = data.find_elements_by_xpath(".//a/span[@class='texto_opcion']")
            tname_home = span_elems[0].text
            tname_away = span_elems[2].text
            return tname_home, tname_away
        except:
            return None, None

    @classmethod
    def get_team_names_from_detail_view(cls, data):
        try:
            tnames_split = data.find_element_by_id(
                "ctl00_MainContent_spanTituloEvento"
            ).text.split(" - ")
            return tnames_split[0].strip(), tnames_split[1].strip()
        except:
            return None, None

    @classmethod
    def get_event_id(cls, data):
        try:
            href = data.find_element_by_xpath(".//a[@href and @class='apu_detalle_mas']").get("href")
            parsed_url = urlparse(href)
            return dict(parse_qsl(parsed_url.query))["id"]
        except:
            pass

    def get_category_identifier(self, event_id):
        try:
            res = self.request_event_detail(event_id)
            cname_split = res.html.find_element_by_id(
                "ctl00_MainContent_divDatoModalidadSubmodalidad"
            ).text.split(":")
            return cname_split[1].strip()
        except:
            return None


class FootballWeb(FootballWebBase):
    category = Category.UNKNOWN
    category_id = -1
