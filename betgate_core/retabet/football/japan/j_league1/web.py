from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import JapanJLeague1Mixin


class JLeague1(JapanJLeague1Mixin, FootballWebBase):
    category_id = 983
    category_name = "JAPÓN J-LEAGUE"
