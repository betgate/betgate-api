from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import ItalySerieBMixin


class SerieB(ItalySerieBMixin, FootballWebBase):
    category_id = 1103
    category_name = "ITALIA SERIE B"
