from betgate_core.retabet.football.italy.serie_a.web import SerieA


def get_events(**kwargs):
    with SerieA() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with SerieA() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with SerieA() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
