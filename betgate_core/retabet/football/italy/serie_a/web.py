from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import ItalySerieAMixin


class SerieA(ItalySerieAMixin, FootballWebBase):
    category_id = 7
    category_name = "SERIE A"
