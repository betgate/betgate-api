from betgate_core.retabet.football.scotland import premiership


def get_events(**kwargs):
    return premiership.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return premiership.lazy_get_events(**kwargs)
