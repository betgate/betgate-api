from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import ScotlandPremiershipMixin


class Premiership(ScotlandPremiershipMixin, FootballWebBase):
    category_id = 367
    category_name = "SCOTTISH PREMIERSHIP"
