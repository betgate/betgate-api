from betgate_core.retabet.football.england.national_league.web import NationalLeague


def get_events(**kwargs):
    with NationalLeague() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with NationalLeague() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with NationalLeague() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
