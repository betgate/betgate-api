from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandNationalLeagueMixin


class NationalLeague(EnglandNationalLeagueMixin, FootballWebBase):
    category_id = 3842
    category_name = "NATIONAL LEAGUE"
