from betgate_core.retabet.football.england.league_two.web import LeagueTwo


def get_events(**kwargs):
    with LeagueTwo() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with LeagueTwo() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with LeagueTwo() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
