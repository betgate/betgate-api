from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandLeagueTwoMixin


class LeagueTwo(EnglandLeagueTwoMixin, FootballWebBase):
    category_id = "22809"
    category_name = "England.League 2"
