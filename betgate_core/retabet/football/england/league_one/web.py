from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandLeagueOneMixin


class LeagueOne(EnglandLeagueOneMixin, FootballWebBase):
    category_id = 618
    category_name = "FOOTBALL LEAGUE ONE"
