from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandPremierLeagueMixin


class PremierLeague(EnglandPremierLeagueMixin, FootballWebBase):
    category_id = 6
    category_name = "PREMIER LEAGUE"
