from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandChampionshipMixin


class Championship(EnglandChampionshipMixin, FootballWebBase):
    category_id = 619
    category_name = "FOOTBALL LEAGUE CHAMPIONSHIP"
