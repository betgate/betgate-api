from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import EnglandNationalLeagueNorthMixin


class NationalLeagueNorth(EnglandNationalLeagueNorthMixin, FootballWebBase):
    category_id = 6131
    category_name = "NATIONAL LEAGUE NORTH"
