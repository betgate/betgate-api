from betgate_core.retabet.football.croatia import first_league


def get_events(**kwargs):
    return first_league.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return first_league.lazy_get_events(**kwargs)
