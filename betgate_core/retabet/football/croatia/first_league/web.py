from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import CroatiaFirstLeagueMixin


class FirstLeague(CroatiaFirstLeagueMixin, FootballWebBase):
    category_id = 1894
    category_name = "CROACIA HNL"
