from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import MoroccoBotolaMixin


class Botola(MoroccoBotolaMixin, FootballWebBase):
    category_id = 2838
    category_name = "MARRUECOS BOTOLA"
