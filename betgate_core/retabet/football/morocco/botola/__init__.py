from betgate_core.retabet.football.morocco.botola.web import Botola


def get_events(**kwargs):
    with Botola() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Botola() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Botola() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
