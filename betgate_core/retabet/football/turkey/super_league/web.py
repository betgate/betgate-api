from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import TurkeySuperLeagueMixin


class SuperLeague(TurkeySuperLeagueMixin, FootballWebBase):
    category_id = 117
    category_name = "TURQUÍA SUPERLIGA"
