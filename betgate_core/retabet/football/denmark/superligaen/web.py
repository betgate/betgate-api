from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import DenmarkSuperligaenMixin


class Superligaen(DenmarkSuperligaenMixin, FootballWebBase):
    category_id = -1
    category_name = ""
