from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import AustriaBundesligaMixin


class Bundesliga(AustriaBundesligaMixin, FootballWebBase):
    category_id = -1
    category_name = ""
