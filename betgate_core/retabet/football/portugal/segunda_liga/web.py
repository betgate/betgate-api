from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import PortugalSegundaLigaMixin


class SegundaLiga(PortugalSegundaLigaMixin, FootballWebBase):
    category_id = 1720
    category_name = "PORTUGAL SEGUNDA LIGA"
