from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import PortugalPrimeiraLigaMixin


class PrimeiraLiga(PortugalPrimeiraLigaMixin, FootballWebBase):
    category_id = 123
    category_name = "PORTUGAL LIGA NOS"
