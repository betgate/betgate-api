from betgate_core.retabet.football.portugal.primeira_liga.web import PrimeiraLiga


def get_events(**kwargs):
    with PrimeiraLiga() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with PrimeiraLiga() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with PrimeiraLiga() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
