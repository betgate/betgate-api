from betgate_core.retabet.football.portugal import primeira_liga, segunda_liga


def get_events(**kwargs):
    return primeira_liga.get_events(**kwargs) + segunda_liga.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return primeira_liga.lazy_get_events(**kwargs) + segunda_liga.lazy_get_events(**kwargs)
