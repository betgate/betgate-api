from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import BrazilSerieBMixin


class SerieB(BrazilSerieBMixin, FootballWebBase):
    category_id = 39039
    category_name = "BRASIL SERIE B"
