from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import BrazilSerieAMixin


class SerieA(BrazilSerieAMixin, FootballWebBase):
    category_id = 326
    category_name = "BRASIL SERIE A"
