from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import SerbiaSuperLigaMixin


class SuperLiga(SerbiaSuperLigaMixin, FootballWebBase):
    category_id = -1
    category_name = ""
