from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import ChilePrimeraDivisionMixin


class PrimeraDivision(ChilePrimeraDivisionMixin, FootballWebBase):
    category_id = 1447
    category_name = "CHILE PRIMERA DIVISIÓN"
