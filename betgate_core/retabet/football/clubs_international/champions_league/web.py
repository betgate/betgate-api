from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import ClubsInternationalChampionsLeagueMixin


class ChampionsLeague(ClubsInternationalChampionsLeagueMixin, FootballWebBase):
    category_id = -1
    category_name = ""
