from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import EcuadorSerieAMixin


class SerieA(EcuadorSerieAMixin, FootballWebBase):
    category_id = 2321
    category_name = "ECUADOR SERIE A"
