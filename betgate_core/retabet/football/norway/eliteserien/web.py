from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import NorwayEliteserienMixin


class Eliteserien(NorwayEliteserienMixin, FootballWebBase):
    category_id = 1178
    category_name = "NORUEGA ELITESERIEN"
