from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import GermanyBundesligaMixin


class Bundesliga(GermanyBundesligaMixin, FootballWebBase):
    category_id = 8
    category_name = "BUNDESLIGA"
