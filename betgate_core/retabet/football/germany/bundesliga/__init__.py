from betgate_core.retabet.football.germany.bundesliga.web import Bundesliga


def get_events(**kwargs):
    with Bundesliga() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Bundesliga() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Bundesliga() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
