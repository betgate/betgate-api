from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import GermanyBundesliga2Mixin


class Bundesliga2(GermanyBundesliga2Mixin, FootballWebBase):
    category_id = 690
    category_name = "BUNDESLIGA 2"
