from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import RussiaPremierLeagueMixin


class PremierLeague(RussiaPremierLeagueMixin, FootballWebBase):
    category_id = 312
    category_name = "RUSIA PREMIER LEAGUE"
