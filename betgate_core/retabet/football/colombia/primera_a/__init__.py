from betgate_core.retabet.football.colombia.primera_a.web import PrimeraA


def get_events(**kwargs):
    with PrimeraA() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with PrimeraA() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with PrimeraA() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
