from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import ColombiaPrimeraAMixin


class PrimeraA(ColombiaPrimeraAMixin, FootballWebBase):
    category_id = 1455
    category_name = "COLOMBIA LIGA ÁGUILA"
