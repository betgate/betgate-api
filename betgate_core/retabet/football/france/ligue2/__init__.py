from betgate_core.retabet.football.france.ligue2.web import Ligue2


def get_events(**kwargs):
    with Ligue2() as web:
        return web.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Ligue2() as web:
        return web.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Ligue2() as web:
        event = web.get_event(event_id)
        return event.odds if event is not None else []
