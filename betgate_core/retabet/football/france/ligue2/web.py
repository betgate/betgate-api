from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import FranceLigue2Mixin


class Ligue2(FranceLigue2Mixin, FootballWebBase):
    category_id = 1236
    category_name = "LIGUE 2"
