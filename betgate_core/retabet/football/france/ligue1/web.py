from betgate_core.retabet.football.web import FootballWebBase
from betgate_core.mixins.football import FranceLigue1Mixin


class Ligue1(FranceLigue1Mixin, FootballWebBase):
    category_id = 9
    category_name = "LIGUE 1"
