from betgate_models.enums import Bookie
from betgate_core.api import BookieApiBase


class WilliamHillApiBase(BookieApiBase):

    DEFAULT_HOST = "https://mobet.williamhill.es"

    COMPETITION_API_URL = "/data/competition/{category_id}/pre-match"
    EVENT_API_URL = "/data/event/{event_id}"

    url_path_params = {"category_id": None}

    category_name = None
    bookie = Bookie.WILLIAM_HILL

    def request_event_detail(self, event_id):
        return self.get(self.EVENT_API_URL.format(event_id=event_id))

    def request_category_detail(self):
        return self.get(self.COMPETITION_API_URL)
