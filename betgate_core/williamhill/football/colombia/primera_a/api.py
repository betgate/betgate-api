from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import ColombiaPrimeraAMixin


class PrimeraA(ColombiaPrimeraAMixin, FootballApiBase):
    url_path_params = {"category_id": 3481}
    category_name = "Primera División"
