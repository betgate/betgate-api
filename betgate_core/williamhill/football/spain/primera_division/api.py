from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import SpainPrimeraDivisionMixin


class PrimeraDivision(SpainPrimeraDivisionMixin, FootballApiBase):
    url_path_params = {"category_id": 338}
    category_name = "LaLiga"
