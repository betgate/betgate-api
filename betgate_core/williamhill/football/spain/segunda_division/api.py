from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import SpainSegundaDivisionMixin


class SegundaDivision(SpainSegundaDivisionMixin, FootballApiBase):
    url_path_params = {"category_id": 32982}
    category_name = "LaLiga 123"
