from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import BrazilSerieAMixin


class SerieA(BrazilSerieAMixin, FootballApiBase):
    url_path_params = {"category_id": 1147}
    category_name = "Paulista"
