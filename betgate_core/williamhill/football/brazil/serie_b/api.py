from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import BrazilSerieBMixin


class SerieB(BrazilSerieBMixin, FootballApiBase):
    url_path_params = {"category_id": 1424}
    category_name = "Campeonato Gaucho"
