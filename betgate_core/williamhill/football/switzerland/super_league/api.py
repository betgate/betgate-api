from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import SwitzerlandSuperLeagueMixin


class SuperLeague(SwitzerlandSuperLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 342}
    category_name = "Super League"
