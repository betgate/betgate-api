from betgate_core.williamhill.football.switzerland.super_league.api import SuperLeague


def get_events(**kwargs):
    with SuperLeague() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with SuperLeague() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with SuperLeague() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
