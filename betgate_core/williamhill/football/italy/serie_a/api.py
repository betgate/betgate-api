from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import ItalySerieAMixin


class SerieA(ItalySerieAMixin, FootballApiBase):
    url_path_params = {"category_id": 321}
    category_name = "Serie A"
