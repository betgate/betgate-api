from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import ItalySerieBMixin

class SerieB(ItalySerieBMixin, FootballApiBase):
    url_path_params = {"category_id": 23532}
    category_name = "Serie B"
