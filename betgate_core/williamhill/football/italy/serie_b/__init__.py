from betgate_core.williamhill.football.italy.serie_b.api import SerieB


def get_events(**kwargs):
    with SerieB() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with SerieB() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with SerieB() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
