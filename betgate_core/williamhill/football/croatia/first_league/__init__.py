from betgate_core.williamhill.football.croatia.first_league.api import FirstLeague


def get_events(**kwargs):
    with FirstLeague() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with FirstLeague() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with FirstLeague() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
