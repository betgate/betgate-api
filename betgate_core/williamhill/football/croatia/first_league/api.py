from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import CroatiaFirstLeagueMixin


class FirstLeague(CroatiaFirstLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 302}
    category_name = "1. HNL Liga"
