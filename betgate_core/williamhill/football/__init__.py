from betgate_core.williamhill.football import austria
from betgate_core.williamhill.football import belgium
from betgate_core.williamhill.football import brazil
from betgate_core.williamhill.football import chile
from betgate_core.williamhill.football import clubs_international
from betgate_core.williamhill.football import colombia
from betgate_core.williamhill.football import croatia
from betgate_core.williamhill.football import denmark
from betgate_core.williamhill.football import ecuador
from betgate_core.williamhill.football import england
from betgate_core.williamhill.football import finland
from betgate_core.williamhill.football import france
from betgate_core.williamhill.football import germany
from betgate_core.williamhill.football import greece
from betgate_core.williamhill.football import iceland
from betgate_core.williamhill.football import italy
from betgate_core.williamhill.football import japan
from betgate_core.williamhill.football import morocco
from betgate_core.williamhill.football import netherlands
from betgate_core.williamhill.football import norway
from betgate_core.williamhill.football import poland
from betgate_core.williamhill.football import portugal
from betgate_core.williamhill.football import russia
from betgate_core.williamhill.football import scotland
from betgate_core.williamhill.football import serbia
from betgate_core.williamhill.football import spain
from betgate_core.williamhill.football import sweden
from betgate_core.williamhill.football import switzerland
from betgate_core.williamhill.football import turkey
from betgate_core.williamhill.football import ukraine
from betgate_core.williamhill.football import usa
from betgate_core.williamhill.football.api import FootballApi


def get_events(**kwargs):
    return austria.get_events(**kwargs) + \
           belgium.get_events(**kwargs) + \
           brazil.get_events(**kwargs) + \
           chile.get_events(**kwargs) + \
           clubs_international.get_events(**kwargs) + \
           colombia.get_events(**kwargs) + \
           croatia.get_events(**kwargs) + \
           denmark.get_events(**kwargs) + \
           ecuador.get_events(**kwargs) + \
           england.get_events(**kwargs) + \
           finland.get_events(**kwargs) + \
           france.get_events(**kwargs) + \
           germany.get_events(**kwargs) + \
           greece.get_events(**kwargs) + \
           iceland.get_events(**kwargs) + \
           italy.get_events(**kwargs) + \
           japan.get_events(**kwargs) + \
           morocco.get_events(**kwargs) + \
           netherlands.get_events(**kwargs) + \
           norway.get_events(**kwargs) + \
           poland.get_events(**kwargs) + \
           portugal.get_events(**kwargs) + \
           russia.get_events(**kwargs) + \
           scotland.get_events(**kwargs) + \
           serbia.get_events(**kwargs) + \
           spain.get_events(**kwargs) + \
           sweden.get_events(**kwargs) + \
           switzerland.get_events(**kwargs) + \
           turkey.get_events(**kwargs) + \
           ukraine.get_events(**kwargs) + \
           usa.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return austria.lazy_get_events(**kwargs) + \
           belgium.lazy_get_events(**kwargs) + \
           brazil.lazy_get_events(**kwargs) + \
           chile.lazy_get_events(**kwargs) + \
           clubs_international.lazy_get_events(**kwargs) + \
           colombia.lazy_get_events(**kwargs) + \
           croatia.lazy_get_events(**kwargs) + \
           denmark.lazy_get_events(**kwargs) + \
           ecuador.lazy_get_events(**kwargs) + \
           england.lazy_get_events(**kwargs) + \
           finland.lazy_get_events(**kwargs) + \
           france.lazy_get_events(**kwargs) + \
           germany.lazy_get_events(**kwargs) + \
           greece.lazy_get_events(**kwargs) + \
           iceland.lazy_get_events(**kwargs) + \
           italy.lazy_get_events(**kwargs) + \
           japan.lazy_get_events(**kwargs) + \
           morocco.lazy_get_events(**kwargs) + \
           netherlands.lazy_get_events(**kwargs) + \
           norway.lazy_get_events(**kwargs) + \
           poland.lazy_get_events(**kwargs) + \
           portugal.lazy_get_events(**kwargs) + \
           russia.lazy_get_events(**kwargs) + \
           scotland.lazy_get_events(**kwargs) + \
           serbia.lazy_get_events(**kwargs) + \
           spain.lazy_get_events(**kwargs) + \
           sweden.lazy_get_events(**kwargs) + \
           switzerland.lazy_get_events(**kwargs) + \
           turkey.lazy_get_events(**kwargs) + \
           ukraine.lazy_get_events(**kwargs) + \
           usa.lazy_get_events(**kwargs)


def get_odds(event_id):
    with FootballApi() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
