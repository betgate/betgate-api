from betgate_core.williamhill.football.norway import eliteserien


def get_events(**kwargs):
    return eliteserien.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return eliteserien.lazy_get_events(**kwargs)
