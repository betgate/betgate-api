from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import NorwayEliteserienMixin


class Eliteserien(NorwayEliteserienMixin, FootballApiBase):
    url_path_params = {"category_id": 9428}
    category_name = "Eliteserien"
