from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import ChilePrimeraDivisionMixin


class PrimeraDivision(ChilePrimeraDivisionMixin, FootballApiBase):
    url_path_params = {"category_id": 3139}
    category_name = "Primera"
