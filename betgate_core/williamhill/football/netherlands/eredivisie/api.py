from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import NetherlandsEredivisieMixin


class Eredivisie(NetherlandsEredivisieMixin, FootballApiBase):
    url_path_params = {"category_id": 306}
    category_name = "Eredivisie"
