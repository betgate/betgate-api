from betgate_core.williamhill.football.netherlands import eredivisie, eerste_divisie


def get_events(**kwargs):
    return eredivisie.get_events(**kwargs) + eerste_divisie.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return eredivisie.lazy_get_events(**kwargs) + eerste_divisie.lazy_get_events(**kwargs)
