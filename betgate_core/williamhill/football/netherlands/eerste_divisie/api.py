from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import NetherlandsEersteDivisieMixin


class EersteDivisie(NetherlandsEersteDivisieMixin, FootballApiBase):
    url_path_params = {"category_id": 1073}
    category_name = "Jupiler League"
