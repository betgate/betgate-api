from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import SerbiaSuperLigaMixin


class SuperLiga(SerbiaSuperLigaMixin, FootballApiBase):
    url_path_params = {"category_id": 335}
    category_name = "Super Liga"
