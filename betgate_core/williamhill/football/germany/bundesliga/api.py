from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import GermanyBundesligaMixin


class Bundesliga(GermanyBundesligaMixin, FootballApiBase):
    url_path_params = {"category_id": 315}
    category_name = "Bundesliga"
