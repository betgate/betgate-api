from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import GermanyBundesliga2Mixin


class Bundesliga2(GermanyBundesliga2Mixin, FootballApiBase):
    url_path_params = {"category_id": 317}
    category_name = "Bundesliga 2"
