from betgate_core.williamhill.football.germany.bundesliga2.api import Bundesliga2


def get_events(**kwargs):
    with Bundesliga2() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with Bundesliga2() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with Bundesliga2() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
