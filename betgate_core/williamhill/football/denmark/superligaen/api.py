from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import DenmarkSuperligaenMixin


class Superligaen(DenmarkSuperligaenMixin, FootballApiBase):
    url_path_params = {"category_id": 25893}
    category_name = ""
