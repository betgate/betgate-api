from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import TurkeySuperLeagueMixin


class SuperLeague(TurkeySuperLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 325}
    category_name = "Süper Liga"
