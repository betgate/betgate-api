from betgate_core.williamhill.football.turkey import super_league


def get_events(**kwargs):
    return super_league.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return super_league.lazy_get_events(**kwargs)
