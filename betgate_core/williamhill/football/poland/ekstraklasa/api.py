from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import PolandEkstraklasaMixin


class Ekstraklasa(PolandEkstraklasaMixin, FootballApiBase):
    url_path_params = {"category_id": 330}
    category_name = "Ekstraklasa"
