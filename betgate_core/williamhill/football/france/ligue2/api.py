from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import FranceLigue2Mixin


class Ligue2(FranceLigue2Mixin, FootballApiBase):
    url_path_params = {"category_id": 314}
    category_name = "Liga 2"
