from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import FranceLigue1Mixin


class Ligue1(FranceLigue1Mixin, FootballApiBase):
    url_path_params = {"category_id": 312}
    category_name = "Ligue 1"
