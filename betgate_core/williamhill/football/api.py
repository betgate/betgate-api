import datetime
import pytz
from betgate_models.models import Odd, Event, BookieId
from betgate_models.enums import Market, Category, Bookie
from betgate_core.williamhill.api import WilliamHillApiBase


class FootballApiBase(WilliamHillApiBase):
    markets_specification = {
        24: {  # Principales
            "Ganador del partido": {
                "selections": [Market.FT_WIN_1, Market.FT_WIN_X, Market.FT_WIN_2]
            },
            "Resultado Exacto": {
                "draw": [Market.FT_RESULT_0_0]
            },
            "Hándicap del partido +1": {
                "selections": [Market.FT_WIN_1_P10, Market.FT_DRAW_1_P10, Market.FT_WIN_2_M10]
            },
            "Hándicap del partido +2": {
                "selections": [Market.FT_WIN_1_P20, Market.FT_DRAW_1_P20, Market.FT_WIN_2_M20]
            },
            "Hándicap del partido +3": {
                "selections": [Market.FT_WIN_1_P30, Market.FT_DRAW_1_P30, Market.FT_WIN_2_M30]
            },
            "Hándicap del partido +4": {
                "selections": [Market.FT_WIN_1_P40, Market.FT_DRAW_1_P40, Market.FT_WIN_2_M40]
            },
            "Hándicap del partido -1": {
                "selections": [Market.FT_WIN_1_M10, Market.FT_DRAW_1_M10, Market.FT_WIN_2_P10]
            },
            "Hándicap del partido -2": {
                "selections": [Market.FT_WIN_1_M20, Market.FT_DRAW_1_M20, Market.FT_WIN_2_P20]
            },
            "Hándicap del partido -3": {
                "selections": [Market.FT_WIN_1_M30, Market.FT_DRAW_1_M30, Market.FT_WIN_2_P30]
            },
            "Hándicap del partido -4": {
                "selections": [Market.FT_WIN_1_M40, Market.FT_DRAW_1_M40, Market.FT_WIN_2_P40]
            },
            "Victoria sin empate (en caso de empate se anula la apuesta)": {
                "selections": [Market.FT_WIN_DNB_1, Market.FT_WIN_DNB_2]
            },
            "Doble oportunidad": {
                "selections": [Market.FT_WIN_1X, Market.FT_WIN_12, Market.FT_WIN_X2]
            },
            "Total de goles Más/Menos 0.5 goles": {
                "selections": [Market.FT_TG_UNDER_05, Market.FT_TG_OVER_05]
            },
            "Total de goles Más/Menos 1.5 goles": {
                "selections": [Market.FT_TG_UNDER_15, Market.FT_TG_OVER_15]
            },
            "Total de goles Más/Menos 2.5 goles": {
                "selections": [Market.FT_TG_UNDER_25, Market.FT_TG_OVER_25]
            },
            "Total de goles Más/Menos 3.5 goles": {
                "selections": [Market.FT_TG_UNDER_35, Market.FT_TG_OVER_35]
            },
            "Total de goles Más/Menos 4.5 goles": {
                "selections": [Market.FT_TG_UNDER_45, Market.FT_TG_OVER_45]
            },
            "Total de goles Más/Menos 5.5 goles": {
                "selections": [Market.FT_TG_UNDER_55, Market.FT_TG_OVER_55]
            },
            "Ambos equipos marcarán": {
                "selections": [Market.FT_BTTS_YES, Market.FT_BTTS_NO]
            },
            "Total de goles en el partido": {
                "selections": [Market.FT_TG_EXACT_0]
            }
        },
        25: {  # goles
            "Primer Tiempo Más / Menos 0.5 goles": {
                "selections": [Market.FH_TG_UNDER_05, Market.FH_TG_OVER_05]
            },
            "Primer Tiempo Más / Menos 1.5 goles": {
                "selections": [Market.FH_TG_UNDER_15, Market.FH_TG_OVER_15]
            },
            "Primer Tiempo Más / Menos 2.5 goles": {
                "selections": [Market.FH_TG_UNDER_25, Market.FH_TG_OVER_25]
            },
            "Primer Tiempo Más / Menos 3.5 goles": {
                "selections": [Market.FH_TG_UNDER_35, Market.FH_TG_OVER_35]
            },
            "Primer Tiempo Más / Menos 4.5 goles": {
                "selections": [Market.FH_TG_UNDER_45, Market.FH_TG_OVER_45]
            },
            "Total de goles del 1er tiempo": {
                "selections": [Market.FH_TG_EXACT_0]
            }
        },
        178: {  # 1er/2º tiempo
            "Ganador del 1er Tiempo": {
                "selections": [Market.FH_WIN_1, Market.FH_WIN_X, Market.FH_WIN_2]
            },
            "Resultado exacto del 1er tiempo": {
                "draw": [Market.FH_RESULT_0_0]
            },
            "Hándicap del 1er Tiempo +1": {
                "selections": [Market.FH_WIN_1_P10, Market.FH_DRAW_1_P10, Market.FH_WIN_2_M10]
            },
            "Hándicap del 1er Tiempo +2": {
                "selections": [Market.FH_WIN_1_P20, Market.FH_DRAW_1_P20, Market.FH_WIN_2_M20]
            },
            "Hándicap del 1er Tiempo +3": {
                "selections": [Market.FH_WIN_1_P30, Market.FH_DRAW_1_P30, Market.FH_WIN_2_M30]
            },
            "Hándicap del 1er Tiempo -1": {
                "selections": [Market.FH_WIN_1_M10, Market.FH_DRAW_1_M10, Market.FH_WIN_2_P10]
            },
            "Hándicap del 1er Tiempo -2": {
                "selections": [Market.FH_WIN_1_M20, Market.FH_DRAW_1_M20, Market.FH_WIN_2_P20]
            },
            "Hándicap del 1er Tiempo -3": {
                "selections": [Market.FH_WIN_1_M30, Market.FH_DRAW_1_M30, Market.FH_WIN_2_P30]
            },
        }
    }

    corners_markets_spec = {
        297: {  # Corners/cards
            "más 0": Market.FT_CORNERS_OVER_0_5,
            "menos 1": Market.FT_CORNERS_UNDER_0_5,
            "más 1": Market.FT_CORNERS_OVER_1_5,
            "menos 2": Market.FT_CORNERS_UNDER_1_5,
            "más 2": Market.FT_CORNERS_OVER_2_5,
            "menos 3": Market.FT_CORNERS_UNDER_2_5,
            "más 3": Market.FT_CORNERS_OVER_3_5,
            "menos 4": Market.FT_CORNERS_UNDER_3_5,
            "más 4": Market.FT_CORNERS_OVER_4_5,
            "menos 5": Market.FT_CORNERS_UNDER_4_5,
            "más 5": Market.FT_CORNERS_OVER_5_5,
            "menos 6": Market.FT_CORNERS_UNDER_5_5,
            "más 6": Market.FT_CORNERS_OVER_6_5,
            "menos 7": Market.FT_CORNERS_UNDER_6_5,
            "más 7": Market.FT_CORNERS_OVER_7_5,
            "menos 8": Market.FT_CORNERS_UNDER_7_5,
            "más 8": Market.FT_CORNERS_OVER_8_5,
            "menos 9": Market.FT_CORNERS_UNDER_8_5,
            "más 9": Market.FT_CORNERS_OVER_9_5,
            "menos 10": Market.FT_CORNERS_UNDER_9_5,
            "más 10": Market.FT_CORNERS_OVER_10_5,
            "menos 11": Market.FT_CORNERS_UNDER_10_5,
            "más 11": Market.FT_CORNERS_OVER_11_5,
            "menos 12": Market.FT_CORNERS_UNDER_11_5,
            "más 12": Market.FT_CORNERS_OVER_12_5,
            "menos 13": Market.FT_CORNERS_UNDER_12_5,
            "más 13": Market.FT_CORNERS_OVER_13_5,
            "menos 14": Market.FT_CORNERS_UNDER_13_5,
            "más 14": Market.FT_CORNERS_OVER_14_5,
            "menos 15": Market.FT_CORNERS_UNDER_14_5,
            "más 15": Market.FT_CORNERS_OVER_15_5,
            "menos 16": Market.FT_CORNERS_UNDER_15_5,
            "más 16": Market.FT_CORNERS_OVER_16_5,
            "menos 17": Market.FT_CORNERS_UNDER_16_5,
            "más 17": Market.FT_CORNERS_OVER_17_5,
            "menos 18": Market.FT_CORNERS_UNDER_17_5,
            "más 18": Market.FT_CORNERS_OVER_18_5,
            "menos 19": Market.FT_CORNERS_UNDER_18_5,
            "más 19": Market.FT_CORNERS_OVER_19_5,
            "menos 20": Market.FT_CORNERS_UNDER_19_5,
            "más 20": Market.FT_CORNERS_OVER_20_5,
            "menos 21": Market.FT_CORNERS_UNDER_20_5,
        }
    }

    def lazy_get_category_events(self, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            to_return = []
            res = self.request_category_detail()
            for evt in res.json["competition"]["matches"]:
                try:
                    if evt.get("nrOfMarkets", None) == 1:  # skip invalid events
                        continue

                    event_id = evt["id"]
                    home_tname = evt["name"].split(" - ")[0]
                    away_tname = evt["name"].split(" - ")[1]
                    utc_dt = datetime.datetime.strptime(evt["dateTime"], "%Y-%m-%dT%H:%M:%S.%fZ")
                    date_time = utc_dt.replace(tzinfo=pytz.utc)

                    event = Event(
                        bookies={str(Bookie.WILLIAM_HILL.value): BookieId(bookie=Bookie.WILLIAM_HILL, value=event_id)},
                        home_team=self.get_team(home_tname),
                        away_team=self.get_team(away_tname),
                        category=self.get_category(),
                        start=date_time,
                        home_team_name=home_tname,
                        away_team_name=away_tname
                    )
                    if validate:
                        event.full_clean(validate_unique=False)
                    to_return.append(event)
                except:
                    pass
            return to_return
        except:
            return []

    def get_event(self, event_id, **kwargs):
        validate = kwargs.pop("validate", True)
        try:
            event = self.request_event_detail(event_id).json["event"]
            home_tname = event["homeTeam"]
            away_tname = event["awayTeam"]
            category = self.get_category()
            utc_dt = datetime.datetime.strptime(event["dateTime"], "%Y-%m-%dT%H:%M:%S.%fZ")
            date_time = utc_dt.replace(tzinfo=pytz.utc)
            odds = self.get_odds(event["collections"])
            event = Event(
                bookies={str(Bookie.WILLIAM_HILL.value): BookieId(bookie=Bookie.WILLIAM_HILL, value=event_id)},
                home_team=self.get_team(home_tname),
                away_team=self.get_team(away_tname),
                category=category,
                start=date_time,
                odds=odds,
                home_team_name=home_tname,
                away_team_name=away_tname
            )
            if validate:
                event.full_clean(validate_unique=False)
            return event
        except:
            pass

    def get_odds(self, collections):
        odds = []
        for coll in (x for x in collections if x["id"] in self.markets_specification):
            for market in (y for y in coll["markets"] if y["name"] in self.markets_specification[coll["id"]]):
                for key, items in self.markets_specification[coll["id"]][market["name"]].items():
                    for i, mkt in zip(items, market[key]):
                        odds.append(Odd(
                            value=mkt["oddsDecimalValue"],
                            market=i,
                            bookie=Bookie.WILLIAM_HILL
                        ))
        return odds + self._get_total_corner_odds(collections)

    def _get_total_corner_odds(self, collections):
        odds = []
        for coll in (x for x in collections if x["id"] in self.corners_markets_spec):
            for market in (y for y in coll["markets"]
                           if y["name"].lower().replace("ó", "o").startswith("total de corners")):
                for mkt in market["selections"]:
                    if mkt["name"].lower() in self.corners_markets_spec[coll["id"]]:
                        odds.append(Odd(
                            value=mkt["oddsDecimalValue"],
                            market=self.corners_markets_spec[coll["id"]][mkt["name"].lower()],
                            bookie=Bookie.WILLIAM_HILL
                        ))

        return odds


class FootballApi(FootballApiBase):
    category = Category.UNKNOWN
