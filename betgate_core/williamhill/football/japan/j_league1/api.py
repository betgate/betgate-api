from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import JapanJLeague1Mixin


class JLeague1(JapanJLeague1Mixin, FootballApiBase):
    url_path_params = {"category_id": 3320}
    category_name = "J1-League"
