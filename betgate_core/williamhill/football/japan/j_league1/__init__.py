from betgate_core.williamhill.football.japan.j_league1.api import JLeague1


def get_events(**kwargs):
    with JLeague1() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with JLeague1() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with JLeague1() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
