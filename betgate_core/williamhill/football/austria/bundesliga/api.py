from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import AustriaBundesligaMixin


class Bundesliga(AustriaBundesligaMixin, FootballApiBase):
    url_path_params = {"category_id": 179}
    category_name = ""
