from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import SwedenAllsvenskanMixin


class Allsvenskan(SwedenAllsvenskanMixin, FootballApiBase):
    url_path_params = {"category_id": 340}
    category_name = "Allsvenskan"
