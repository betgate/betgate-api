from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import SwedenSuperettanMixin


class Superettan(SwedenSuperettanMixin, FootballApiBase):
    url_path_params = {"category_id": 341}
    category_name = "Superettan"
