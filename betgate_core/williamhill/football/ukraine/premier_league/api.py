from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import UkrainePremierLeagueMixin


class PremierLeague(UkrainePremierLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 327}
    category_name = "Premier Liga"
