from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import MoroccoBotolaMixin


class Botola(MoroccoBotolaMixin, FootballApiBase):
    url_path_params = {"category_id": 1651}
    category_name = "Botola"
