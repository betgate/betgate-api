from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import EcuadorSerieAMixin


class SerieA(EcuadorSerieAMixin, FootballApiBase):
    url_path_params = {"category_id": 8502}
    category_name = "Primera A"
