from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import BelgiumFirstDivisionAMixin


class FirstDivisionA(BelgiumFirstDivisionAMixin, FootballApiBase):
    url_path_params = {"category_id": 28159}
    category_name = "Primera División A"
