from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import ScotlandPremiershipMixin


class Premiership(ScotlandPremiershipMixin, FootballApiBase):
    url_path_params = {"category_id": 297}
    category_name = "Premiership de Escocia"
