from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import UsaMlsMixin


class Mls(UsaMlsMixin, FootballApiBase):
    url_path_params = {"category_id": 353}
    category_name = "MLS"
