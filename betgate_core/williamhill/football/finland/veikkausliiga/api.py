from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import FinlandVeikkausliigaMixin


class Veikkausliiga(FinlandVeikkausliigaMixin, FootballApiBase):
    url_path_params = {"category_id": 311}
    category_name = "Veikkausliiga"
