from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandLeagueOneMixin


class LeagueOne(EnglandLeagueOneMixin, FootballApiBase):
    url_path_params = {"category_id": 293}
    category_name = "Liga Uno"
