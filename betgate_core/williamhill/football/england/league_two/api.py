from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandLeagueTwoMixin


class LeagueTwo(EnglandLeagueTwoMixin, FootballApiBase):
    url_path_params = {"category_id": 294}
    category_name = "Liga Dos"
