from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandNationalLeagueNorthMixin


class NationalLeagueNorth(EnglandNationalLeagueNorthMixin, FootballApiBase):
    url_path_params = {"category_id": 21076}
    category_name = "Liga Nacional Norte"
