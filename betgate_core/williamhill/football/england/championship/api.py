from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandChampionshipMixin


class Championship(EnglandChampionshipMixin, FootballApiBase):
    url_path_params = {"category_id": 292}
    category_name = "Championship"
