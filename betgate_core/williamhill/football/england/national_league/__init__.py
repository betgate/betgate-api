from betgate_core.williamhill.football.england.national_league.api import NationalLeague


def get_events(**kwargs):
    with NationalLeague() as api:
        return api.get_category_events(**kwargs)


def lazy_get_events(**kwargs):
    with NationalLeague() as api:
        return api.lazy_get_category_events(**kwargs)


def get_odds(event_id):
    with NationalLeague() as api:
        event = api.get_event(event_id)
        return event.odds if event is not None else []
