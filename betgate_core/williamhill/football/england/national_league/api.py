from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandNationalLeagueMixin


class NationalLeague(EnglandNationalLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 21075}
    category_name = "Liga Nacional"
