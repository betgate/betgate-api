from betgate_core.williamhill.football.england import premier_league, championship, league_one, league_two, \
    national_league, national_league_north


def get_events(**kwargs):
    return premier_league.get_events(**kwargs) + championship.get_events(**kwargs) + league_one.get_events(**kwargs) + \
           league_two.get_events(**kwargs) + national_league.get_events(**kwargs) + \
           national_league_north.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return premier_league.lazy_get_events(**kwargs) + championship.lazy_get_events(**kwargs) + \
           league_one.lazy_get_events(**kwargs) + league_two.lazy_get_events(**kwargs) + \
           national_league.lazy_get_events(**kwargs) + national_league_north.lazy_get_events(**kwargs)
