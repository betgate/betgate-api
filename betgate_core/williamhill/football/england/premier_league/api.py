from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import EnglandPremierLeagueMixin


class PremierLeague(EnglandPremierLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 295}
    category_name = "Premier League"
