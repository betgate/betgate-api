from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import ClubsInternationalChampionsLeagueMixin


class ChampionsLeague(ClubsInternationalChampionsLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 344}
    category_name = "UEFA Champions League"
