from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import ClubsInternationalEuropeLeagueMixin


class EuropeLeague(ClubsInternationalEuropeLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 1935}
    category_name = "UEFA Europa League"
