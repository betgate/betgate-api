from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import GreeceSuperLeagueMixin


class SuperLeague(GreeceSuperLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 318}
    category_name = "Super League"
