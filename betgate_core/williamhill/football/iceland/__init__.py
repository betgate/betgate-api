from betgate_core.williamhill.football.iceland import urvalsdeild


def get_events(**kwargs):
    return urvalsdeild.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return urvalsdeild.lazy_get_events(**kwargs)
