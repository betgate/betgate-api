from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import IcelandUrvalsdeildMixin


class Urvalsdeild(IcelandUrvalsdeildMixin, FootballApiBase):
    url_path_params = {"category_id": 1707}
    category_name = "Úrvalsdeild"
