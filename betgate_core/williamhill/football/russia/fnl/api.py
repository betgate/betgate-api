from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import RussiaFnlMixin


class Fnl(RussiaFnlMixin, FootballApiBase):
    url_path_params = {"category_id": 9456}
    category_name = "Liga Nacional"
