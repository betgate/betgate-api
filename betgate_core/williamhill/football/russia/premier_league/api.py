from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import RussiaPremierLeagueMixin


class PremierLeague(RussiaPremierLeagueMixin, FootballApiBase):
    url_path_params = {"category_id": 334}
    category_name = "Premier League"
