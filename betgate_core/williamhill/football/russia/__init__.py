from betgate_core.williamhill.football.russia import fnl
from betgate_core.williamhill.football.russia import premier_league


def get_events(**kwargs):
    return fnl.get_events(**kwargs) + \
           premier_league.get_events(**kwargs)


def lazy_get_events(**kwargs):
    return fnl.lazy_get_events(**kwargs) + \
           premier_league.lazy_get_events(**kwargs)
