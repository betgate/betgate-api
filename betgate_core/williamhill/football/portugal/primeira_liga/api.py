from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import PortugalPrimeiraLigaMixin


class PrimeiraLiga(PortugalPrimeiraLigaMixin, FootballApiBase):
    url_path_params = {"category_id": 331}
    category_name = "Primeira Liga"
