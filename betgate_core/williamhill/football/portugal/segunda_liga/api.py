from betgate_core.williamhill.football.api import FootballApiBase
from betgate_core.mixins.football import PortugalSegundaLigaMixin


class SegundaLiga(PortugalSegundaLigaMixin, FootballApiBase):
    url_path_params = {"category_id": 332}
    category_name = "LigaPro"
