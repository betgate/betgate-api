import time
from datetime import timedelta, datetime
import pytz
from django.test import TestCase
from betgate_models.models import Event, Surebet, BookieId, Odd
from betgate_models.enums import Team, Bookie, Market, Category
from betgate_core.test.football import Mock


MARKETS = [
    mkt
    for mkt in Market if mkt != Market.UNKNOWN
]

KAMBI_MARKET_ODDS = [
    Odd(value=1.1, market=mkt, bookie=Bookie.SPORT888)
    for mkt in MARKETS
]

SPORTIUM_MARKET_ODDS = [
    Odd(value=1.1, market=mkt, bookie=Bookie.SPORTIUM)
    for mkt in MARKETS
]

BETFAIR_MARKET_ODDS = [
    Odd(value=1.1, market=mkt, bookie=Bookie.BETFAIR)
    for mkt in MARKETS
]

MARATHONBET_MARKET_ODDS = [
    Odd(value=1.1, market=mkt, bookie=Bookie.MARATHONBET)
    for mkt in MARKETS
]

SUREBET_MARKET_ODDS = [
    Odd(value=2.3, market=Market.FT_BTTS_YES, bookie=Bookie.LUCKIA),
    Odd(value=2.3, market=Market.FT_BTTS_NO, bookie=Bookie.LUCKIA)
]

BOOKIES = [
    bki
    for bki in Bookie if bki not in [Bookie.UNKNOWN, Bookie.TEST]
]


class TimingTestCase(TestCase):
    test_timings = []
    _test_started_at = None
    _given_stats = None

    def init_performance_test(self):
        self._given_stats = self.get_stats()
        self._test_started_at = time.time()

    def end_performance_test(self, name):
        elapsed = time.time() - self._test_started_at
        then_stats = self.get_stats()
        self.test_timings.append((name, elapsed, self._given_stats, then_stats))

    @classmethod
    def get_stats(cls):
        n_surebets = Surebet.objects.count()
        n_events = Event.objects.count()
        n_odds = Event.objects.count_odds()
        n_bookies = Event.objects.count_bookies()

        return "Surebets => {0}, Events => {1}, Odds => {2}, Bookies => {3}".format(
            n_surebets, n_events, n_odds, n_bookies)

    @classmethod
    def print_results(cls):
        print("-" * 70)
        print(" <Time> | <{0}>".format(cls.__name__))
        for name, elapsed, given, then in cls.test_timings:
            print("{0:6.2f}s | {1}".format(elapsed, name))
            print("        | Given: {0}".format(given))
            print("        | Then:  {0}".format(then))

    @classmethod
    def setUpClass(cls):
        cls.test_timings = []

    @classmethod
    def tearDownClass(cls):
        cls.print_results()


class BetgateTestCase(TestCase):
    _surebet_mock = None
    _kambi_mock = None
    _sportium_mock = None
    _betfair_mock = None
    _marathonbet_mock = None
    _williamhill_mock = None

    def set_up_scenario(self):
        from betgate_core.luckia.football.spain import primera_division as luckia_primera_division
        from betgate_core.betfair.football.spain import primera_division as betfair_primera_division
        from betgate_core.marathonbet.football.spain import primera_division as marathonbet_primera_division
        from betgate_core.sportium.football.spain import primera_division as sportium_primera_division
        from betgate_core.kambi.football.spain import primera_division as kambi_primera_division
        from betgate_core.williamhill.football.spain import primera_division as williamhill_primera_division

        self._surebet_mock = Mock()
        self._kambi_mock = Mock()
        self._sportium_mock = Mock()
        self._betfair_mock = Mock()
        self._marathonbet_mock = Mock()
        self._williamhill_mock = Mock()

        luckia_primera_division.get_odds = self._surebet_mock.get_odds
        luckia_primera_division.lazy_get_events = self._surebet_mock.lazy_get_events
        luckia_primera_division.get_events = self._surebet_mock.get_events
        betfair_primera_division.get_odds = self._betfair_mock.get_odds
        betfair_primera_division.lazy_get_events = self._betfair_mock.lazy_get_events
        betfair_primera_division.get_events = self._betfair_mock.get_events
        marathonbet_primera_division.get_odds = self._marathonbet_mock.get_odds
        marathonbet_primera_division.lazy_get_events = self._marathonbet_mock.lazy_get_events
        marathonbet_primera_division.get_events = self._marathonbet_mock.get_events
        sportium_primera_division.get_odds = self._sportium_mock.get_odds
        sportium_primera_division.lazy_get_events = self._sportium_mock.lazy_get_events
        sportium_primera_division.get_events = self._sportium_mock.get_events
        kambi_primera_division.get_odds = self._kambi_mock.get_odds
        kambi_primera_division.lazy_get_events = self._kambi_mock.lazy_get_events
        kambi_primera_division.get_events = self._kambi_mock.get_events
        williamhill_primera_division.get_odds = self._kambi_mock.get_odds
        williamhill_primera_division.lazy_get_events = self._kambi_mock.lazy_get_events
        williamhill_primera_division.get_events = self._kambi_mock.get_events

    def set_up_spain_primera_mock(self, n_new_events, n_new_surebets, n_odds):
        for days in range(n_new_events-n_new_surebets):
            event = Event(
                home_team=Team.BARCELONA.value,
                away_team=Team.ZARAGOZA.value,
                category=Category.SPAIN__PRIMERA_DIVISION,
                start=datetime.now(tz=pytz.utc) + timedelta(days=days + 2)
            )
            self._surebet_mock.append_event(
                Event(
                    home_team=event.home_team,
                    away_team=event.away_team,
                    category=event.category,
                    start=event.start,
                    bookies={
                        str(Bookie.LUCKIA.value): BookieId(bookie=Bookie.LUCKIA, value=event.id),
                    },
                    odds=KAMBI_MARKET_ODDS[:n_odds]
                ))
            self._kambi_mock.append_event(
                Event(
                    home_team=event.home_team,
                    away_team=event.away_team,
                    category=event.category,
                    start=event.start,
                    bookies={
                        str(Bookie.SPORT888.value): BookieId(bookie=Bookie.SPORT888, value=event.id)
                    },
                    odds=KAMBI_MARKET_ODDS[:n_odds]
                ))
            self._sportium_mock.append_event(
                Event(
                    home_team=event.home_team,
                    away_team=event.away_team,
                    category=event.category,
                    start=event.start,
                    bookies={
                        str(Bookie.SPORTIUM.value): BookieId(bookie=Bookie.SPORTIUM, value=event.id),
                    },
                    odds=SPORTIUM_MARKET_ODDS[:n_odds]
                ))
            self._betfair_mock.append_event(
                Event(
                    home_team=event.home_team,
                    away_team=event.away_team,
                    category=event.category,
                    start=event.start,
                    bookies={
                        str(Bookie.BETFAIR.value): BookieId(bookie=Bookie.BETFAIR, value=event.id),
                    },
                    odds=BETFAIR_MARKET_ODDS[:n_odds]
                ))
            self._marathonbet_mock.append_event(
                Event(
                    home_team=event.home_team,
                    away_team=event.away_team,
                    category=event.category,
                    start=event.start,
                    bookies={
                        str(Bookie.MARATHONBET.value): BookieId(bookie=Bookie.MARATHONBET, value=event.id),
                    },
                    odds=MARATHONBET_MARKET_ODDS[:n_odds]
                ))

        for days in range(n_new_surebets):
            event = Event(
                home_team=Team.MAASTRICHT.value,
                away_team=Team.ALAVES.value,
                category=Category.SPAIN__PRIMERA_DIVISION,
                start=datetime.now(tz=pytz.utc) + timedelta(days=days + 2)
            )

            self._surebet_mock.append_event(
                Event(
                    home_team=event.home_team,
                    away_team=event.away_team,
                    category=event.category,
                    start=event.start,
                    bookies={str(Bookie.LUCKIA.value): BookieId(bookie=Bookie.LUCKIA, value=event.id)},
                    odds=SUREBET_MARKET_ODDS
                ))

    def set_up_spain_mock(self, n_new_surebets):
        for event in Surebet.objects.distint_events()[:n_new_surebets]:
            self._surebet_mock.append_event(
                Event(
                    home_team=event.home_team,
                    away_team=event.away_team,
                    category=event.category,
                    start=event.start,
                    bookies={str(Bookie.LUCKIA.value): BookieId(bookie=Bookie.LUCKIA, value=event.id)},
                    odds=SUREBET_MARKET_ODDS
                ))
            self._surebet_mock.set_odds(event_id=event.id, odds=SUREBET_MARKET_ODDS)

    def set_up_db(self, n_old_events, n_old_surebets, n_bookies, n_odds, category):
        for days in range(n_old_events):
            odds = [Odd(market=mkt, value=1.1, bookie=bkie) for mkt, bkie in zip(MARKETS[:n_odds], BOOKIES[:n_bookies])]
            event = Event.objects.create(
                home_team=Team.GETAFE.value,
                away_team=Team.SPORTING_LISBOA.value,
                category=category,
                start=datetime.now(tz=pytz.utc) + timedelta(days=days + 2),
                odds=odds
            )
            event.bookies = {
                str(bookie.value): BookieId(bookie=bookie, value=event.id) for bookie in BOOKIES[:n_bookies]
            }
            event.save()

        for event in Event.objects.filter(category=category)[:n_old_surebets]:
            surebet = Surebet(
                profit_factor=1.019,
                event=event,
                odds=SUREBET_MARKET_ODDS,
                bookies=[Bookie.LUCKIA]
            )
            surebet.save()


class PerformanceTestCase(TimingTestCase, BetgateTestCase):
    pass
