import os
import errno
import importlib
from collections import OrderedDict
from django.core.management.base import BaseCommand
from django.template import loader
from django.conf import settings
from betgate_models.enums import Category, Country


class Command(BaseCommand):
    help = 'Generate code from scratch'

    CHAMPIONSHIP_ACTION = 1
    TEAM_ACTION = 2
    JS_MODELS_ACTION = 3

    _bookies = {
        "betfair": {
            "scrapping_mode": "web"
        },
        "kambi": {
            "scrapping_mode": "api"
        },
        "luckia": {
            "scrapping_mode": "api"
        },
        "marathonbet": {
            "scrapping_mode": "web"
        },
        "retabet": {
            "scrapping_mode": "web"
        },
        "sportium": {
            "scrapping_mode": "web"
        },
        "williamhill": {
            "scrapping_mode": "api"
        }
    }

    def add_arguments(self, parser):
        subparsers = parser.add_subparsers()
        championship_parser = subparsers.add_parser(
            name='championship',
            help='Generate a championship'
        )
        championship_parser.add_argument(
            '<country>.<championship_name>',
            type=str,
            help='Country dot and name of the championship separate by underscore.'
        )
        championship_parser.add_argument(
            '--action',
            nargs='?',
            const=self.CHAMPIONSHIP_ACTION,
            default=self.CHAMPIONSHIP_ACTION,
            type=int
        )
        team_parser = subparsers.add_parser(
            name='team',
            help='Generate a team'
        )
        team_parser.add_argument(
            '<team_name>',
            type=str,
            help='Name of the team separate by underscore.'
        )
        team_parser.add_argument(
            '--action',
            nargs='?',
            const=self.TEAM_ACTION,
            default=self.TEAM_ACTION,
            type=int
        )
        team_parser.add_argument(
            '--country',
            default=False,
            type=str,
            help='Name of the country separate by underscore.'
        )
        team_parser.add_argument(
            '-n',
            '--name',
            nargs='+',
            default=False,
            type=str,
            help='Truly name of the team found in web pages.'
        )
        js_models_parser = subparsers.add_parser(
            name='js',
            help='Generate js-models library'
        )
        js_models_parser.add_argument(
            '--action',
            nargs='?',
            const=self.JS_MODELS_ACTION,
            default=self.JS_MODELS_ACTION,
            type=int
        )

    def handle(self, *args, **options):
        if options['action'] == self.CHAMPIONSHIP_ACTION:
            self.generate_championship(**options)
        elif options['action'] == self.TEAM_ACTION:
            self.generate_team(**options)
        elif options['action'] == self.JS_MODELS_ACTION:
            self.generate_js_models()
        else:
            self.print_help("manage.py", "generate")

    def generate_js_models(self):
        self.generate_js_models_models()
        self.generate_js_models_readme()
        self.generate_js_models_package()
        self.generate_js_models_package_lock()
        self.generate_js_models_tsconfig()

    def generate_js_models_models(self):
        template = loader.get_template("js_models/index.ts.template")

        path_to_filename = "js-models/index.ts"

        mod_teams = importlib.import_module("betgate_models.choices.teams")
        teams = [(val[0], val[1]) for val in getattr(mod_teams, "TEAMS")]

        mod_bookies = importlib.import_module("betgate_models.choices.bookies")
        bookies = [(val[0], val[1]) for val in getattr(mod_bookies, "BOOKIES")]

        context = {
            "teams": sorted(teams, key=lambda tup: tup[0]),
            "bookies": sorted(bookies, key=lambda tup: tup[0])
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def generate_js_models_readme(self):
        template = loader.get_template("js_models/README.md.template")

        path_to_filename = "js-models/README.md"

        context = {}
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def generate_js_models_package(self):
        template = loader.get_template("js_models/package.json.template")

        path_to_filename = "js-models/package.json"

        context = {
            "version": getattr(settings, "BASE_VERSION", "")
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def generate_js_models_package_lock(self):
        template = loader.get_template("js_models/package-lock.json.template")

        path_to_filename = "js-models/package-lock.json"

        context = {
            "version": getattr(settings, "BASE_VERSION", "")
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def generate_js_models_tsconfig(self):
        template = loader.get_template("js_models/tsconfig.json.template")

        path_to_filename = "js-models/tsconfig.json"

        context = {}
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def generate_team(self, **options):
        country_name = options['country']
        team_name = options['<team_name>'].lower()
        self._generate_enums("team", team_name)
        self._generate_teams(team_name)
        if options['country']:
            championships = set()
            for bookie in self._bookies:
                championships.update(
                    self._get_directories("betgate_core/{0}/football/{1}".format(bookie, country_name)))
            self._generate_mixins(country_name, championships, team_name)
            self._generate_country_infos(country_name, team=team_name)
        if options['name']:
            self._generate_team_names(team_name, options['name'])
        self.stdout.write(self.style.SUCCESS('Team generated!'))  # pylint: disable=maybe-no-member

    def generate_championship(self, **options):
        country_name = options['<country>.<championship_name>'].split('.')[0]
        championship_name = options['<country>.<championship_name>'].split('.')[1]
        championships = {championship_name}
        for bookie in self._bookies:
            championships.update(self._get_directories("betgate_core/{0}/football/{1}".format(bookie, country_name)))
            self._generate_country_init(bookie, country_name, championships)
            self._generate_championship_init(bookie, country_name, championship_name)
            self._generate_championship_class(bookie, country_name, championship_name)
            self._generate_football_init(bookie)
        self._generate_enums("country", country_name)
        self._generate_enums("category", "{0}__{1}".format(country_name, championship_name))
        self._generate_mixins(country_name, championships)
        self._generate_mixins_init()
        self._generate_categories(country_name, championship_name)
        self._generate_countries(country_name)
        self._generate_country_infos(country_name, championship=championship_name)
        self.stdout.write(self.style.SUCCESS('Championship generated!'))  # pylint: disable=maybe-no-member

    def _generate_country_init(self, bookie, country, championships):
        template = loader.get_template("betgate/country.__init__.py.template")
        context = {
            "country": country,
            "championships": championships,
            "bookie": bookie
        }
        code = template.render(context)

        path_to_filename = "betgate_core/{0}/football/{1}/__init__.py".format(bookie, country)
        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_championship_init(self, bookie, country, championship):
        template = loader.get_template("betgate/championship.__init__.py.template")
        context = {
            "country": country,
            "championship": championship,
            "bookie": bookie,
            "scrapping_mode": self._bookies[bookie]["scrapping_mode"]
        }
        code = template.render(context)

        path_to_filename = "betgate_core/{0}/football/{1}/{2}/__init__.py".format(bookie, country, championship)
        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_football_init(self, bookie):
        template = loader.get_template("betgate/football.__init__.py.template")
        context = {
            "countries": self._get_directories("betgate_core/{0}/football".format(bookie)),
            "bookie": bookie,
            "scrapping_mode": self._bookies[bookie]["scrapping_mode"]
        }
        code = template.render(context)

        path_to_filename = "betgate_core/{0}/football/__init__.py".format(bookie)
        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_championship_class(self, bookie, country, championship):
        template = loader.get_template("betgate/{0}.championship.scrapping_mode.py.template".format(bookie))
        context = {
            "country": country,
            "championship": championship
        }
        code = template.render(context)

        path_to_filename = "betgate_core/{0}/football/{1}/{2}/{3}.py".format(
            bookie, country, championship, self._bookies[bookie]["scrapping_mode"]
        )
        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_mixins(self, country, championships, team=None):
        path_to_filename = "betgate_core/mixins/football/{0}.py".format(country)

        try:
            mod = importlib.import_module("betgate_core.mixins.football." + country)
            teams = [
                team_val.name for team_val in getattr(
                    mod, "{0}FootballMixin".format(country.title().replace("_", ""))).teams
            ]
        except:
            teams = []

        if team and team.upper() not in teams:
            teams.append(team.upper())

        template = loader.get_template("betgate/mixins.country.py.template")
        context = {
            "country": country,
            "championships": championships,
            "teams": sorted(teams)
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_mixins_init(self):
        path_to_filename = "betgate_core/mixins/football/__init__.py"

        template = loader.get_template("betgate/mixins.__init__.py.template")
        context = {
            "countries": self._get_modules("betgate_core/mixins/football"),
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_enums(self, enum_type, new_value):
        template = loader.get_template("betgate/enum.py.template")

        path_to_filename = "betgate_models/enums/{0}.py".format(enum_type)

        mod = importlib.import_module("betgate_models.enums." + enum_type)
        values = [(val.name, val.value) for val in getattr(mod, enum_type.title())]

        if new_value.lower() not in [val[0].lower() for val in values]:
            values.append((new_value.upper(), max([val[1] for val in values]) + 1))

        context = {
            "enum": enum_type,
            "values": sorted(values, key=lambda tup: tup[0])
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_categories(self, country, championship):
        template = loader.get_template("betgate/models.categories.py.template")
        new_value = "{0}__{1}".format(country.upper(), championship.upper())

        path_to_filename = "betgate_models/choices/categories.py"

        mod = importlib.import_module("betgate_models.choices.categories")
        values = [(Category(val[0]).name, val[1]) for val in getattr(mod, "CATEGORIES")]

        if new_value.lower() not in [val[0].lower() for val in values]:
            values.append((new_value.upper(), "{0} {1}".format(country.title().replace("_", " "),
                                                               championship.title().replace("_", " "))))

        context = {
            "categories": sorted(values, key=lambda tup: tup[0])
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_countries(self, country):
        template = loader.get_template("betgate/models.countries.py.template")
        new_value = country.upper()

        path_to_filename = "betgate_models/choices/countries.py"

        mod = importlib.import_module("betgate_models.choices.countries")
        values = [(Country(val[0]).name, val[1]) for val in getattr(mod, "COUNTRIES")]

        if new_value.lower() not in [val[0].lower() for val in values]:
            values.append((new_value.upper(), country.title().replace("_", " ")))

        context = {
            "countries": sorted(values, key=lambda tup: tup[0])
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_teams(self, team):
        template = loader.get_template("betgate/models.teams.py.template")
        new_value = team.upper()

        path_to_filename = "betgate_models/choices/teams.py"

        team_mod = importlib.import_module("betgate_models.enums.team")
        team_enum = getattr(team_mod, "Team")

        mod = importlib.import_module("betgate_models.choices.teams")
        values = [(team_enum(val[0]).name, val[1]) for val in getattr(mod, "TEAMS")]

        if new_value.lower() not in [val[0].lower() for val in values]:
            values.append((new_value.upper(), team.title().replace("_", " ")))

        context = {
            "teams": sorted(values, key=lambda tup: tup[0])
        }
        code = template.render(context)

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(code)

    def _generate_team_names(self, team, team_names):
        template = loader.get_template("betgate/team_names.py.template")
        new_value = team.upper()

        path_to_filename = "betgate_core/models/football/teams.py"

        mod = importlib.import_module("betgate_core.models.football.teams")
        try:
            values = [(key.lower(), val.name) for key, val in getattr(mod, "TEAM_NAMES").items()]
        except:  # old format
            values = []
            for team_key, teams_values in getattr(mod, "TEAM_NAMES").items():
                for team_name_value in teams_values:
                    values.append((team_name_value.lower(), team_key.name))

        for team_name in team_names:
            if team_name.lower() not in [val[0] for val in values]:
                values.append((team_name.lower(), new_value))

        context = {
            "teams": sorted(values, key=lambda tup: tup[1])
        }

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(template.render(context))

    def _generate_country_infos(self, country, championship=None, team=None):  # pylint: disable=too-many-locals
        country_mod = importlib.reload(importlib.import_module("betgate_models.enums.country"))
        category_mod = importlib.reload(importlib.import_module("betgate_models.enums.category"))
        team_mod = importlib.reload(importlib.import_module("betgate_models.enums.team"))

        template = loader.get_template("betgate/country_infos.py.template")
        path_to_filename = "betgate_core/models/football/countries.py"

        country_enum = getattr(country_mod, "Country")[country.upper()]
        category_enum = getattr(category_mod, "Category")["{0}__{1}".format(country.upper(), championship.upper())] \
            if championship else None
        team_enum = getattr(team_mod, "Team")[team.upper()] if team else None

        mod = importlib.import_module("betgate_core.models.football.countries")
        country_info_dict = getattr(mod, "COUNTRY_INFO_DICT")
        if country_enum not in country_info_dict:
            country_info_dict[country_enum] = dict()
        if category_enum and category_enum not in country_info_dict[country_enum].get("categories", []):
            values = country_info_dict[country_enum].get("categories", [])
            values.append(category_enum)
            country_info_dict[country_enum]["categories"] = values
        if team_enum and team_enum not in country_info_dict[country_enum]["teams"]:
            values = country_info_dict[country_enum].get("teams", [])
            values.append(team_enum)
            country_info_dict[country_enum]["teams"] = values

        context = {
            "countries_dict": OrderedDict(sorted(country_info_dict.items(), key=lambda key: key[0].name))
        }

        self._make_dir(path_to_filename)
        with open(path_to_filename, "w", encoding='utf-8') as f:
            f.write(template.render(context))

    def _make_dir(self, filename):
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

    def _get_directories(self, path_to_dir):
        try:
            return [
                directory for directory in os.listdir(path_to_dir)
                if os.path.isdir("{0}/{1}".format(path_to_dir, directory)) and directory != "__pycache__"
            ]
        except:
            return []

    def _get_modules(self, path_to_dir):
        try:
            return [
                file[:-3] for file in os.listdir(path_to_dir)
                if os.path.isfile(path_to_dir + "/{0}".format(file)) and file != "__init__.py"
            ]
        except:
            return []
