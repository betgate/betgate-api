from django.apps import AppConfig


class BetgateConfig(AppConfig):
    name = 'betgate.apps.code'
