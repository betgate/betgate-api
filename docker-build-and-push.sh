#!/bin/sh

APP_VERSION=$(cat version.txt)
IMAGE_REGISTRY=registry.gitlab.com/betgate/betgate-api
API_LABEL=api
TASKS_LABEL=tasks

# commands
while test $# -gt 0
do
    case "$1" in
        --test)
            docker build --no-cache --force-rm -t $IMAGE_REGISTRY:test \
                                               -f dockerfiles/test/Dockerfile \
                                               .
            docker push $IMAGE_REGISTRY:test
            ;;
        --apps)
            docker build --no-cache --force-rm -t $IMAGE_REGISTRY:$API_LABEL-latest \
                                               -t $IMAGE_REGISTRY:$API_LABEL-$APP_VERSION \
                                               -f dockerfiles/api/Dockerfile \
                                               .
            docker build --no-cache --force-rm -t $IMAGE_REGISTRY:$TASKS_LABEL-latest \
                                               -t $IMAGE_REGISTRY:$TASKS_LABEL-$APP_VERSION \
                                               -f dockerfiles/tasks/Dockerfile \
                                               .
            docker push $IMAGE_REGISTRY:$API_LABEL-latest
            docker push $IMAGE_REGISTRY:$API_LABEL-$APP_VERSION
            docker push $IMAGE_REGISTRY:$TASKS_LABEL-latest
            docker push $IMAGE_REGISTRY:$TASKS_LABEL-$APP_VERSION
            ;;
        --*) echo "bad option $1"
            ;;
        *) echo "argument $1"
            ;;
    esac
    shift
done
