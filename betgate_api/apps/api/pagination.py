from rest_framework.pagination import LimitOffsetPagination as DrfLimitOffsetPagination


class LimitOffsetPagination(DrfLimitOffsetPagination):
    max_limit = 100


class SurebetsLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 5
