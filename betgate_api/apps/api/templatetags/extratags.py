from django.template import Library
from django.conf import settings
from betgate_models.choices import MARKETS, BOOKIES


register = Library()


@register.simple_tag
def app_version():
    return getattr(settings, "APP_VERSION", "")


@register.filter
def pascal_case(value):
    return value.title().replace("_", "")


@register.filter
def get_market_display(value):
    return dict(MARKETS).get(value)


@register.filter
def get_bookie_display(value):
    return dict(BOOKIES).get(value)
