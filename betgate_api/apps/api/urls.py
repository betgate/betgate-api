from django.urls import path
from django.contrib import admin
from rest_framework import routers
from betgate_api.apps.api.views.api import EventViewSet, SurebetViewSet
from betgate_api.apps.api.views.metrics import metrics_view


router = routers.DefaultRouter()
router.register(r'surebets', SurebetViewSet)
router.register(r'events', EventViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('metrics', metrics_view, name='betgate-metrics'),
]

urlpatterns += router.urls
