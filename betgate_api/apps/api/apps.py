from django.apps import AppConfig


class BetgateApiConfig(AppConfig):
    name = 'betgate_api.apps.api'
