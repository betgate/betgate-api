from django_filters import FilterSet
from betgate_models.models import Event


class EventFilter(FilterSet):
    class Meta:
        model = Event
        fields = ['category']
