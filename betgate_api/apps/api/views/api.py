from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response
from betgate_models.models import Event, Surebet
from betgate_api.apps.api.filters import EventFilter
from betgate_api.apps.api.serializers import (
    SurebetSerializer, EventListSerializer, EventOddListSerializer
)
from betgate_api.apps.api.pagination import SurebetsLimitOffsetPagination


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class EventViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows events to be viewed.
    """
    permission_classes = (AllowAny,)
    queryset = Event.objects.all()
    serializer_class = EventListSerializer
    filter_class = EventFilter

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return EventOddListSerializer
        return self.serializer_class

    @action(detail=True, url_path='max-odds')
    def max_odds(self, request, pk=None):  # pylint: disable=invalid-name,unused-argument
        """
        Returns a list of odds that belong to given event
        """
        queryset = Event.objects.max_odds(pk=pk)
        serializer = EventOddListSerializer(queryset)
        return Response(serializer.data)

    @action(detail=True)
    def odds(self, request, pk=None):  # pylint: disable=invalid-name,unused-argument
        """
        Returns a list of odds that belong to given event
        """
        market = request.query_params.get('market', None)
        if market and is_int(market):
            market = int(market)

        odds = Event.objects.get(pk=pk).odds

        if market is not None:
            filtered_odds = [odd for odd in odds if odd["market"] == market]
            # sort by value
            filtered_odds = sorted(filtered_odds, key=lambda i: i['value'], reverse=True)
        else:
            filtered_odds = odds

        return Response(filtered_odds)

    @action(detail=False, url_path='categories')
    def count_by_category(self, request):  # pylint: disable=invalid-name,unused-argument
        """
        Count events by category
        """
        queryset = Event.objects.count_by_category()
        data = [
            {
                "category": group.get("category"),
                "count": group.get("total")
            }
            for group in queryset
        ]
        return Response(data)


class SurebetViewSet(viewsets.ReadOnlyModelViewSet):  # pylint: disable=too-many-ancestors
    """
    API endpoint that allows surebets to be viewed.
    """
    permission_classes = (AllowAny,)
    queryset = Surebet.objects.all()
    serializer_class = SurebetSerializer
    pagination_class = SurebetsLimitOffsetPagination
