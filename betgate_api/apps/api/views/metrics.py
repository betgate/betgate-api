import prometheus_client
from prometheus_client import Gauge
from django.http import HttpResponse
from betgate_models.models import Event, Surebet
from betgate_models.enums import Market
from betgate_models.choices import CATEGORIES, GLOBAL_OR_BOOKIES, GLOBAL_OR_BOOKIES_DICT


MAX_SUREBET_BY_TARGET = Gauge(
    'betgate_max_surebet_by_target',
    'Top Surebet profit factor by target (global or bookie).',
    ['target'])

SUREBETS_BY_TARGET = Gauge(
    'betgate_surebets_total_by_target',
    'Total Surebets by target (global or bookie).',
    ['target'])

EVENTS_BY_TARGET = Gauge(
    'betgate_events_total_by_target',
    'Total Events by target (global or bookie).',
    ['target', 'category'])

MARKETS_BY_TARGET = Gauge(
    'betgate_markets_total_by_target',
    'Total Markets by target (global or bookie).',
    ['target', 'market'])


def update_metrics():
    "Update prometheus metrics."
    targets = [t[0] for t in GLOBAL_OR_BOOKIES]

    odds_counters = Event.objects.count_odds_by_bookie()
    for tar in targets:
        target_name = GLOBAL_OR_BOOKIES_DICT[tar]

        max_surebet = Surebet.objects.filter(bookies__contains=[tar]).first() if tar else Surebet.objects.first()
        if max_surebet:
            MAX_SUREBET_BY_TARGET.labels(target_name).set(max_surebet.profit_factor)
        else:
            try:
                del MAX_SUREBET_BY_TARGET._metrics[(target_name,)]  # pylint: disable=protected-access
            except KeyError:
                pass

        n_surebets = Surebet.objects.filter(bookies__contains=[tar]).count() \
            if tar else Surebet.objects.count()
        SUREBETS_BY_TARGET.labels(target_name).set(n_surebets)

        for category in CATEGORIES:
            category_name = category[1]
            n_events = Event.objects.filter(category=category[0], bookies__has_key=str(tar)).count() if tar \
                else Event.objects.filter(category=category[0]).count()
            EVENTS_BY_TARGET.labels(target=target_name, category=category_name).set(n_events)

        for market in Market:
            if tar:
                n_odds = odds_counters.get((tar, market.value), 0)
                MARKETS_BY_TARGET.labels(target=target_name, market=market.name).set(n_odds)


def metrics_view(request):  # pylint: disable=unused-argument
    """
    Exports metrics as a Django view.
    """
    update_metrics()

    registry = prometheus_client.REGISTRY
    metrics_page = prometheus_client.generate_latest(registry)
    return HttpResponse(
        metrics_page,
        content_type=prometheus_client.CONTENT_TYPE_LATEST)
