from datetime import datetime
from django.core.management.base import BaseCommand
from django.utils import timezone
from betgate_models.models import Surebet, Event, Odd, BookieId
from betgate_models.enums import Team, Category, Bookie, Market


BOOKIES = [Bookie.LUCKIA, Bookie.SPORT888, Bookie.BETFAIR, Bookie.RETABET, Bookie.MARATHONBET]


class Command(BaseCommand):
    help = 'Add fake data to DataBase'

    def handle(self, *args, **options):
        odds = [
            Odd(market=Market.FT_WIN_DNB_1, value=1.7, bookie=Bookie.RETABET, surebet_coeff=0.5882352941176471),
            Odd(market=Market.FT_WIN_DNB_2, value=2.62, bookie=Bookie.LUCKIA, surebet_coeff=0.38167938931297707)
        ]
        datetime_obj = datetime(2018, 2, 1, 10, 10, tzinfo=timezone.get_current_timezone())
        evt, _ = Event.objects.get_or_create(
            home_team=Team.BARCELONA,
            away_team=Team.REAL_MADRID,
            category=Category.SPAIN__PRIMERA_DIVISION,
            start=datetime_obj,
            odds=odds,
            bookies={
                str(Bookie.RETABET.value): BookieId(bookie=Bookie.RETABET, value=3),
                str(Bookie.LUCKIA.value): BookieId(bookie=Bookie.LUCKIA, value=3),
            }
        )
        Surebet.objects.create(
            odds=odds,
            profit_factor=1.031,
            event=evt,
            bookies=[Bookie.RETABET, Bookie.LUCKIA]
        )

        self.stdout.write(self.style.SUCCESS('Surebet created'))  # pylint: disable=maybe-no-member
