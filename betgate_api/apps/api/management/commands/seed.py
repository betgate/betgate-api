from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.db.utils import IntegrityError


class Command(BaseCommand):
    help = 'Seed DataBase with initial data'

    def handle(self, *args, **options):
        try:
            User.objects.create_superuser('betgate', 'betgate.project@gmail.com', 'marcosestagordo')
            self.stdout.write(self.style.SUCCESS('Superuser created'))  # pylint: disable=maybe-no-member
        except IntegrityError:
            self.stdout.write(self.style.SUCCESS('Superuser has already been created'))  # pylint: disable=maybe-no-member
