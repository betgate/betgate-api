from django.contrib import admin
from betgate_models.models import Event, Surebet
from betgate_models.models import Notification


admin.site.register(Notification)
admin.site.register(Event)
admin.site.register(Surebet)
