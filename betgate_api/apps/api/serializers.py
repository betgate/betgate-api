from rest_framework import serializers
from betgate_models.models import Event, Surebet


class EventListSerializer(serializers.ModelSerializer):
    home_team_name = serializers.CharField(source='get_home_team_display')
    away_team_name = serializers.CharField(source='get_away_team_display')

    class Meta:
        model = Event
        fields = ('id', 'update_timestamp', 'home_team', 'home_team_name', 'away_team', 'away_team_name', 'category',
                  'start')


class EventOddListSerializer(serializers.ModelSerializer):
    home_team_name = serializers.CharField(source='get_home_team_display')
    away_team_name = serializers.CharField(source='get_away_team_display')

    class Meta:
        model = Event
        fields = ('id', 'update_timestamp', 'home_team', 'home_team_name', 'away_team', 'away_team_name', 'category',
                  'start', 'odds')


class SurebetSerializer(serializers.ModelSerializer):
    event = EventListSerializer()

    class Meta:
        model = Surebet
        fields = ('id', 'profit_factor', 'odds', 'event', 'bookies', 'creation_timestamp', 'update_timestamp')
