import logging
from datetime import datetime
import pytz
from celery import shared_task
from betgate_models.enums import Category, Team
from betgate_models.models import Event, Surebet, CategoryTask
from betgate_core import get_all_football_events, get_odds
from betgate_core.utils.surebets import find_all_surebets
from betgate_tasks.apps.celery_tasks.notifications import ALL_NOTIFICATIONS


logger = logging.getLogger(__name__)


@shared_task()
def search_event_odds(category, search_surebets=True):
    logger.info(f"Starting search event odds for category '{Category(category).name}'...")

    events = get_all_football_events(category=Category(category))
    if len(events) > 0:
        surebet_events = Surebet.objects.distint_events()
        cleaned_events = [event for event in events if event not in surebet_events]

        if len(cleaned_events) > 0:
            Event.objects.filter(category=category).exclude(id__in=[event.id for event in surebet_events]).delete()
            Event.objects.bulk_create(cleaned_events)

            if search_surebets:
                surebets = []
                for event in cleaned_events:
                    surebets.extend(find_all_surebets(event))

                if len(surebets) > 0:
                    Surebet.objects.bulk_create(surebets)
    else:
        Surebet.objects.filter(event__category=category, event__start__lt=datetime.now(tz=pytz.UTC)).delete()
        Event.objects.filter(category=category, start__lt=datetime.now(tz=pytz.UTC)).delete()
        logger.info("Events not found in category: %s", Category(category).name)

    logger.info(f"... finished search event odds for category '{Category(category).name}'.")


@shared_task()
def update_surebets():
    for event in Surebet.objects.distint_events():
        odds = get_odds(event)
        if len(odds) > 0:
            event.odds = odds
            event.save()

            surebets = find_all_surebets(event)
            if len(surebets) > 0:
                surebet_ids = []
                for surebet in surebets:
                    surebet_ids.append(surebet.id)
                    Surebet.objects.update_or_create(
                        id=surebet.id,
                        defaults={
                            'profit_factor': surebet.profit_factor,
                            'event': surebet.event,
                            'odds': surebet.odds,
                            'bookies': surebet.bookies
                        },
                    )
                Surebet.objects.filter(event=event).exclude(id__in=surebet_ids).delete()
            else:
                Surebet.objects.filter(event=event).delete()
                logger.info(
                    f"There is no longer any surebet in event: {Team(event.home_team).name} vs "
                    f"{Team(event.away_team).name} ({event.id})"
                )
        else:
            Surebet.objects.filter(event=event).delete()
            event.delete()
            logger.info(f"Odds not found in event: {Team(event.home_team).name} vs {Team(event.away_team).name} "
                        f"({event.id})")


@shared_task()
def check_notifications():
    for notification_cls in ALL_NOTIFICATIONS:
        notification = notification_cls()
        if notification.is_ready() and notification.is_triggered():
            notification.notify_to_users()
            notification.update_timestamp()


@shared_task()
def run_search_event_odds():
    first = CategoryTask.objects.order_by('update_timestamp').first()
    search_event_odds.delay(first.category)
    first.update_timestamp = datetime.utcnow()
    first.save()
