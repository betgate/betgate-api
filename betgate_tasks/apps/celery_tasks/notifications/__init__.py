from betgate_tasks.apps.celery_tasks.notifications.max_surebet_notification import (
    MaxSurebetNotification, MaxSurebetMedium, MaxSurebetLow
)

ALL_NOTIFICATIONS = [
    MaxSurebetNotification, MaxSurebetMedium, MaxSurebetLow
]
