import random
from decimal import Decimal
from django.conf import settings
from django.template import loader
from betgate_models.models import NotifiedSurebet
from betgate_models.enums import NotificationId
from betgate_tasks.apps.celery_tasks.notifications.base import NotificationTaskBase
from betgate_tasks.apps.celery_tasks.notifications.providers.telegram import TelegramNotification
from betgate_tasks.apps.celery_tasks.notifications.providers.email import EmailNotification


class MaxSurebetNotification(EmailNotification, TelegramNotification, NotificationTaskBase):

    notification_id = NotificationId.MAX_SUREBET
    task_interval_in_minutes = 10

    min_profit_factor = Decimal('1.03')
    max_profit_factor = None

    _email_subject = "Surebet de gran valor"
    _email_format_msg = "¡Ojo! En estos momentos hay una surebet con una ganancia asegurada del " \
                        "{0:.2f}%.\n\nAccede a {1}/surebets"
    _title_msg = ["Nueva surebet!", "Viene otra!", "Otra surebet!", "Nueva surebet!"]

    def __init__(self):
        self._surebet = None

    @property
    def email_message(self):
        return self._email_format_msg.format(self._surebet.profit_factor * 100 - 100, settings.WEB_BASE_URL)

    @property
    def email_subject(self):
        return self._email_subject

    @property
    def telegram_message(self):
        template = loader.get_template("notifications/max_surebet_notification.template")
        context = {
            "title": random.choice(self._title_msg),
            "home_team": self._surebet.event.get_home_team_display(),
            "away_team": self._surebet.event.get_away_team_display(),
            "category": self._surebet.event.get_category_display(),
            "start": self._surebet.event.start,
            "odds": [
                {
                    "odd": odd,
                    "bet_per_cent": odd["surebet_coeff"] * float(self._surebet.profit_factor) * 100
                }
                for odd in self._surebet.odds
            ],
            "profit_factor": self._surebet.profit_factor*100 - 100,
            "url": f"{settings.WEB_BASE_URL}/calculator/{self._surebet.id}"
        }
        return template.render(context)

    def is_triggered(self):
        qset = NotifiedSurebet.objects.all_surebets_greater_or_not_in()
        qset = qset.exclude(profit_factor__lt=self.min_profit_factor)
        if self.max_profit_factor:
            qset = qset.exclude(profit_factor__gte=self.max_profit_factor)

        self._surebet = qset.first()
        return self._surebet is not None

    def update_timestamp(self):
        super().update_timestamp()
        _, created = NotifiedSurebet.objects.update_or_create(
            event=self._surebet.event,
            defaults={'profit_factor': self._surebet.profit_factor},
        )
        return created


class MaxSurebetMedium(MaxSurebetNotification):
    notification_id = NotificationId.MAX_SUREBET_MEDIUM
    task_interval_in_minutes = 60
    min_profit_factor = Decimal('1.02')
    max_profit_factor = Decimal('1.03')


class MaxSurebetLow(MaxSurebetNotification):
    notification_id = NotificationId.MAX_SUREBET_LOW
    task_interval_in_minutes = 1000
    min_profit_factor = Decimal('1.01')
    max_profit_factor = Decimal('1.02')
