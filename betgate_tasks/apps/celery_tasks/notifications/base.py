from abc import ABCMeta, abstractmethod
from datetime import datetime, timedelta
from django.utils import timezone
from betgate_models.models import Notification


class NotificationTaskBase:
    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def notification_id(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def task_interval_in_minutes(self):
        raise NotImplementedError

    def is_ready(self):
        try:
            notification = Notification.objects.get(notification_id=self.notification_id)
            return (notification.timestamp + timedelta(minutes=self.task_interval_in_minutes)) < timezone.now()
        except Notification.DoesNotExist:
            return True

    @abstractmethod
    def is_triggered(self):
        raise NotImplementedError

    def update_timestamp(self):
        notification, created = Notification.objects.get_or_create(notification_id=self.notification_id)
        if not created:
            notification.timestamp = datetime.utcnow()
            notification.save()

    @abstractmethod
    def notify_email_to_users(self):
        pass

    @abstractmethod
    def notify_telegram_to_users(self):
        pass

    def notify_to_users(self):
        self.notify_email_to_users()
        self.notify_telegram_to_users()
