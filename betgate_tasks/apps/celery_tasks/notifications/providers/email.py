import logging
import smtplib
from abc import ABCMeta, abstractmethod
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.conf import settings


logger = logging.getLogger(__name__)


class EmailNotification:
    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def email_subject(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def email_message(self):
        raise NotImplementedError

    def get_email_subscribers(self):
        return User.objects.all()

    def notify_email_to_users(self):
        receivers = []
        for user in self.get_email_subscribers():
            if user.email:
                receivers.append(user.email)

        if receivers:
            subject = self.email_subject
            message = self.email_message
            email_from = '"Betgate" <{0}>'.format(settings.EMAIL_HOST_USER)
            try:
                send_mail(subject, message, email_from, receivers)
            except smtplib.SMTPAuthenticationError as err:
                logger.info("SMTP auth error: %s", err.strerror)
