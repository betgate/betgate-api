import logging
from abc import ABCMeta, abstractmethod
from django.conf import settings
from telegram_api.base import TelegramApi, ParseMode


logger = logging.getLogger(__name__)


def send_message(telegram_token, message, receivers, parse_mode=None):
    api = TelegramApi(telegram_token)
    for receiver in set(receivers):
        api.send_message(receiver, message, parse_mode=parse_mode)


class TelegramNotification:
    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def telegram_message(self):
        raise NotImplementedError

    def notify_telegram_to_users(self):
        try:
            send_message(
                telegram_token=settings.TELEGRAM_BOT_TOKEN,
                message=self.telegram_message,
                parse_mode=ParseMode.HTML,
                receivers=[settings.TELEGRAM_CHANNEL]
            )
        except Exception as err:
            logger.error("Unexpected error notifying telegram to users: %s", err)
