from django.template import Library
from betgate_models.choices import MARKETS, BOOKIES


register = Library()


@register.filter
def pascal_case(value):
    return value.title().replace("_", "")


@register.filter
def get_market_display(value):
    return dict(MARKETS).get(value)


@register.filter
def get_bookie_display(value):
    return dict(BOOKIES).get(value)
