from django.apps import AppConfig


class BetgateTasksConfig(AppConfig):
    name = 'betgate_tasks.apps.celery_tasks'
