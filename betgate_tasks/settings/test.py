from betgate_tasks.settings.base import *
from betgate_core.betfair.web import BetfairWebBase
from betgate_core.kambi.api import KambiApiBase

BetfairWebBase.DEFAULT_PROXY = "socks5://tor-service:9050"
KambiApiBase.DEFAULT_PROXY = "socks5://tor-service:9050"

ALLOWED_HOSTS = []


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '==t*%1m4q!h)qak9=2#u#)mdpcy_#o^=fearkw=l$zo(lohdo*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'postgres',
        'PORT': '5432',
    },
}
