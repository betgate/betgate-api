from sentry_sdk import init as init_sentry
from sentry_sdk.integrations.django import DjangoIntegration
from betgate_core.betfair.web import BetfairWebBase
from betgate_core.kambi.api import KambiApiBase
from betgate_tasks.settings.base import *


BetfairWebBase.DEFAULT_PROXY = "socks5://proxy_betfair:9050"
KambiApiBase.DEFAULT_PROXY = "socks5://proxy_kambi:9050"

# Telegram Bot
TELEGRAM_BOT_TOKEN = "831308750:AAFeAMYZWoSxo3WNt9cge6pijr7oc_0rLcI"
TELEGRAM_CHANNEL = "@ivanprjctsChannel"

WEB_BASE_URL = os.getenv('BETGATE_WEB_BASE_URL', 'https://next.betgate.com.es')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Error tracking settings
init_sentry(
    dsn="https://e1332f1ea1634095b09f2860a16d9862@sentry.io/1396021",
    integrations=[DjangoIntegration()]
)
