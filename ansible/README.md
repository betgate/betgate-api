ansible-django-stack
====================

## Running the Ansible Playbook to provision servers

Run the playbook:

```
ansible-playbook -i production webservers.yml
```
